#ifndef EdarWidget_H
#define EdarWidget_H

#include <QHBoxLayout>
#include <QFormLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMainWindow>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QTabWidget>
#include <QToolBox>
#include <QWidget>
#include <QWidget>
#include <QLayout>

#include <vector>

#include <Geom_TrimmedCurve.hxx>
#include <gp_Ax1.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <menuwidget.h>

#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>

#include <unordered_map>

#include <vtkActor.h>
#include <vtkChartXY.h>
#include <vtkSmartPointer.h>
#include <vtkviewer.h>

#include <QSettings>
#include <QComboBox>
#include <QCheckBox>
#include <QFileDialog>
#include <QPushButton>
#include <QSlider>

#include "tier_application/boxlinefield.h"
#include "tier_application/fileeditor.h"
#include "globals.h"
#include <QDir>

using namespace std;

class EdarWidget : public QWidget {
    Q_OBJECT

public:
    explicit EdarWidget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, QToolBox* reportWidget = 0, QListWidget* console = 0);
    ~EdarWidget();
    void loadMainWindow();

    void clearLayout(QLayout *layout);
    QWidget* getInfoDesignFeedwellTab();
    QWidget* getInfoDesignGeneralTab();
    QWidget* getInfoDesignInputMixedLiquorTab();
    QWidget* getInfoDesignScraperTab();
    void remove(QLayout *layout);
    vtkSmartPointer<vtkChartXY> updateModel10Layers();
    void setImageInViewer(int option, char *filepath);
    void getLinearRegresion(int);

public slots:
    void applyProcess();
    void dataInputProperties();
    void designProperties();
    void onSettlingTypeInputTextChanged();
    void onSliderQiStop();
    void onSliderQrStop();
    void onSliderV0Stop();
    void onSliderRhStop();
    void updateSliderQi();
    void updateSliderQr();
    void updateSliderV0();
    void updateSliderRh();
    void processProperties();
    void rheologyProperties();
    QLayout* settlingBaseLayout(int);
    void settlingProperties();
    QLayout* dataInputBaseLayout(int);

private:
    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;
    vtkSmartPointer <vtkContextActor> statePoint;
    vtkSmartPointer <vtkContextActor> model10;
    vtkSmartPointer <vtkContextActor> settlingCurve;

    QFormLayout* settlingForm;
    QFormLayout* processForm;

    double diam;
    double v0;
    double rh;
    double rt;
    double Qi;
    double Qr;
    double A;
    double SST;

private:
    void createAreaButtons();
    void createAreaActions();

private slots:
    void loadSettingsDesign();
    void saveSettingsDesign();
    void loadSettingsSettling(int);
    void saveSettingsSettling();
    void loadSettingsDataInput(int);
    void saveSettingsDataInput();
    void loadSettingsRheology();
    void saveSettingsRheology();
    void loadSettingsProcess();
    void saveSettingsProcess();

    void toolBoxChanged();
    void changeSettlingsTab();
    void openFileDialog();
    void openCreateWindow();
    void saveSludge();
    void updateSludgeProfilesList();
    void openEditWindow();
    void createConcentrationTabs();
    void concentrationTabChanged();
    void updateConcentrationTab();
    void showAllConcentrationsChart();
    void changeSludgeFile();
    void showSettlingsExperimental(QVBoxLayout *);
    void showSettlingsManual(QVBoxLayout *);
    int parseSludgeFile(std::string); //return 0 if ok, else line number
    void showFileParserError(int, QString="");
    QWidget *showConcentrationTab(int);
    void showConcentrationChart();

    void showDataInputStationary(QVBoxLayout *);
    void showDataInputTranscient(QVBoxLayout *);
    void changeFlowTab();
    void openFlowFileDialog();
    void updateFlowProfilesList();
    int parseFlowFile(std::string); //return 0 if ok, else line number
    void onFlowProfileChanged();
    void createFilterFields();
    void clearFlowLayout();
    void showFlowChart();
    void checkFilters();
    void movingAverage(int, int);
    void getFlowValuesFromFile();
    void removeOutliers();

private:
    QString m_sSettingsFile;
    QSettings* settings;
    QLineEdit* line_diameter_clarifier;
    QLineEdit* line_diameter_liquor;
    QLineEdit* line_diameter_concrete;
    QLineEdit* line_diameter_upper_sludge;
    QLineEdit* line_diameter_base_sludge;
    QLineEdit* line_draught;
    QLineEdit* line_height_sludge;
    QLineEdit* line_height_well;
    QLineEdit* line_length_bridge;
    QLineEdit* line_length_scrapers;
    QLineEdit* line_width_inner_wall;
    QLineEdit* line_width_sink;
    QLineEdit* line_width_drain;
    QLineEdit* line_height_outer_wall;
    QLineEdit* line_height_inner_wall;
    QLineEdit* line_height_slab;

    QLineEdit* line_windows;
    QLineEdit* line_height_window;
    QLineEdit* line_width_inner_window;
    QLineEdit* line_width_outer_window;
    QLineEdit* line_thickness_window;
    QComboBox* line_geometry_window;
    QCheckBox* line_deflector_window;

    QLineEdit* line_angle;
    QComboBox* line_type;
    QComboBox* line_form;

    QLineEdit* line_diameter_inner_feedwell;
    QLineEdit* line_height_feedwell;
    QLineEdit* line_diameter_min_feedwell;
    QLineEdit* line_height_min_feedwell;
    QLineEdit* line_diameter_max_feedwell;

    QComboBox* line_sedimentation_model;
    QLabel* label_sedimentation_model;
    QLineEdit* line_v0;
    QLineEdit* line_rh;
    QLineEdit* line_rt;
    QLabel* label_rt;
    QLineEdit* line_weight_max;
    QLabel* label_weight_max;
    QLineEdit* line_weight_min;
    QLabel* label_weight_min;
    QSlider * slider_v0;
    QSlider * slider_rh;

    QLineEdit* line_caudal;
    QLabel* label_caudal;
    QLineEdit* line_solids;
    QLabel* label_solids;

    QComboBox* line_rheology_type;
    QLabel* label_rheology_type;
    QLineEdit* line_tau;
    QLabel* label_tau;
    QLineEdit* line_k;
    QLabel* label_k;
    QLineEdit* line_n;
    QLabel* label_n;

    QLineEdit* line_proc_diam;
    QLabel* label_proc_diam;
    QLineEdit* line_proc_area;
    QLabel* label_proc_area;
    QLineEdit* line_proc_qi;
    QLabel* label_proc_qi;
    QLineEdit* line_proc_qr;
    QLabel* label_proc_qr;
    QLineEdit* line_proc_tanks;
    QLabel* label_proc_tanks;
    QLineEdit* line_proc_va;
    QLabel* label_proc_va;
    QLineEdit* line_proc_sst;
    QLabel* label_proc_sst;
    QSlider * slider_qi;
    QSlider * slider_qr;

    BoxLineField *box_v0;
    BoxLineField *box_rh;

    QToolBox *designInfo;
    QTabBar *tabbar;
    QPushButton *btnSaveSettling;
    QPushButton *btnSaveDesignGeneral;
    QLineEdit *sludge_filename;
    FileEditor *editor;
    QWidget *editorWindow;
    QComboBox *sludge_profiles;
    QList<double> *concentrationList;
    QList<QList<int>*> *concentrationTimes;
    QList<QList<double>*> *concentrationValues;
    QVBoxLayout *sludge_layout;
    QToolBox *concentrationTabs;
    double *adjustmentLower;
    double *adjustmentUpper;
    double *linearRegression;
    double *interceptArray;
    QLineEdit *lineLowerArray;
    QLineEdit *lineUpperArray;
    bool lineArraysCreated;
    QPushButton *btn_show_tabs;
    double vesilind_v0;
    double vesilind_rh;
    QLabel *label_v0;
    QLabel *label_rh;

    QTabBar *flowTabbar;
    QVBoxLayout *flow_layout;
    QPushButton *btnSaveDataInput;
    QComboBox *flow_profiles;
    QPushButton *btnNewFilter;
    QPushButton *btnRmvFilter;
    QLineEdit *lineFilterId;
    QList<double> *flowValues;
    vtkSmartPointer<vtkChartXY> flowChart;
    QCheckBox *checkRemoveOutliers;
    QPushButton *btnApplyFilters;
    QComboBox *func1;
    QComboBox *func2;
    //QComboBox *func3;
    QLineEdit *val1;
    QLineEdit *val2;
    //QLineEdit *val3;

    double flowAverage;
    double flowStd;
};

#endif // EdarWidget_H
