#ifndef VTKVIEWER_H
#define VTKVIEWER_H

#include <cassert>
#include <QTimer>
#include <QVBoxLayout>
#include <QPushButton>
#include <QWidget>

#include <graphobject.h>

#include <QVTKWidget.h>
#include <vtkAxesActor.h>
#include <vtkContextActor.h>
#include <vtkContourFilter.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkXYPlotActor.h>

//#include <IVtkVTK_View.hxx>

#include <vtkUnstructuredGrid.h>
#include <vtkProbeFilter.h>
#include "tier_application/resultentity.h"

class vtkPolyData;

class VTKViewer : public QVTKWidget {
    Q_OBJECT;
public:
    VTKViewer();
    //connect(btnXYZ2, SIGNAL(clicked()), this, SLOT(switchShowAxes()));
    VTKViewer(QWidget* parent);
    ~VTKViewer();

public:    
    QPushButton* btnXYZ2;
    QPushButton* btnMesh2;
    vtkActor* add(vtkPolyData * polyData);
    void add(vtkActor* actor);
    void add(vtkActor2D* actor);
    void add(vtkContextActor* contextActor);
    void add(vtkXYPlotActor* actor);
    vtkActor* add(const char * file_name);
    void addGraphOld(double stepTime);
    void addGraph(int, int, QString, double*, double*);
    void addGraph(vtkSmartPointer<vtkProbeFilter>, QString);
    void addGraph(ResultEntity *, ResultEntity *, ResultEntity *, ResultEntity *);
    void addGraph(QList<ResultEntity*>*);
    vtkSmartPointer<vtkProbeFilter> addLine(double*, double*);
    void addSludge();
    void addPlane(double*, double*);
    QWidget* getControlArea();
    void readOpenFoam(double stepTime, QString displayMode, bool showScalarBar, bool showWireFrame, bool showTimeStep, bool mesh);
    void renderOpenFoamReader(QString, int, QString);
    void remove(vtkContextActor* contextActor);
    double* bounds();
    //QPushButton* btnXYZ2;

public slots:
    void cameraToPositiveX();
    void cameraToNegativeX();
    void cameraToPositiveY();
    void cameraToNegativeY();
    void cameraToPositiveZ();
    void cameraToNegativeZ();
    float getOpacityModel();
    void reset();
    void rotate();
    void resetCamera();
    void screenshot();
    void switchShowAxes();
    void switchShowWireframe();
    void setShowAxes(bool status);
    void setShowModel(bool status);
    void setShowSludge(bool status);
    void setOpacityModel(float value);
    void zoomDecrement();
    void zoomIncrement();
    void getScreenshot();
    void pan();
    void zoomToBox();

public: //private:
    QWidget* parent;
    QVTKWidget* viewer;

    QTimer m_timer;


    vtkSmartPointer<vtkRenderer> m_renderer;
    vtkSmartPointer<vtkRenderWindowInteractor> m_render_window_interactor;
    vtkSmartPointer<vtkPOpenFOAMReader> m_reader;
    vtkSmartPointer<vtkAxesActor> axes;
    vtkSmartPointer<vtkActor> mainActor;

    int numActors;
    float opacityModel;
    bool showModel;
    bool showSludge;
    bool showAxes;
    bool showWireframe;


public:
    GraphObject* graphs;
    vtkSmartPointer<vtkActor> sludgeActor;
    vtkSmartPointer<vtkActor> planeActor;
    vtkSmartPointer<vtkActor> lineActor;
    vtkSmartPointer<vtkVolume> isoVolume;

    void viewerAddVolume(double);
    void viewerAddSurface(double);


public slots:

    double* getMetricRange(QString, int);
    void addIsovolume(double);

private:
    float timesStep;
    bool model;
    bool clip;
    int oppacity;
    QString curr_metric;
    vtkUnstructuredGrid* workingBlock;
    vtkSmartPointer<vtkXYPlotActor> getChart(ResultEntity*);
};
#endif // VTKVIEWER_H
