#include "cadwidget.h"
#include "cfdwidget.h"
#include "globals.h"
#include "edarwidget.h"
#include "homewidget.h"
#include "menuwidget.h"
#include "meshwidget.h"
#include "resultswidget.h"

#include <QDebug>
#include <QDesktopServices>
#include <QDialog>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QMessageBox>
#include <QObject>
#include <QPushButton>
#include <QString>

MenuWidget::MenuWidget(QWidget* widget, QWidget* centralwidget): QWidget(widget){
    parent = widget;
    parentCentralWidget = centralwidget;
}

MenuWidget::~MenuWidget(){

}

QHBoxLayout* MenuWidget::createMenu() {
    // LOGO
    QPushButton* btnLogo = new QPushButton;
    btnLogo->setFlat(true);
    btnLogo->setAutoFillBackground(true);
    btnLogo->setStyleSheet(Globals::instance()->getCssMenu());
    btnLogo->setDisabled(true);

    // HOME TAB
    btnHome = new QPushButton(Globals::instance()->getTabHome(), this);
    btnHome->setStyleSheet(Globals::instance()->getCssMenuSelected());
    //QObject::connect(btnHome, SIGNAL(clicked()), this, SLOT(goToHomeView()));

    QMenu* btnHomeMenu = new QMenu(this);
    btnHomeMenu->setStyleSheet(Globals::instance()->getCssSubMenu());

    /*
    QAction* loadProject = new QAction("Load Project", this);
    loadProject->setObjectName("btnMenuLoadProject");
    btnHomeMenu->addAction(loadProject);
    QObject::connect(loadProject, SIGNAL(triggered()), parent, SLOT(showLoadProjectDialog()));

    QAction* newWorkSpace = new QAction("New WorkSpace", this);
    newWorkSpace->setObjectName("btnMenuNewWorkSpace");
    btnHomeMenu->addAction(newWorkSpace);
    QObject::connect(newWorkSpace, SIGNAL(triggered()), parent, SLOT(showNewWorkSpaceDialog()));

    QAction* newProject = new QAction("New Project", this);
    newProject->setObjectName("btnMenuNewProject");
    btnHomeMenu->addAction(newProject);
    QObject::connect(newProject, SIGNAL(triggered()), parent, SLOT(showNewProjectDialog()));

    QAction* changeWorkspace = new QAction("Change Workspace", this);
    changeWorkspace->setObjectName("btnMenuChangeWorkspace");
    btnHomeMenu->addAction(changeWorkspace);
    QObject::connect(changeWorkspace, SIGNAL(triggered()), parent, SLOT(showChangeWorkspaceDialog()));

    btnHome->setMenu(btnHomeMenu);
    */

    // EDAR
    btnWwtp = new QPushButton("WWTP", this);
    btnWwtp->setStyleSheet(Globals::instance()->getCssMenu());
    btnWwtp->setDisabled(!Globals::instance()->getCurrentProjectEdarModel());
    QObject::connect(btnWwtp, SIGNAL(clicked()), this, SLOT(goToEdarView()));


    // CAD TAB
    btnCad = new QPushButton(Globals::instance()->getTabCad(), this);
    btnCad->setStyleSheet(Globals::instance()->getCssMenu());
    btnCad->setDisabled(!Globals::instance()->getCurrentProjectCadModel());
    QObject::connect(btnCad, SIGNAL(clicked()), this, SLOT(goToCadView()));

    // MESH TAB
    btnMesh = new QPushButton(Globals::instance()->getTabMesh(), this);
    btnMesh->setStyleSheet(Globals::instance()->getCssMenu());
    btnMesh->setDisabled(!Globals::instance()->getCurrentProjectMeshModel());
    QObject::connect(btnMesh, SIGNAL(clicked()), this, SLOT(goToMeshView()));

    // CFD TAB
    btnCfd = new QPushButton(Globals::instance()->getTabCfd(), this);
    btnCfd->setStyleSheet(Globals::instance()->getCssMenu());
    btnCfd->setDisabled(!Globals::instance()->getCurrentProjectCfdModel());
    QObject::connect(btnCfd, SIGNAL(clicked()), this, SLOT(goToCfdView()));

    // RESULTS TAB
    btnResults = new QPushButton(Globals::instance()->getTabResults(), this);
    btnResults->setStyleSheet(Globals::instance()->getCssMenu());
    btnResults->setDisabled(!Globals::instance()->getCurrentProjectResultsModel());
    QObject::connect(btnResults, SIGNAL(clicked()), this, SLOT(goToResultsView()));

    // HELP
    btnHelp = new QPushButton("HELP", this);
    btnHelp->setDisabled(false);
    btnHelp->setStyleSheet(Globals::instance()->getCssMenu());
    QObject::connect(btnHelp,SIGNAL(clicked()),this,SLOT(aboutHelp()));

    /*
    QMenu* btnHelpMenu = new QMenu(this);
    btnHelpMenu->setStyleSheet(Globals::instance()->getCssSubMenu());

    QAction* aboutHelp = new QAction("About", this);
    aboutHelp->setObjectName("btnMenuAboutHelp");
    btnHelpMenu->addAction(aboutHelp);
    QObject::connect(aboutHelp, SIGNAL(triggered()), this, SLOT(aboutHelp()));

    QAction* onlineTutorialHelp = new QAction("Online Tutorial", this);
    onlineTutorialHelp->setObjectName("btnMenuOnlineTutorialHelp");
    btnHelpMenu->addAction(onlineTutorialHelp);
    QObject::connect(onlineTutorialHelp, SIGNAL(triggered()), this, SLOT(onlineTutorialHelp()));

    QAction* userGuideHelp = new QAction("User Guide", this);
    userGuideHelp->setObjectName("btnMenuUserGuideMesh");
    userGuideHelp->setDisabled(true);
    btnHelpMenu->addAction(userGuideHelp);
    QObject::connect(userGuideHelp, SIGNAL(triggered()), this, SLOT(userGuideHelp()));

    btnHelp->setMenu(btnHelpMenu);
    */

    QHBoxLayout* layout = new QHBoxLayout;
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setObjectName("area_menu");
    layout->addWidget(btnHome);
    layout->addWidget(btnWwtp);
    layout->addWidget(btnCad);
    layout->addWidget(btnMesh);
    layout->addWidget(btnCfd);
    layout->addWidget(btnResults);
    //layout->addWidget(btnAnalysis);
    layout->addWidget(btnHelp);

    return layout;
}

void MenuWidget::updateMenu(const QString& currentTab, QWidget* widget){
    // Quitamos los submenus de todas las pestañas
    btnHome->setMenu(NULL);
    btnWwtp->setMenu(NULL);
    btnCad->setMenu(NULL);
    btnMesh->setMenu(NULL);
    btnCfd->setMenu(NULL);
    btnResults->setMenu(NULL);
    btnHelp->setMenu(NULL);

    // (Des)Habilitamos segun el estado del proyecto
    btnHome->setDisabled(false);
    //btnWwtp->setDisabled(Globals::instance()->getCurrentProjectName().isEmpty());
    //btnCad->setDisabled(!Globals::instance()->getCurrentProjectCadModel());
    //btnMesh->setDisabled(!Globals::instance()->getCurrentProjectMeshModel());
    //btnCfd->setDisabled(!Globals::instance()->getCurrentProjectCfdModel());
    //btnResults->setDisabled(!Globals::instance()->getCurrentProjectResultsModel());
    btnWwtp->setDisabled(false);
    btnCad->setDisabled(false);
    btnMesh->setDisabled(false);
    btnCfd->setDisabled(false);
    btnResults->setDisabled(false);
    btnHelp->setDisabled(false);

    btnHome->setStyleSheet(Globals::instance()->getCssMenu());
    btnWwtp->setStyleSheet(Globals::instance()->getCssMenu());
    btnCad->setStyleSheet(Globals::instance()->getCssMenu());
    btnMesh->setStyleSheet(Globals::instance()->getCssMenu());
    btnCfd->setStyleSheet(Globals::instance()->getCssMenu());
    btnResults->setStyleSheet(Globals::instance()->getCssMenu());
    btnHelp->setStyleSheet(Globals::instance()->getCssMenu());

    // Ponemos el submenu segun la pestaña en que estemos
    if(Globals::instance()->getTabHome() == currentTab){
        btnHome->setDisabled(true);

        QMenu* btnHomeMenu = new QMenu(this);
        btnHomeMenu->setStyleSheet(Globals::instance()->getCssSubMenu());

        QAction* changeWorkspace = new QAction("Change Workspace", this);
        changeWorkspace->setObjectName("btnMenuChangeWorkspace");
        btnHomeMenu->addAction(changeWorkspace);
        QObject::connect(changeWorkspace, SIGNAL(triggered()), widget, SLOT(showChangeWorkspaceDialog()));

        btnHome->setMenu(btnHomeMenu);
        btnHome->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabEdar() == currentTab){
        btnWwtp->setDisabled(true);
        btnWwtp->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabCad() == currentTab){
        btnCad->setDisabled(true);
        btnCad->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabMesh() == currentTab){
        btnMesh->setDisabled(true);
        btnMesh->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabCfd() == currentTab){
        btnCfd->setDisabled(true);
        btnCfd->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabResults() == currentTab){
        btnResults->setDisabled(true);
        btnResults->setStyleSheet(Globals::instance()->getCssMenuSelected());
    }
    else if(Globals::instance()->getTabHelp() == currentTab){
        QMenu* btnHelpMenu = new QMenu(this);
        btnHelpMenu->setStyleSheet(Globals::instance()->getCssSubMenu());

        QAction* aboutHelp = new QAction("About", this);
        aboutHelp->setObjectName("btnMenuAboutHelp");
        btnHelpMenu->addAction(aboutHelp);
        QObject::connect(aboutHelp, SIGNAL(triggered()), this, SLOT(aboutHelp()));

        QAction* onlineTutorialHelp = new QAction("Online Tutorial", this);
        onlineTutorialHelp->setObjectName("btnMenuOnlineTutorialHelp");
        btnHelpMenu->addAction(onlineTutorialHelp);
        QObject::connect(onlineTutorialHelp, SIGNAL(triggered()), this, SLOT(onlineTutorialHelp()));

        QAction* userGuideHelp = new QAction("User Guide", this);
        userGuideHelp->setObjectName("btnMenuUserGuideMesh");
        userGuideHelp->setDisabled(true);
        btnHelpMenu->addAction(userGuideHelp);
        QObject::connect(userGuideHelp, SIGNAL(triggered()), this, SLOT(userGuideHelp()));

        btnHelp->setStyleSheet(Globals::instance()->getCssMenuSelected());
        btnHelp->setMenu(btnHelpMenu);
        btnHelp->showMenu();
    }
}

void MenuWidget::setViewers(VTKViewer* vtk, VTKViewer* graphs){
    vtkViewer = vtk;
    vtkGraphs = graphs;
    vtkObject::GlobalWarningDisplayOff();
}

void MenuWidget::setConsole(QListWidget* consoleWidget){
    console = consoleWidget;
}

void MenuWidget::setHome(QStackedWidget* homeWidget){
    home = homeWidget;
}

void MenuWidget::cleanAreas(){
    if(parentCentralWidget->findChild<QScrollArea*>("area_buttons") != NULL && parentCentralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        QLayoutItem *child;
        while ((child = parentCentralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete parentCentralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }

    if(parentCentralWidget->findChild<QScrollArea*>("area_actions") != NULL && parentCentralWidget->findChild<QScrollArea*>("area_actions")->layout() != NULL){
        QLayoutItem *child;
        while ((child = parentCentralWidget->findChild<QScrollArea*>("area_actions")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete parentCentralWidget->findChild<QScrollArea*>("area_actions")->layout();
    }

    if(parentCentralWidget->findChild<QScrollArea*>("area_actions_results") != NULL && parentCentralWidget->findChild<QScrollArea*>("area_actions_results")->layout() != NULL){
        QLayoutItem *child;
        while ((child = parentCentralWidget->findChild<QScrollArea*>("area_actions_results")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }

        delete parentCentralWidget->findChild<QScrollArea*>("area_actions_results")->layout();
    }
}

void MenuWidget::aboutHelp(){
    QString textInfo = "<strong>HYDRODECA</strong> <br> <br>";
    textInfo+="Software used:";
    textInfo+="<ul>";
    textInfo+="<li>Qt version: 5.6.1</li>";
    textInfo+="<li>Docker version: 2.3.0.3(45519)</li>";
    textInfo+="<li>Open Cascade version: 6.9.1</li>";
    textInfo+="<li>CMake version: 3.5.2</li>";
    textInfo+="<li>VTK version: 6.1</li>";
    textInfo+="</ul>";

    textInfo += "Universitat Jaume I";

    QMessageBox* msgBox = new QMessageBox();
    msgBox->setIcon(QMessageBox::Information);
    //msgBox->setWindowTitle("ABOUT");
    msgBox->setModal(true);
    msgBox->setFixedWidth(700);
    msgBox->setFixedHeight(600);
    msgBox->setStyleSheet("border: none;");
    msgBox->setText(textInfo);
    msgBox->show();
}

void MenuWidget::onlineTutorialHelp(){
    QDesktopServices::openUrl(QUrl("http://www.uji.es"));
}

void MenuWidget::userGuideHelp(){
    QDesktopServices::openUrl(QUrl("http://www.uji.es"));
}

void MenuWidget::goToCadView(){
    cleanAreas();
    CadPreset1Widget* cad = new CadPreset1Widget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, reportInfo, console);
    cad->loadMainWindow();
}

void MenuWidget::goToCfdView(){
    cleanAreas();

    // Cambiamos a la pantalla de CFD
    CfdWidget* cfd = new CfdWidget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, reportInfo, console);
    cfd->loadMainWindow();
}

void MenuWidget::goToEdarView(){
    cleanAreas();

    // Cambiamos a la pantalla de EDAR
    parent->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);

    EdarWidget* edar = new EdarWidget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, reportInfo, console);
    edar->loadMainWindow();
}

void MenuWidget::goToHomeView(){
    //qDebug() << "Go to HomeView" <<endl;
    cleanAreas();

    // Cambiamos a la pantalla de HOME
    HomeWidget* home = new HomeWidget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, console);
    home->loadMainWindow();
}

void MenuWidget::goToMeshView(){
    cleanAreas();

    // Cambiamos a la pantalla de MESH
    MeshWidget* mesh = new MeshWidget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, reportInfo, console);
    mesh->loadMainWindow();
}

void MenuWidget::goToResultsView(bool runDriftFlux){
    cleanAreas();

    // Cambiamos a la pantalla de RESULTS
    ResultsWidget* results = new ResultsWidget(parent, parentCentralWidget, vtkViewer, vtkGraphs, this, runDriftFlux, reportInfo, console);
    results->loadMainWindow();
}
