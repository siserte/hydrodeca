#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#define vtkFloatingPointType double


#include "vtkviewer.h"
#include "globals.h"
#include "graphobject.h"

#include <iostream>

#include <QDebug>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWidget>
#include <QVTKWidget.h>
#include <QFile>

//#include <IVtkTools_ShapePicker.hxx>
//#include <IVtkDraw_Interactor.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkAppendPolyData.h>
#include <vtkAxesActor.h>
#include <vtkCamera.h>
#include <vtkCallbackCommand.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkColorTransferFunction.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkContextActor.h>
#include <vtkCornerAnnotation.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSetReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkExecutive.h>
#include <vtkFloatArray.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkGeometryFilter.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkParallelCoordinatesInteractorStyle.h>
#include <vtkInteractorStyle.h>
#include <vtkInteractorStyleImage.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkJPEGReader.h>
#include <vtkLineSource.h>
#include <vtkLookupTable.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkGlyph3D.h>
#include <vtkOBJReader.h>
#include <vtkObjectFactory.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPDBReader.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPLYReader.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolyDataReader.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkProbeFilter.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkPropPicker.h>
#include <vtkQImageToImageSource.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStructuredGrid.h>
#include <vtkStructuredGridOutlineFilter.h>
#include <vtkSTLReader.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTextRenderer.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkTubeFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGridGeometryFilter.h>
#include <vtkUnstructuredGridVolumeRayCastMapper.h>
#include <vtkDataSetTriangleFilter.h>
#include <vtkExtractUnstructuredGrid.h>
#include <vtkShrinkFilter.h>
#include <vtkVersion.h>
#include <vtkVolumeMapper.h>
#include <vtkWindowToImageFilter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXYPlotActor.h>
#include <vtkLegendScaleActor.h>

#include <vtkLODActor.h>
#include <vtkMarchingCubes.h>

#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkPiecewiseFunction.h>
#include <vtkColorTransferFunction.h>
#include <vtkClipPolyData.h>
#include <vtkExtractGeometry.h>
#include <vtkShrinkFilter.h>
#include <vtkChartXY.h>
#include <vtkOpenGLProjectedTetrahedraMapper.h>

#include <vtkSmartVolumeMapper.h>
#include <vtkVolumeRayCastCompositeFunction.h>
#include <vtkVolumeRayCastMapper.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>

#if VTK_MAJOR_VERSION <= 5
#define setInputData(x,y) ((x)->SetInput(y))
#define addInputData(x,y) ((x)->AddInput(y))
#else
#define setInputData(x,y) ((x)->SetInputData(y))
#define addInputData(x,y) ((x)->AddInputData(y))
#endif

using std::cout;

VTKViewer::VTKViewer(QWidget* widget): QVTKWidget(widget){
    showAxes = false;
    showModel = true;
    showSludge = true;
    opacityModel = 1.0;

    parent = widget;
    graphs = new GraphObject[4];
}

VTKViewer::~VTKViewer(){

}

static vtkSmartPointer<vtkPolyData> ReadPDB(const char* file_name){
    vtkSmartPointer<vtkPDBReader> pdb = vtkSmartPointer<vtkPDBReader>::New();
    pdb->SetFileName(file_name);
    pdb->SetHBScale(1.0);
    pdb->SetBScale(1.0);
    pdb->Update();

    vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
    sphere->SetCenter(0, 0, 0);
    sphere->SetRadius(1);

    vtkSmartPointer<vtkGlyph3D> glyph = vtkSmartPointer<vtkGlyph3D>::New();
    glyph->SetInputConnection(pdb->GetOutputPort());
    glyph->SetSourceConnection(sphere->GetOutputPort());
    glyph->SetOrient(1);
    glyph->SetColorMode(1);
    glyph->SetScaleMode(2);
    glyph->SetScaleFactor(.25);
    glyph->Update();

    vtkSmartPointer<vtkTubeFilter> tube = vtkSmartPointer<vtkTubeFilter>::New();
    tube->SetInputConnection(pdb->GetOutputPort());
    tube->SetNumberOfSides(6);
    tube->CappingOff();
    tube->SetRadius(0.2);
    tube->SetVaryRadius(0);
    tube->SetRadiusFactor(10);
    tube->Update();

    vtkSmartPointer<vtkPolyData> tubeMesh = vtkSmartPointer< vtkPolyData >::New();
    tubeMesh->ShallowCopy(tube->GetOutput());
    vtkIdType N = tubeMesh->GetNumberOfPoints();

    vtkUnsignedCharArray* rgb_colors = vtkUnsignedCharArray::SafeDownCast(tubeMesh->GetPointData()->GetArray("rgb_colors"));
    const unsigned char tuple[3] = {127,127,127};
    if ((rgb_colors != NULL) & (rgb_colors->GetNumberOfComponents() == 3)){
        for (vtkIdType i = 0; i < N ; ++i){
            rgb_colors->SetTupleValue(i, tuple);
        }
    }

    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer< vtkAppendPolyData >::New();
    appendFilter->AddInputConnection(glyph->GetOutputPort());
    addInputData(appendFilter, tubeMesh);
    appendFilter->Update();

    vtkSmartPointer< vtkPolyData > polyData = appendFilter->GetOutput();
    return polyData;
}

static vtkSmartPointer<vtkPolyData> ConvertDataSetToSurface(vtkAlgorithmOutput* outputPort){
    vtkSmartPointer<vtkDataSetSurfaceFilter> dataSetSurfaceFilter = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    dataSetSurfaceFilter->SetInputConnection(outputPort);
    dataSetSurfaceFilter->Update();
    vtkSmartPointer< vtkPolyData > polyData = dataSetSurfaceFilter->GetOutput();
    return polyData;
}

template <typename T>
static vtkSmartPointer< vtkPolyData > read(const char* file_name){
    vtkSmartPointer<T> reader = vtkSmartPointer<T>::New();
    reader->SetFileName(file_name);
    reader->Update();
    vtkSmartPointer< vtkPolyData > polyData = reader->GetOutput();
    return polyData;
}

template <typename T>
static vtkSmartPointer< vtkPolyData > readDataSet(const char* file_name){
    vtkSmartPointer< T > reader = vtkSmartPointer< T >::New();
    reader->SetFileName(file_name);
    reader->Update();
    return ConvertDataSetToSurface(reader->GetOutputPort());
}

static vtkSmartPointer< vtkPolyData > ReadLegacyVTK(const char* file_name){
    vtkSmartPointer <vtkDataSetReader> reader = vtkSmartPointer <vtkDataSetReader>::New();
    reader->SetFileName(file_name);
    reader->Update();

    if (NULL != reader->GetPolyDataOutput()){
        vtkSmartPointer< vtkPolyData > polyData = reader->GetPolyDataOutput();
        return polyData;
    }
    else if ( (NULL != reader->GetUnstructuredGridOutput()) || (NULL != reader->GetStructuredPointsOutput()) || (NULL != reader->GetStructuredGridOutput()) || (NULL != reader->GetRectilinearGridOutput())){
        return ConvertDataSetToSurface(reader->GetOutputPort());
    }
    else{
        std::cerr << "unsupported: ????????\n";
        return vtkSmartPointer< vtkPolyData >::New();
    }
}

VTKViewer::VTKViewer() : m_renderer(vtkSmartPointer <vtkRenderer>::New()){
    //Añadido por Sergio
    viewer = new QVTKWidget();

    vtkSmartPointer<vtkAxesActor> axesActor = vtkSmartPointer<vtkAxesActor>::New();
    axes = axesActor;

    vtkSmartPointer <vtkRenderWindow> renderWindow = vtkSmartPointer <vtkRenderWindow>::New();
    //renderWindow->StereoCapableWindowOn();
    renderWindow->AddRenderer(m_renderer);
    m_renderer->SetBackground(1,1,1);
    m_renderer->ResetCamera();

    // Begin mouse interaction
    this->SetRenderWindow(renderWindow);
    this->resize(480, 480);
    this->setMinimumSize(480, 360);
    connect(&(m_timer), SIGNAL(timeout()), this, SLOT(rotate()));
}

void VTKViewer::add(vtkContextActor* contextActor){
    m_renderer->AddActor(contextActor);
}




vtkActor* VTKViewer::add(vtkPolyData* polyData){
    double range[2];
    polyData->GetScalarRange(range);
    vtkSmartPointer<vtkColorTransferFunction> colorMap = vtkSmartPointer<vtkColorTransferFunction>::New();
    colorMap->SetColorSpaceToLab();
    colorMap->AddRGBPoint(range[0], 0.865, 0.865, 0.865);
    colorMap->AddRGBPoint(range[1], 0.706, 0.016, 0.150);
    colorMap->Build();

    vtkSmartPointer <vtkPolyDataMapper> mapper = vtkSmartPointer <vtkPolyDataMapper>::New();
    mapper->SetLookupTable(colorMap);

    if (polyData->GetPointData()->GetNormals() == NULL){
        vtkSmartPointer<vtkPolyDataNormals> polyDataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
        setInputData(polyDataNormals, polyData);
        polyDataNormals->SetFeatureAngle(90.0);
        mapper->SetInputConnection(polyDataNormals->GetOutputPort());
    }
    else{
        setInputData(mapper, polyData);
    }

    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->GetProperty()->SetPointSize(3);
    actor->GetProperty()->SetColor(0.2,0.2,0.2);
    actor->GetProperty()->SetLineWidth(2);
    actor->SetMapper(mapper);
    m_renderer->AddActor(actor);

    return actor.GetPointer();
}

void VTKViewer::add(vtkActor* actor){
    m_renderer->AddActor(actor);

    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->Translate(0.0, 0.0, 0.0);

    // The axes are positioned with a user transform
    if(showAxes){
        /*   vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
        axes->SetUserTransform(transform);*/
        btnXYZ2->setChecked(true);
        m_renderer->AddActor(axes);
    }
}

void VTKViewer::add(vtkActor2D* actor){
    m_renderer->AddActor(actor);

    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->Translate(0.0, 0.0, 0.0);

    // The axes are positioned with a user transform
    if(showAxes){
        /* vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
        axes->SetUserTransform(transform);*/
        btnXYZ2->setChecked(true);
        m_renderer->AddActor(axes);
    }
}

void VTKViewer::add(vtkXYPlotActor* actor){
    m_renderer->AddActor(actor);
}

vtkActor* VTKViewer::add(const char* file_name){
    // TODO:  add logic for other file formats.
    QString filename = QString::fromUtf8(file_name).toLower();
    if (filename.endsWith(".vtk")){
        return this->add(ReadLegacyVTK(file_name));
    }
    else if (filename.endsWith(".vtp")){
        return this->add(read<vtkXMLPolyDataReader>(file_name));
    }
    else if (filename.endsWith(".ply")){
        return this->add(read<vtkPLYReader>(file_name));
    }
    else if (filename.endsWith(".obj")){
        return this->add(read<vtkOBJReader>(file_name));
    }
    else if (filename.endsWith(".stl")){
        return this->add(read<vtkSTLReader>(file_name));
    }
    else if (filename.endsWith(".vtu")){
        vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        reader->SetFileName(file_name);
        reader->Update();
        return this->add(ConvertDataSetToSurface(reader->GetOutputPort()));
    }
    else if (filename.endsWith(".pdb")){
        return this->add(ReadPDB(file_name));
    }
    else if (filename.endsWith(".vti")){
        return this->add(readDataSet<vtkXMLImageDataReader>(file_name));
    }
    else if (filename.endsWith(".vts")){
        return this->add(readDataSet<vtkXMLStructuredGridReader>(file_name));
    }
    else if (filename.endsWith(".vtr")){
        return this->add(readDataSet<vtkXMLRectilinearGridReader>(file_name));
    }
    else{
        std::cerr << file_name
                  << ": BAD FILE NAME.  "
                     "Should end in VTK, VTP, PLY, OBJ, STL, VTU, or PDB.\n";
        exit(1);
        return nullptr;
    }
}

void VTKViewer::addGraph(int timestep, int opacity, QString metric, double*p0, double*p1){
    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->Update();
    reader->SetTimeValue(timestep);
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    reader->ReadZonesOn();
    reader->Update();

    vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
    gf->SetInputConnection(0, reader->GetOutputPort(0));
    vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    block0->GetCellData()->SetActiveScalars(metric.toStdString().c_str());
    block0->GetPointData()->SetActiveScalars(metric.toStdString().c_str());

    workingBlock = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    workingBlock->GetCellData()->SetActiveScalars(metric.toStdString().c_str());
    workingBlock->GetPointData()->SetActiveScalars(metric.toStdString().c_str());

    vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
    lineSource->SetResolution(100);
    lineSource->SetPoint1(p0);
    lineSource->SetPoint2(p1);
    lineSource->Update();

    vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
    probe->SetInputConnection(lineSource->GetOutputPort());
    probe->SetSourceData(block0);

    vtkSmartPointer<vtkPolyDataMapper> mapper =  vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(probe->GetOutputPort());

    lineActor = vtkSmartPointer<vtkActor>::New();
    lineActor->SetMapper(mapper);

    vtkSmartPointer<vtkXYPlotActor> xyplot = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot->AddDataSetInputConnection(probe->GetOutputPort());
    //xyplot->SetBorder(200);
    xyplot->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
    xyplot->GetPosition2Coordinate()->SetValue(0.95, 0.95, 0.0); //relative to Position

    xyplot->SetXValuesToArcLength();
    //xyplot->SetXValuesToValue();
    xyplot->SetNumberOfXLabels(6);
    xyplot->SetTitle("sergio");
    xyplot->SetXTitle("Length");
    xyplot->SetYTitle("");
    xyplot->AdjustTitlePositionOff();
    //xyplot->SetAdjustTitlePositionMode(vtkXYPlotActor::AlignBottom);
    xyplot->GetProperty()->SetColor(0, 0, 0);
    xyplot->GetProperty()->SetLineWidth(2);
    //xyplot->LegendOn();
    //xyplot->SetYRange(0,1);
    //xyplot->SetXRange(-1,3);

    vtkSmartPointer<vtkTextProperty> tprop = vtkSmartPointer<vtkTextProperty>::New();
    tprop = xyplot->GetTitleTextProperty();
    tprop->SetColor(xyplot->GetProperty()->GetColor());
    xyplot->SetAxisTitleTextProperty(tprop);
    xyplot->SetAxisLabelTextProperty(tprop);

    m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot);
}


void VTKViewer::addGraph(QList<ResultEntity*>*list_entities){
    reset();
    if(list_entities->size() == 1){
        vtkSmartPointer<vtkXYPlotActor> xyplot = getChart(list_entities->first());
        xyplot->GetPositionCoordinate()->SetValue(0, 0, 0);
        xyplot->GetPosition2Coordinate()->SetValue(1, 1, 0);
        m_renderer->AddActor2D(xyplot);
    } else if (list_entities->size() == 2){
        vtkSmartPointer<vtkXYPlotActor> xyplot1 = getChart(list_entities->first());
        xyplot1->GetPositionCoordinate()->SetValue(0, 0.5, 0);
        xyplot1->GetPosition2Coordinate()->SetValue(1, 0.5, 0);
        m_renderer->AddActor2D(xyplot1);
        vtkSmartPointer<vtkXYPlotActor> xyplot2 = getChart(list_entities->at(1));
        xyplot2->GetPositionCoordinate()->SetValue(0, 0, 0);
        xyplot2->GetPosition2Coordinate()->SetValue(1, 0.5, 0);
        m_renderer->AddActor2D(xyplot2);
    } else {
        vtkSmartPointer<vtkXYPlotActor> xyplot1 = getChart(list_entities->first());
        xyplot1->GetPositionCoordinate()->SetValue(0, 0.5, 0);
        xyplot1->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0);
        m_renderer->AddActor2D(xyplot1);
        vtkSmartPointer<vtkXYPlotActor> xyplot2 = getChart(list_entities->at(1));
        xyplot2->GetPositionCoordinate()->SetValue(0, 0, 0);
        xyplot2->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0);
        m_renderer->AddActor2D(xyplot2);
        vtkSmartPointer<vtkXYPlotActor> xyplot3 = getChart(list_entities->at(2));
        xyplot3->GetPositionCoordinate()->SetValue(0.5, 0.5, 0);
        xyplot3->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0);
        m_renderer->AddActor2D(xyplot3);
        if(list_entities->size() == 4){
            vtkSmartPointer<vtkXYPlotActor> xyplot4 = getChart(list_entities->at(3));
            xyplot4->GetPositionCoordinate()->SetValue(0.5, 0, 0);
            xyplot4->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0);
            m_renderer->AddActor2D(xyplot4);
        }
    }
    update();
}

vtkSmartPointer<vtkXYPlotActor> VTKViewer::getChart(ResultEntity* entity){
    vtkSmartPointer<vtkXYPlotActor> xyplot = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot->AddDataSetInputConnection(entity->probe->GetOutputPort());

    xyplot->SetXValuesToArcLength();
    xyplot->SetNumberOfXLabels(3);
    xyplot->SetXTitle((entity->getName() + "(" + entity->getMetric() + "/length)").toStdString().c_str());
    xyplot->SetYTitle("");
    xyplot->GetProperty()->SetColor(0, 0, 0);
    xyplot->GetProperty()->SetLineWidth(2);
    xyplot->SetLabelFormat("%g");
    xyplot->SetNumberOfYLabels(2);

    vtkSmartPointer<vtkTextProperty> tprop = vtkSmartPointer<vtkTextProperty>::New();
    tprop = xyplot->GetTitleTextProperty();
    tprop->SetColor(xyplot->GetProperty()->GetColor());

    xyplot->SetAxisTitleTextProperty(tprop);
    xyplot->SetAxisLabelTextProperty(tprop);

    return xyplot;
}

void VTKViewer::addGraph(ResultEntity *e1, ResultEntity *e2, ResultEntity *e3, ResultEntity *e4){
    reset();
    vtkSmartPointer<vtkXYPlotActor> xyplot = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot->AddDataSetInputConnection(e1->probe->GetOutputPort());
    //xyplot->GetPositionCoordinate()->SetValue(0.0, 0.02, 0);
    //xyplot->GetPosition2Coordinate()->SetValue(1, 0.98, 0.0); //relative to Position
    xyplot->GetPositionCoordinate()->SetValue(0.0, 0.5, 0);
    xyplot->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);


    xyplot->SetXValuesToArcLength();
    xyplot->SetNumberOfXLabels(3);
    xyplot->SetXTitle((e1->getMetric() + "/length").toStdString().c_str());
    xyplot->SetYTitle("");
    xyplot->GetProperty()->SetColor(0, 0, 0);
    xyplot->GetProperty()->SetLineWidth(2);
    xyplot->SetLabelFormat("%g");
    xyplot->SetNumberOfYLabels(2);

    vtkSmartPointer<vtkTextProperty> tprop = vtkSmartPointer<vtkTextProperty>::New();
    tprop = xyplot->GetTitleTextProperty();
    tprop->SetColor(xyplot->GetProperty()->GetColor());
    //tprop->SetOrientation(90);

    xyplot->SetAxisTitleTextProperty(tprop);
    xyplot->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot);

    /******************************************************/
    /******************************************************/
    /******************************************************/
    vtkSmartPointer<vtkXYPlotActor> xyplot2 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot2->AddDataSetInputConnection(e1->probe->GetOutputPort());
    xyplot2->GetPositionCoordinate()->SetValue(0.5, 0.5, 0);
    xyplot2->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot2->SetXValuesToArcLength();
    xyplot2->SetNumberOfXLabels(5);
    xyplot2->SetXTitle((e1->getMetric() + "/length").toStdString().c_str());
    xyplot2->SetYTitle("");
    xyplot2->SetNumberOfYLabels(3);
    xyplot2->GetProperty()->SetColor(0, 0, 0);
    xyplot2->GetProperty()->SetLineWidth(2);
    xyplot2->SetLabelFormat("%g");

    xyplot2->SetAxisTitleTextProperty(tprop);
    xyplot2->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot2);
    /******************************************************/
    /******************************************************/
    /******************************************************/

    /******************************************************/
    /******************************************************/
    /******************************************************/
    vtkSmartPointer<vtkXYPlotActor> xyplot3 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot3->AddDataSetInputConnection(e2->probe->GetOutputPort());
    xyplot3->GetPositionCoordinate()->SetValue(0.0, 0.0, 0);
    xyplot3->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot3->SetXValuesToArcLength();
    xyplot3->SetNumberOfXLabels(3);
    xyplot3->SetNumberOfYLabels(3);
    xyplot3->SetXTitle((e2->getMetric() + "/length").toStdString().c_str());
    xyplot3->SetYTitle("");
    xyplot3->GetProperty()->SetColor(0, 0, 0);
    xyplot3->GetProperty()->SetLineWidth(2);
    xyplot3->SetLabelFormat("%g");

    tprop->SetFontFamilyToArial();
    xyplot3->SetAxisTitleTextProperty(tprop);
    xyplot3->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot3);
    /******************************************************/
    /******************************************************/
    /******************************************************/

    /******************************************************/
    /******************************************************/
    /******************************************************/
    vtkSmartPointer<vtkXYPlotActor> xyplot4 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot4->AddDataSetInputConnection(e2->probe->GetOutputPort());
    xyplot4->GetPositionCoordinate()->SetValue(0.5, 0.0, 0);
    xyplot4->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot4->SetXValuesToArcLength();
    xyplot4->SetNumberOfXLabels(3);
    xyplot4->SetNumberOfYLabels(3);
    xyplot4->SetXTitle((e2->getMetric() + "/length").toStdString().c_str());
    xyplot4->SetYTitle("");
    xyplot4->GetProperty()->SetColor(0, 0, 0);
    xyplot4->GetProperty()->SetLineWidth(2);
    xyplot4->SetLabelFormat("%g");

    xyplot4->SetAxisTitleTextProperty(tprop);
    xyplot4->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot4);
    /******************************************************/
    /******************************************************/
    /******************************************************/
}


void VTKViewer::addGraph(vtkSmartPointer<vtkProbeFilter> probe, QString ytitle){
    //reset();
    vtkSmartPointer<vtkXYPlotActor> xyplot = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot->AddDataSetInputConnection(probe->GetOutputPort());
    xyplot->GetPositionCoordinate()->SetValue(0.0, 0.02, 0);
    xyplot->GetPosition2Coordinate()->SetValue(1, 0.98, 0.0); //relative to Position
    //xyplot->GetPositionCoordinate()->SetValue(0.0, 0.5, 0);
    //xyplot->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);


    xyplot->SetXValuesToArcLength();
    xyplot->SetNumberOfXLabels(3);
    xyplot->SetXTitle((ytitle + "/length").toStdString().c_str());
    xyplot->SetYTitle("");
    xyplot->GetProperty()->SetColor(0, 0, 0);
    xyplot->GetProperty()->SetLineWidth(2);
    xyplot->SetLabelFormat("%g");
    xyplot->SetNumberOfYLabels(2);

    vtkSmartPointer<vtkTextProperty> tprop = vtkSmartPointer<vtkTextProperty>::New();
    tprop = xyplot->GetTitleTextProperty();
    tprop->SetColor(xyplot->GetProperty()->GetColor());
    //tprop->SetOrientation(90);

    xyplot->SetAxisTitleTextProperty(tprop);
    xyplot->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot);

    /******************************************************/
    /******************************************************/
    /******************************************************/
    /*vtkSmartPointer<vtkXYPlotActor> xyplot2 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot2->AddDataSetInputConnection(probe->GetOutputPort());
    xyplot2->GetPositionCoordinate()->SetValue(0.5, 0.5, 0);
    xyplot2->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot2->SetXValuesToArcLength();
    xyplot2->SetNumberOfXLabels(5);
    xyplot2->SetXTitle((ytitle + "/length").toStdString().c_str());
    xyplot2->SetYTitle("");
    xyplot2->SetNumberOfYLabels(3);
    xyplot2->GetProperty()->SetColor(0, 0, 0);
    xyplot2->GetProperty()->SetLineWidth(2);
    xyplot2->SetLabelFormat("%g");

    xyplot2->SetAxisTitleTextProperty(tprop);
    xyplot2->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot2);*/
    /******************************************************/
    /******************************************************/
    /******************************************************/

    /******************************************************/
    /******************************************************/
    /******************************************************/
    /*vtkSmartPointer<vtkXYPlotActor> xyplot3 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot3->AddDataSetInputConnection(probe->GetOutputPort());
    xyplot3->GetPositionCoordinate()->SetValue(0.0, 0.0, 0);
    xyplot3->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot3->SetXValuesToArcLength();
    xyplot3->SetNumberOfXLabels(3);
    xyplot3->SetNumberOfYLabels(3);
    xyplot3->SetXTitle((ytitle + "/length").toStdString().c_str());
    xyplot3->SetYTitle("");
    xyplot3->GetProperty()->SetColor(0, 0, 0);
    xyplot3->GetProperty()->SetLineWidth(2);
    xyplot3->SetLabelFormat("%g");

    tprop->SetFontFamilyToArial();
    xyplot3->SetAxisTitleTextProperty(tprop);
    xyplot3->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot3);*/
    /******************************************************/
    /******************************************************/
    /******************************************************/

    /******************************************************/
    /******************************************************/
    /******************************************************/
    /*vtkSmartPointer<vtkXYPlotActor> xyplot4 = vtkSmartPointer<vtkXYPlotActor>::New();
    xyplot4->AddDataSetInputConnection(probe->GetOutputPort());
    xyplot4->GetPositionCoordinate()->SetValue(0.5, 0.0, 0);
    xyplot4->GetPosition2Coordinate()->SetValue(0.5, 0.5, 0.0);
    xyplot4->SetXValuesToArcLength();
    xyplot4->SetNumberOfXLabels(3);
    xyplot4->SetNumberOfYLabels(3);
    xyplot4->SetXTitle((ytitle + "/length").toStdString().c_str());
    xyplot4->SetYTitle("");
    xyplot4->GetProperty()->SetColor(0, 0, 0);
    xyplot4->GetProperty()->SetLineWidth(2);
    xyplot4->SetLabelFormat("%g");

    xyplot4->SetAxisTitleTextProperty(tprop);
    xyplot4->SetAxisLabelTextProperty(tprop);

    //m_renderer->RemoveAllViewProps();
    m_renderer->AddActor2D(xyplot4);*/
    /******************************************************/
    /******************************************************/
    /******************************************************/
}


vtkSmartPointer<vtkProbeFilter> VTKViewer::addLine(double *p0, double *p1){
    vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
    lineSource->SetResolution(100);
    lineSource->SetPoint1(p0);
    lineSource->SetPoint2(p1);
    lineSource->Update();

    vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
    probe->SetInputConnection(lineSource->GetOutputPort());
    probe->SetSourceData(workingBlock);

    // Create a vtkAppendPolyData to merge the output of the three probe filters into one data set.
    vtkSmartPointer<vtkAppendPolyData> appendF = vtkSmartPointer<vtkAppendPolyData>::New();
    appendF->AddInputConnection(probe->GetOutputPort());

    // Create a tube filter to represent the lines as tubes.  Set up the associated mapper and actor.
    vtkSmartPointer<vtkTubeFilter> tuber = vtkSmartPointer<vtkTubeFilter>::New();
    tuber->SetInputConnection(appendF->GetOutputPort());
    tuber->SetInputConnection(probe->GetOutputPort());
    tuber->SetRadius(0.1);
    tuber->SetNumberOfSides(50);

    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper =  vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(tuber->GetOutputPort());

    vtkSmartPointer<vtkActor> lineActor = vtkSmartPointer<vtkActor>::New();
    lineActor->SetMapper(mapper);
    //lineActor->GetProperty()->SetLineWidth(10);

    m_renderer->AddActor(lineActor);

    return probe;
}



void VTKViewer::addSludge(){
    vtkSmartPointer<vtkContourFilter> contourFilter = vtkSmartPointer<vtkContourFilter>::New();

    contourFilter->SetInputData(workingBlock);

    contourFilter->GenerateValues(1, 0.001, 0.001); // (numContours, rangeStart, rangeEnd)

    // Map the contours to graphical primitives
    vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    contourMapper->SetInputConnection(contourFilter->GetOutputPort());

    // Create an actor for the contours
    sludgeActor = vtkSmartPointer<vtkActor>::New();
    sludgeActor->SetMapper(contourMapper);

    m_renderer->AddActor(sludgeActor);
}

void VTKViewer::addPlane(double *origin, double *normal){
    // Create a plane to cut,here it cuts in the XZ direction (xz normal=(1,0,0);XY =(0,0,1),YZ =(0,1,0)
    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin(origin);
    plane->SetNormal(normal);

    // Create cutter
    vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
    cutter->SetCutFunction(plane);
    cutter->SetInputData(workingBlock);
    cutter->Update();

    vtkSmartPointer<vtkPolyDataMapper> cutterMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    cutterMapper->SetInputConnection(cutter->GetOutputPort());

    // Create plane actor
    planeActor = vtkSmartPointer<vtkActor>::New();
    planeActor->GetProperty()->SetColor(0.54,0.46,0.46);
    planeActor->GetProperty()->SetRepresentationToSurface();
    planeActor->GetProperty()->SetLineWidth(2);
    planeActor->SetMapper(cutterMapper);

    vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
    hueLut->SetHueRange(0.667,0.0);
    hueLut->Build();

    cutterMapper->SetLookupTable(hueLut);

    double *range = workingBlock->GetCellData()->GetScalars()->GetRange();
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - "
             << curr_metric.toStdString().c_str() << " - "
             << QString::number(range[0]) << " - " << QString::number(range[1])
             << endl;
    cutterMapper->SetScalarRange(workingBlock->GetCellData()->GetScalars()->GetRange());
    cutterMapper->SetScalarModeToUsePointData();
    cutterMapper->SetColorModeToMapScalars();
    cutterMapper->ScalarVisibilityOn();

    vtkSmartPointer<vtkScalarBarActor> scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    scalarBar->SetLookupTable(cutterMapper->GetLookupTable());
    scalarBar->SetTitle(curr_metric.toStdString().c_str());
    scalarBar->SetNumberOfLabels(4);
    scalarBar->SetLookupTable(hueLut);
    //scalarBar->DragableOn();
    scalarBar->SetWidth(0.1);
    scalarBar->SetHeight(0.5);

    m_renderer->AddActor(planeActor);
    //invoking this after the plane updates the range of the sacalar bar
    m_renderer->AddActor2D(scalarBar);
}


void VTKViewer::readOpenFoam(double stepTime, QString displayMode, bool showScalarBar, bool showWireFrame, bool showTimeStep, bool mesh){
    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->Update();
    reader->SetTimeValue(stepTime);
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    if (mesh) {
        reader->DecomposePolyhedraOff();
    }
    reader->ReadZonesOn();
    reader->Update();

    vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
    gf->SetInputConnection(0, reader->GetOutputPort(0));
    vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    block0->GetCellData()->SetActiveScalars(displayMode.toStdString().c_str());
    block0->GetPointData()->SetActiveScalars(displayMode.toStdString().c_str());

    if(showSludge){
        // Create an isosurface
        vtkSmartPointer<vtkContourFilter> contourFilter = vtkSmartPointer<vtkContourFilter>::New();
        contourFilter->SetInputData(block0);
        contourFilter->GenerateValues(1, 0.001, 0.001); // (numContours, rangeStart, rangeEnd)

        // Map the contours to graphical primitives
        vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        contourMapper->SetInputConnection(contourFilter->GetOutputPort());

        // Create an actor for the contours
        vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();
        contourActor->SetMapper(contourMapper);

        m_renderer->AddActor(contourActor);
    }

    if(showModel){
        vtkSmartPointer<vtkPolyDataMapper> polyDataMapper1 = vtkSmartPointer<vtkPolyDataMapper>::New();
        polyDataMapper1->SetInputConnection(gf->GetOutputPort());
        polyDataMapper1->CreateDefaultLookupTable(); // Use default color lookup table
        polyDataMapper1->SetScalarModeToUseCellFieldData();// # Use cell data for color mapping
        polyDataMapper1->SelectColorArray("alpha.sludge"); //# Specify array name used for color mapping

        vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
        mapper->SetInputData(block0);
        //vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
        actor->SetMapper(mapper);

        if(showWireFrame){
            actor->GetProperty()->SetColor(0.52,0.62,66);
            actor->GetProperty()->SetRepresentationToSurface();
            actor->GetProperty()->SetLineWidth(2);
            actor->GetProperty()->EdgeVisibilityOn();
            actor->GetProperty()->SetOpacity(0.7);
            actor->GetProperty()->BackfaceCullingOn();
        } else if (mesh) {
            actor->GetProperty()->SetColor(0.54,0.46,0.46);
            actor->GetProperty()->SetRepresentationToSurface();
            actor->GetProperty()->SetLineWidth(2);
            actor->GetProperty()->EdgeVisibilityOn();
            actor->GetProperty()->SetOpacity(1);
        } else{
            actor->GetProperty()->SetColor(0.54,0.46,0.46);
            actor->GetProperty()->SetRepresentationToSurface();
            //actor->GetProperty()->SetRepresentationToWireframe();
            actor->GetProperty()->SetLineWidth(2);
            actor->GetProperty()->SetOpacity(opacityModel);
            actor->SetNumberOfCloudPoints(10);
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << actor->GetProperty()->GetPointSize() << endl;
            actor->GetProperty()->SetPointSize(10);
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << actor->GetNumberOfCloudPoints() << endl;

        }

        // Create a lookup table to share between the mapper and the scalarbar
        vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
        hueLut->SetHueRange(0.667,0.0);
        //hueLut->SetHueRange(block0->GetCellData()->GetScalars()->GetRange());
        hueLut->Build();
        mapper->SetLookupTable(hueLut);
        m_renderer->AddActor(actor);

        // Visualize
        if(showScalarBar){
            mapper->SetScalarRange(block0->GetCellData()->GetScalars()->GetRange());
            //mapper->SetScalarRange(0,0.00275);
            mapper->SetScalarModeToUsePointData();
            mapper->SetColorModeToMapScalars();
            mapper->ScalarVisibilityOn();

            vtkSmartPointer<vtkScalarBarActor> scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
            scalarBar->SetLookupTable(mapper->GetLookupTable());
            scalarBar->SetTitle(displayMode.toStdString().c_str());
            scalarBar->SetNumberOfLabels(4);
            scalarBar->SetLookupTable(hueLut);
            //scalarBar->DragableOn();
            scalarBar->SetWidth(0.1);
            scalarBar->SetHeight(0.5);

            m_renderer->AddActor2D(scalarBar);
        }

        if(showTimeStep){
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
            vtkSmartPointer<vtkCornerAnnotation> cornerAnnotation = vtkSmartPointer<vtkCornerAnnotation>::New();
            cornerAnnotation->SetLinearFontScaleFactor(2);
            cornerAnnotation->SetNonlinearFontScaleFactor(1);
            cornerAnnotation->SetMaximumFontSize(30);

            std::string str = "Time:\n" + std::to_string(stepTime/60) + " min.";
            cornerAnnotation->SetText(0, str.c_str());
            cornerAnnotation->SetDragable(1);
            cornerAnnotation->GetTextProperty()->SetColor(0, 0 , 0);
            cornerAnnotation->GetTextProperty()->BoldOn();
            cornerAnnotation->GetTextProperty()->GetFontFamilyFromString("Calibri");
            m_renderer->AddViewProp(cornerAnnotation);
        }

        // The axes are positioned with a user transform
        if(showAxes){
            btnXYZ2->setChecked(true);
            m_renderer->AddActor(axes);
        }

        //Legend Scale
        vtkSmartPointer<vtkLegendScaleActor> legendScaleActor = vtkSmartPointer<vtkLegendScaleActor>::New();
        legendScaleActor->GetLegendLabelProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendTitleProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendLabelProperty()->SetFontSize(legendScaleActor->GetLegendLabelProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetFontSize(legendScaleActor->GetLegendTitleProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetLineOffset(-25);
        legendScaleActor->GetLegendLabelProperty()->SetLineOffset(-25);
        legendScaleActor->AllAxesOff();
        //legendScaleActor->AllAnnotationsOff();
        m_renderer->AddActor(legendScaleActor);
    }
}

double* VTKViewer::getMetricRange(QString metric, int timestep){
    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->Update();
    reader->SetTimeValue(timestep);
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    reader->ReadZonesOn();
    reader->Update();

    vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
    gf->SetInputConnection(0, reader->GetOutputPort(0));
    vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    block0->GetCellData()->SetActiveScalars(metric.toStdString().c_str());
    block0->GetPointData()->SetActiveScalars(metric.toStdString().c_str());

    //this methos is not thread safe and it crashes.
    //double *range = block0->GetCellData()->GetScalars()->GetRange();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << range[0] << " - " << range[1] << endl;
    double range[2];
    block0->GetCellData()->GetScalars()->GetRange(range);
    return range;
}


void VTKViewer::renderOpenFoamReader(QString timestep, int opacity, QString metric){
    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->Update();
    reader->SetTimeValue(timestep.toDouble());
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    reader->ReadZonesOn();
    reader->Update();

    curr_metric = metric;

    vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
    gf->SetInputConnection(0, reader->GetOutputPort(0));
    vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    block0->GetCellData()->SetActiveScalars(metric.toStdString().c_str());
    block0->GetPointData()->SetActiveScalars(metric.toStdString().c_str());

    workingBlock = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    workingBlock->GetCellData()->SetActiveScalars(metric.toStdString().c_str());
    workingBlock->GetPointData()->SetActiveScalars(metric.toStdString().c_str());

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << metric.toStdString().c_str() << endl;

    vtkSmartPointer<vtkPolyDataMapper> polyDataMapper1 = vtkSmartPointer<vtkPolyDataMapper>::New();
    polyDataMapper1->SetInputConnection(gf->GetOutputPort());
    polyDataMapper1->CreateDefaultLookupTable(); // Use default color lookup table
    polyDataMapper1->SetScalarModeToUseCellFieldData();// # Use cell data for color mapping
    polyDataMapper1->SelectColorArray(metric.toStdString().c_str()); //# Specify array name used for color mapping

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(block0);
    //vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
    actor->SetMapper(mapper);
    /*
        if(showWireFrame){
            actor->GetProperty()->SetColor(0.52,0.62,66);
            actor->GetProperty()->SetRepresentationToSurface();
            actor->GetProperty()->SetLineWidth(2);
            actor->GetProperty()->EdgeVisibilityOn();
            actor->GetProperty()->SetOpacity(0.7);
            actor->GetProperty()->BackfaceCullingOn();
        }
  */
    actor->GetProperty()->SetColor(0.54,0.46,0.46);
    actor->GetProperty()->SetRepresentationToSurface();
    //actor->GetProperty()->SetRepresentationToWireframe();
    actor->GetProperty()->SetLineWidth(2);
    actor->GetProperty()->SetOpacity(opacity / 100.0);
    //actor->GetProperty()->SetOpacity(0);
    actor->SetNumberOfCloudPoints(10);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << actor->GetProperty()->GetPointSize() << endl;
    actor->GetProperty()->SetPointSize(10);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << actor->GetNumberOfCloudPoints() << endl;

    // }

    // Create a lookup table to share between the mapper and the scalarbar
    vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
    hueLut->SetHueRange(0.667,0.0);
    hueLut->Build();
    mapper->SetLookupTable(hueLut);

    m_renderer->AddActor(actor);

    //return;

    // Visualize
    // if(showScalarBar){
    mapper->SetScalarRange(block0->GetCellData()->GetScalars()->GetRange());
    //mapper->SetScalarRange(0,0.00275);
    mapper->SetScalarModeToUsePointData();
    mapper->SetColorModeToMapScalars();
    mapper->ScalarVisibilityOn();

    vtkSmartPointer<vtkScalarBarActor> scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    scalarBar->SetLookupTable(mapper->GetLookupTable());
    scalarBar->SetTitle(metric.toStdString().c_str());
    scalarBar->SetNumberOfLabels(4);
    scalarBar->SetLookupTable(hueLut);
    //scalarBar->DragableOn();
    scalarBar->SetWidth(0.1);
    scalarBar->SetHeight(0.5);

    //m_renderer->AddActor2D(scalarBar);
    //}

    //if(showTimeStep){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
    vtkSmartPointer<vtkCornerAnnotation> cornerAnnotation = vtkSmartPointer<vtkCornerAnnotation>::New();
    cornerAnnotation->SetLinearFontScaleFactor(2);
    cornerAnnotation->SetNonlinearFontScaleFactor(1);
    cornerAnnotation->SetMaximumFontSize(30);

    std::string str = "Time:\n" + std::to_string(timestep.toDouble()/60) + " min.";
    cornerAnnotation->SetText(0, str.c_str());
    cornerAnnotation->SetDragable(1);
    cornerAnnotation->GetTextProperty()->SetColor(0, 0 , 0);
    cornerAnnotation->GetTextProperty()->BoldOn();
    cornerAnnotation->GetTextProperty()->GetFontFamilyFromString("Calibri");
    m_renderer->AddViewProp(cornerAnnotation);
    //}

    // The axes are positioned with a user transform
    //if(showAxes){
    btnXYZ2->setChecked(true);
    m_renderer->AddActor(axes);
    //}

    //Legend Scale
    vtkSmartPointer<vtkLegendScaleActor> legendScaleActor = vtkSmartPointer<vtkLegendScaleActor>::New();
    legendScaleActor->GetLegendLabelProperty()->SetColor(0,0,0);
    legendScaleActor->GetLegendTitleProperty()->SetColor(0,0,0);
    legendScaleActor->GetLegendLabelProperty()->SetFontSize(legendScaleActor->GetLegendLabelProperty()->GetFontSize() * 2);
    legendScaleActor->GetLegendTitleProperty()->SetFontSize(legendScaleActor->GetLegendTitleProperty()->GetFontSize() * 2);
    legendScaleActor->GetLegendTitleProperty()->SetLineOffset(-25);
    legendScaleActor->GetLegendLabelProperty()->SetLineOffset(-25);
    legendScaleActor->AllAxesOff();
    //legendScaleActor->AllAnnotationsOff();
    m_renderer->AddActor(legendScaleActor);
}

void VTKViewer::addIsovolume(double max){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << " - " << max << endl;

    /*
    vtkSmartPointer<vtkContourFilter> contourFilter = vtkSmartPointer<vtkContourFilter>::New();
    contourFilter->SetInputData(workingBlock);
    contourFilter->ComputeNormalsOn();
    //contourFilter->ComputeGradientsOn();
    contourFilter->GenerateValues(10, min, max); // (numContours, rangeStart, rangeEnd)
    //contourFilter->SetValue(0, max);
    */

    vtkSmartPointer<vtkDataSetTriangleFilter> trifilter = vtkSmartPointer<vtkDataSetTriangleFilter>::New();
    trifilter->SetInputData(workingBlock);

    vtkSmartPointer<vtkOpenGLProjectedTetrahedraMapper> volumeMapper = vtkSmartPointer<vtkOpenGLProjectedTetrahedraMapper>::New();
    volumeMapper->SetInputConnection(trifilter->GetOutputPort());
    volumeMapper->SetBlendModeToComposite();

    // Create transfer mapping scalar value to opacity.
    vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
    opacityTransferFunction->AddPoint(0, 0);
    opacityTransferFunction->AddPoint(max, 1);

    // Create transfer mapping scalar value to color.
    vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
    //colorTransferFunction->AddRGBPoint(min, 0.0, 0.0, 1.0);
    colorTransferFunction->AddRGBPoint(max, 1.0, 0.0, 0.0);

    // The property describes how the data will look.
    vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();
    volumeProperty->SetColor(colorTransferFunction);
    volumeProperty->SetScalarOpacity(opacityTransferFunction);
    //volumeProperty->SetScalarOpacityUnitDistance(1);
    volumeProperty->ShadeOff();
    volumeProperty->SetInterpolationType(VTK_LINEAR_INTERPOLATION);
    //volumeProperty->ShadeOn();

    isoVolume = vtkSmartPointer<vtkVolume>::New();
    isoVolume->SetMapper(volumeMapper);
    isoVolume->SetProperty(volumeProperty);

    m_renderer->AddVolume(isoVolume);

    /*
    // Map the contours to graphical primitives
    vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    contourMapper->SetInputConnection(contourFilter->GetOutputPort());

    // Create an actor for the contours
    contourActor = vtkSmartPointer<vtkActor>::New();
    contourActor->SetMapper(contourMapper);
    m_renderer->AddActor(contourActor);*/

}


void VTKViewer::reset(){
    if(m_renderer->GetRenderWindow()->GetRenderers()->GetNumberOfItems() != 0){
        m_renderer->RemoveAllViewProps(); // Oculta todos los actores anteriores
        //showLogo();
        //m_renderer->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActors()->RemoveAllItems();
    }
}

void VTKViewer::rotate(){
    vtkCamera* camera = m_renderer->GetActiveCamera();
    assert(camera != NULL);
    camera->Azimuth(1);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::remove(vtkContextActor* contextActor){
    m_renderer->RemoveActor(contextActor);
    //return context.GetPointer();
}

double* VTKViewer::bounds(){
    double* b;
    b = m_renderer->ComputeVisiblePropBounds();
    return b;
    //return context.GetPointer();
}

void VTKViewer::screenshot(){
    vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
    windowToImageFilter->SetInput(this->GetRenderWindow() );
    windowToImageFilter->SetMagnification(1.99);
    //windowToImageFilter->SetInputBufferTypeToRGBA();
    windowToImageFilter->Update();

    vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetInputConnection(windowToImageFilter->GetOutputPort());
    //    QFile filename(Globals::instance()->getAppProjectPath().toStdString().c_str());

    char *path = strdup(Globals::instance()->getAppProjectPath().toStdString().c_str());
    char *name = "a.png";
    const char *filename = strcat(path,name); // append string two to the result.

    //const char *filename = Globals::instance()->getAppProjectPath().toStdString().c_str() + ".png";
    writer->SetFileName(filename);
    writer->Write();
}

/********************************************************************************
 * CONTROL AREA
 ********************************************************************************/

QWidget* VTKViewer::getControlArea(){


    QPushButton *btnXAxesPositive = new QPushButton;
    btnXAxesPositive->setIcon(QIcon(":/Resources/View_left.png"));
    btnXAxesPositive->setIconSize(QSize(28,28));
    //btnXAxesPositive->setText("X+");
    btnXAxesPositive->setMinimumSize(QSize(36,36));
    btnXAxesPositive->setMaximumSize(QSize(36,36));
    //btnXAxesPositive->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnXAxesPositive->setToolTip(tr("Set camera to x+ axe"));
    btnXAxesPositive->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnXAxesPositive, SIGNAL(clicked()), this, SLOT(cameraToPositiveX()));

    QPushButton *btnXAxesNegative = new QPushButton;
    btnXAxesNegative->setIcon(QIcon(":/Resources/View_right.png"));
    btnXAxesNegative->setIconSize(QSize(28,28));
    //btnXAxesNegative->setText("X-");
    btnXAxesNegative->setMinimumSize(QSize(36,36));
    btnXAxesNegative->setMaximumSize(QSize(36,36));
    //btnXAxesNegative->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnXAxesNegative->setToolTip(tr("Set camera to x- axe"));
    btnXAxesNegative->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnXAxesNegative, SIGNAL(clicked()), this, SLOT(cameraToNegativeX()));

    QPushButton *btnYAxesPositive = new QPushButton;
    btnYAxesPositive->setIcon(QIcon(":/Resources/View_top.png"));
    btnYAxesPositive->setIconSize(QSize(28,28));
    //btnYAxesPositive->setText("Y+");
    btnYAxesPositive->setMinimumSize(QSize(36,36));
    btnYAxesPositive->setMaximumSize(QSize(36,36));
    //btnYAxesPositive->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnYAxesPositive->setToolTip(tr("Set camera to y+ axe"));
    btnYAxesPositive->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnYAxesPositive, SIGNAL(clicked()), this, SLOT(cameraToNegativeY()));

    QPushButton *btnYAxesNegative = new QPushButton;
    btnYAxesNegative->setIcon(QIcon(":/Resources/View_bottom.png"));
    btnYAxesNegative->setIconSize(QSize(28,28));
    //btnYAxesNegative->setText("Y-");
    btnYAxesNegative->setMinimumSize(QSize(36,36));
    btnYAxesNegative->setMaximumSize(QSize(36,36));
    //btnYAxesNegative->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnYAxesNegative->setToolTip(tr("Set camera to y- axe"));
    btnYAxesNegative->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnYAxesNegative, SIGNAL(clicked()), this, SLOT(cameraToPositiveY()));

    QPushButton *btnZAxesPositive = new QPushButton;
    btnZAxesPositive->setIcon(QIcon(":/Resources/View_front.png"));
    btnZAxesPositive->setIconSize(QSize(28,28));
    //btnZAxesPositive->setText("Z+");
    btnZAxesPositive->setMinimumSize(QSize(36,36));
    btnZAxesPositive->setMaximumSize(QSize(36,36));
    //btnZAxesPositive->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnZAxesPositive->setToolTip(tr("Set camera to z+ axe"));
    btnZAxesPositive->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnZAxesPositive, SIGNAL(clicked()), this, SLOT(cameraToPositiveZ()));
    QPushButton *btnZAxesNegative = new QPushButton;
    btnZAxesNegative->setIcon(QIcon(":/Resources/View_rear.png"));
    btnZAxesNegative->setIconSize(QSize(28,28));
    //btnZAxesNegative->setText("Z-");
    btnZAxesNegative->setMinimumSize(QSize(36,36));
    btnZAxesNegative->setMaximumSize(QSize(36,36));
    //btnZAxesNegative->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnZAxesNegative->setToolTip(tr("Set camera to z- axe"));
    btnZAxesNegative->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnZAxesNegative, SIGNAL(clicked()), this, SLOT(cameraToNegativeZ()));

    QPushButton *btnReset = new QPushButton;
    btnReset->setIcon(QIcon(":/Resources/View_fullscreen.png"));
    btnReset->setIconSize(QSize(26,26));
    btnReset->setMinimumSize(QSize(36,36));
    btnReset->setMaximumSize(QSize(36,36));
    //btnReset->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnReset->setToolTip(tr("Adjust to camera"));
    btnReset->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnReset, SIGNAL(clicked()), this, SLOT(resetCamera()));

    QPushButton *btnPan = new QPushButton;
    btnPan->setIcon(QIcon(":/Resources/Draft_Move.png"));
    btnPan->setIconSize(QSize(28,28));
    btnPan->setMinimumSize(QSize(36,36));
    btnPan->setMaximumSize(QSize(36,36));
    //btnPan->setStyleSheet("QPushButton{border: none;outline: none;}");
    //btnPan->setDisabled(true);
    btnPan->setToolTip(tr("Pan zone"));
    btnPan->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnPan, SIGNAL(clicked()), this, SLOT(pan()));

    QPushButton *btnRange = new QPushButton;
    btnRange->setIcon(QIcon(":/Resources/Fem_Result.png"));
    btnRange->setIconSize(QSize(28,28));
    btnRange->setMinimumSize(QSize(36,36));
    btnRange->setMaximumSize(QSize(36,36));
    //btnPan->setStyleSheet("QPushButton{border: none;outline: none;}");
    //btnPan->setDisabled(true);
    btnRange->setToolTip(tr("Adjust bar range"));
    btnRange->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    //connect(btnRange, SIGNAL(clicked()), this, SLOT());

    QPushButton *btnCamera = new QPushButton;
    btnCamera->setIcon(QIcon(":/Resources/Camera-photo.png"));
    btnCamera->setIconSize(QSize(28,28));
    btnCamera->setMinimumSize(QSize(36,36));
    btnCamera->setMaximumSize(QSize(36,36));
    //btnCamera->setStyleSheet("QPushButton{border: none;outline: none;}");
    //btnCamera->setDisabled(true);
    btnCamera->setToolTip(tr("Save screenshot"));
    btnCamera->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnCamera, SIGNAL(clicked()), this, SLOT(getScreenshot()));

    //QPushButton *btnXYZ2 = new QPushButton;
    btnXYZ2 = new QPushButton;
    btnXYZ2->setIcon(QIcon(":/Resources/xyz.png"));
    btnXYZ2->setIconSize(QSize(28,28));
    btnXYZ2->setMinimumSize(QSize(36,36));
    btnXYZ2->setMaximumSize(QSize(36,36));
    btnXYZ2->setCheckable(true);
    //btnCamera->setStyleSheet("QPushButton{border: none;outline: none;}");
    //btnCamera->setDisabled(true);
    btnXYZ2->setToolTip(tr("Show/hide axis"));
    //  if(showAxes)
    btnXYZ2->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    //  else
    //      btnXYZ->setStyleSheet(Globals::instance()->getCssAreaNavButtonsSelected());
    connect(btnXYZ2, SIGNAL(clicked()), this, SLOT(switchShowAxes()));

    //    QPushButton *btnMesh = new QPushButton;
    btnMesh2 = new QPushButton;
    btnMesh2->setIcon(QIcon(":/Resources/Path-Stock-Mesh.png"));
    btnMesh2->setIconSize(QSize(32,32));
    btnMesh2->setMinimumSize(QSize(36,36));
    btnMesh2->setMaximumSize(QSize(36,36));
    btnMesh2->setCheckable(true);
    //btnCamera->setStyleSheet("QPushButton{border: none;outline: none;}");
    //btnCamera->setDisabled(true);
    btnMesh2->setToolTip(tr("Show/hide wireframe"));
    btnMesh2->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnMesh2, SIGNAL(clicked()), this, SLOT(switchShowWireframe()));



    QPushButton *btnZoomDecrement = new QPushButton;
    btnZoomDecrement->setIcon(QIcon(":/Resources/View_zoom_in2.png"));
    btnZoomDecrement->setIconSize(QSize(28,28));
    btnZoomDecrement->setMinimumSize(QSize(36,36));
    btnZoomDecrement->setMaximumSize(QSize(36,36));
    //btnZoomDecrement->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnZoomDecrement->setToolTip(tr("Decrement the zoom view"));
    btnZoomDecrement->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnZoomDecrement, SIGNAL(clicked()), this, SLOT(zoomDecrement()));

    QPushButton *btnZoomIncrement = new QPushButton;
    btnZoomIncrement->setIcon(QIcon(":/Resources/View_zoom_out2.png"));
    btnZoomIncrement->setIconSize(QSize(28,28));
    btnZoomIncrement->setMinimumSize(QSize(36,36));
    btnZoomIncrement->setMaximumSize(QSize(36,36));
    //btnZoomIncrement->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnZoomIncrement->setToolTip(tr("Increment the zoom view"));
    btnZoomIncrement->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnZoomIncrement, SIGNAL(clicked()), this, SLOT(zoomIncrement()));

    QPushButton *btnZoomToBox = new QPushButton;
    btnZoomToBox->setIcon(QIcon(":/Resources/View_isometric.png"));
    btnZoomToBox->setIconSize(QSize(26,26));
    btnZoomToBox->setMinimumSize(QSize(36,36));
    btnZoomToBox->setMaximumSize(QSize(36,36));
    btnZoomToBox->setToolTip(tr("Set isometric view"));
    //btnZoomToBox->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnZoomToBox->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    //btnZoomToBox->setDisabled(true);
    connect(btnZoomToBox, SIGNAL(clicked()), this, SLOT(zoomToBox()));

    /* QPushButton *btnPan = new QPushButton;
    btnPan->setIcon(QIcon(":/Resources/fitAll.png"));
    btnPan->setIconSize(QSize(36,36));
    btnPan->setMinimumSize(QSize(36,36));
    btnPan->setMaximumSize(QSize(36,36));
    //btnPan->setStyleSheet("QPushButton{border: none;outline: none;}");
    btnPan->setDisabled(true);
    btnPan->setStatusTip(tr("Make zoom to box"));
    btnPan->setStyleSheet(Globals::instance()->getCssAreaNavButtons());
    connect(btnPan, SIGNAL(clicked()), this, SLOT(Pan()));*/

    QFrame* line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    QSpacerItem * my_spacer = new QSpacerItem(0,2000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->addWidget(btnReset);
    layout->addWidget(btnPan);
    //layout->addWidget(line);
    layout->addWidget(btnZoomIncrement);
    layout->addWidget(btnZoomDecrement);
    layout->addWidget(btnZoomToBox);
    //layout->addWidget(line);
    layout->addWidget(btnXAxesPositive);
    layout->addWidget(btnXAxesNegative);
    layout->addWidget(btnYAxesPositive);
    layout->addWidget(btnYAxesNegative);
    layout->addWidget(btnZAxesPositive);
    layout->addWidget(btnZAxesNegative);
    //layout->addWidget(btnCamera);
    layout->addSpacerItem(my_spacer);
    layout->addWidget(btnMesh2);
    layout->addWidget(btnXYZ2);
    layout->addWidget(btnRange);
    layout->addWidget(btnCamera);


    QWidget* widget = new QWidget();
    widget->setObjectName("area_control_vtk_viewer");
    widget->setLayout(layout);

    return widget;

}

void VTKViewer::cameraToPositiveX(){
    m_renderer->GetActiveCamera()->SetPosition  (0.0,0.0,0.0);
    m_renderer->GetActiveCamera()->SetFocalPoint(1,0,0);
    m_renderer->GetActiveCamera()->SetViewUp(0,0,1);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::cameraToNegativeX(){
    m_renderer->GetActiveCamera()->SetPosition(0.0,0.0,0.0);
    m_renderer->GetActiveCamera()->SetFocalPoint(-1,0,0);
    m_renderer->GetActiveCamera()->SetViewUp(0,0,1);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::cameraToPositiveY(){
    m_renderer->GetActiveCamera()->SetPosition  (0.0,0.0,0.0);
    m_renderer->GetActiveCamera()->SetFocalPoint(0,1,0);
    m_renderer->GetActiveCamera()->SetViewUp(0,0,1);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::cameraToNegativeY(){
    m_renderer->GetActiveCamera()->SetPosition  (0.0,0.0,0.0);
    m_renderer->GetActiveCamera()->SetFocalPoint(0,-1,0);
    m_renderer->GetActiveCamera()->SetViewUp(0,0,1);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::cameraToPositiveZ(){    
    m_renderer->GetActiveCamera()->SetPosition  (0,0,0);
    m_renderer->GetActiveCamera()->SetFocalPoint(0,0,1);
    m_renderer->GetActiveCamera()->SetViewUp(0,1,0);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::cameraToNegativeZ(){
    m_renderer->GetActiveCamera()->SetPosition  (0,0,0);
    m_renderer->GetActiveCamera()->SetFocalPoint(0,0,-1);
    m_renderer->GetActiveCamera()->SetViewUp(0,1,0);
    m_renderer->ResetCamera();
    //camera->Zoom(1.3);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::resetCamera(){
    m_renderer->ResetCamera(bounds());
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::setShowAxes(bool status){
    showAxes = status;
}

void VTKViewer::setShowModel(bool status){
    showModel = status;
}

void VTKViewer::setShowSludge(bool status){
    showSludge = status;
}

void VTKViewer::setOpacityModel(float value){
    opacityModel = value;
}

float VTKViewer::getOpacityModel(){
    return opacityModel;
}

void VTKViewer::zoomDecrement(){
    vtkCamera* camera = m_renderer->GetActiveCamera();
    assert(camera != NULL);
    camera->Zoom(0.75);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::zoomIncrement(){
    vtkCamera* camera = m_renderer->GetActiveCamera();
    assert(camera != NULL);
    camera->Zoom(1.33333333333);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::getScreenshot(){
    screenshot();
}

void VTKViewer::switchShowAxes(){
    showAxes = !showAxes;

    if (showAxes) {
        btnXYZ2->setChecked(true);
        m_renderer->AddActor(axes);
    } else {
        btnXYZ2->setChecked(false);
        m_renderer->RemoveActor(axes);
    }
    update();
}

void VTKViewer::switchShowWireframe(){
    showWireframe = !showWireframe;
    update();
}


void VTKViewer::pan(){
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = this->GetInteractor();
    vtkSmartPointer<vtkInteractorStyle> InteractorStyle = vtkInteractorStyle::SafeDownCast(renderWindowInteractor->GetInteractorStyle());

    //vtkSmartPointer<vtkParallelCoordinatesInteractorStyle> ParallelInteractorStyle = vtkParallelCoordinatesInteractorStyle::SafeDownCast(renderWindowInteractor->GetInteractorStyle());
    //vtkSmartPointer<vtkCallbackCommand> callback = vtkSmartPointer<vtkCallbackCommand>::New();
    //callback->Set

    int x = renderWindowInteractor->GetEventPosition()[0];
    int y = renderWindowInteractor->GetEventPosition()[1];

    renderWindowInteractor->SetEventPosition(x,y);
    /*
   ParallelInteractorStyle->FindPokedRenderer(x,y);
   ParallelInteractorStyle->CursorStartPosition[0] = x;
   ParallelInteractorStyle->CursorStartPosition[1] = y;
   ParallelInteractorStyle->CursorLastPosition[0] = x;
   ParallelInteractorStyle->CursorLastPosition[1] = y;
   ParallelInteractorStyle->CursorCurrentPosition[0] = x;
   ParallelInteractorStyle->CursorCurrentPosition[1] = y;

*/
    //InteractorStyle->AddObserver(vtkCommand::LeftButtonPressEvent,callback);

    InteractorStyle->StartPan();
    renderWindowInteractor->SetEventPosition(x,y);
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::zoomToBox(){
    m_renderer->GetActiveCamera()->SetPosition  (0,0,0);
    m_renderer->GetActiveCamera()->SetFocalPoint(0,0,-1);
    m_renderer->GetActiveCamera()->SetViewUp(0,1,0);

    m_renderer->GetActiveCamera()->Azimuth(-45);
    m_renderer->GetActiveCamera()->Elevation(35.26);

    m_renderer->ResetCamera(bounds());
    m_renderer->GetRenderWindow()->Render();
}

void VTKViewer::viewerAddVolume(double value){
    //value = 0.4;
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << workingBlock->GetScalarRange()[0] << " - " << workingBlock->GetScalarRange()[1] << endl;

    vtkSmartPointer<vtkDataSetTriangleFilter> trifilter = vtkSmartPointer<vtkDataSetTriangleFilter>::New();
    trifilter->SetInputData(workingBlock);

    vtkSmartPointer<vtkOpenGLProjectedTetrahedraMapper> volumeMapper = vtkSmartPointer<vtkOpenGLProjectedTetrahedraMapper>::New();
    volumeMapper->SetInputConnection(trifilter->GetOutputPort());
    volumeMapper->SetBlendModeToComposite();

    vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();
    volumeProperty->ShadeOff();
    volumeProperty->SetInterpolationType(VTK_LINEAR_INTERPOLATION);

    // Create transfer mapping scalar value to color.
    vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
    colorTransferFunction->AddRGBPoint(workingBlock->GetScalarRange()[0], 1,1,1);
    colorTransferFunction->AddRGBPoint(value, 1.0,0.0,0.0);
    colorTransferFunction->AddRGBPoint(workingBlock->GetScalarRange()[1], 0,0,0);
    volumeProperty->SetColor(colorTransferFunction);

    vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
    opacityTransferFunction->AddPoint(workingBlock->GetScalarRange()[0], 0);
    opacityTransferFunction->AddPoint(value, 1);
    opacityTransferFunction->AddPoint(workingBlock->GetScalarRange()[1], 0);

    volumeProperty->SetScalarOpacity(opacityTransferFunction);
    //volumeProperty->SetGradientOpacity(opacityTransferFunction);

    vtkSmartPointer<vtkVolume> volume = vtkSmartPointer<vtkVolume>::New();
    volume->SetMapper(volumeMapper);
    volume->SetProperty(volumeProperty);

    //m_renderer->AddVolume(volume);
    m_renderer->AddActor(volume);
}

void VTKViewer::viewerAddSurface(double value){
    vtkSmartPointer<vtkContourFilter> contourFilter = vtkSmartPointer<vtkContourFilter>::New();
    contourFilter->SetInputData(workingBlock);
    contourFilter->GenerateValues(int(value), workingBlock->GetScalarRange()[0], workingBlock->GetScalarRange()[1]); // (numContours, rangeStart, rangeEnd)
    //Set a particular contour value at contour number i. The index i ranges between 0<=i<NumberOfContours.
    //contourFilter->SetValue(0, value);

    vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    contourMapper->SetInputConnection(contourFilter->GetOutputPort());

    vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
    hueLut->SetHueRange(0.667,0.0);
    hueLut->Build();
    contourMapper->SetLookupTable(hueLut);

    vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();
    contourActor->SetMapper(contourMapper);

    m_renderer->AddActor(contourActor);
}
