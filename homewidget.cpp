#include "borderlayout.h"
#include "homewidget.h"
#include "menuwidget.h"
#include "globals.h"

#include <QComboBox>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSignalMapper>
#include <QTextStream>

HomeWidget::HomeWidget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    console = consoleWidget;

    vtkViewer->setShowAxes(false);
    vtkViewer->setShowModel(true);
    vtkViewer->setOpacityModel(1.0);
}

HomeWidget::~HomeWidget(){

}

void HomeWidget::loadMainWindow(){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " << endl;
    createAreaButtons();
    createAreaActions();
    createAreaViewers();

    centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(0);

    menu->updateMenu(Globals::instance()->getTabHome(), this);
}

/********************************************************************************
 * GUI FUNCTIONS
 ********************************************************************************/

void HomeWidget::createAreaActions(){
    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QLabel* lNameNewProject = new QLabel("New project");
    QLineEdit* iNameNewProject = new QLineEdit;
    iNameNewProject->setObjectName("iNameNewProject");
    iNameNewProject->setMinimumHeight(20);
    iNameNewProject->setStyleSheet(Globals::instance()->getCssLineEdit());

    QComboBox* cbTypeProject = new QComboBox(this);
    cbTypeProject->setObjectName("cbTypeProject");
    cbTypeProject->setMaximumHeight(30);
    //cbTypeProject->addItem("NEW");
    cbTypeProject->addItem("Circular");
    cbTypeProject->addItem("Hopper");
    cbTypeProject->addItem("Lamella");
    cbTypeProject->addItem("Rectangular");
    cbTypeProject->setStyleSheet(Globals::instance()->getCssSelect());

    QPushButton* btnCreateNewProject = new QPushButton("APPLY", this);
    btnCreateNewProject->setObjectName("btnCreateNewProject");
    //btnCreateNewProject->setMaximumHeight(30);
    btnCreateNewProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnCreateNewProject->setMinimumHeight(30);
    connect(btnCreateNewProject, SIGNAL(clicked()), this, SLOT(createNewProject()));

    // Miramos si existe el fichero de settings.txt (contiene informacion del proyecto actual)
    QLabel* lSelectProject = new QLabel("Existing project");
    QDir dir = QDir(Globals::instance()->getAppCurrentWorkSpacePath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList();

    QComboBox* cbSelectProject = new QComboBox(this);
    cbSelectProject->setObjectName("cbSelectProject");
    cbSelectProject->setMaximumHeight(20);
    cbSelectProject->setStyleSheet(Globals::instance()->getCssSelect());

    cbSelectProject->addItem(" ");

    foreach(QString file, dirList){
        if(QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + file).exists()){
            cbSelectProject->addItem(file);
        }
    }

    cbSelectProject->setCurrentText(" ");

    // Creamos el boton de
    QPushButton* btnLoadProject = new QPushButton("LOAD PROJECT", this);
    btnLoadProject->setObjectName("btnLoadProject");
    btnLoadProject->setMinimumHeight(20);
    btnLoadProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btnLoadProject, SIGNAL(clicked()), this, SLOT(selectProject()));

    QPushButton* btnRemoveProject = new QPushButton("DELETE PROJECT", this);
    btnRemoveProject->setObjectName("btnRemoveProject");
    btnRemoveProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnRemoveProject->setMinimumHeight(20);
    connect(btnRemoveProject, SIGNAL(clicked()), this, SLOT(removeProject()));

    QSpacerItem * my_spacer2 = new QSpacerItem(0,30, QSizePolicy::Expanding, QSizePolicy::Expanding);

    layoutAreaActions->addWidget(lNameNewProject);
    layoutAreaActions->addWidget(iNameNewProject);
    //layoutAreaActions->addWidget(cbTypeProject);
    layoutAreaActions->addWidget(btnCreateNewProject);
    layoutAreaActions->addSpacerItem(my_spacer2);
    layoutAreaActions->addWidget(lSelectProject);
    layoutAreaActions->addWidget(cbSelectProject);
    layoutAreaActions->addWidget(btnLoadProject);
    layoutAreaActions->addWidget(btnRemoveProject);
    layoutAreaActions->addSpacerItem(my_spacer);

    // Cambiamos el contenido de Area Actions
    QScrollArea* areaActions = new QScrollArea(this);
    areaActions->setWidgetResizable(true);
    areaActions->setObjectName("area_actions");
    areaActions->setMinimumWidth(300);
    areaActions->setBackgroundRole(QPalette::Midlight);
    areaActions->setStyleSheet(Globals::instance()->getCssAreaActions());
    areaActions->setFrameShape(QFrame::NoFrame);
    areaActions->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    areaActions->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    areaActions->setLayout(layoutAreaActions);

    QWidget* areaActionWidget = new QWidget(areaActions);
    areaActionWidget->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

}

void HomeWidget::createAreaViewers(){
    if(vtkViewer != NULL ){
        vtkViewer->reset();
        vtkViewer->update();
    }
}

void HomeWidget::createAreaButtons(){
    // SPACER
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Creamos el widget para WestArea
    QVBoxLayout *grid = new QVBoxLayout;
    grid->setSpacing(0);
    grid->setMargin(0);

    // Ponemos los datos del proyecto actual
    QVBoxLayout* layoutDataArea = new QVBoxLayout();
    layoutDataArea->setSpacing(0);
    layoutDataArea->setMargin(0);

    QLabel* workspace = new QLabel("WorkSpace");
    workspace->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* name = new QLabel("Project");
    name->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* cre_date = new QLabel("Cre. Date");
    cre_date->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* mod_date = new QLabel("Mod. Date");
    mod_date->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* edar = new QLabel("Edar Model");
    edar->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* cad = new QLabel("Cad Model");
    cad->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* cfd = new QLabel("Cfd Model");
    cfd->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* mesh = new QLabel("Mesh Model");
    mesh->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* results = new QLabel("Results Model");
    results->setStyleSheet("font-size: 14px; font-weight: bold;");
    layoutDataArea->addWidget(workspace);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getAppWorkSpace()));
    layoutDataArea->addWidget(name);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectName()));
    layoutDataArea->addWidget(cre_date);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCreationDate()));
    layoutDataArea->addWidget(mod_date);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectLastModification()));
    layoutDataArea->addWidget(edar);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectEdarModel()?"True":"False"));
    layoutDataArea->addWidget(cad);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCadModel()?"True":"False"));
    layoutDataArea->addWidget(cfd);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCfdModel()?"True":"False"));
    layoutDataArea->addWidget(mesh);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectMeshModel()?"True":"False"));
    layoutDataArea->addWidget(results);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectResultsModel()?"True":"False"));

    QGroupBox* currentProjectDataArea = new QGroupBox();
    currentProjectDataArea->setStyleSheet("border: none;");
    currentProjectDataArea->setLayout(layoutDataArea);

    //grid->addWidget(currentProjectDataArea);

    /* QPushButton* btnReset = new QPushButton("RESET", this);
    btnReset->setObjectName("btnReset");
    btnReset->setDisabled(true);
    connect(btnReset, SIGNAL(clicked()), this, SLOT(resetTab()));

    QSpacerItem * my_spacer2 = new QSpacerItem(500,500, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QPushButton* btnPrev = new QPushButton;
    btnPrev->setObjectName("btnPrev");
    btnPrev->setIcon(QIcon(":/Resources/back-24838.png"));
    btnPrev->setFixedSize(30,30);
    btnPrev->setIconSize(QSize(30,30));
    connect(btnPrev, SIGNAL(clicked()), this, SLOT(prevTab()));

    QPushButton* btnNext = new QPushButton;
    btnNext->setObjectName("btnNext");
    btnNext->setIcon(QIcon(":/Resources/right-24837.png"));
    btnNext->setFixedSize(30,30);
    btnNext->setIconSize(QSize(30,30));
    connect(btnNext, SIGNAL(clicked()), this, SLOT(nextTab()));


    // Creamos el widget para WestArea
    QHBoxLayout* navArea = new QHBoxLayout(this);
    navArea->setSpacing(0);
    navArea->setMargin(0);
    navArea->setObjectName("area_nav");
    navArea->addWidget(btnPrev);
    navArea->addWidget(btnNext);

    QWidget* navWidget = new QWidget(this);
    navWidget->setBackgroundRole(QPalette::Midlight);
    navWidget->setStyleSheet(Globals::instance()->getCssAreaNav());
    navWidget->setStyleSheet(Globals::instance()->getCssPushButton());
    navWidget->setLayout(navArea);*/


    grid->addSpacerItem(my_spacer);
    //grid->addWidget(navWidget);

    if(centralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        QLayoutItem *child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }

        delete centralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }
    centralWidget->findChild<QScrollArea*>("area_buttons")->setLayout(grid);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(10);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setStyleSheet(Globals::instance()->getCssAreaActions());

}

void HomeWidget::nextTab(){
    menu->goToEdarView();
}

void HomeWidget::prevTab(){

}

void HomeWidget::resetTab(){

}

/********************************************************************************
 * PRIVATE FUNCTIONS
 ********************************************************************************/



/********************************************************************************
 * SLOTS FUNCTIONS
 ********************************************************************************/
void HomeWidget::onNewProjectTextChanged(const QString &text) {
    if(text.length() >= 3 && !centralWidget->findChild<QPushButton*>("btnCreateNewProject")->isEnabled()){
        centralWidget->findChild<QPushButton*>("btnCreateNewProject")->setEnabled(true);
    }
    else if(text.length() < 3 && centralWidget->findChild<QPushButton*>("btnCreateNewProject")->isEnabled()){
        centralWidget->findChild<QPushButton*>("btnCreateNewProject")->setEnabled(false);
    }
}

void HomeWidget::createNewProject(){
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    // Deshabilitamos el boton de crear proyect
    centralWidget->findChild<QPushButton*>("btnCreateNewProject")->setEnabled(false);

    // Comprobamos que no exista el proyecto en el sistema
    QString newProjectName = centralWidget->findChild<QLineEdit*>("iNameNewProject")->text();

    QString str = "HydroDeca - " + Globals::instance()->getCurrentProjectName();
    //QString fileCadModel = getAppProjectPath() + "geometry_preset_1.txt";
    //qDebug() << "Info p: " << str << " " << Globals::instance()->getCurrentProjectName() <<endl;
    setWindowTitle(str);

    //qDebug() << "ject1:  " << endl;

    if(!QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName).exists()){
        //qDebug() << "new project2:  " << endl;
        // Creamos la carpeta del proyecto
        QDir().mkdir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName);
        QString filename = Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName;
        QFile file(filename);

        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setCurrentProjectName(newProjectName);
        Globals::instance()->checkProject();
        Globals::instance()->setCurrentProjectCadModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();
        //QFile::copy(Globals::instance()->getAppDefaultPath() + "\\preset\\geometry_preset_1.txt", filename);
        QFile::copy(Globals::instance()->getAppDefaultPath() + "/recommendations.ini", filename);
        qDebug() << Globals::instance()->getAppDefaultPath() << "/recommendations.ini - " << Globals::instance()->getCurrentProjectName() <<endl;
        //Globals::instance()->copyFolder(Globals::instance()->getAppDefaultPath()+"\\openfoam\\case_1", Globals::instance()->getAppProjectPath());

        QString str = "HydroDeca - " + Globals::instance()->getCurrentProjectName();
        //QString fileCadModel = getAppProjectPath() + "geometry_preset_1.txt";
        //qDebug() << "Info p: " << str << " " << Globals::instance()->getCurrentProjectName() <<endl;
        setWindowTitle(str);

        // Cambiamos a la pestaña de
        //qDebug() << "Antes cambio EDAR: " << Globals::instance()->getAppDefaultPath()+"\\openfoam\\case_1" <<endl;
        menu->goToEdarView();
        //qDebug() << "Despues cambio EDAR" <<endl;
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newProjectName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }
}
/*
void HomeWidget::selectProject(const QString& text) {

    //qDebug() << "Punto 4: " << text << endl;

    // Cambiamos los datos de settings.txt
    Globals::instance()->setCurrentProjectName(text);
    Globals::instance()->checkProject();
    Globals::instance()->setCurrentProjectCadModel(true);
    Globals::instance()->writeSettingsFile();

    // Cambiamos a la pantalla de CAD para continuar con la geometria
    menu->goToEdarView();
}*/

void HomeWidget::selectProject() {

    //qDebug() << "select projectt " << endl;
    QComboBox* select = centralWidget->findChild<QComboBox*>("cbSelectProject");

    // Cambiamos los datos de settings.txt
    Globals::instance()->setCurrentProjectName(select->currentText());
    Globals::instance()->checkProject();
    Globals::instance()->setCurrentProjectCadModel(true);
    Globals::instance()->writeSettingsFile();

    QString str = "HydroDeca";
    //QString fileCadModel = getAppProjectPath() + "geometry_preset_1.txt";
    //qDebug() << "Info p: " << str << " " << Globals::instance()->getCurrentProjectName() <<endl;
    setWindowTitle(str);

    // Cambiamos a la pantalla de CAD para continuar con la geometria
    menu->goToEdarView();
}


void HomeWidget::showLoadProjectDialog() {
    QString dir = QFileDialog::getExistingDirectory(parent,
                                                    "Select Project",
                                                    Globals::instance()->getAppCurrentWorkSpacePath(),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    //qDebug() << "Punto 1: " << dir << endl;

    if(Globals::instance()->getCurrentProjectName() != dir){
        //qDebug() << "Dentroooo: " << dir << endl;

        Globals::instance()->setCurrentProjectName(dir);
        Globals::instance()->checkProject();
        menu->goToEdarView();
    }
    else{
        QMessageBox::about(this, tr("ERROR LOAD PROJECT"),
                           tr("<p>El proyecto seleccionado ya esta en uso</p>"));
    }
}

void HomeWidget::showChangeWorkspaceDialog() {
    QString dir = QFileDialog::getExistingDirectory(parent,
                                                    "Select Workspace",
                                                    Globals::instance()->getAppWorkSpacePath(),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    dir = dir.replace(Globals::instance()->getAppWorkSpacePath().replace("\\", "/"), "");

    if(Globals::instance()->getAppWorkSpace() != dir){
        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setAppWorkSpace(dir);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->readSettingsData();
        menu->goToHomeView();
    }
    else{
        QMessageBox::about(this, tr("ERROR CHANGE WORKSPACE"),
                           tr("<p>El espacio de trabajo seleccionado ya esta en uso</p>"));
    }
}

void HomeWidget::showNewProjectDialog() {
    QLineEdit* iNameDialogNewProject = new QLineEdit;
    iNameDialogNewProject->setObjectName("iNameDialogNewProject");
    QObject::connect(iNameDialogNewProject, SIGNAL(textChanged(QString)), this, SLOT(onNewProjectDialogTextChanged(QString)));

    QPushButton* btnCreateDialogNewProject = new QPushButton("NEW PROJECT", this);
    btnCreateDialogNewProject->setObjectName("btnCreateDialogNewProject");
    btnCreateDialogNewProject->setDisabled(true);
    QObject::connect(btnCreateDialogNewProject, SIGNAL(clicked()), this, SLOT(createNewProjectDialog()));

    QFormLayout* layoutNewArea = new QFormLayout();
    layoutNewArea->addRow("NAME", iNameDialogNewProject);
    layoutNewArea->addWidget(btnCreateDialogNewProject);

    QScrollArea* createNewProjectArea = new QScrollArea(this);
    createNewProjectArea->setFrameShape(QFrame::NoFrame);
    createNewProjectArea->setLayout(layoutNewArea);

    QGridLayout *grid = new QGridLayout;
    grid->addWidget(createNewProjectArea, 1, 0);

    QDialog* dialog = new QDialog(parent);
    dialog->setObjectName("dialogNewProject");
    dialog->setLayout(grid);
    dialog->exec();
}

void HomeWidget::createNewProjectDialog(){
    // Comprobamos que no exista el proyecto en el sistema
    QString newProjectName = parent->findChild<QLineEdit*>("iNameDialogNewProject")->text();

    if(!QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName).exists()){
        // Creamos la carpeta del proyecto
        QDir().mkdir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName);

        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setCurrentProjectName(newProjectName);
        Globals::instance()->checkProject();
        Globals::instance()->setCurrentProjectCadModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();

        // Escondemos el dialog de nuevo proyecto
        QDialog* dialogNewProject = parent->findChild<QDialog*>("dialogNewProject");
        dialogNewProject->hide();

        // Cambiamos a la pestaña de cad
        menu->goToEdarView();
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newProjectName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }
}

void HomeWidget::onNewProjectDialogTextChanged(const QString &text) {
    if(text.length() >= 3 && !parent->findChild<QPushButton*>("btnCreateDialogNewProject")->isEnabled()){
        parent->findChild<QPushButton*>("btnCreateDialogNewProject")->setEnabled(true);
    }
    else if(text.length() < 3 && parent->findChild<QPushButton*>("btnCreateDialogNewProject")->isEnabled()){
        parent->findChild<QPushButton*>("btnCreateDialogNewProject")->setEnabled(false);
    }
}

void HomeWidget::showNewWorkSpaceDialog() {
    QLineEdit* iNameDialogNewWorkSpace = new QLineEdit;
    iNameDialogNewWorkSpace->setObjectName("iNameDialogNewWorkSpace");
    QObject::connect(iNameDialogNewWorkSpace, SIGNAL(textChanged(QString)), this, SLOT(onNewWorkSpaceDialogTextChanged(QString)));

    QPushButton* btnCreateDialogNewWorkSpace = new QPushButton("NEW WORKSPACE", this);
    btnCreateDialogNewWorkSpace->setObjectName("btnCreateDialogNewWorkSpace");
    btnCreateDialogNewWorkSpace->setDisabled(true);
    QObject::connect(btnCreateDialogNewWorkSpace, SIGNAL(clicked()), this, SLOT(createNewWorkSpaceDialog()));

    QFormLayout* layoutNewArea = new QFormLayout();
    layoutNewArea->addRow("NAME", iNameDialogNewWorkSpace);
    layoutNewArea->addWidget(btnCreateDialogNewWorkSpace);

    QScrollArea* createNewWorkSpaceArea = new QScrollArea(this);
    createNewWorkSpaceArea->setFrameShape(QFrame::NoFrame);
    createNewWorkSpaceArea->setLayout(layoutNewArea);

    QGridLayout *grid = new QGridLayout;
    grid->addWidget(createNewWorkSpaceArea, 1, 0);

    QDialog* dialog = new QDialog(parent);
    dialog->setObjectName("dialogNewWorkSpace");
    dialog->setLayout(grid);
    dialog->exec();
}

void HomeWidget::createNewWorkSpaceDialog(){
    // Deshabilitamos el boton de crear proyecto
    parent->findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->setEnabled(false);

    // Comprobamos que no exista el proyecto en el sistema
    QString newWorkSpaceName = parent->findChild<QLineEdit*>("iNameDialogNewWorkSpace")->text();

    if(!QDir(Globals::instance()->getAppWorkSpacePath() + "\\" + newWorkSpaceName).exists()){
        // Creamos la carpeta del proyecto
        QDir().mkdir(Globals::instance()->getAppWorkSpacePath() + "\\" + newWorkSpaceName);

        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setAppWorkSpace(newWorkSpaceName);
        Globals::instance()->setCurrentProjectName("");
        Globals::instance()->writeSettingsFile();
        Globals::instance()->readSettingsData();

        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Message</h2>"
                                  "<p>. The workspace " + newWorkSpaceName + " has been create Successful. </p>" +
                                  "<p>Please, create a new project to start.</p>");
        msgBox.exec();

        // Escondemos el dialog de nuevo proyecto
        QDialog* dialogNewWorkSpace = parent->findChild<QDialog*>("dialogNewWorkSpace");
        dialogNewWorkSpace->hide();

        delete parent->findChild<QDialog*>("dialogNewWorkSpace");

        menu->goToHomeView();
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newWorkSpaceName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }
}

void HomeWidget::onNewWorkSpaceDialogTextChanged(const QString &text) {
    if(text.length() >= 3 && !parent->findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->isEnabled()){
        parent->findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->setEnabled(true);
    }
    else if(text.length() < 3 && parent->findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->isEnabled()){
        parent->findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->setEnabled(false);
    }
}
