#ifndef STRINGMETHODS_H
#define STRINGMETHODS_H

#include <string>
#include <vector>
#include <sstream>
#include <string.h>

using namespace std;

class StringMethods
{
public:
    static std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems);
    static std::vector<std::string>  split(const std::string &s, char delim);
    static std::string change_char(const std::string &s, char origin, char destination);

    //static std::vector<std::string>& split(const std::string &s, const std::string& delims, std::vector<std::string> &elems);
    //static std::vector<std::string> split(const std::string &s, const std::string& delims);
};

#endif // STRINGMETHODS_H
