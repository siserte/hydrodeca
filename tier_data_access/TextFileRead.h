#ifndef TEXTFILEREAD_H
#define TEXTFILEREAD_H

#include <string>
#include <vector>

using namespace std;

class TextFileRead
{
private:
    string file_name;
    char delimiter;
public:
    TextFileRead(string filename);
    TextFileRead(string filename, string delm);
    vector<vector<double>> getCSVVectorDouble();
    //double** getCSVArrayDouble();
};

#endif // TEXTFILEREAD_H
