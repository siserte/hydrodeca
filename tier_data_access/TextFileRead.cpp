#include "TextFileRead.h"
#include "StringMethods.h"

#include <fstream>
#include <QDebug>

using namespace std;

TextFileRead::TextFileRead(string filename){
    file_name = filename;
    delimiter = ' ';
}
TextFileRead::TextFileRead(string filename, string delm){
    file_name = filename;
    if (delm.size()<0){
        const char *deli = delm.c_str();
        delimiter = deli[0];
    }
    else delimiter = ';';
}
vector<vector<double>> TextFileRead::getCSVVectorDouble(){
    vector<vector<double>> data_array;
    string line;

    ifstream file(file_name);

    while(std::getline(file,line)){
        vector<double> v_line;
        string n_line = StringMethods::change_char(line, ',', '.');
        vector<string> s_line = StringMethods::split(n_line,delimiter);
        for(int i = 0; i < s_line.size(); i++){
            v_line.push_back( stod(s_line[i]) );
            /*qDebug( ("name_file:" + file_name + " [" + to_string(i) +"]:" + s_line[i]
                     + " double:" +  to_string( stod(s_line[i]) )
                    + " vector[" + to_string( data_array.size() ) + "]:" + to_string( v_line[v_line.size()-1]) ).c_str() );*/
        }
        data_array.push_back(v_line);
    }

    return data_array;
}


/*double** TextFileRead::getCSVArrayDouble(){
    vector<vector<double>> vec_double = getCSVVectorDouble();
    double data_array[vec_double.size()][vec_double[0].size()];

    for(int i = 0; i < vec_double.size() ; i++){
        for( int j = 0; j < vec_double[0].size(); j++)
            data_array[i][j] = vec_double[i][j];

    return data_array;
}*/
