#include "StringMethods.h"

#include <istream>
#include <regex>


std::vector<std::string>& StringMethods::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> StringMethods::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

std::string StringMethods::change_char(const std::string &s, char origin, char destination) {
    std::string result = s;
    for(int i = 0; i < s.size(); i++){
        if ( result[i] == origin )
            result[i] = destination;
    }
    return result;
}

/*std::vector<std::string>& StringMethods::split(const std::string &s, const std::string& delims, std::vector<std::string> &elems) {
    char* tok;
    char cchars [s.size()+1];
    char* cstr = &cchars[0];
    strcpy(cstr, s.c_str());
    tok = strtok(cstr, delims.c_str());
    while (tok != NULL) {
        elems.push_back(tok);
        tok = strtok(NULL, delims.c_str());
    }
    return elems;
}

std::vector<std::string> StringMethods::split(const std::string &s, const std::string& delims) {
    std::vector<std::string> elems;
    return split(s, delims, elems);
}*/
