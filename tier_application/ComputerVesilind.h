#ifndef COMPUTERVESILIND_H
#define COMPUTERVESILIND_H

#include <vector>

using namespace std;

template< typename T > inline size_t Countof( const T (&) ) { return 0; }
template< typename T, size_t S > inline size_t Countof( const T (&)[S] ) { return S; }


class ComputerVesilind
{
public:
    ComputerVesilind();
    static int ajuste_vesilind(vector<double> SST_exp, vector<double> sedimentation_velocity_exp, double *v0, double *rh);
    static double ajuste_lineal(vector<double> x, vector<double> y);// -> sedimentation_velociti
    static int vesilind ( vector<vector<double>> velocidad_sedimentacion_x, vector<vector<double>> velocidad_sedimentacion_y, vector<double> solidos_suspendidos_totales, vector<double> *Vs_exp, double *v0, double *rh);
    static int regresion_lineal(vector<double> x, vector<double> y, double *a, double *b);
    static int regresion_lineal(vector<vector<double>> x_y, double *a, double *b);
    static int regresion_lineal(vector<double> x, vector<double> y, double *a, double *b, int lower_limit, int top_limit);
    static int regresion_lineal(vector<vector<double>> x_y, double *a, double *b, int lower_limit, int top_limit);
};

#endif // COMPUTERVESILIND_H
