#ifndef MOUSEINTERACTORSTYLE3_H
#define MOUSEINTERACTORSTYLE3_H

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkCoordinate.h>
#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <qDebug>

// Define interaction style
class MouseInteractorStyle3 : public vtkInteractorStyleTrackballCamera
{
  public:
    static MouseInteractorStyle3* New();

    virtual void OnLeftButtonDown()
    {
      std::cout << "Pressed left mouse button." << std::endl;
      int x = this->Interactor->GetEventPosition()[0];
      int y = this->Interactor->GetEventPosition()[1];
      std::cout << "(x,y) = (" << x << "," << y << ")" << std::endl;
      vtkSmartPointer<vtkCoordinate> coordinate =
        vtkSmartPointer<vtkCoordinate>::New();
      coordinate->SetCoordinateSystemToDisplay();
      coordinate->SetValue(x,y,0);

      // This doesn't produce the right value if the sphere is zoomed in???
      double* world = coordinate->GetComputedWorldValue(this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
      qDebug() << "World coordinate: " << world[0] << ", " << world[1] << ", " << world[2] << endl;

      // Forward events
      vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
    }

};

#endif // MOUSEINTERACTORSTYLE3_H
