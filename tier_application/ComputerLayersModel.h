#ifndef COMPUTERLAYERSMODEL_H
#define COMPUTERLAYERSMODEL_H

#include <QThread>

class ComputerLayersModel : public QThread
{
    Q_OBJECT
public:
    //explicit ComputerLayersModel(QObject *parent = 0);
    ComputerLayersModel(double altura, double cabal_de_salida, double R_externa, double diametro_decantador, double Xentrada, double v0, double rh, double t_hours);
    double* rk4vec_test();

protected:
    double altura;
    const static int n_capas;
    double cabal_de_salida; // entrada
    double R_externa;
    double cabal_de_recirculacion;
    double diametro_decantador; //entrada
    double Xentrada; // entrada
    double v0; // entrada, calculada en ajuste_vesilind
    double rh; // entrada, calculada en ajuste_vesilind
    double t_max;


    void run() override;
    double* rk4vec_test_f(int n, double u[]);
    double* rk4vec(double t0, int m, double u0[], double dt);


signals:

public slots:
};

#endif // COMPUTERLAYERSMODEL_H
