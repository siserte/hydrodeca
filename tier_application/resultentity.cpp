#include "resultentity.h"

ResultEntity::ResultEntity(int i) {
    id = i;
}

ResultEntity::ResultEntity(int i, int t) {
    id = i;
    type = t;
}

ResultEntity::ResultEntity(int i, int t, QString n) {
    id = i;
    type = t;
    name = n;
    opacity=100;
    timestep=1;
    metric="U";
    rowModelBased=1;
    minval=0.0;
    maxval=0.0;
}

ResultEntity::ResultEntity(const ResultEntity &ent) {
    type = ent.type;
    name = ent.name;
}

ResultEntity::~ResultEntity(){
}

int ResultEntity::getId(){
    return id;
}

int ResultEntity::getType(){
    return type;
}

void ResultEntity::setType(int t){
    type = t;
}

QString ResultEntity::getName(){
    return name;
}

void ResultEntity::setName(QString n){
    name = n;
}

int ResultEntity::getOpacity(){
    return opacity;
}

void ResultEntity::setOpacity(int n){
    opacity = n;
}

QString ResultEntity::getMetric(){
    return metric;
}

void ResultEntity::setMetric(QString s){
    metric = s;
}

int ResultEntity::getTimestep(){
    return timestep;
}

void ResultEntity::setTimestep(int n){
    timestep = n;
}

int ResultEntity::getRowModelBased(){
    return rowModelBased;
}

void ResultEntity::setRowModelBased(int n){
    rowModelBased = n;
}

double ResultEntity::getMinval(){
    return minval;
}

void ResultEntity::setMinval(double n){
    minval = n;
}

double ResultEntity::getMaxval(){
    return maxval;
}

void ResultEntity::setMaxval(double n){
    maxval = n;
}

double* ResultEntity::getOrigin(){
    return origin;
}

void ResultEntity::setOrigin(double* n){
    origin[0] = n[0];
    origin[1] = n[1];
    origin[2] = n[2];
}

double* ResultEntity::getNormal(){
    return normal;
}

void ResultEntity::setNormal(double* n){
    normal[0] = n[0];
    normal[1] = n[1];
    normal[2] = n[2];
}

double* ResultEntity::getPoint1(){
    return point1;
}

void ResultEntity::setPoint1(double* n){
    point1[0] = n[0];
    point1[1] = n[1];
    point1[2] = n[2];
}

double* ResultEntity::getPoint2(){
    return point2;
}

void ResultEntity::setPoint2(double* n){
    point2[0] = n[0];
    point2[1] = n[1];
    point2[2] = n[2];
}

void ResultEntity::setChecked(bool x){
    checked=x;
}

bool ResultEntity::isChecked(){
    return checked;
}
