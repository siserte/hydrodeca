#ifndef FILEEDITOR_H
#define FILEEDITOR_H

#include <QPlainTextEdit>

class FileEditor : public QPlainTextEdit
{
    Q_OBJECT

public:
    //FileEditor();
    FileEditor(QWidget *parent = 0);
    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();

protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &, int);

private:
    QWidget *lineNumberArea;
};

#endif // FILEEDITOR_H
