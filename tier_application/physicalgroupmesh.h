#ifndef PHYSICALGROUPMESH_H
#define PHYSICALGROUPMESH_H

#include <QString>

class physicalGroupMesh
{
private:
    QString name;
    int refinement_min;
    int refinement_max;
    int layers;
    int type; //0-> surface; 1->equipment

public:
    physicalGroupMesh(QString);
    physicalGroupMesh(QString, int, int);
    QString getName();
    void setRefinementMin(int);
    void setRefinementMax(int);
    int getRefinementMin();
    int getRefinementMax();
    int getLayers();
    void setLayers(int);
    int getType();
    void setType(int);
};

#endif // PHYSICALGROUPMESH_H
