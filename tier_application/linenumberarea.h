#ifndef LINENUMBERAREA_H
#define LINENUMBERAREA_H

#include <QWidget>

class LineNumberArea : public QWidget
{
public:
    LineNumberArea(FileEditor *editor) : QWidget(editor) {
        fileEditor = editor;
    }

    QSize sizeHint() const override {
        return QSize(fileEditor->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) override {
        fileEditor->lineNumberAreaPaintEvent(event);
    }

private:
    FileEditor *fileEditor;
};

#endif LINENUMBERAREA_H
