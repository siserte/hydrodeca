#ifndef CONVERTCOLORS_H
#define CONVERTCOLORS_H

class RGB{
public:
    unsigned char R;
    unsigned char G;
    unsigned char B;

    RGB(unsigned char r, unsigned char g, unsigned char b);
    bool Equals(RGB rgb);
};

class HSV{
public:
    double H;
    double S;
    double V;

    HSV(double h, double s, double v);
    bool Equals(HSV hsv);
};

class ConvertColors
{
public:
    ConvertColors();
    static RGB HSVToRGB(HSV hsv);
};

#endif // CONVERTCOLORS_H
