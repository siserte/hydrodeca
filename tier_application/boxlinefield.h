#ifndef BOXLINEFIELD_H
#define BOXLINEFIELD_H

#include <QString>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QSettings>

class BoxLineField : public QVBoxLayout
{
    Q_OBJECT

private:
    QToolButton *button;
    QLabel *status;
    QHBoxLayout *qhbox;
    QLineEdit *field;
    QMessageBox *infoBox;
    QSettings *recommendations;

public:
    int id;
    QLabel *label;

public:
    BoxLineField(QString, QLineEdit **);
    BoxLineField(int, QString, QLineEdit **);
    double getValue();
    //void setText(double);

private:
    void init(int, QString, QLineEdit **);

public slots:
    void checkValues();
    void showInfo();
    void checkRange();
};

#endif // BOXLINEFIELD_H
