#include "ComputerLayersModel.h"
#include <QDebug>
#include <string>

const int ComputerLayersModel::n_capas = 10;
ComputerLayersModel::ComputerLayersModel(double altura, double cabal_de_salida, double R_externa, double diametro_decantador, double Xentrada, double v0, double rh, double t_hours)
{
    this->altura = altura; //entrada
    //n_capas = 10;//n_capas;	//fijo
    this->cabal_de_salida = cabal_de_salida; // entrada
    this->R_externa = R_externa;  // entrada
    this->cabal_de_recirculacion = R_externa*cabal_de_salida;
    this->diametro_decantador = diametro_decantador; //entrada
    this->Xentrada = Xentrada; // entrada
    this->v0 = v0; // entrada, calculada en ajuste_vesilind
    this->rh = rh; // entrada, calculada en ajuste_vesilind
    this->t_max = t_hours;
}

void ComputerLayersModel::run(){

}

double* ComputerLayersModel::rk4vec(double t0, int m, double u0[], double dt){
    double *f0;
    double *f1;
    double *f2;
    double *f3;
    int i;
    double t1;
    double t2;
    double t3;
    double *u;
    double *u1;
    double *u2;
    double *u3;
    //
    //  Get four sample values of the derivative.
    //
    f0 = rk4vec_test_f(m, u0);

    t1 = t0 + dt / 2.0;
    u1 = new double[m];
    for (i = 0; i < m; i++)
    {
        u1[i] = u0[i] + dt * f0[i] / 2.0;
    }
    f1 = rk4vec_test_f(m, u1);

    t2 = t0 + dt / 2.0;
    u2 = new double[m];
    for (i = 0; i < m; i++)
    {
        u2[i] = u0[i] + dt * f1[i] / 2.0;
    }
    f2 = rk4vec_test_f(m, u2);

    t3 = t0 + dt;
    u3 = new double[m];
    for (i = 0; i < m; i++)
    {
        u3[i] = u0[i] + dt * f2[i];
    }
    f3 = rk4vec_test_f(m, u3);
    //
    //  Combine them to estimate the solution.
    //
    u = new double[m];
    for (i = 0; i < m; i++)
    {
        u[i] = u0[i] + dt * (f0[i] + 2.0 * f1[i] + 2.0 * f2[i] + f3[i]) / 6.0;
    }
    //
    //  Free memory.
    //
    delete[] f0;
    delete[] f1;
    delete[] f2;
    delete[] f3;
    delete[] u1;
    delete[] u2;
    delete[] u3;

    return u;
}

double* ComputerLayersModel::rk4vec_test_f(int n, double u[])
{
    /*altura = 4; //entrada
    const int n_capas = 10;	//fijo
    double cabal_de_salida = 587.7; // entrada
    double R_externa = 0.7775;  // entrada
    double cabal_de_recirculacion = R_externa*cabal_de_salida;
    double diametro_decantador = 25; //entrada
    double Xentrada = 3; // entrada
    double v0 = 7.936; // entrada, calculada en ajuste_vesilind
    double rh = 0.581; // entrada, calculada en ajuste_vesilind*/

    double altura_capa = altura / n_capas;
    // farà falta calcular un vector amb l'altura "absoluta" de cada capa per a poder representar SST=f(altura)

    double area_decantador = 3.1415 / 4 * pow(diametro_decantador, 2);
    double Vun = cabal_de_recirculacion / area_decantador;
    double Vov = cabal_de_salida / area_decantador;
    double velocidad_sedimentacion_cada_capa[n_capas];
    //double velocidad_sedimentacion_cada_capa_corregida[n_capas];
    double Fx[n_capas];
    // he de definir els nous vectors amb un bucle for?
    int i;
    for (i = 0; i < n_capas; i = i + 1)
    {
        velocidad_sedimentacion_cada_capa[i] = v0*(exp(-rh*u[i]));
        //velocidad_sedimentacion_cada_capa_corregida[i] = max(0, velocidad_sedimentacion_cada_capa[i]);
        Fx[i] = velocidad_sedimentacion_cada_capa[i] * u[i];
    }

    double *dudt;
    dudt = new double[n];
    dudt[0] = (Vun*(u[1] - u[0]) + Fx[1]) / altura_capa;
    dudt[1] = (Vun*(u[2] - u[1]) + Fx[2] - Fx[1]) / altura_capa;
    dudt[2] = (Vun*(u[3] - u[2]) + Fx[3] - Fx[2]) / altura_capa;
    dudt[3] = ((Vun + Vov)*(Xentrada - u[3]) + Fx[4] - Fx[3]) / altura_capa;
    dudt[4] = (Vov*(u[3] - u[4]) + Fx[5] - Fx[4]) / altura_capa;
    dudt[5] = (Vov*(u[4] - u[5]) + Fx[6] - Fx[5]) / altura_capa;
    dudt[6] = (Vov*(u[5] - u[6]) + Fx[7] - Fx[6]) / altura_capa;
    dudt[7] = (Vov*(u[6] - u[7]) + Fx[8] - Fx[7]) / altura_capa;
    dudt[8] = (Vov*(u[7] - u[8]) + Fx[9] - Fx[8]) / altura_capa;
    dudt[9] = (Vov*(u[8] - u[9]) - Fx[9]) / altura_capa;

    return dudt;
}

double* ComputerLayersModel::rk4vec_test()
{
    double dt = 0.01;
    int i;
    double t0;
    double t1;
    //double t_max = 10; //entrada
    //int n_capas = 10;
    double *u0; //sortida (SST)
    double *u1;

    t0 = 0.0;

    u0 = new double[n_capas];


    for (i = 0; i < n_capas; i++)
    {
        u0[i] = 0.0;
    }

    //qDebug() << "n capas: " << n_capas << "t_max: " << t_max <<endl;

    int j=0;
    for (;;)
    {
        j++;
        //
        //  Stop if we've exceeded TMAX.
        //
        if (t_max <= t0)
        {
            break;
        }
        //
        //  Otherwise, advance to time T1, and have RK4 estimate
        //  the solution U1 there.
        //
        t1 = t0 + dt;
        u1 = rk4vec(t0, n_capas, u0, dt);
        //
        //  Shift the data to prepare for another step.
        //
        t0 = t1;
        for (i = 0; i < n_capas; i++)
        {
            u0[i] = u1[i];
        }
        delete[] u1;
    }
    //for(i=0;i<n_capas;i++)
    //    qDebug( ("test: " + std::to_string(u0[i]) ).c_str() );

    //qDebug() << "nTotal: " << j << endl;
    return u0;
}
