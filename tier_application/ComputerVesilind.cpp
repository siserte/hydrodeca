#include "ComputerVesilind.h"
#include <cmath>
#include <cstdlib>
#include <numeric>

#include <QDebug>

/*Funcionamiento ajuste_lineal
    static const double xa[] = {60,90,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600};
    static const double ya[] = {186,186.3,186.5,187,187.4,187.7,187.9,188.1,188.3,188.5,188.7,189,189.2,189.4,189.7,189.9,190,190.2,190.4,190.6,190.7,190.9,191.2,191.3,191.4,191.5,191.6};
    vector<double> x(begin(xa), end(xa));
    vector<double> y(begin(ya), end(ya));

    double m = linear(x,y);
 */

double ComputerVesilind::ajuste_lineal(vector<double> x, vector<double> y){
    if(x.size() != y.size()){
        throw exception("...");
    }

    double n = x.size();
    double avgX = accumulate(x.begin(), x.end(), 0.0) / n;
    double avgY = accumulate(y.begin(), y.end(), 0.0) / n;
    double numerator = 0.0;
    double denominator = 0.0;

    for(int i=0; i<n; ++i){
        numerator += (x[i] - avgX) * (y[i] - avgY);
        denominator += (x[i] - avgX) * (x[i] - avgX);
    }

    return -1* numerator/denominator;
}


//TODO Error amb la funcio log()
int ComputerVesilind::ajuste_vesilind(vector<double> SST_exp, vector<double> sedimentation_velocity_exp, double *v0, double *rh){
    int n_experimentos = int(SST_exp.size());
    //printf(" %d = %d / %d\n\n", n_experimentos, sizeof(SST_exp), sizeof(double));
    qDebug( ("SST.size:" + to_string( n_experimentos )
             + " sedimentation_velocity_exp.size:" + to_string( sedimentation_velocity_exp.size() )
             + " v0:" + to_string( *v0 )
             + " rh:" + to_string( *rh ) ).c_str() );

    //calculo
    int i;
    double a, b, c;
    double xsum = 0, x2sum = 0, ysum = 0, xysum = 0;                //variables for sums/sigma of xi,yi,xi^2,xiyi etc

    /*for (i = 0; i < n_experimentos; i++){
        //printf("%d\tSST:%g\tsve:%g\tln:%g\n", i, SST_exp[i], sedimentation_velocity_exp[i], log(sedimentation_velocity_exp[i]));
    }*/

    for ( i = 0; i < n_experimentos; i++)
    {
        double exp_log = log( sedimentation_velocity_exp[i] );
        qDebug( ("i:" + to_string( i )
                 + " sst:" + to_string( SST_exp[i] )
                 + " sedimentation_velocity_exp:" + to_string( sedimentation_velocity_exp[i] )
                 + " log(exp):" + to_string( exp_log ) ).c_str() );
        xsum = xsum + SST_exp[i];                        //calculate sigma(xi)
        ysum = ysum + exp_log;                        //calculate sigma(yi)
        x2sum = x2sum + pow(SST_exp[i], 2);                //calculate sigma(x^2i)
        xysum = xysum + SST_exp[i] * log(sedimentation_velocity_exp[i]);                    //calculate sigma(xi*yi)
    }
    qDebug( ("xsum:" + to_string( xsum )
             + " ysum:" + to_string( ysum )
             + " x2sum:" + to_string( x2sum )
             + " xysum:" + to_string( xysum ) ).c_str() );

    //printf("\n xsum:%g \tysum:%g \tx2sum:%g \txysum:%g \n", xsum, ysum, x2sum, xysum);

    a = (n_experimentos*xysum - xsum*ysum) / (n_experimentos*x2sum - xsum*xsum);            //calculate slope(or the the power of exp)
    b = (x2sum*ysum - xsum*xysum) / (x2sum*n_experimentos - xsum*xsum);            //calculate intercept
    c = pow(2.71828, b);                        //since b=ln(c)
    /*double y_fit[n_experimentos];                        //an array to store the new fitted values of y
    for (i = 0; i < n_experimentos; i++)
    y_fit[i] = c*pow(2.71828, a*SST_exp[i]);                    //to calculate y(fitted) at given x points
    */
    *v0 = c;
    *rh = a;

    qDebug( ("a:" + to_string( a )
             + " rh:" + to_string( *rh )
             + " b:" + to_string( b )
             + " c:" + to_string( c )
             + " v0:" + to_string( *v0 ) ).c_str() );

    /*printf("##\tfuncion ajuste vesilind\t##\n");
    printf("Parametros Vesilind:\t a=%g,\tb= %g \t c= %g\t v0= %g\t rh= %g\n", a, b, c, *v0, *rh);

    //Muestra en una tabla los valores introducidos
    /*cout << "S.no" << setw(5) << "x" << setw(19) << "y(observed)" << setw(19) << "y(fitted)" << endl;
    cout << "-----------------------------------------------------------------\n";
    for (i = 0; i<n; i++)
    cout << i + 1 << "." << setw(8) << x[i] << setw(15) << y[i] << setw(18) << y_fit[i] << endl;//print a table of x,y(obs.) and y(fit.)
    */
    //Muestra el resultado de los valores
    /*cout << "\nThe corresponding line is of the form:\n\nlny = " << a << "x + " << b << endl;
    cout << "\nThe exponential fit is given by:\ny = " << c << "e^" << a << "x\n";

*/
    return 0;
}

int ComputerVesilind::vesilind( vector<vector<double>> velocidad_sedimentacion_x, vector<vector<double>> velocidad_sedimentacion_y, vector<double> solidos_suspendidos_totales, vector<double> *Vs_exp, double *v0, double *rh){

    int n_experimentos = int(solidos_suspendidos_totales.size());
    //double *Vs = ( double * ) malloc ( n_experimentos * sizeof ( double ) );
    /*double valores_x[n_experimentos][5] = { { 0, 30, 60, 80, 100 }, { 120, 140, 160, 180, 200 }, { 120, 140, 160, 180, 210 }, { 120, 140, 160, 180, 200 }, { 120, 140, 160, 180, 200 }, { 80, 100, 120, 140, 160 } }; //Entrada
    double valores_y[n_experimentos][5] = { { 14.8, 14.5, 14.4, 14.2, 14 }, { 13.5, 13, 12.6, 12.3, 12.1 }, { 12.4, 11.7, 11.2, 10.5, 9.7 }, { 11.5, 10.7, 10.1, 9.5, 9.1 }, { 11, 9, 8, 7.3, 6.8 }, { 13, 11.5, 9, 8, 7 } }; //Entrada
    double SST_exp[n_experimentos] = { 2.972, 2.772, 2.547, 2.217, 1.663, 1.109 };*/
    //Vs_exp = ( double * ) malloc ( n_experimentos * sizeof ( double ) );
    //double v0, rh;//Sortida

    for (int experimento = 0; experimento < n_experimentos; experimento = experimento + 1)
    {
        (*Vs_exp)[experimento] = ajuste_lineal(velocidad_sedimentacion_x[experimento], velocidad_sedimentacion_y[experimento]);
    }
    /*int i;
    for ( int i = 0; i < n_experimentos; i++)
    {
        printf("Vs_exp[%d]:\t%g\n", i, Vs_exp[i]);
    }*/

    //double Vs_exp[n_experimentos] = { 0.0072, 0.0108, 0.025, 0.0333, 0.0553, 0.065, 0.111 };

    ajuste_vesilind(solidos_suspendidos_totales, (*Vs_exp), v0, rh);
    //printf("Parametros Vesilind:\t v0=%g,\trh= %g \n", v0, rh);









    return 0;
}


int ComputerVesilind::regresion_lineal(vector<double> x, vector<double> y, double *a, double *b){
    int n = int(x.size());
    return regresion_lineal(x, y, a, b, 0, n);
}

int ComputerVesilind::regresion_lineal(vector<vector<double>> x_y, double *a, double *b){
    vector<double> x, y;

    for(int i = 0; i < x_y.size(); i++){
        x.push_back( x_y[i][0] );
        y.push_back( x_y[i][1] );
    }

    return regresion_lineal(x, y, a, b);
}

int ComputerVesilind::regresion_lineal(vector<double> x, vector<double> y, double *a, double *b, int lower_limit, int top_limit){
    double sumX, sumY, sumX2, sumY2, sumXY, promX, promY;
    int n = top_limit - lower_limit;
    if( lower_limit < 0 || top_limit > x.size() || top_limit <= lower_limit )
        return 1;
    sumX = sumY = sumX2 = sumY2 = sumXY = 0;
    for(int i = lower_limit; i < top_limit ; i++){
        sumX += x[i];
        sumY += y[i];
        sumX2 += x[i]*x[i];
        sumY2 += y[i]*y[i];
        sumXY += x[i]*y[i];
    }
    promX = sumX/n;
    promY = sumY/n;
    //(*b) = ( sumX + sumY - n * sumXY) / (sumX*sumX - n * sumX2);
    (*b) = ( n * sumXY - sumX * sumY) / ( n * sumX2 - sumX * sumX );
    qDebug( ("RL -> a:" + to_string( *a ) + "b:" + to_string( *b ) ).c_str() );
    (*a) = promY - (*b)*promX;
    //(*a) = ( sumY - (*b) * sumX ) / n;
    qDebug( ("lower:" + to_string( lower_limit )
             + " top:" + to_string( top_limit )
             + " sumX:" + to_string( sumX )
             + " sumY:" + to_string( sumY )
             + " sumXY:" + to_string( sumXY )
             + " sumX2:" + to_string( sumX2 )
             + " sumY2:" + to_string( sumY2 )
             + " promX:" + to_string( promX )
             + " promY:" + to_string( promY )
             + " n:" + to_string( n )
             + " b:" + to_string( *b )
             + " a:" + to_string( *a ) ).c_str() );
    return 0;
}

int ComputerVesilind::regresion_lineal(vector<vector<double>> x_y, double *a, double *b, int lower_limit, int top_limit){
    vector<double> x, y;

    for(int i = 0; i < x_y.size(); i++){
        x.push_back( x_y[i][0] );
        y.push_back( x_y[i][1] );
    }

    return regresion_lineal(x, y, a, b, lower_limit, top_limit);
}

ComputerVesilind::ComputerVesilind()
{

}
