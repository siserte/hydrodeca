#include "physicalgroupmesh.h"

physicalGroupMesh::physicalGroupMesh(QString s) {
    name = s;
    type=0;
}

physicalGroupMesh::physicalGroupMesh(QString s, int x, int y) {
    name = s;
    refinement_min = x;
    refinement_max = y;
    type=0;
}

QString physicalGroupMesh::getName(){
    return name;
}

void physicalGroupMesh::setRefinementMin(int x){
    refinement_min = x;
}

void physicalGroupMesh::setRefinementMax(int x){
    refinement_max = x;
}

int physicalGroupMesh::getRefinementMin(){
    return refinement_min;
}

int physicalGroupMesh::getRefinementMax(){
   return refinement_max;
}

void physicalGroupMesh::setLayers(int x){
    layers = x;
}

int physicalGroupMesh::getLayers(){
    return layers;
}

void physicalGroupMesh::setType(int x){
    type = x;
}

int physicalGroupMesh::getType(){
    return type;
}
