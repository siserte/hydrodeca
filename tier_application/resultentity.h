#ifndef RESULTENTITY_H
#define RESULTENTITY_H

#include <QMetaType>
#include <QString>
#include <vtkProbeFilter.h>
#include <vtkSmartPointer.h>

class ResultEntity {
public:
    ResultEntity(int);
    ResultEntity(int, int);
    ResultEntity(int, int, QString);
    ResultEntity(const ResultEntity &);
    ~ResultEntity();
    int getId();
    int getType();
    void setType(int);
    void setName(QString);
    QString getName();
    void setOpacity(int);
    int getOpacity();
    void setMetric(QString);
    QString getMetric();
    void setTimestep(int);
    int getTimestep();
    void setRowModelBased(int);
    int getRowModelBased();
    void setMinval(double);
    double getMinval();
    void setMaxval(double);
    double getMaxval();
    void setOrigin(double*);
    double* getOrigin();
    void setNormal(double*);
    double* getNormal();
    void setPoint1(double*);
    double* getPoint1();
    void setPoint2(double*);
    double* getPoint2();
    void setChecked(bool);
    bool isChecked();
    vtkSmartPointer<vtkProbeFilter> probe;

private:
    int id;
    int type;
    QString name;
    int opacity;
    QString metric;
    bool clip;
    bool wireframe;
    int timestep;
    int rowModelBased;
    double minval;
    double maxval;
    double normal[3];
    double origin[3];
    double point1[3];
    double point2[3];
    bool checked;

};

Q_DECLARE_METATYPE(ResultEntity*);

#endif // RESULTENTITY_H

