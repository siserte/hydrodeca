#include "boxlinefield.h"
#include "qdebug.h"
#include "globals.h"

#include <QDebug>

BoxLineField::BoxLineField(QString strlabel, QLineEdit **f)
{
    init(0, strlabel, f);
}

BoxLineField::BoxLineField(int i, QString strlabel, QLineEdit **f)
{
    init(i, strlabel, f);
}

double BoxLineField::getValue(void){
    return this->field->text().toDouble();
}

//void BoxLineField::setText(double num){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << num << "---" << QString::number(num) << endl;
//    this->field->setText(QString::number(num));
//}

void BoxLineField::init(int i, QString strlabel, QLineEdit **f)
{
    recommendations = new QSettings(Globals::instance()->getAppPath() + "/settings/recommendations.ini", QSettings::IniFormat);

    id = i;
    if(id==0)
        label = new QLabel(strlabel);
    else
        label = new QLabel(QString::number(id) + ". " + strlabel);

    (*f) = new QLineEdit("");
    field = (*f);
    field->setFixedWidth(80);

    //connect(field, SIGNAL(returnPressed()), this, SLOT(checkValues()));
    //connect(field, SIGNAL(editingFinished()), this, SLOT(checkValues()));
    connect(field, SIGNAL(textChanged(QString)), this, SLOT(checkValues()));

    status = new QLabel();
    status->setFixedSize(20,20);
    status->setPixmap(QPixmap(":/Resources/ok.png").scaled(status->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));

    button = new QToolButton();
    button->setIcon(QIcon(":/Resources/info.png"));
    connect(button, SIGNAL(clicked(bool)), this, SLOT(showInfo()));

    qhbox = new QHBoxLayout();
    qhbox->addWidget(field);
    qhbox->setAlignment(Qt::AlignLeft);
    qhbox->addWidget(status);
    qhbox->addWidget(button);

    this->addWidget(label);
    this->addLayout(qhbox);
}

void BoxLineField::checkValues(){
    int mode = recommendations->value(QString::number(id) + "/check").toInt();

    switch(mode){
    case 1: //Range
        checkRange();
        break;
    case 3: //None
        status->setPixmap(QPixmap(":/Resources/ok.png").scaled(status->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    //default:
        //qDebug() << "Mode: " << QString::number(mode) << " not found.";
    }
}


void BoxLineField::checkRange(){
    double upperBound = recommendations->value(QString::number(id) + "/upper").toDouble();
    double lowerBound = recommendations->value(QString::number(id) + "/lower").toDouble();
    double value = field->text().toDouble();
    //qDebug() << value << ": "<< upperBound << " - " << lowerBound;
    if ((value >= lowerBound) && (value <= upperBound))
        status->setPixmap(QPixmap(":/Resources/ok.png").scaled(status->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    else
        status->setPixmap(QPixmap(":/Resources/caution.png").scaled(status->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void BoxLineField::showInfo(){
    infoBox = new QMessageBox();
    infoBox->setIcon(QMessageBox::Information);
    infoBox->setWindowTitle("Recommended values for parameter: " + label->text());
    infoBox->setText(recommendations->value(QString::number(id) + "/info").toString());
    infoBox->setStandardButtons(QMessageBox::Close);
    infoBox->exec();
}
