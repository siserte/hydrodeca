#define ApplicationName 'HydroSludge'
#define ApplicationVersion "1.0.12"

[Setup]
ChangesEnvironment=True
AppName={#ApplicationName}
AppVersion={#ApplicationVersion}
DefaultDirName={commonpf}\{#ApplicationName}
DefaultGroupName={#ApplicationName}
UninstallDisplayIcon={app}\{#ApplicationName}.exe
SetupIconFile= HydroDeca.ico
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename={#ApplicationName}-v{#ApplicationVersion}

[Icons]
Name: "{userdesktop}\{#ApplicationName}"; Filename: "{app}\HydroDeca.exe"; IconFilename: "{app}\HydroDeca.ico"
Name: "{group}\{#ApplicationName}"; Filename: "{app}\HydroDeca.exe"; IconFilename: "{app}\HydroDeca.ico"
Name: "{commondesktop}\{#ApplicationName}"; Filename: "{app}\HydroDeca.exe"; IconFilename: "{app}\HydroDeca.ico"
Name: "{commonprograms}\{#ApplicationName}"; Filename: "{app}\HydroDeca.exe"; IconFilename: "{app}\HydroDeca.ico"
Name: "{commonstartup}\{#ApplicationName}"; Filename: "{app}\HydroDeca.exe"; IconFilename: "{app}\HydroDeca.ico"

[Files]
; Interface                                                          
Source: "F:\build-hydrodeca-Desktop_Qt_5_5_0_MSVC2012_64_bit-Release\release\Hydrodeca.exe"; DestDir: "{app}"

; OpenCASCADE
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\FWOSPlugin.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\PTKernel.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBin.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBinL.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBinTObj.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBinXCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBO.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBool.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKCDF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKDCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKBRep.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKDraw.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKernel.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKFeat.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKFillet.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKG2d.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKG3d.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKGeomAlgo.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKGeomBase.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKHLR.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKIGES.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKIVtk.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKIVtkDraw.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKLCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKMath.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKMesh.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKMeshVS.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKNIS.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKOffset.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKOpenGl.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKPCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKPLCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKPrim.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKPShape.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKQADraw.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKService.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKShapeSchema.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKShHealing.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKStdLSchema.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKStdSchema.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKSTEP.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKSTEP209.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKSTEPAttr.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKSTEPBase.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKSTL.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKTObj.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKTObjDRAW.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKTopAlgo.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKTopTest.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKV3d.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKViewerTest.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKVoxel.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKVRML.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXCAFSchema.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXDEDRAW.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXDEIGES.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXDESTEP.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXMesh.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXml.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXmlL.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXmlTObj.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXmlXCAF.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXSBase.dll";  DestDir: "{app}"
Source: "C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\bin\TKXSDRAW.dll";  DestDir: "{app}"

; External OpenCASCADE
Source: "C:\OpenCASCADE6.9.1-vc12-64\tbb42_20140416oss\bin\intel64\vc12\tbb.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\OpenCASCADE6.9.1-vc12-64\tbb42_20140416oss\bin\intel64\vc12\tbbmalloc.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\OpenCASCADE6.9.1-vc12-64\freeimage-3.17.0-vc12-64\bin\FreeImage.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\OpenCASCADE6.9.1-vc12-64\freeimage-3.17.0-vc12-64\bin\FreeImagePlus.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\OpenCASCADE6.9.1-vc12-64\freetype-2.5.5-vc12-64\bin\freetype.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\OpenCASCADE6.9.1-vc12-64\gl2ps-1.3.8-vc12-64\bin\gl2ps.dll"; DestDir: "{app}"; Flags: ignoreversion

; VTK
Source: "C:\Program Files (x86)\VTK\bin\vtkalglib-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkChartsCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonColor-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonComputationalGeometry-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonDataModel-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonExecutionModel-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonMath-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonMisc-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonSystem-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkCommonTransforms-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkDICOMParser-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkDomainsChemistry-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkexoIIc-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkexpat-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersAMR-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersExtraction-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersFlowPaths-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersGeneral-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersGeneric-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersGeometry-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersHybrid-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersHyperTree-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersImaging-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersModeling-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersParallel-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersParallelImaging-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersProgrammable-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersSelection-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersSMP-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersSources-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersStatistics-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersTexture-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkFiltersVerdict-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkfreetype-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkftgl-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkGeovisCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkgl2ps-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkGUISupportQt-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkGUISupportQtOpenGL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkGUISupportQtSQL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkhdf5_hl-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkhdf5-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingColor-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingFourier-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingGeneral-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingHybrid-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingMath-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingMorphological-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingSources-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingStatistics-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkImagingStencil-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkInfovisCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkInfovisLayout-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkInteractionImage-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkInteractionStyle-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkInteractionWidgets-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOAMR-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOEnSight-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOExodus-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOExport-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOGeometry-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOImage-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOImport-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOInfovis-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOLegacy-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOLSDyna-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOMINC-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOMovie-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIONetCDF-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOParallel-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOPLY-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOSQL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOVideo-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOXML-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkIOXMLParser-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkjpeg-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkjsoncpp-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtklibxml2-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkmetaio-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkNetCDF_cxx-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkNetCDF-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkoggtheora-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkParallelCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkpng-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkproj4-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingAnnotation-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingContext2D-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingFreeType-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingFreeTypeOpenGL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingGL2PS-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingImage-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingLabel-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingLIC-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingLOD-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingOpenGL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingQt-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingVolume-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingVolumeAMR-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkRenderingVolumeOpenGL-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtksys-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtktiff-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkverdict-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkViewsContext2D-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkViewsCore-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkViewsGeovis-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkViewsInfovis-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkViewsQt-6.1.dll";  DestDir: "{app}"
Source: "C:\Program Files (x86)\VTK\bin\vtkzlib-6.1.dll";  DestDir: "{app}"
; QT
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Qml.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Quick.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Sql.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Svg.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5Xml.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\bin\Qt5OpenGL.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\qt-5.5.0-x64-msvc2012-rev0\qt-5.5.0-x64-msvc2012-rev0\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms"; Flags: ignoreversion

; Working folders
Source: "C:\Users\siser\AppData\Roaming\HydroDeca\settings\*"; DestDir: "{userappdata}\HydroDeca\settings"; Flags: ignoreversion recursesubdirs createallsubdirs

;Ico
Source: "HydroDeca.ico"; DestDir: "{app}"

;First time set up (uncomment)
  ;Source: "../install-sw/vcredist_x64.exe"; DestDir: {tmp}; Flags: deleteafterinstall
  ;Source: "../install-sw/7z1900.exe"; DestDir: "{userappdata}\HydroDeca"; Flags: deleteafterinstall
  
  ;Docker image update (uncomment)
    ;Source: "F:\ofubuntu-asm.zip"; DestDir: "{userappdata}\HydroDeca"
    ;Source: "C:\Users\siser\extract.bat"; DestDir: "{userappdata}\HydroDeca" ; Flags: deleteafterinstall
    ;Source: "C:\Users\siser\extract.ps1"; DestDir: "{userappdata}\HydroDeca" ; Flags: deleteafterinstall
    ;Source: "C:\Users\siser\clean.bat"; DestDir: "{userappdata}\HydroDeca"
    ;Source: "C:\Users\siser\clean.ps1"; DestDir: "{userappdata}\HydroDeca" 

[Run]
;First time set up (uncomment)
  ;Filename: "{userappdata}\HydroDeca\7z1900.exe"; StatusMsg: "Installing 7zip..."
  ;Filename: {tmp}\vcredist_x64.exe; Parameters: "/q /passive /Q:a /c:""msiexec /q /i vcredist.msi"""; StatusMsg: "Installing VC++ 2012 Redistributables..."
  
  ;Docker image update (uncomment)
    ;Filename: "{userappdata}\HydroDeca\extract.bat"; Parameters: "preinstall"; Flags: runhidden; StatusMsg: "Loading Docker image..."   
    ;Filename: {app}\Readme.txt; Description: View the README file; Flags: postinstall shellexec skipifsilent unchecked
    ;Filename: "{userappdata}\HydroDeca\clean.bat"; Parameters: "postinstall"; Flags: runhidden; StatusMsg: "Cleaning the installation..." 

Filename: {app}\HydroDeca.exe; Description: Run Application; Flags: postinstall nowait skipifsilent 