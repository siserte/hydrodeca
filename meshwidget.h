#ifndef MESHWIDGET_H
#define MESHWIDGET_H

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMainWindow>
#include <QProcess>
#include <QProgressDialog>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QTabWidget>
#include <QToolBox>
#include <QWidget>

#include <Geom_TrimmedCurve.hxx>
#include <gp_Ax1.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <menuwidget.h>

#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>

#include <unordered_map>

#include <vtkActor.h>
#include <vtkDataSet.h>
#include <vtkSmartPointer.h>

#include <vtkviewer.h>

#include <QSettings>
#include <QCheckBox>
#include <QList>

#include "tier_application/physicalgroupmesh.h"

class MeshWidget : public QWidget {
    Q_OBJECT

private:
    //QList<physicalGroupMesh*> *groups;
    QMap<QString, physicalGroupMesh*> *groups;

public:
    explicit MeshWidget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, QToolBox* reportWidget = 0, QListWidget* console = 0);
    ~MeshWidget();
    void loadMainWindow();
    void resetAreas();

    void getFacesNames();
    void copyFoamFiles();

public slots:
    void blockMesh();
    void checkMesh();
    void checkMeshStarted();
    void checkMeshOutput();
    void checkMeshFinished();
    void blockMeshStarted();
    void blockMeshOutput();
    void blockMeshFinished();

    void cancelBlockMesh();
    void cancelSurface();
    void cancelSnappyHexMeshStep1();
    void cancelSnappyHexMeshStep2();
    void cancelSnappyHexMeshStep3();

    void calculateBase();
    void onChangeOrigin(const QString& text);
    void onChangeLength(const QString& text);
    void onRegularCellsChange(int value);
    void onSliderStop();
    void updateSlider();
    void showBase();
    void showEquipments();
    void showSculpt();
    void showSnap();
    void showLayer();
    void surface();
    void surfaceStarted();
    void surfaceOutput();
    void surfaceFinished();
    void snappyHexMeshStep1();
    void snappyHexMeshStep1Started();
    void snappyHexMeshStep1Output();
    void snappyHexMeshStep1Finished();
    void snappyHexMeshStep2();
    void snappyHexMeshStep2Started();
    void snappyHexMeshStep2Output();
    void snappyHexMeshStep2Finished();
    void snappyHexMeshStep3();
    void snappyHexMeshStep3Started();
    void snappyHexMeshStep3Output();
    void snappyHexMeshStep3Finished();

    void stateChanged(QProcess::ProcessState);
    void pasteText();
    void pasteTextLayers();
    void changeLocationInMesh();
    void createSnappyHexMeshDictFile();

    void slotLoadEquipment();
    void removeEquipment();
    void slotSaveEquipment();

private:
    vtkActor* cadActor;
    double* centerCadActor;
    vtkDataSet* dataSetCadActor;
    double maxBound[3];
    double minBound[3];
    QProgressDialog* progressBarProcess;
    int progressBarValue;
    QProcess* processBlockMesh;
    QProcess* processSurface;
    QProcess* processSnappyStep1;
    QProcess* processSnappyStep2;
    QProcess* processSnappyStep3;
    QProcess* processCheckMesh;
    bool isAutoMeshWorking;
    bool isCancelProcess;
    int runCheck;   //0-> No, 1->OK, -1->Fail

    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;

    QPushButton* btnBase;
    QPushButton* btnEquip;
    QPushButton* btnSnappy ;
    QPushButton* btnSnappy2;
    QPushButton* btnSnappy3;
    QPushButton* btnCheckMesh;
    QSlider* line_slider;

    void createAreaButtons();
    void createAreaActions();
    void createAreaViewers();

    void loadSettingsBase();
    void saveSettingsBase();
    void loadSettingsSnap();
    void saveSettingsSnap();

    void loadSettingsEquipment();
    void loadEquipment(QString);
    void showEquipmentValidationError(QString);
    void createTopoSetDictFile();
    void resetDirectoriesExclude(QStringList);
    void createControlDictFile();

private:
    QString m_sSettingsFile;
    QSettings* settings;

    QLineEdit* line_origin_x;
    QLabel* label_origin_x;
    QLineEdit* line_origin_y;
    QLabel* label_origin_y;
    QLineEdit* line_origin_z;
    QLabel* label_origin_z;
    QLineEdit* line_length_x;
    QLabel* label_length_x;
    QLineEdit* line_length_y;
    QLabel* label_length_y;
    QLineEdit* line_length_z;
    QLabel* label_length_z;
    QCheckBox* line_regular;
    QLabel* label_regular;
    QLineEdit* line_resolution;
    QLabel* label_resolution;
    QLineEdit* line_nodes_x;
    QLabel* label_nodes_x;
    QLineEdit* line_nodes_y;
    QLabel* label_nodes_y;
    QLineEdit* line_nodes_z;
    QLabel* label_nodes_z;
    QLabel* label_nodes_resolution;

    QCheckBox* line_explicit;
    QLineEdit* line_snap_it;
    QLineEdit* line_relax_it;
    QLineEdit* line_feature_it;
    QLineEdit* line_smooth_it;
    QLineEdit* line_tolerance;

    QLineEdit* line_thickness_first;
    QLineEdit* line_expansion;

    QList<QString> *list_faces_names;
    QVBoxLayout *layout_sculpt;
    QLineEdit* line_sludge_level;
    QLineEdit* line_sludge_height;

    QLineEdit* line_location_x;
    QLineEdit* line_location_y;
    QLineEdit* line_location_z;

    vtkSmartPointer<vtkActor> vtkLocationMesh;

    QVBoxLayout *layoutAreaEquipment;
    QSettings *settingsUser;
    int cnt_equips;
    QString equips_dir;
};

#endif // MESHWIDGET_H
