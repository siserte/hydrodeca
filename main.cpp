#include "mainwindow.h"
#include "globals.h"
#include <QApplication>
#include <QtGlobal>
#include <QSplashScreen>
#include <QDebug>
#include <QtCore>

int main(int argc, char *argv[]){
    QApplication a(argc, argv);
    QFont font("Calibri", 12);
    QApplication::setFont(font);

    QSplashScreen* splash = new QSplashScreen();
    //splash->setPixmap(QPixmap(":/Resources/Logo_Hydrodeca_transp.png"));
    splash->setPixmap(QPixmap(":/Resources/Logo_Hydrosludge_transp.png").
                      scaled(1024, 1024, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    splash->show();

    MainWindow w;
    QTimer::singleShot(2500, splash, SLOT(close()));
    QTimer::singleShot(2500, &w, SLOT(showMaximized()));

    return a.exec();
}
