#include "borderlayout.h"
#include "mainwindow.h"
#include "menuwidget.h"
#include "resultswidget.h"
#include "globals.h"
#include "ui_defaultwindow.h"

#include <QCheckBox>
#include <QComboBox>
#include <qDebug>
#include <QDir>
#include <QFile>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QMessageBox>
#include <QTextBrowser>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QScrollArea>
#include <QSignalMapper>
#include <QSplitter>
#include <QString>
#include <QThread>
#include <QTimer>
#include <QVBoxLayout>
#include <unordered_map>

#include <AIS_Shape.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBndLib.hxx>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>

#include <BRepLib.hxx>

#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>

#include <BRepPrim_Cone.hxx>
#include <BRepPrimAPI_MakeOneAxis.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepOffsetAPI_ThruSections.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <gp_Ax1.hxx>
#include <gp_Ax3.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_DisplayModeFilter.hxx>
#include <IVtkTools_ShapeDataSource.hxx>

#include <MeshVS_MeshPrsBuilder.hxx>
#include <MeshVS_DrawerAttribute.hxx>

#include <OSD_Path.hxx>

#include <QVTKWidget.h>

#include <RWStl.hxx>

#include <STEPCAFControl_Writer.hxx>

#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <StlMesh_Mesh.hxx>
#include <StlMesh_MeshExplorer.hxx>
#include <StlTransfer.hxx>

#include <TDocStd_Document.hxx>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkCutter.h>
#include <vtkDataObject.h>
#include <vtkDataSetMapper.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkDoubleArray.h>
#include <vtkExecutive.h>
#include <vtkFieldData.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkInteractorStyleImage.h>
#include <vtkJPEGReader.h>
#include <vtkLookupTable.h>
#include <vtkMath.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkMultiBlockPLOT3DReader.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataReader.h>
#include <vtkProperty.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStructuredGrid.h>
#include <vtkStructuredGridGeometryFilter.h>
#include <vtkTextProperty.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkVersion.h>
#include <vtkXYPlotActor.h>

#include <XCAFApp_Application.hxx>
#include <XCAFDoc_DocumentTool.hxx>
#include <XCAFDoc_ShapeTool.hxx>

#include <XSDRAWSTLVRML_DataSource.hxx>

#include <QToolBox>

#include <vtkLineSource.h>
#include <vtkProbeFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkTubeFilter.h>
#include <vtkProperty2D.h>
#include <vtkStructuredGridOutlineFilter.h>
#include <vtkExtractVOI.h>
#include <vtkMarchingCubes.h>
#include <vtkLODActor.h>
#include <vtkGenericCell.h>
#include <vtkIdTypeArray.h>
#include <QStandardItemModel>
#include "tier_application/resultentity.h"
#include <QCollator>

#define MAX2(X, Y)      (Abs(X)>Abs(Y)?Abs(X):Abs(Y))
#define MAX3(X, Y, Z)   (MAX2(MAX2(X,Y),Z))

ResultsWidget::ResultsWidget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, bool simulating, QToolBox* reportWidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    reportInfo = reportWidget;
    //console = consoleWidget;
    processDriftFlux = NULL;
    runningSimulation = simulating;
    list_excluded_dirs << "flow_profiles" << "sludge_profiles" << "processor" << "0" << "profiles" << "constant" << "equipments" << "system" << "0.1" << "0.2" << "0.3";

    vtkViewer->setShowAxes(true);
    vtkViewer->setShowModel(true);
    vtkViewer->setOpacityModel(1.0);
    //vtkGraphs->graphs = new GraphObject[4];
    vtkGraphs = graphs;

    boolShowSurface = false;
    boolShowVolume = false;
    boolShowPlane = false;
    boolShowLine = false;
}

ResultsWidget::~ResultsWidget(){
}

void ResultsWidget::loadMainWindow(){
    settingsUser = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);

    //displayMode = "U";
    //stepTime = 1;
    //isShowPlane=false;
    // isShowGraph=false;
    //isCancelProcess=false;

    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(0);

    createAreaButtons();

    tabCentral = centralWidget->findChild<QTabWidget*>("area_viewers");
    tabCentral->setCurrentIndex(1);
    //connect(tabCentral, SIGNAL(currentChanged(int)), this, SLOT(onTabCentralChanged(int)));

    menu->updateMenu(Globals::instance()->getTabResults(), this);
}
/*
void ResultsWidget::showDomain(){

    //}

    //void ResultsWidget::createAreaActions(){
    //QWidget* widgetProcess = new QWidget();
    //widgetProcess->setObjectName("widgetProcess");

    /*
    if(processDriftFlux != NULL){
        QLabel* lblCancelProcess = new QLabel("SOLVER IS RUNNING");
        lblCancelProcess->setMaximumHeight(30);
        lblCancelProcess->setStyleSheet("font-size: 20px; text-align: center; background-color: #ff0000; color: #ffffff; padding: 5px;");


        QPushButton* btnCancel = new QPushButton("STOP SOLVER", this);
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelDriftFluxFoam()));

        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(lblCancelProcess);
        layout->addWidget(btnCancel);

        widgetProcess->setLayout(layout);
        widgetProcess->setMaximumHeight(100);

        connect(processDriftFlux,SIGNAL(readyReadStandardOutput()),this,SLOT(driftFluxFoamOutput()));
        connect(processDriftFlux, SIGNAL(finished(int)), this, SLOT(driftFluxFoamFinished()));
    }

    QCheckBox* cb_show_model = new QCheckBox();
    cb_show_model->setChecked(true);
    connect(cb_show_model, SIGNAL(stateChanged(int)),this, SLOT(onShowModelChange(int)));

    QCheckBox* cb_show_sludge = new QCheckBox();
    cb_show_sludge->setObjectName("cb_show_sludge");
    cb_show_sludge->setChecked(false);
    connect(cb_show_sludge, SIGNAL(stateChanged(int)),this, SLOT(onShowSludgeChange(int)));

    QLabel* l_opacity = new QLabel("Transparency (0 to 100)");
    QSlider *slider_opacity = new QSlider(Qt::Horizontal);
    slider_opacity->setObjectName("slider_opacity");
    slider_opacity->setRange(0, 100);
    slider_opacity->setValue(50);
    connect(slider_opacity, SIGNAL(sliderReleased()),this, SLOT(onSliderOpacityStop()));

    QLabel* lblDisplayOption = new QLabel("Display Options:");
    lblDisplayOption->setMaximumHeight(30);

    QComboBox* comboDisplayOptions = new QComboBox(this);
    //comboDisplayOptions->setMaximumHeight(30);
    comboDisplayOptions->addItem("alpha.sludge");
    comboDisplayOptions->addItem("epsilon");
    comboDisplayOptions->addItem("k");
    comboDisplayOptions->addItem("p_rgh");
    comboDisplayOptions->addItem("U");
    comboDisplayOptions->addItem("Udm");
    connect(comboDisplayOptions, SIGNAL(currentTextChanged(QString)), this, SLOT(onChangeDisplayOption(const QString&)));

    /*
    QScrollArea* actionsResults = new QScrollArea();
    actionsResults->setObjectName("area_actions_results");
    actionsResults->setFrameShape(QFrame::NoFrame);
    actionsResults->setWidgetResizable(true);
    actionsResults->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);


    QPushButton* btnPlane = new QPushButton("Add new plane", this);
    connect(btnPlane, SIGNAL(clicked()), this, SLOT(addPlane()));

    QWidget *wPlanes = new QWidget();
    lPlanes = new QVBoxLayout();
    wPlanes->setLayout(lPlanes);

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
    QGridLayout* layout = new QGridLayout(this);
    int pos=0;
    layout->addWidget(lblDisplayOption, pos++,0);
    layout->addWidget(comboDisplayOptions, pos++,0);
    layout->addWidget(new QLabel("Show model:"),pos,0);
    layout->addWidget(cb_show_model, pos++, 0, Qt::AlignRight);
    layout->addWidget(l_opacity, pos++,0);
    layout->addWidget(slider_opacity, pos++,0);
    layout->addWidget(new QLabel("Show sludge:"), pos,0);
    layout->addWidget(cb_show_sludge, pos++, 0, Qt::AlignRight);
    //if(processDriftFlux != NULL)
    //    layout->addWidget(widgetProcess,pos++,0);
    //layout->addWidget(actionsResults,pos++,0);
    layout->addWidget(wPlanes, pos++,0);
    layout->addWidget(btnPlane, pos++,0, Qt::AlignJustify);
    layout->addItem(my_spacer, pos,0);

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    //centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);

    addSolid();
}*/
/*
void ResultsWidget::showIsosurface(){

    //}

    //void ResultsWidget::createAreaActions(){
    //QWidget* widgetProcess = new QWidget();
    //widgetProcess->setObjectName("widgetProcess");

    /*
    if(processDriftFlux != NULL){
        QLabel* lblCancelProcess = new QLabel("SOLVER IS RUNNING");
        lblCancelProcess->setMaximumHeight(30);
        lblCancelProcess->setStyleSheet("font-size: 20px; text-align: center; background-color: #ff0000; color: #ffffff; padding: 5px;");


        QPushButton* btnCancel = new QPushButton("STOP SOLVER", this);
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelDriftFluxFoam()));

        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(lblCancelProcess);
        layout->addWidget(btnCancel);

        widgetProcess->setLayout(layout);
        widgetProcess->setMaximumHeight(100);

        connect(processDriftFlux,SIGNAL(readyReadStandardOutput()),this,SLOT(driftFluxFoamOutput()));
        connect(processDriftFlux, SIGNAL(finished(int)), this, SLOT(driftFluxFoamFinished()));
    }

    QCheckBox* cb_show_model = new QCheckBox();
    cb_show_model->setChecked(true);
    connect(cb_show_model, SIGNAL(stateChanged(int)),this, SLOT(onShowModelChange(int)));

    QCheckBox* cb_show_sludge = new QCheckBox();
    cb_show_sludge->setObjectName("cb_show_sludge");
    cb_show_sludge->setChecked(true);
    connect(cb_show_sludge, SIGNAL(stateChanged(int)),this, SLOT(onShowSludgeChange(int)));

    QLabel* l_opacity = new QLabel("Transparency (0 to 100)");
    QSlider *slider_opacity = new QSlider(Qt::Horizontal);
    slider_opacity->setObjectName("slider_opacity");
    slider_opacity->setRange(0, 100);
    slider_opacity->setValue(50);
    connect(slider_opacity, SIGNAL(sliderReleased()),this, SLOT(onSliderOpacityStop()));

    QLabel* lblDisplayOption = new QLabel("Display Options:");
    lblDisplayOption->setMaximumHeight(30);

    QComboBox* comboDisplayOptions = new QComboBox(this);
    //comboDisplayOptions->setMaximumHeight(30);
    comboDisplayOptions->addItem("alpha.sludge");
    comboDisplayOptions->addItem("epsilon");
    comboDisplayOptions->addItem("k");
    comboDisplayOptions->addItem("p_rgh");
    comboDisplayOptions->addItem("U");
    comboDisplayOptions->addItem("Udm");
    connect(comboDisplayOptions, SIGNAL(currentTextChanged(QString)), this, SLOT(onChangeDisplayOption(const QString&)));

    /*
    QScrollArea* actionsResults = new QScrollArea();
    actionsResults->setObjectName("area_actions_results");
    actionsResults->setFrameShape(QFrame::NoFrame);
    actionsResults->setWidgetResizable(true);
    actionsResults->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);


    QPushButton* btnPlane = new QPushButton("Add new plane", this);
    connect(btnPlane, SIGNAL(clicked()), this, SLOT(addPlane()));

    QWidget *wPlanes = new QWidget();
    lPlanes = new QVBoxLayout();
    wPlanes->setLayout(lPlanes);

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
    QGridLayout* layout = new QGridLayout(this);
    int pos=0;
    //layout->addWidget(lblDisplayOption, pos++,0);
    //layout->addWidget(comboDisplayOptions, pos++,0);
    //layout->addWidget(new QLabel("Show model:"),pos,0);
    //layout->addWidget(cb_show_model, pos++, 0, Qt::AlignRight);
    //layout->addWidget(l_opacity, pos++,0);
    //layout->addWidget(slider_opacity, pos++,0);
    //layout->addWidget(new QLabel("Show sludge:"), pos,0);
    //layout->addWidget(cb_show_sludge, pos++, 0, Qt::AlignRight);

    //if(processDriftFlux != NULL)
    //    layout->addWidget(widgetProcess,pos++,0);
    //layout->addWidget(actionsResults,pos++,0);

    //layout->addWidget(wPlanes, pos++,0);
    //layout->addWidget(btnPlane, pos++,0, Qt::AlignJustify);
    //layout->addItem(my_spacer, pos,0);

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    //centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);

    vtkViewer->reset();

    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->SetTimeValue(stepTime);
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    reader->DecomposePolyhedraOff();
    reader->ReadZonesOn();
    reader->Update();

    vtkSmartPointer<vtkExtractVOI> voiHead = vtkSmartPointer<vtkExtractVOI>::New();
    voiHead->SetInputConnection(0, reader->GetOutputPort(0));
    voiHead->SetVOI( 0,255, 60,255, 0,100 );
    voiHead->SetSampleRate( 1,1,1 );

    vtkSmartPointer<vtkMarchingCubes> contourBoneHead = vtkSmartPointer<vtkMarchingCubes>::New();
    contourBoneHead->SetInputConnection(0, voiHead->GetOutputPort(0));
    contourBoneHead->ComputeNormalsOn();
    contourBoneHead->SetValue(0, 0.0001);

    vtkSmartPointer<vtkPolyDataMapper> geoBoneMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    geoBoneMapper->SetInputConnection(0, contourBoneHead->GetOutputPort(0));
    geoBoneMapper->ScalarVisibilityOff();

    vtkSmartPointer<vtkLODActor> actorBone = vtkSmartPointer<vtkLODActor>::New();
    actorBone->SetNumberOfCloudPoints(1000000);
    actorBone->SetMapper(geoBoneMapper);
    actorBone->GetProperty()->SetColor(1,1,1);

    //vtkViewer->m_renderer->AddActor(actorBone);

    //addSolid();
}*/
/*
void ResultsWidget::createAreaViewers(){
    if(vtkViewer != 0){
        vtkViewer->reset();
        vtkViewer->readOpenFoam(stepTime, displayMode, false, false, false, false);
        vtkViewer->update();
        vtkViewer->resetCamera();
    }
}*/

void ResultsWidget::saveLine(){
    settingsUser->setValue("ResultsLine/p1x", QString::number(p1[0]));
    settingsUser->setValue("ResultsLine/p1y", QString::number(p1[1]));
    settingsUser->setValue("ResultsLine/p1z", QString::number(p1[2]));
    settingsUser->setValue("ResultsLine/p2x", QString::number(p2[0]));
    settingsUser->setValue("ResultsLine/p2y", QString::number(p2[1]));
    settingsUser->setValue("ResultsLine/p2z", QString::number(p2[2]));
}

void ResultsWidget::stopOngoingSimulation(){
    QProcess* process = new QProcess();
    QString commands = "docker stop $(cat " + Globals::instance()->getAppProjectPath() + "container_id)";
    process->start(POWERSHELL, commands.split(" "));
    process->waitForFinished();

    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    btn->setHidden(true);

    //menu->goToResultsView();
}

bool ResultsWidget::isSimulationRunning(){
    QProcess* process = new QProcess();
    QString commands = "docker ps -a | findstr " + QString(DOCKERIMAGE) + " | Measure-Object | Select-Object -expand count";
    process->start(POWERSHELL, commands.split(" "));
    process->waitForFinished();
    QString output = process->readAllStandardOutput();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< QString::number(output.split("\r")[0].toInt()) << endl;
    if (output.split("\r")[0].toInt() > 0){
        return true;
    }
    return false;
}

void ResultsWidget::onChangeComboMetrics(){
    updateViewers(0);
}

void ResultsWidget::createAreaButtons(){
    QGridLayout *layout = new QGridLayout();

    stopMedia = true;
    reproducing = false;

    combo_metric = new QComboBox(this);

    //addMetrics();
    comboTimestep = new QComboBox();

    updateComboTimesteps();

    if((comboTimestep->count() == 0) && (!runningSimulation)){
        QGridLayout *layout = new QGridLayout();
        layout->addWidget(new QLabel("Please, run a simulation\nbefore analyzing the results."));
        QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
        areaActionWidget->setLayout(layout);
        centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
        return;
    }

    totalSteps = comboTimestep->count();

    if(totalSteps == 0){
        QGridLayout *layout = new QGridLayout();
        layout->addWidget(new QLabel("Please, run a simulation\nbefore analyzing the results."));
        QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
        areaActionWidget->setLayout(layout);
        centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
        return;
    }

    firstStep = comboTimestep->itemText(0).toInt();
    lastStep = comboTimestep->itemText(totalSteps-1).toInt();
    singleStep = ceil((1.0 * lastStep) / totalSteps);

    sliderTimestep = new QSlider(Qt::Horizontal);
    sliderTimestep->setRange(firstStep, lastStep);
    sliderTimestep->setSingleStep(singleStep);
    QGridLayout *layout_slider = new QGridLayout;
    layout_slider->addWidget(sliderTimestep, 0, 0, 1, 6);
    layout_slider->addWidget(new QLabel(QString::number(firstStep)), 1, 0, 1, 1);
    layout_slider->addWidget(new QLabel(QString::number(lastStep)), 1, 5, 1, 1, Qt::AlignRight);
    widget_slider = new QWidget();
    widget_slider->setLayout(layout_slider);

    if(totalSteps == 1)
        widget_slider->setHidden(true);

    connect(comboTimestep, SIGNAL(currentTextChanged(QString)), this, SLOT(onChangeComboTimestep(const QString&)));
    //connect(sliderTimestep, SIGNAL(valueChanged(int)), this, SLOT(onChangeSliderTimestep(int)));

    btnPlay = new QPushButton();
    btnPlay->setIcon(QIcon(":/Resources/play-button.png"));
    connect(btnPlay, SIGNAL(clicked()), this, SLOT(onPlay()));

    btnStop = new QPushButton();
    btnStop->setIcon(QIcon(":/Resources/square.png"));
    connect(btnStop, SIGNAL(clicked()), this, SLOT(onStop()));

    btnPrevstep = new QPushButton();
    btnPrevstep->setIcon(QIcon(":/Resources/left-arrow-inside-a-circle.png"));
    connect(btnPrevstep, SIGNAL(clicked()), this, SLOT(onPrevTimestep()));

    btnNextstep = new QPushButton();
    btnNextstep->setIcon(QIcon(":/Resources/play-button-1.png"));
    connect(btnNextstep, SIGNAL(clicked()), this, SLOT(onNextTimestep()));

    btnPause = new QPushButton();
    btnPause->setIcon(QIcon(":/Resources/pause-button.png"));
    connect(btnPause, SIGNAL(clicked()), this, SLOT(onPause()));

    btnRefresh = new QPushButton();
    btnRefresh->setIcon(QIcon(":/Resources/two-circling-arrows.png"));
    connect(btnRefresh, SIGNAL(clicked()), this, SLOT(onRefresh()));

    btnPrevstep->setDisabled(true);
    btnPlay->setDisabled(false);
    if(totalSteps == 1){
        btnPlay->setDisabled(true);
        btnNextstep->setDisabled(true);
    } else {
        btnPlay->setDisabled(false);
        btnNextstep->setDisabled(false);
    }
    btnPause->setDisabled(true);
    btnStop->setDisabled(true);
    btnRefresh->setDisabled(false);


    slider_base_opacity = new QSlider(Qt::Horizontal);
    slider_base_opacity->setRange(0, 100);
    //slider_base_opacity->setTracking(false);

    connect(slider_base_opacity, SIGNAL(sliderReleased()),this, SLOT(onSliderOpacityStop()));
    //connect(slider_base_opacity, SIGNAL(valueChanged(int)), this, SLOT(onSliderOpacityStop()));

    slider_base_opacity->setValue(100);
    onSliderOpacityStop();

    QToolBox *toolEntities = new QToolBox();
    toolEntities->addItem(showPlaneTab(), "Plane");
    toolEntities->addItem(showVolumeTab(), "Volume");
    toolEntities->addItem(showSurfaceTab(), "Surface");
    toolEntities->addItem(showLineTab(), "Line");

    toolEntities->setStyleSheet(Globals::instance()->getCssToolBox());
    //toolEntities->setFixedSize(toolEntities->sizeHint());
    toolEntities->setFixedHeight(300);

    /**************************************************************/
    /**************************************************************/
    /**************************************************************/
    /**************************************************************/
    /**************************************************************/

    int pos=0;
    if(isSimulationRunning()){
        QPushButton *btnSimulation = new QPushButton();
        btnSimulation->setObjectName("btn_simulation");
        btnSimulation->setText("Stop Ongoing Simulation");
        btnSimulation->setStyleSheet("background-color: rgb(255,0,0);");
        connect(btnSimulation, SIGNAL(clicked()), this, SLOT(stopOngoingSimulation()));
        layout->addWidget(btnSimulation, pos, 0, 1, 4, Qt::AlignCenter);
        layout->addItem(new QSpacerItem(0, 25), ++pos, 0);
    }
    layout->addWidget(new QLabel("Timestep selector:"), ++pos, 0, 1, 2);
    layout->addWidget(comboTimestep, pos, 2, 1, 2);
    //layout->addWidget(widget_slider, ++pos, 0, 1, 4);
    layout->addWidget(btnPrevstep, ++pos, 0, 1, 1);
    layout->addWidget(btnPlay, pos, 1, 1, 2);
    layout->addWidget(btnNextstep, pos, 3, 1, 1);
    layout->addWidget(btnPause, ++pos, 0, 1, 1);
    layout->addWidget(btnStop, pos, 1, 1, 2);
    layout->addWidget(btnRefresh, pos, 3, 1, 1);
    layout->addItem(new QSpacerItem(0, 25), ++pos, 0);
    layout->addWidget(new QLabel("Metric selector:"), ++pos, 0, 1, 2);
    layout->addWidget(combo_metric, pos, 2, 1, 2);
    layout->addWidget(new QLabel("Base opacity:"), ++pos, 0, 1, 2);
    layout->addWidget(slider_base_opacity, pos, 2, 1, 2);
    layout->addItem(new QSpacerItem(0, 25), ++pos, 0);
    layout->addWidget(toolEntities, ++pos, 0, 1, 0);

    layout->addItem(new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding), ++pos,0);

    layout->setColumnStretch(0, 1);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 1);
    layout->setColumnStretch(3, 1);

    QWidget * widget_layout = new QWidget();
    widget_layout->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(widget_layout);

    connect(combo_metric, SIGNAL(currentIndexChanged(int)), this, SLOT(onChangeComboMetrics()));
    //updateViewers(0);
    vtkViewer->zoomToBox();
    vtkViewer->update();
}

bool ResultsWidget::checkMetricInTimestep(QString metric, QString ts){
    QFileInfo file(Globals::instance()->getAppProjectPath() + ts + "/" + metric);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << file.path() << endl;
    if (file.exists() && file.isFile()) {
        return true;
    } else {
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - No existe " << metric << " en " << ts << endl;
        return false;
    }
}

void ResultsWidget::addMetrics(){
    QString path = Globals::instance()->getAppProjectPath();
    QDir metricsDir(path + "1");
    foreach(const QFileInfo &info, metricsDir.entryInfoList(QDir::Files)) {
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< info.fileName() << endl;
        combo_metric->addItem(info.fileName());
    }
    //combo_metric->addItem("U");
    //combo_metric->addItem("p");
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! SLOTS
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void ResultsWidget::showAreaPlane(){
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Generamos los inputs del formulario de datos
    QLabel* l_point_origin = new QLabel("Origin");
    QLineEdit* i_origin_x = new QLineEdit;
    i_origin_x->setObjectName("i_origin_x");
    i_origin_x->setValidator(Globals::instance()->getDoubleValidator(this));
    i_origin_x->setText("10");
    QLineEdit* i_origin_y = new QLineEdit;
    i_origin_y->setObjectName("i_origin_y");
    i_origin_y->setValidator(Globals::instance()->getDoubleValidator(this));
    i_origin_y->setText("0");
    QLineEdit* i_origin_z = new QLineEdit;
    i_origin_z->setObjectName("i_origin_z");
    i_origin_z->setValidator(Globals::instance()->getDoubleValidator(this));
    i_origin_z->setText("0");

    QHBoxLayout* layoutPointOrigin = new QHBoxLayout();
    layoutPointOrigin->addWidget(i_origin_x);
    layoutPointOrigin->addWidget(i_origin_y);
    layoutPointOrigin->addWidget(i_origin_z);

    QWidget* widgetPointOrigin = new QWidget();
    widgetPointOrigin->setLayout(layoutPointOrigin);
    widgetPointOrigin->setMaximumHeight(40);

    QLabel* l_point_normal = new QLabel("Normal");
    QLineEdit* i_normal_x = new QLineEdit;
    i_normal_x->setObjectName("i_normal_x");
    i_normal_x->setValidator(Globals::instance()->getDoubleValidator(this));
    i_normal_x->setText("1");
    QLineEdit* i_normal_y = new QLineEdit;
    i_normal_y->setObjectName("i_normal_y");
    i_normal_y->setValidator(Globals::instance()->getDoubleValidator(this));
    i_normal_y->setText("0");
    QLineEdit* i_normal_z = new QLineEdit;
    i_normal_z->setObjectName("i_normal_z");
    i_normal_z->setValidator(Globals::instance()->getDoubleValidator(this));
    i_normal_z->setText("0");

    QHBoxLayout* layoutPointNormal = new QHBoxLayout();
    layoutPointNormal->addWidget(i_normal_x);
    layoutPointNormal->addWidget(i_normal_y);
    layoutPointNormal->addWidget(i_normal_z);

    QWidget* widgetPointNormal = new QWidget();
    widgetPointNormal->setLayout(layoutPointNormal);
    widgetPointNormal->setMaximumHeight(40);

    // Creamos el boton de plano de corte
    QPushButton* btnPlane = new QPushButton("PLANE", this);
    btnPlane->setObjectName("btnGraph");
    btnPlane->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    //btnPlane->setMaximumHeight(30);
    connect(btnPlane, SIGNAL(clicked()), this, SLOT(addPlane()));

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->addWidget(l_point_origin);
    layoutAreaActions->addWidget(widgetPointOrigin);
    layoutAreaActions->addWidget(l_point_normal);
    layoutAreaActions->addWidget(widgetPointNormal);
    layoutAreaActions->addWidget(btnPlane);
    layoutAreaActions->addSpacerItem(my_spacer);

    if(centralWidget->findChild<QScrollArea*>("area_actions_results")->layout() != NULL){
        QLayoutItem *child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_actions_results")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_actions_results")->layout();
    }

    centralWidget->findChild<QScrollArea*>("area_actions_results")->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_actions_results")->setMaximumWidth(200);
    centralWidget->findChild<QScrollArea*>("area_actions_results")->setMinimumHeight(300);
}

void ResultsWidget::updatePlaneClicked(){

    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    QWidget *w = qobject_cast<QWidget *>(btn->parent());
    QToolBox *box = w->findChild<QToolBox *>("box_plane");
    int box_index = box->currentIndex();

    if (box_index == 0) { //simple
        QComboBox *combo = w->findChild<QComboBox *>("combo_plane");
        int combo_index = combo->currentIndex();
        QLineEdit *line_simple_value = w->findChild<QLineEdit *>("line_simple_value");
        double simple_value = line_simple_value->text().toDouble();
        if (combo_index == 0){
            normal[0] = 0.0;
            normal[1] = 0.0;
            normal[2] = 1.0;
            origin[0] = 0.0;
            origin[1] = 0.0;
            origin[2] = simple_value;
        } else if (combo_index == 1) {
            normal[0] = 0.0;
            normal[1] = 1.0;
            normal[2] = 0.0;
            origin[0] = 0.0;
            origin[1] = simple_value;
            origin[2] = 0.0;
        } else {
            normal[0] = 1.0;
            normal[1] = 0.0;
            normal[2] = 0.0;
            origin[0] = simple_value;
            origin[1] = 0.0;
            origin[2] = 0.0;
        }
    } else { // advanced
        QLineEdit *line_normal_x = w->findChild<QLineEdit *>("line_normal_x");
        normal[0] = line_normal_x->text().toDouble();
        QLineEdit *line_normal_y = w->findChild<QLineEdit *>("line_normal_y");
        normal[1] = line_normal_y->text().toDouble();
        QLineEdit *line_normal_z = w->findChild<QLineEdit *>("line_normal_z");
        normal[2] = line_normal_z->text().toDouble();
        QLineEdit *line_origin_x = w->findChild<QLineEdit *>("line_origin_x");
        origin[0] = line_origin_x->text().toDouble();
        QLineEdit *line_origin_y = w->findChild<QLineEdit *>("line_origin_y");
        origin[1] = line_origin_y->text().toDouble();
        QLineEdit *line_origin_z = w->findChild<QLineEdit *>("line_origin_z");
        origin[2] = line_origin_z->text().toDouble();
    }

    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << normal[0] << " - " << normal[1]<< " - " << normal[2] << " - " << origin[0] << " - " << origin[1]<< " - " << origin[2] << endl;

    //vtkViewer->reset();
    //vtkViewer->addPlane(stepTime, displayMode, origin, normal);
    //vtkViewer->setOpacityModel(0.1);
    //vtkViewer->readOpenFoam(0.3, "solid color", false, false, false, false);
    //vtkViewer->update();

    //isShowPlane=true;
    //isShowGraph=false;

    boolShowPlane = !boolShowPlane;
    updateViewers(0);
}

void ResultsWidget::onChangedDataFromEntity(int i){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget *w = qobject_cast<QWidget *>(combo->parent());

    QModelIndex indexOfTheCellIWant = combo->model()->index(i,0);
    ResultEntity *entity = indexOfTheCellIWant.data(Qt::UserRole + 1).value<ResultEntity*>();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << entity->getName() << endl;

    QSlider *slider = w->findChild<QSlider *>("slider_model_opacity");
    slider->setValue(entity->getOpacity());

    //comboTimestep->setCurrentText(QString::number(entity->getTimestep()));
}
/*
QStandardItemModel* ResultsWidget::getVolumeEntities(){
    QStandardItemModel* model  = new QStandardItemModel();
    int cnt = model_entities->rowCount();
    for(int r = 0; r < cnt; ++r) {
        QModelIndex index = model_entities->index(r,0);
        ResultEntity *entity = index.data(Qt::UserRole + 1).value<ResultEntity*>();
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << entity->getName() << endl;
        if (entity->getType() == 1){
            qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
            QStandardItem *item = new QStandardItem;
            item->setData(QVariant::fromValue(entity));
            model->appendRow(item);
            ResultEntity* o = item->data(Qt::UserRole + 1).value<ResultEntity*>();
            item->setText(o->getName());
        }
    }
    return model;
}
*/
void ResultsWidget::onBasicPlaneChanged(int i){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget *w = qobject_cast<QWidget *>(combo->parent());
    QLabel *l = w->findChild<QLabel *>("label_basic_plane_value");

    if(i == 0)
        l->setText("Z:");
    else if(i == 1)
        l->setText("Y:");
    else if(i == 2)
        l->setText("X:");
}

void ResultsWidget::removePlane(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());
    delete w;
}

void ResultsWidget::addPlane(){
    int lineEditWidth = 30;

    QWidget *wPlane = new QWidget();
    QGridLayout *lPlane = new QGridLayout();
    wPlane->setStyleSheet("background-color: cyan");
    wPlane->setLayout(lPlane);
    wPlane->setFixedHeight(220);

    QPushButton* btnUpd = new QPushButton("Update");
    connect(btnUpd, SIGNAL(clicked(bool)), this, SLOT(updatePlaneClicked()));

    QPushButton *btn_remove = new QPushButton();
    connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(removePlane()));
    btn_remove->setFixedWidth(20);
    btn_remove->setIcon(QIcon(":/Resources/remove.png"));
    btn_remove->setIconSize(QSize(17,17));

    /***********************************************************************/
    QWidget *wBasic = new QWidget();
    QVBoxLayout *lBasic = new QVBoxLayout();
    wBasic->setLayout(lBasic);
    wBasic->setFixedHeight(60);

    QHBoxLayout *layout_select = new QHBoxLayout();
    QComboBox *combo_plane = new QComboBox();
    combo_plane->setObjectName("combo_plane");
    //combo_plane->setFixedWidth(100);
    combo_plane->addItem("XY");
    combo_plane->addItem("XZ");
    combo_plane->addItem("YZ");
    connect(combo_plane, SIGNAL(currentIndexChanged(int)), this, SLOT(onBasicPlaneChanged(int)));
    QLabel *label_value = new QLabel("Z:");
    label_value->setObjectName("label_basic_plane_value");
    QLineEdit *line_value = new QLineEdit();
    line_value->setFixedWidth(lineEditWidth);
    line_value->setObjectName("line_simple_value");
    line_value->setText("0");
    layout_select->addWidget(new QLabel("Plane:"));
    layout_select->addWidget(combo_plane);
    layout_select->addWidget(label_value);
    layout_select->addWidget(line_value);
    layout_select->addStretch();

    QHBoxLayout *layout_rotate = new QHBoxLayout();
    layout_rotate->addWidget(new QLabel("Rotate clockwise (º):"));
    layout_rotate->addWidget(new QLineEdit());

    lBasic->addWidget(new QLabel("Select a plane and set the value:"));
    lBasic->addLayout(layout_select);
    //lBasic->addLayout(layout_rotate);

    /***********************************************************************/
    QWidget *wAdvanced = new QWidget();
    QVBoxLayout *lAdvanced = new QVBoxLayout();
    wAdvanced->setLayout(lAdvanced);
    //wAdvanced->setFixedHeight(300);
    //wAdvanced->setFixedWidth(300);

    QLineEdit* line_origin_x = new QLineEdit;
    line_origin_x->setObjectName("line_origin_x");
    line_origin_x->setText("0");
    line_origin_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_x->setFixedWidth(lineEditWidth);
    QLineEdit* line_origin_y = new QLineEdit;
    line_origin_y->setObjectName("line_origin_y");
    line_origin_y->setText("0");
    line_origin_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_y->setFixedWidth(lineEditWidth);
    QLineEdit* line_origin_z = new QLineEdit;
    line_origin_z->setObjectName("line_origin_z");
    line_origin_z->setText("0");
    line_origin_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_z->setFixedWidth(lineEditWidth);
    QHBoxLayout *layout_origin = new QHBoxLayout();
    layout_origin->addWidget(new QLabel("Origin"));
    layout_origin->addWidget(new QLabel("X:"));
    layout_origin->addWidget(line_origin_x);
    layout_origin->addWidget(new QLabel("Y:"));
    layout_origin->addWidget(line_origin_y);
    layout_origin->addWidget(new QLabel("Z:"));
    layout_origin->addWidget(line_origin_z);
    lAdvanced->addLayout(layout_origin);

    QLineEdit* line_normal_x = new QLineEdit;
    line_normal_x->setObjectName("line_normal_x");
    line_normal_x->setText("0");
    line_normal_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_x->setFixedWidth(lineEditWidth);
    QLineEdit* line_normal_y = new QLineEdit;
    line_normal_y->setObjectName("line_normal_y");
    line_normal_y->setText("0");
    line_normal_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_y->setFixedWidth(lineEditWidth);
    QLineEdit* line_normal_z = new QLineEdit;
    line_normal_z->setObjectName("line_normal_z");
    line_normal_z->setText("0");
    line_normal_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_z->setFixedWidth(lineEditWidth);
    QHBoxLayout *layout_normal = new QHBoxLayout();
    layout_normal->addWidget(new QLabel("Normal"));
    layout_normal->addWidget(new QLabel("X:"));
    layout_normal->addWidget(line_normal_x);
    layout_normal->addWidget(new QLabel("Y:"));
    layout_normal->addWidget(line_normal_y);
    layout_normal->addWidget(new QLabel("Z:"));
    layout_normal->addWidget(line_normal_z);
    lAdvanced->addLayout(layout_normal);
    /***********************************************************************/

    QToolBox *boxPlane = new QToolBox();
    boxPlane->setObjectName("box_plane");
    boxPlane->addItem(wBasic, "BASIC");
    boxPlane->addItem(wAdvanced, "ADVANCED");
    boxPlane->setStyleSheet(Globals::instance()->getCssToolBox());
    //boxPlane->setCurrentIndex(1);

    lPlane->addWidget(boxPlane,0,0);
    lPlane->addWidget(btnUpd,1,0);
    lPlane->addWidget(btn_remove,1,1);

    lPlanes->addWidget(wPlane);

    /*
    QString origin_x = centralWidget->findChild<QLineEdit*>("i_origin_x")->text();
    QString origin_y = centralWidget->findChild<QLineEdit*>("i_origin_y")->text();
    QString origin_z = centralWidget->findChild<QLineEdit*>("i_origin_z")->text();

    QString normal_x = centralWidget->findChild<QLineEdit*>("i_normal_x")->text();
    QString normal_y = centralWidget->findChild<QLineEdit*>("i_normal_y")->text();
    QString normal_z = centralWidget->findChild<QLineEdit*>("i_normal_z")->text();

    origin[0] = origin_x.toDouble();
    origin[1] = origin_y.toDouble();
    origin[2] = origin_z.toDouble();

    normal[0] = normal_x.toDouble();
    normal[1] = normal_y.toDouble();
    normal[2] = normal_z.toDouble();

    vtkViewer->reset();
    vtkViewer->addPlane(stepTime, displayMode, origin, normal);
    vtkViewer->setOpacityModel(0.1);
    vtkViewer->readOpenFoam(0.3, "solid color", false, false, false, false);
    vtkViewer->update();

    isShowPlane=true;
    isShowGraph=false;
    */

}

void ResultsWidget::onShowModelChange(int value){
    vtkViewer->setShowModel(value != 0);
    //updateViewers();
}

void ResultsWidget::onShowSludgeChange(int value){
    //vtkViewer->setShowSludge(value != 0);
    //updateViewers();

    vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
    reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
    reader->Update();
    reader->SetTimeValue(stepTime);
    reader->CreateCellToPointOn();
    reader->SetCreateCellToPoint(1);
    reader->DecomposePolyhedraOn();
    reader->ReadZonesOn();
    reader->Update();

    vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
    gf->SetInputConnection(0, reader->GetOutputPort(0));
    vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
    block0->GetCellData()->SetActiveScalars(displayMode.toStdString().c_str());
    block0->GetPointData()->SetActiveScalars(displayMode.toStdString().c_str());

    vtkViewer->reset();
    if(value){
        // Create an isosurface
        vtkSmartPointer<vtkContourFilter> contourFilter = vtkSmartPointer<vtkContourFilter>::New();
        contourFilter->SetInputData(block0);
        contourFilter->GenerateValues(1, 0.001, 1); // (numContours, rangeStart, rangeEnd)

        // Map the contours to graphical primitives
        vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        contourMapper->SetInputConnection(contourFilter->GetOutputPort());

        // Create an actor for the contours
        vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();
        contourActor->SetMapper(contourMapper);

        vtkViewer->m_renderer->AddActor(contourActor);
    }

    if (vtkViewer->showModel) {
        vtkSmartPointer<vtkPolyDataMapper> polyDataMapper1 = vtkSmartPointer<vtkPolyDataMapper>::New();
        polyDataMapper1->SetInputConnection(gf->GetOutputPort());
        polyDataMapper1->CreateDefaultLookupTable(); // Use default color lookup table
        polyDataMapper1->SetScalarModeToUseCellFieldData();// # Use cell data for color mapping
        polyDataMapper1->SelectColorArray("alpha.sludge"); //# Specify array name used for color mapping

        vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
        mapper->SetInputData(block0);
        vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
        actor->SetMapper(mapper);
        vtkViewer->m_renderer->AddActor(actor);

        vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
        hueLut->SetHueRange(0.667,0.0);
        hueLut->Build();
        mapper->SetLookupTable(hueLut);
        vtkViewer->m_renderer->AddActor(actor);

        mapper->SetScalarRange(block0->GetCellData()->GetScalars()->GetRange());
        //mapper->SetScalarRange(0,0.00275);

        mapper->SetScalarModeToUsePointData();
        mapper->SetColorModeToMapScalars();
        mapper->ScalarVisibilityOn();

        vtkSmartPointer<vtkScalarBarActor> scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
        scalarBar->SetLookupTable(mapper->GetLookupTable());
        scalarBar->SetTitle(displayMode.toStdString().c_str());
        scalarBar->SetNumberOfLabels(4);
        scalarBar->SetLookupTable(hueLut);
        scalarBar->DragableOn();
        scalarBar->SetWidth(0.1);
        scalarBar->SetHeight(0.5);

        vtkViewer->m_renderer->AddActor2D(scalarBar);
    }
    vtkViewer->update();
}


void ResultsWidget::onSliderOpacityStop(){
    //int opacity = slider_base_opacity->value();
    /*int timestep = comboTimestep->currentText().toInt();
    QString metric = combo_metric->currentText();

    vtkViewer->reset();
    vtkViewer->renderOpenFoamReader(timestep, opacity, metric);
    vtkViewer->update();*/

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< QString::number(opacity) << endl;

    updateViewers(0);
}

void ResultsWidget::onModelDataChanged(QStandardItem *item){
    disconnect(model_entities, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onModelDataChanged(QStandardItem*)));

    ResultEntity* entity = item->data(Qt::UserRole + 1).value<ResultEntity*>();

    bool checkboxChanged=false;
    if(entity->isChecked()){
        if(item->checkState() == Qt::Unchecked) //se ha modificado el check box
            checkboxChanged = true;
    } else {
        if(item->checkState() == Qt::Checked)
            checkboxChanged = true;
    }
    if(checkboxChanged){
        //if the checkbox state has changed, the item is selected in the model.
        listview_entities->setCurrentIndex(item->index());
        showEntity(entity);
    }
    //if (tabCentral->currentIndex() != 2){
    QWidget* areaActionWidget = centralWidget->findChild<QScrollArea*>("area_actions");
    QLineEdit *line = areaActionWidget->findChild<QLineEdit*>("entity_name");
    item->setText(line->text());
    //}
    //ResultEntity* o = item->data(Qt::UserRole + 1).value<ResultEntity*>();
    entity->setChecked(item->checkState());

    connect(model_entities, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onModelDataChanged(QStandardItem*)));
}

void ResultsWidget::onEntityChanged(QModelIndex index){
    stopMedia=true;
    ResultEntity *entity = index.data(Qt::UserRole + 1).value<ResultEntity*>();
    showEntity(entity);
}

void ResultsWidget::onChangeComboEntity(int index){
    disconnect(model_entities, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onModelDataChanged(QStandardItem*)));

    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    int cnt = model_entities->rowCount();

    ResultEntity *entity = new ResultEntity(model_entities->rowCount(), index, combo->currentText());
    entity->setTimestep(1);
    QVariant variant;
    variant.setValue(entity);
    QStandardItem *item = new QStandardItem;
    item->setData(QVariant::fromValue(entity));
    model_entities->appendRow(item);
    QModelIndex indexOfTheCellIWant = model_entities->index(cnt,0);
    listview_entities->setCurrentIndex(indexOfTheCellIWant);
    ResultEntity* o = item->data(Qt::UserRole + 1).value<ResultEntity*>();
    item->setText(o->getName());
    item->setEditable(false);
    if (index==3){
        item->setCheckable(true);
        item->setCheckState(Qt::Checked);
        entity->setChecked(true);
    }
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << o->getName() << endl;

    connect(model_entities, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onModelDataChanged(QStandardItem*)));
    showEntity(entity);

    disconnect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(onChangeComboEntity(int)));
    combo->setCurrentIndex(0);
    connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(onChangeComboEntity(int)));
}

void ResultsWidget::showPrevTimestep(){
    int index = comboTimestep->currentIndex();
    comboTimestep->setCurrentIndex(--index); //automatically updates the viewer because of the slot
    //stepTime = comboTimestep->currentText().toDouble();

    if (index == 0) {
        btnPrevstep->setDisabled(true);
    }
}

void ResultsWidget::showNextTimestep(){
    int index = comboTimestep->currentIndex();
    comboTimestep->setCurrentIndex(++index); //automatically updates the viewer because of the slot
    if (index == comboTimestep->count()-1){
        stopMedia=true;
        btnNextstep->setDisabled(true);
    }
    if(!stopMedia){
        QTime myTimer;
        myTimer.start();
        QTimer::singleShot(1, this, SLOT(showNextTimestep()));
    } else {
        reproducing = false;
        btnPause->setDisabled(true);
        btnRefresh->setDisabled(false);
        btnPrevstep->setDisabled(false);
    }
}

void ResultsWidget::showSequenceTimesteps(){
    if(!stopMedia){
        //if (btnPlay->isEnabled()){
        //    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
        showNextTimestep();
        QTime myTimer;
        myTimer.start();
        QTimer::singleShot(1, this, SLOT(showSequenceTimesteps()));
    }
}

void ResultsWidget::nextStepTime(){
    /*    if(! stop){
        int index = comboTimestep->currentIndex();
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << index << " - " << timesteps << endl;
        if (index == comboTimestep->count()-1) {
            //stepTime = comboTimestep->currentText().toDouble();
            //updateViewers();
            onPauseEnd();
        } else {
            comboTimestep->setCurrentIndex(++index); //automatically updates the viewer because of the slot
            stepTime = comboTimestep->currentText().toDouble();
            QTime myTimer;
            myTimer.start();
            QTimer::singleShot(1, this, SLOT(nextStepTime()));
        }
    } else {
      //  onPauseEnd();
    }*/
}

void ResultsWidget::processFoamOutput(){
    // Escribimos por la consola lo que sucede
    QString text;
    text.append(processFoam->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();

    QFile outputfile(Globals::instance()->getAppProjectPath() + "/pimpleFoam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Append)) {
        QTextStream stream(&outputfile);
        stream << text;
    }
}

void ResultsWidget::driftFluxFoamOutput(){
    // Escribimos por la consola lo que sucede
    QString text;
    text.append(processDriftFlux->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();

    // Si se han creado carpeta refrescamos los selectores
    /*QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::Time | QDir::Reversed);

    QStringList dirList = dir.entryList();

    if(dirList.count() > currentNumFolders){
        currentNumFolders = dirList.count();
        onRefreshTimeStep();
    }*/
}

void ResultsWidget::cancelDriftFluxFoam(){
    processDriftFlux->kill();
    isCancelProcess = true;
}

void ResultsWidget::driftFluxFoamFinished(){
    processDriftFlux = NULL;
    //createAreaActions();
    onRefresh();
}

void ResultsWidget::startPimpleFoam(){
    QFile outputfile(Globals::instance()->getAppProjectPath() + "/foam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Truncate));

    processFoam = new QProcess();
    processFoam->setObjectName("process_simulation");
    //connect(processFoam, SIGNAL(started()), this, SLOT(driftFluxFoamStarted()));
    connect(processFoam,SIGNAL(readyReadStandardOutput()),this,SLOT(processFoamOutput()));

    processFoam->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processFoam->setProcessChannelMode(QProcess::MergedChannels);
    //processFoam->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/pimpleFoam.output");
    //processDriftFlux->start("driftFluxFoam");

    QString script = "execFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "hostname > /tmp/project/container_id" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "export PATH=$HOME/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/bin/:$PATH" << endl;
        stream << "export LD_LIBRARY_PATH=/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/fftw-3.3.7/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/CGAL-4.9.1/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/boost_1_64_0/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/gperftools-2.5/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/openmpi-system:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/user/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/site/1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/dummy" << endl;
        //stream << "export OMPI_MCA_btl_vader_single_copy_mechanism=none" << endl;
        //stream << "decomposePar" << endl;
        stream << "pimpleFoam" << endl;
        //stream << "mpirun -np 4 driftFluxFoam -parallel" << endl;
        //stream << "reconstructPar" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processFoam->start(POWERSHELL, commands.split(" "));
}

void ResultsWidget::startDriftFluxFoam(){
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::Time | QDir::Reversed);

    QStringList dirList = dir.entryList();
    currentNumFolders = dirList.count();

    // Ejecutamos el comando driftFluxFoam
    processDriftFlux = new QProcess();
    processDriftFlux->setObjectName("process_simulation");
    connect(processDriftFlux, SIGNAL(started()), this, SLOT(driftFluxFoamStarted()));
    connect(processDriftFlux,SIGNAL(readyReadStandardOutput()),this,SLOT(driftFluxFoamOutput()));

    processDriftFlux->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processDriftFlux->setProcessChannelMode(QProcess::MergedChannels);
    //processDriftFlux->start("driftFluxFoam");

    QString script = "execFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "hostname > /tmp/project/container_id" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "export OMPI_MCA_btl_vader_single_copy_mechanism=none" << endl;
        //stream << "decomposePar" << endl;
        stream << "driftFluxFoam" << endl;
        //stream << "mpirun -np 4 driftFluxFoam -parallel" << endl;
        //stream << "reconstructPar" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processDriftFlux->start(POWERSHELL, commands.split(" "));
}

void ResultsWidget::driftFluxFoamStarted(){
    // Pasamos a la pantalla de resultados
    Globals::instance()->setCurrentProjectResultsModel(true);
    Globals::instance()->writeSettingsFile();
    Globals::instance()->writeProjectInfoFile();
}

void ResultsWidget::onChangeDisplayOption(const QString& newDisplayMode){
    updateViewers(0);

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
    //displayMode = newDisplayMode;
    //updateViewers();
}

void ResultsWidget::onChangeSliderTimestep(const int ts){
    comboTimestep->setCurrentText(QString::number(ts));
}

void ResultsWidget::onChangeComboTimestep(const QString& ts){
    disconnect(sliderTimestep, SIGNAL(valueChanged(int)), this, SLOT(onChangeSliderTimestep(int)));
    sliderTimestep->setValue(ts.toInt());
    connect(sliderTimestep, SIGNAL(valueChanged(int)), this, SLOT(onChangeSliderTimestep(int)));

    updateViewers(0);
}

void ResultsWidget::onPrevTimestep(){
    stopMedia = true;
    //btnPrevstep->setDisabled(true);
    btnPlay->setDisabled(false);
    btnNextstep->setDisabled(false);
    btnPause->setDisabled(true);
    btnStop->setDisabled(true);
    btnRefresh->setDisabled(false);
    showPrevTimestep();
}

void ResultsWidget::onNextTimestep(){
    stopMedia = true;
    btnPrevstep->setDisabled(false);
    btnPlay->setDisabled(false);
    //btnNextstep->setDisabled(false);
    btnPause->setDisabled(true);
    btnStop->setDisabled(true);
    btnRefresh->setDisabled(false);
    showNextTimestep();
}

void ResultsWidget::onStop(){
    stopMedia = true;
    btnPlay->setDisabled(false);
    btnPause->setDisabled(true);
    btnRefresh->setDisabled(false);
    btnStop->setDisabled(true);
    btnNextstep->setDisabled(false);
    comboTimestep->setCurrentIndex(-1);
}

void ResultsWidget::onPause(){
    stopMedia = true;
    btnPrevstep->setDisabled(false);
    btnPlay->setDisabled(false);
    btnPause->setDisabled(true);
    btnRefresh->setDisabled(false);
    btnStop->setDisabled(false);
    btnNextstep->setDisabled(false);
}

void ResultsWidget::onPauseEnd(){
    btnPlay->setDisabled(false);
    btnPause->setDisabled(true);
    btnRefresh->setDisabled(false);
    //stop = false;
}

void ResultsWidget::onPlay() {
    stopMedia = false;
    reproducing = true;
    btnPrevstep->setDisabled(true);
    btnPlay->setDisabled(true);
    btnNextstep->setDisabled(true);
    btnPause->setDisabled(false);
    btnStop->setDisabled(false);
    btnRefresh->setDisabled(true);
    showNextTimestep();
}

void ResultsWidget::showEntity(ResultEntity *entity) {
    QLineEdit *line_name = new QLineEdit();
    line_name->setObjectName("entity_name");
    line_name->setText(entity->getName());

    QPushButton *btn_apply = new QPushButton("Apply");

    QWidget *widget_layout = new QWidget();

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
    QGridLayout* layout = new QGridLayout();
    widget_layout->setLayout(layout);
    int pos=0;
    //layout->setContentsMargins(10, 10, 10, 10);
    layout->addWidget(new QLabel("Name:"), pos, 0, 1, 1);
    layout->addWidget(line_name, pos++, 1, 1, 3);

    QSlider *slider_opacity = new QSlider(Qt::Horizontal);
    slider_opacity->setObjectName("slider_opacity");
    slider_opacity->setRange(0, 100);
    //slider_opacity->setValue(100);

    QComboBox* comboDisplayOptions = new QComboBox(this);
    comboDisplayOptions->setObjectName("combo_metric");
    //comboDisplayOptions->setMaximumHeight(30);
    //comboDisplayOptions->addItem("alpha.sludge");
    //comboDisplayOptions->addItem("epsilon");
    //comboDisplayOptions->addItem("k");
    //omboDisplayOptions->addItem("p_rgh");
    comboDisplayOptions->addItem("U");
    //comboDisplayOptions->addItem("Udm");

    comboDisplayOptions->setCurrentText(entity->getMetric());
    slider_opacity->setValue(entity->getOpacity());

    connect(slider_opacity, SIGNAL(sliderReleased()),this, SLOT(onSliderOpacityStop()));
    connect(comboDisplayOptions, SIGNAL(currentTextChanged(QString)), this, SLOT(onChangeDisplayOption(const QString&)));

    QString strEntity = "Entity_" + QString::number(entity->getId()) + "/";

    layout->addWidget(new QLabel("Metric:"), pos, 0, 1, 1);
    layout->addWidget(comboDisplayOptions, pos++, 1, 1, 3);
    layout->addWidget(new QLabel("Opacity:"),pos, 0, 1, 1);
    layout->addWidget(slider_opacity,pos++, 1, 1, 3);


    if (entity->getType() == 0){ //Base (results from a simulation)
        /*layout->addWidget(new QLabel("Metric:"), pos, 0, 1, 1);
        layout->addWidget(comboDisplayOptions, pos++, 1, 1, 3);
        layout->addWidget(new QLabel("Opacity:"),pos, 0, 1, 1);
        layout->addWidget(slider_opacity,pos++, 1, 1, 3);*/

    } else if (entity->getType() == 1){ //Volume
        QLineEdit *line_min = new QLineEdit();
        line_min->setObjectName("line_volume_min");
        //line_min->setText(QString::number(0));
        //line_min->setFixedWidth(25);
        QLineEdit *line_max = new QLineEdit();
        line_max->setObjectName("line_volume_max");
        //line_max->setFixedWidth(25);

        //double *range = vtkViewer->getMetricRange(entity->getMetric(), entity->getTimestep());
        //entity->setMinval(range[0]);
        //entity->setMaxval(range[1]);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << entity->getMinval() << " - " << entity->getMaxval() << endl;

        //entity->setMinval(0);
        //entity->setMaxval(0);
        //line_min->setText(QString::number(entity->getMinval()));
        line_max->setText(QString::number(entity->getMaxval()));
        /*
        layout->addWidget(new QLabel("Metric:"), pos, 0, 1, 1);
        layout->addWidget(comboDisplayOptions, pos++, 1, 1, 3);
        layout->addWidget(new QLabel("Opacity:"),pos, 0, 1, 1);
        layout->addWidget(slider_opacity,pos++, 1, 1, 3);*/
        layout->addWidget(new QLabel("Isovolume Value:"), pos, 0,1,1);
        layout->addWidget(line_max, pos++, 1,1,1);
        //layout->addWidget(line_min, pos, 1,1,1);
        //layout->addWidget(new QLabel("to"), pos, 2, 1,1 );
        //layout->addWidget(line_max, pos++, 3,1,1);

    } else if (entity->getType() == 2){ //plane
        int lineEditWidth = 30;

        QWidget *wBasic = new QWidget();
        QVBoxLayout *lBasic = new QVBoxLayout();
        wBasic->setLayout(lBasic);
        //wBasic->setFixedHeight(60);

        QHBoxLayout *layout_select = new QHBoxLayout();
        QComboBox *combo_plane = new QComboBox();
        combo_plane->setObjectName("combo_plane");
        //combo_plane->setFixedWidth(100);
        combo_plane->addItem("XY");
        combo_plane->addItem("XZ");
        combo_plane->addItem("YZ");
        connect(combo_plane, SIGNAL(currentIndexChanged(int)), this, SLOT(onBasicPlaneChanged(int)));
        QLabel *label_value = new QLabel("Z:");
        label_value->setObjectName("label_basic_plane_value");
        QLineEdit *line_value = new QLineEdit();
        line_value->setFixedWidth(lineEditWidth);
        line_value->setObjectName("line_simple_value");
        line_value->setText("0");
        layout_select->addWidget(new QLabel("Plane:"));
        layout_select->addWidget(combo_plane);
        layout_select->addWidget(label_value);
        layout_select->addWidget(line_value);
        layout_select->addStretch();

        QHBoxLayout *layout_rotate = new QHBoxLayout();
        layout_rotate->addWidget(new QLabel("Rotate clockwise (º):"));
        layout_rotate->addWidget(new QLineEdit());

        lBasic->addWidget(new QLabel("Select a plane and set the value:"));
        lBasic->addLayout(layout_select);
        //lBasic->addLayout(layout_rotate);

        /***********************************************************************/
        QWidget *wAdvanced = new QWidget();
        QVBoxLayout *lAdvanced = new QVBoxLayout();
        wAdvanced->setLayout(lAdvanced);

        QLineEdit* line_origin_x = new QLineEdit;
        line_origin_x->setObjectName("line_origin_x");
        //line_origin_x->setText("0");
        line_origin_x->setValidator(Globals::instance()->getDoubleValidator(this));
        line_origin_x->setFixedWidth(lineEditWidth);
        QLineEdit* line_origin_y = new QLineEdit;
        line_origin_y->setObjectName("line_origin_y");
        //line_origin_y->setText("0");
        line_origin_y->setValidator(Globals::instance()->getDoubleValidator(this));
        line_origin_y->setFixedWidth(lineEditWidth);
        QLineEdit* line_origin_z = new QLineEdit;
        line_origin_z->setObjectName("line_origin_z");
        //line_origin_z->setText("0");
        line_origin_z->setValidator(Globals::instance()->getDoubleValidator(this));
        line_origin_z->setFixedWidth(lineEditWidth);
        QHBoxLayout *layout_origin = new QHBoxLayout();
        layout_origin->addWidget(new QLabel("Origin"));
        layout_origin->addWidget(new QLabel("X:"));
        layout_origin->addWidget(line_origin_x);
        layout_origin->addWidget(new QLabel("Y:"));
        layout_origin->addWidget(line_origin_y);
        layout_origin->addWidget(new QLabel("Z:"));
        layout_origin->addWidget(line_origin_z);
        lAdvanced->addLayout(layout_origin);

        QLineEdit* line_normal_x = new QLineEdit;
        line_normal_x->setObjectName("line_normal_x");
        //line_normal_x->setText("0");
        line_normal_x->setValidator(Globals::instance()->getDoubleValidator(this));
        line_normal_x->setFixedWidth(lineEditWidth);
        QLineEdit* line_normal_y = new QLineEdit;
        line_normal_y->setObjectName("line_normal_y");
        //line_normal_y->setText("0");
        line_normal_y->setValidator(Globals::instance()->getDoubleValidator(this));
        line_normal_y->setFixedWidth(lineEditWidth);
        QLineEdit* line_normal_z = new QLineEdit;
        line_normal_z->setObjectName("line_normal_z");
        //line_normal_z->setText("0");
        line_normal_z->setValidator(Globals::instance()->getDoubleValidator(this));
        line_normal_z->setFixedWidth(lineEditWidth);
        QHBoxLayout *layout_normal = new QHBoxLayout();
        layout_normal->addWidget(new QLabel("Normal"));
        layout_normal->addWidget(new QLabel("X:"));
        layout_normal->addWidget(line_normal_x);
        layout_normal->addWidget(new QLabel("Y:"));
        layout_normal->addWidget(line_normal_y);
        layout_normal->addWidget(new QLabel("Z:"));
        layout_normal->addWidget(line_normal_z);
        lAdvanced->addLayout(layout_normal);
        /***********************************************************************/

        QToolBox *boxPlane = new QToolBox();
        boxPlane->setFixedHeight(150);
        boxPlane->setObjectName("box_plane");
        boxPlane->addItem(wBasic, "BASIC");
        boxPlane->addItem(wAdvanced, "ADVANCED");
        boxPlane->setStyleSheet(Globals::instance()->getCssToolBox());

        int planemode = settingsUser->value(strEntity + "planemode", 0).toInt();
        boxPlane->setCurrentIndex(planemode);
        if (boxPlane == 0){
            int basicindex = settingsUser->value(strEntity + "basicindex", 0).toInt();
            combo_plane->setCurrentIndex(basicindex);
        }

        line_origin_x->setText(QString::number(entity->getOrigin()[0]));
        line_origin_y->setText(QString::number(entity->getOrigin()[1]));
        line_origin_z->setText(QString::number(entity->getOrigin()[2]));
        line_normal_x->setText(QString::number(entity->getNormal()[0]));
        line_normal_y->setText(QString::number(entity->getNormal()[1]));
        line_normal_z->setText(QString::number(entity->getNormal()[2]));

        //Get data from an existent entity. It works, but we are not going to use it.
        /*
            QComboBox *comboDataFrom = new QComboBox();
            comboDataFrom->setModel(getVolumeEntities());
            connect(comboDataFrom, SIGNAL(currentIndexChanged(int)), this, SLOT(onChangedDataFromEntity(int)));
            layout->addWidget(new QLabel("Data from model:"), pos, 0, 1, 1);
            layout->addWidget(comboDataFrom, pos++, 1, 1, 1);
        */

        /*layout->addWidget(new QLabel("Metric:"), pos, 0, 1, 1);
        layout->addWidget(comboDisplayOptions, pos++, 1, 1, 1);
        layout->addWidget(new QLabel("Opacity:"),pos, 0, 1, 1);
        layout->addWidget(slider_opacity,pos++, 1, 1, 1);*/
        layout->addWidget(boxPlane, pos++, 0, 1 ,3);
    } else if(entity->getType()==3){
        int size_line = 40;

        QLineEdit* line_PIX = new QLineEdit;
        line_PIX->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PIX->setFixedWidth(size_line);
        line_PIX->setObjectName("PIX");
        QLineEdit* line_PIY = new QLineEdit;
        line_PIY->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PIY->setFixedWidth(size_line);
        line_PIY->setObjectName("PIY");
        QLineEdit* line_PIZ = new QLineEdit;
        line_PIZ->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PIZ->setFixedWidth(size_line);
        line_PIZ->setObjectName("PIZ");

        QLineEdit* line_PFX = new QLineEdit;
        line_PFX->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PFX->setFixedWidth(size_line);
        line_PFX->setObjectName("PFX");
        QLineEdit* line_PFY = new QLineEdit;
        line_PFY->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PFY->setFixedWidth(size_line);
        line_PFY->setObjectName("PFY");
        QLineEdit* line_PFZ = new QLineEdit;
        line_PFZ->setValidator(Globals::instance()->getDoubleValidator(this));
        line_PFZ->setFixedWidth(size_line);
        line_PFZ->setObjectName("PFZ");

        line_PIX->setText(QString::number(entity->getPoint1()[0]));
        line_PIY->setText(QString::number(entity->getPoint1()[1]));
        line_PIZ->setText(QString::number(entity->getPoint1()[2]));
        line_PFX->setText(QString::number(entity->getPoint2()[0]));
        line_PFY->setText(QString::number(entity->getPoint2()[1]));
        line_PFZ->setText(QString::number(entity->getPoint2()[2]));

        layout->addWidget(new QLabel("Point I:"), pos, 0, 1, 0);
        layout->addWidget(line_PIX, pos, 1, 1, 1);
        layout->addWidget(line_PIY, pos, 2, 1, 2);
        layout->addWidget(line_PIZ, pos++, 3, 1, 3);
        layout->addWidget(new QLabel("Point F:"), pos, 0);
        layout->addWidget(line_PFX, pos, 1);
        layout->addWidget(line_PFY, pos, 2);
        layout->addWidget(line_PFZ, pos++, 3);
    } else if(entity->getType()==4){

    }

    connect(btn_apply, SIGNAL(clicked(bool)), this, SLOT(onSaveEntity()));

    layout->addWidget(btn_apply, pos++, 0, 1, 2);
    layout->addItem(my_spacer, pos++,0, 1, 1);

    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(widget_layout);

    //QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    //areaActionWidget->setLayout(layout);
    //centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    //comboTimestep->setCurrentText(QString::number(entity->getTimestep()));
    updateViewers(0);
}

void ResultsWidget::loadEntity(int id){
    QString strEntity = "Entity_" + QString::number(id) + "/";

    ResultEntity *entity = new ResultEntity(id, settingsUser->value(strEntity + "type").toInt(), settingsUser->value(strEntity + "name").toString());

    entity->setOpacity(settingsUser->value(strEntity + "opacity").toInt());
    entity->setMetric(settingsUser->value(strEntity + "metric").toString());
    entity->setTimestep(settingsUser->value(strEntity + "timestep").toInt());
    entity->setChecked(settingsUser->value(strEntity + "checked").toBool());

    QVariant variant;
    variant.setValue(entity);
    QStandardItem *item = new QStandardItem;
    item->setEditable(false);
    item->setData(QVariant::fromValue(entity));  // this defaults to Qt::UserRole + 1

    model_entities->appendRow(item);
    item->setText(entity->getName());
    if(entity->getType()==3){
        item->setCheckable(true);
        if (entity->isChecked())
            item->setCheckState(Qt::Checked);
    }

    if (entity->getType()==1){
        //entity->setMinval(settingsUser->value(strEntity + "min").toDouble());
        entity->setMaxval(settingsUser->value(strEntity + "max").toDouble());
    } else if(entity->getType() == 2){
        double origin[3];
        double normal[3];
        origin[0] = settingsUser->value(strEntity + "origin0",0).toDouble();
        origin[1] = settingsUser->value(strEntity + "origin1",0).toDouble();
        origin[2] = settingsUser->value(strEntity + "origin2",0).toDouble();
        normal[0] = settingsUser->value(strEntity + "normal0",0).toDouble();
        normal[1] = settingsUser->value(strEntity + "normal1",0).toDouble();
        normal[2] = settingsUser->value(strEntity + "normal2",0).toDouble();
        entity->setOrigin(origin);
        entity->setNormal(normal);
    } else if(entity->getType() ==  3){
        double point1[3];
        double point2[3];
        point1[0] = settingsUser->value(strEntity + "pIni0",0).toDouble();
        point1[1] = settingsUser->value(strEntity + "pIni1",0).toDouble();
        point1[2] = settingsUser->value(strEntity + "pIni2",0).toDouble();
        point2[0] = settingsUser->value(strEntity + "pEnd0",0).toDouble();
        point2[1] = settingsUser->value(strEntity + "pEnd1",0).toDouble();
        point2[2] = settingsUser->value(strEntity + "pEnd2",0).toDouble();
        entity->setPoint1(point1);
        entity->setPoint2(point2);
    }
}

void ResultsWidget::onSaveEntity(){
    //vtkViewer->reset();

    //this function only adds new objects over the model (previously displayed by updateViewers()).
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());
    QModelIndex index = listview_entities->currentIndex();
    emit model_entities->dataChanged(index,index); //call onModelDataChanged()
    QLineEdit *line = w->findChild<QLineEdit*>("entity_name");
    ResultEntity *entity = index.data(Qt::UserRole + 1).value<ResultEntity*>();
    entity->setName(line->text());
    settingsUser->setValue("Entities/cnt", model_entities->rowCount());
    QString strEntity = "Entity_" + QString::number(entity->getId()) + "/";
    settingsUser->setValue(strEntity + "type", entity->getType());
    settingsUser->setValue(strEntity + "name", entity->getName());
    settingsUser->setValue(strEntity + "opacity", entity->getOpacity());
    settingsUser->setValue(strEntity + "metric", entity->getMetric());
    settingsUser->setValue(strEntity + "timestep", entity->getTimestep());
    settingsUser->setValue(strEntity + "checked", entity->isChecked());

    if (entity->getType()==1){
        //QLineEdit *line = w->findChild<QLineEdit*>("line_volume_min");
        //entity->setMinval(line->text().toDouble());
        line = w->findChild<QLineEdit*>("line_volume_max");
        entity->setMaxval(line->text().toDouble());

        //settingsUser->setValue(strEntity + "min", entity->getMinval());
        settingsUser->setValue(strEntity + "max", entity->getMaxval());

        if (vtkViewer->isoVolume)
            vtkViewer->m_renderer->RemoveVolume(vtkViewer->isoVolume);
        vtkViewer->addIsovolume(entity->getMaxval());

    } else if(entity->getType()==2){
        double origin[3];
        double normal[3];

        QToolBox *box = w->findChild<QToolBox *>("box_plane");
        int box_index = box->currentIndex();

        if (box_index == 0) { //simple
            QComboBox *combo = w->findChild<QComboBox *>("combo_plane");
            int combo_index = combo->currentIndex();
            settingsUser->setValue(strEntity + "basicindex", combo_index);
            QLineEdit *line_simple_value = w->findChild<QLineEdit *>("line_simple_value");
            int simple_value = line_simple_value->text().toInt();
            if (combo_index == 0){
                normal[0] = 0;
                normal[1] = 0;
                normal[2] = 1;
                origin[0] = 0;
                origin[1] = 0;
                origin[2] = simple_value;
            } else if (combo_index == 1) {
                normal[0] = 0;
                normal[1] = 1;
                normal[2] = 0;
                origin[0] = 0;
                origin[1] = simple_value;
                origin[2] = 0;
            } else {
                normal[0] = 1;
                normal[1] = 0;
                normal[2] = 0;
                origin[0] = simple_value;
                origin[1] = 0;
                origin[2] = 0;
            }
        } else { // advanced
            QLineEdit *line_normal_x = w->findChild<QLineEdit *>("line_normal_x");
            normal[0] = line_normal_x->text().toInt();
            QLineEdit *line_normal_y = w->findChild<QLineEdit *>("line_normal_y");
            normal[1] = line_normal_y->text().toInt();
            QLineEdit *line_normal_z = w->findChild<QLineEdit *>("line_normal_z");
            normal[2] = line_normal_z->text().toInt();
            QLineEdit *line_origin_x = w->findChild<QLineEdit *>("line_origin_x");
            origin[0] = line_origin_x->text().toInt();
            QLineEdit *line_origin_y = w->findChild<QLineEdit *>("line_origin_y");
            origin[1] = line_origin_y->text().toInt();
            QLineEdit *line_origin_z = w->findChild<QLineEdit *>("line_origin_z");
            origin[2] = line_origin_z->text().toInt();
        }

        entity->setNormal(normal);
        entity->setOrigin(origin);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << origin[2] << endl;

        settingsUser->setValue(strEntity + "planemode", box_index);
        settingsUser->setValue(strEntity + "normal0", entity->getNormal()[0]);
        settingsUser->setValue(strEntity + "normal1", entity->getNormal()[1]);
        settingsUser->setValue(strEntity + "normal2", entity->getNormal()[2]);
        settingsUser->setValue(strEntity + "origin0", entity->getOrigin()[0]);
        settingsUser->setValue(strEntity + "origin1", entity->getOrigin()[1]);
        settingsUser->setValue(strEntity + "origin2", entity->getOrigin()[2]);

        if (vtkViewer->planeActor)
            vtkViewer->m_renderer->RemoveActor(vtkViewer->planeActor);
        vtkViewer->addPlane(entity->getOrigin(), entity->getNormal());

    } else if(entity->getType()==3){
        double pI[3];
        double pF[3];

        QLineEdit *line_edit = w->findChild<QLineEdit *>("PIX");
        pI[0] = line_edit->text().toDouble();
        line_edit = w->findChild<QLineEdit*>("PIY");
        pI[1] = line_edit->text().toDouble();
        line_edit = w->findChild<QLineEdit*>("PIZ");
        pI[2] = line_edit->text().toDouble();
        line_edit = w->findChild<QLineEdit*>("PFX");
        pF[0] = line_edit->text().toDouble();
        line_edit = w->findChild<QLineEdit*>("PFY");
        pF[1] = line_edit->text().toDouble();
        line_edit = w->findChild<QLineEdit*>("PFZ");
        pF[2] = line_edit->text().toDouble();

        entity->setPoint1(pI);
        entity->setPoint2(pF);

        settingsUser->setValue(strEntity + "pIni0", entity->getPoint1()[0]);
        settingsUser->setValue(strEntity + "pIni1", entity->getPoint1()[1]);
        settingsUser->setValue(strEntity + "pIni2", entity->getPoint1()[2]);
        settingsUser->setValue(strEntity + "pEnd0", entity->getPoint2()[0]);
        settingsUser->setValue(strEntity + "pEnd1", entity->getPoint2()[1]);
        settingsUser->setValue(strEntity + "pEnd2", entity->getPoint2()[2]);

        if (vtkViewer->lineActor)
            vtkViewer->m_renderer->RemoveActor(vtkViewer->lineActor);
        entity->probe = vtkViewer->addLine(entity->getPoint1(), entity->getPoint2());
        updateChartViewer();
    } else if(entity->getType()==4){
        if (vtkViewer->sludgeActor)
            vtkViewer->m_renderer->RemoveActor(vtkViewer->sludgeActor);
        vtkViewer->addSludge();
    }

    vtkViewer->update();

    /*
    int cnt = listview_entities->model()->rowCount();
    for(int r = 0; r < cnt; ++r) {
            QModelIndex index = model_entities->index(r,0);
            QVariant name = model_entities->data(index);
            qDebug() << name;
            // here is your applicable code
    }
    */
}
/*
void ResultsWidget::updateTabGraphs(){

}
*/

void ResultsWidget::updateComboTimesteps(){
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::NoSort);  // will sort manually with std::sort
    QStringList entryList = dir.entryList();

    QCollator collator;
    collator.setNumericMode(true);
    std::sort(
                entryList.begin(),
                entryList.end(),
                [&collator](const QString &file1, const QString &file2)
    {
        return collator.compare(file1, file2) < 0;
    });

    QString strLastTimestep;
    foreach(QString filename, entryList){
        if(list_excluded_dirs.contains(filename)){
        } else{
            comboTimestep->addItem(filename);
            strLastTimestep = filename;
        }
    }
    combo_metric->clear();

    QDir metricsDir(Globals::instance()->getAppProjectPath() + strLastTimestep);
    foreach(const QFileInfo &info, metricsDir.entryInfoList(QDir::Files)) {
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< info.fileName() << endl;
        combo_metric->addItem(info.fileName());
    }
}

void ResultsWidget::onRefresh(){
    comboTimestep->clear();

    updateComboTimesteps();

    totalSteps = comboTimestep->count();
    lastStep = comboTimestep->itemText(totalSteps-1).toInt();
    singleStep = ceil((1.0 * lastStep) / totalSteps);

    sliderTimestep->setRange(firstStep, lastStep);
    sliderTimestep->setSingleStep(singleStep);
    QWidget *parent = (QWidget *)sliderTimestep->parent();
    QListIterator<QObject *> it(parent->children());
    int cnt = 0; //count labels
    while (it.hasNext()){
        QObject *o = it.next();
        if (QLatin1String(o->metaObject()->className()) == "QLabel"){
            cnt++;
            if (cnt==2){
                QLabel *label = (QLabel*) o;
                label->setText(QString::number(lastStep));
            }
        }
    }

    btnPrevstep->setDisabled(true);
    btnPlay->setDisabled(false);
    if(totalSteps == 1){
        btnPlay->setDisabled(true);
        btnNextstep->setDisabled(true);
    } else {
        btnPlay->setDisabled(false);
        btnNextstep->setDisabled(false);
        widget_slider->setHidden(false);
    }
    btnPause->setDisabled(true);
    btnStop->setDisabled(true);
    btnRefresh->setDisabled(false);

    QWidget *w = qobject_cast<QWidget *>((QPushButton*)(qobject_cast<QPushButton*>(sender()))->parent());
    if (QPushButton *btn = w->findChild<QPushButton *>("btn_simulation"))
        if(! isSimulationRunning())
            btn->setHidden(true);
}

void ResultsWidget::onTabCentralChanged(int tabIndex){
    return;
    if (tabIndex == 2){
        if(model_entities){
            centralWidget->findChild<QScrollArea*>("area_buttons")->findChild<QWidget*>("widget_media")->setHidden(true);

            QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
            QGridLayout *layout = new QGridLayout();
            int pos = 0;
            layout->addWidget(new QLabel("Showing only the first 4 checked lines."), pos++, 0);

            QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
            areaActionWidget->setLayout(layout);
            centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

            int cnt = model_entities->rowCount();
            for(int r = 0; r < cnt; ++r) {
                QModelIndex index = model_entities->index(r,0);
                ResultEntity *entity = index.data(Qt::UserRole + 1).value<ResultEntity*>();
                if (entity->getType() == 3){ //line
                    //QStandardItem *item = new QStandardItem;
                    //item->setData(QVariant::fromValue(entity));
                    if (entity->isChecked()){
                        //QGridLayout *layout_checked = new QGridLayout();
                        layout->addWidget(new QLabel(entity->getName()), pos++, 0);
                        //layout->addLayout(layout_checked);
                        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << entity->getName() << endl;
                    }
                }
            }
            layout->addItem(my_spacer, pos++, 0);
        }
    } else {
        centralWidget->findChild<QScrollArea*>("area_buttons")->findChild<QWidget*>("widget_media")->setHidden(false);
        QModelIndex indexOfTheCellIWant = model_entities->index(0,0);
        onEntityChanged(indexOfTheCellIWant);
        listview_entities->setCurrentIndex(indexOfTheCellIWant);
    }
}

void ResultsWidget::updateChartViewer(){
    QList<ResultEntity*> *list_entities = new QList<ResultEntity*>();
    if(tabCentral->currentIndex() == 2){
        int cnt = model_entities->rowCount();
        for(int r = 0; r < cnt; ++r) {
            QModelIndex index = model_entities->index(r,0);
            ResultEntity *entity = index.data(Qt::UserRole + 1).value<ResultEntity*>();
            if (entity->getType() == 3){ //line
                if (entity->isChecked()){
                    list_entities->append(entity);
                    if (!entity->probe)
                        entity->probe = vtkViewer->addLine(entity->getPoint1(), entity->getPoint2());
                    if(list_entities->size() == 4)
                        break;
                }
            }
        }
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << list_entities->size() << endl;
        vtkGraphs->addGraph(list_entities);
    }
}

void ResultsWidget::updateViewers(int type){
    int opacity = slider_base_opacity->value();
    QString timestep = comboTimestep->currentText();
    QString metric = combo_metric->currentText();

    vtkViewer->reset();
    if (checkMetricInTimestep(metric, timestep)) {
        vtkViewer->renderOpenFoamReader(timestep, opacity, metric);

        if (boolShowVolume){
            vtkViewer->viewerAddVolume(line_volume_value->text().toDouble());
        }
        if (boolShowSurface){
            vtkViewer->viewerAddSurface(line_surface_value->text().toDouble());
        }
        if (boolShowPlane){
            vtkViewer->addPlane(origin, normal);
        }
        if (boolShowLine){
            saveLine();
            vtkSmartPointer<vtkProbeFilter> probe = vtkViewer->addLine(p1, p2);
            vtkGraphs->reset();
            vtkGraphs->addGraph(probe, combo_metric->currentText());
            vtkGraphs->update();
        }
    }
    vtkViewer->update();
}

void ResultsWidget::showSurfaceObject(){
    if(boolShowSurface)
        boolShowSurface = false;
    else
        boolShowSurface = true;
    updateViewers(0);
}

void ResultsWidget::showVolumeObject(){
    if(boolShowVolume)
        boolShowVolume = false;
    else
        boolShowVolume = true;
    updateViewers(0);
}

QWidget *ResultsWidget::showVolumeTab(){
    QWidget * widget = new QWidget();
    QGridLayout *layout = new QGridLayout();

    line_volume_value = new QLineEdit();
    QPushButton *btn = new QPushButton("Show/Hide");
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(showVolumeObject()));

    int pos=0;
    layout->addWidget(new QLabel("Isovolume Value:"), pos, 0);
    layout->addWidget(line_volume_value, pos, 1);
    layout->addWidget(btn, ++pos, 0, 1, 0, Qt::AlignCenter);

    widget->setLayout(layout);
    return widget;
}
QWidget *ResultsWidget::showPlaneTab(){
    int lineEditWidth = 30;

    QWidget * widget = new QWidget();
    QGridLayout *layout = new QGridLayout();

    QPushButton* btnUpd = new QPushButton("Update");
    connect(btnUpd, SIGNAL(clicked(bool)), this, SLOT(updatePlaneClicked()));

    /***********************************************************************/
    QWidget *wBasic = new QWidget();
    QVBoxLayout *lBasic = new QVBoxLayout();
    wBasic->setLayout(lBasic);
    wBasic->setFixedHeight(60);

    QHBoxLayout *layout_select = new QHBoxLayout();
    QComboBox *combo_plane = new QComboBox();
    combo_plane->setObjectName("combo_plane");
    combo_plane->addItem("XY");
    combo_plane->addItem("XZ");
    combo_plane->addItem("YZ");
    connect(combo_plane, SIGNAL(currentIndexChanged(int)), this, SLOT(onBasicPlaneChanged(int)));
    QLabel *label_value = new QLabel("Z:");
    label_value->setObjectName("label_basic_plane_value");
    QLineEdit *line_value = new QLineEdit();
    line_value->setFixedWidth(lineEditWidth);
    line_value->setObjectName("line_simple_value");
    line_value->setText("0");
    layout_select->addWidget(new QLabel("Plane:"));
    layout_select->addWidget(combo_plane);
    layout_select->addWidget(label_value);
    layout_select->addWidget(line_value);
    layout_select->addStretch();

    QHBoxLayout *layout_rotate = new QHBoxLayout();
    layout_rotate->addWidget(new QLabel("Rotate clockwise (º):"));
    layout_rotate->addWidget(new QLineEdit());

    lBasic->addWidget(new QLabel("Select a plane and set the value:"));
    lBasic->addLayout(layout_select);

    /***********************************************************************/
    QWidget *wAdvanced = new QWidget();
    QVBoxLayout *lAdvanced = new QVBoxLayout();
    wAdvanced->setLayout(lAdvanced);

    QLineEdit* line_origin_x = new QLineEdit;
    line_origin_x->setObjectName("line_origin_x");
    line_origin_x->setText("0");
    line_origin_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_x->setFixedWidth(lineEditWidth);
    QLineEdit* line_origin_y = new QLineEdit;
    line_origin_y->setObjectName("line_origin_y");
    line_origin_y->setText("0");
    line_origin_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_y->setFixedWidth(lineEditWidth);
    QLineEdit* line_origin_z = new QLineEdit;
    line_origin_z->setObjectName("line_origin_z");
    line_origin_z->setText("0");
    line_origin_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_origin_z->setFixedWidth(lineEditWidth);
    QHBoxLayout *layout_origin = new QHBoxLayout();
    layout_origin->addWidget(new QLabel("Origin"));
    layout_origin->addWidget(new QLabel("X:"));
    layout_origin->addWidget(line_origin_x);
    layout_origin->addWidget(new QLabel("Y:"));
    layout_origin->addWidget(line_origin_y);
    layout_origin->addWidget(new QLabel("Z:"));
    layout_origin->addWidget(line_origin_z);
    lAdvanced->addLayout(layout_origin);

    QLineEdit* line_normal_x = new QLineEdit;
    line_normal_x->setObjectName("line_normal_x");
    line_normal_x->setText("0");
    line_normal_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_x->setFixedWidth(lineEditWidth);
    QLineEdit* line_normal_y = new QLineEdit;
    line_normal_y->setObjectName("line_normal_y");
    line_normal_y->setText("0");
    line_normal_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_y->setFixedWidth(lineEditWidth);
    QLineEdit* line_normal_z = new QLineEdit;
    line_normal_z->setObjectName("line_normal_z");
    line_normal_z->setText("0");
    line_normal_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_normal_z->setFixedWidth(lineEditWidth);
    QHBoxLayout *layout_normal = new QHBoxLayout();
    layout_normal->addWidget(new QLabel("Normal"));
    layout_normal->addWidget(new QLabel("X:"));
    layout_normal->addWidget(line_normal_x);
    layout_normal->addWidget(new QLabel("Y:"));
    layout_normal->addWidget(line_normal_y);
    layout_normal->addWidget(new QLabel("Z:"));
    layout_normal->addWidget(line_normal_z);
    lAdvanced->addLayout(layout_normal);
    /***********************************************************************/

    QToolBox *boxPlane = new QToolBox();
    boxPlane->setObjectName("box_plane");
    boxPlane->addItem(wBasic, "Basic configuration");
    boxPlane->addItem(wAdvanced, "Advanced configuration");
    //boxPlane->setStyleSheet(Globals::instance()->getCssToolBox());

    ///////////////////////////////////////////////////////////////////////////////
    //only for testing purposes
    /*
    boxPlane->setCurrentIndex(1);
    sliderTimestep->setValue(491);
    slider_base_opacity->setValue(0);
    line_normal_y->setText("1");
    line_origin_y->setText("5.4");
    */
    ///////////////////////////////////////////////////////////////////////////////

    layout->addWidget(boxPlane,0,0);
    layout->addWidget(btnUpd, 1, 0, 1, 0, Qt::AlignCenter);

    widget->setLayout(layout);
    return widget;
}

QWidget *ResultsWidget::showLineTab(){
    int size_line = 40;

    QWidget * widget = new QWidget();
    QGridLayout *layout = new QGridLayout();

    QLineEdit* line_PIX = new QLineEdit;
    line_PIX->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PIX->setFixedWidth(size_line);
    line_PIX->setObjectName("PIX");
    line_PIX->setText(settingsUser->value("ResultsLine/p1x",0).toString());
    QLineEdit* line_PIY = new QLineEdit;
    line_PIY->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PIY->setFixedWidth(size_line);
    line_PIY->setObjectName("PIY");
    line_PIY->setText(settingsUser->value("ResultsLine/p1y",0).toString());
    QLineEdit* line_PIZ = new QLineEdit;
    line_PIZ->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PIZ->setFixedWidth(size_line);
    line_PIZ->setObjectName("PIZ");
    line_PIZ->setText(settingsUser->value("ResultsLine/p1z",0).toString());

    QLineEdit* line_PFX = new QLineEdit;
    line_PFX->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PFX->setFixedWidth(size_line);
    line_PFX->setObjectName("PFX");
    line_PFX->setText(settingsUser->value("ResultsLine/p2x",0).toString());
    QLineEdit* line_PFY = new QLineEdit;
    line_PFY->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PFY->setFixedWidth(size_line);
    line_PFY->setObjectName("PFY");
    line_PFY->setText(settingsUser->value("ResultsLine/p2y",0).toString());
    QLineEdit* line_PFZ = new QLineEdit;
    line_PFZ->setValidator(Globals::instance()->getDoubleValidator(this));
    line_PFZ->setFixedWidth(size_line);
    line_PFZ->setObjectName("PFZ");
    line_PFZ->setText(settingsUser->value("ResultsLine/p2z",0).toString());

    int pos=0;
    layout->addWidget(new QLabel("Point I:"), pos, 0, 1, 0);
    layout->addWidget(line_PIX, pos, 1, 1, 1);
    layout->addWidget(line_PIY, pos, 2, 1, 2);
    layout->addWidget(line_PIZ, pos++, 3, 1, 3);
    layout->addWidget(new QLabel("Point F:"), pos, 0);
    layout->addWidget(line_PFX, pos, 1);
    layout->addWidget(line_PFY, pos, 2);
    layout->addWidget(line_PFZ, pos++, 3);

    QPushButton* btnUpd = new QPushButton("Update");
    connect(btnUpd, SIGNAL(clicked(bool)), this, SLOT(updateLineClicked()));
    layout->addWidget(btnUpd, pos++, 0, 1, 0, Qt::AlignCenter);

    widget->setLayout(layout);

    return widget;
}

void ResultsWidget::updateLineClicked(){
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    QWidget *w = qobject_cast<QWidget *>(btn->parent());

    QLineEdit *line_normal_x = w->findChild<QLineEdit *>("PIX");
    p1[0] = line_normal_x->text().toDouble();
    QLineEdit *line_normal_y = w->findChild<QLineEdit *>("PIY");
    p1[1] = line_normal_y->text().toDouble();
    QLineEdit *line_normal_z = w->findChild<QLineEdit *>("PIZ");
    p1[2] = line_normal_z->text().toDouble();
    QLineEdit *line_origin_x = w->findChild<QLineEdit *>("PFX");
    p2[0] = line_origin_x->text().toDouble();
    QLineEdit *line_origin_y = w->findChild<QLineEdit *>("PFY");
    p2[1] = line_origin_y->text().toDouble();
    QLineEdit *line_origin_z = w->findChild<QLineEdit *>("PFZ");
    p2[2] = line_origin_z->text().toDouble();

    if(boolShowLine)
        boolShowLine = false;
    else
        boolShowLine = true;
    updateViewers(0);
}

QWidget *ResultsWidget::showSurfaceTab(){
    QWidget * widget = new QWidget();
    QGridLayout *layout = new QGridLayout();

    line_surface_value = new QLineEdit();
    QPushButton *btn = new QPushButton("Show/Hide");
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(showSurfaceObject()));
    //boolShowSurface = true;

    int pos=0;
    layout->addWidget(new QLabel("Contours:"), pos, 0);
    layout->addWidget(line_surface_value, pos, 1);
    layout->addWidget(btn, ++pos, 0, 1, 0, Qt::AlignCenter);

    widget->setLayout(layout);

    return widget;
}
