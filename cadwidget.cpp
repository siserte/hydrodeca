#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#define vtkFloatingPointType double

#include "borderlayout.h"
#include "cadwidget.h"
#include "globals.h"
#include "menuwidget.h"
#include "ui_defaultwindow.h"

#include <QCheckBox>
#include <qDebug>
#include <QDir>
#include <QFile>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QTextBrowser>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QScrollArea>
#include <QSignalMapper>
#include <QSplitter>
#include <QString>
#include <unordered_map>
#include <QVBoxLayout>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBndLib.hxx>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepCheck_Analyzer.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>

#include <BRepLib.hxx>

#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>

#include <BRepPrim_Cone.hxx>
#include <BRepPrimAPI_MakeOneAxis.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>
#include <BRepTools.hxx>

#include <BRepOffsetAPI_ThruSections.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <gp_Ax1.hxx>
#include <gp_Ax3.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_DisplayModeFilter.hxx>
#include <IVtkTools_ShapeDataSource.hxx>
//#include <IVtkOCC_ShapePickerAlgo.hxx>
#include <IVtkVTK_View.hxx>
#include <IVtkTools_ShapePicker.hxx>

#include <MeshVS_MeshPrsBuilder.hxx>
#include <MeshVS_DrawerAttribute.hxx>

#include <OSD_Path.hxx>

#include <QVTKWidget.h>

#include <RWStl.hxx>

#include <STEPCAFControl_Writer.hxx>

#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <StlMesh_Mesh.hxx>
#include <StlMesh_MeshExplorer.hxx>
#include <StlTransfer.hxx>

#include <TDocStd_Document.hxx>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkExecutive.h>
#include <vtkFloatArray.h>
#include <vtkFloatingPointExceptions.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkInteractorStyleImage.h>
#include <vtkJPEGReader.h>
#include <vtkLabelHierarchy.h>
#include <vtkLabelPlacementMapper.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPointData.h>
#include <vtkPointSetToLabelHierarchy.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkPolyDataReader.h>
#include <vtkProperty.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStringArray.h>
#include <vtkStructuredGrid.h>
#include <vtkTextProperty.h>
#include <vtkTextRenderer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkVersion.h>

#include <XCAFApp_Application.hxx>
#include <XCAFDoc_DocumentTool.hxx>
#include <XCAFDoc_ShapeTool.hxx>

#include <XSDRAWSTLVRML_DataSource.hxx>

#include <vtkPolygon.h>
#include <math.h>
#include <algorithm>

#define MAX2(X, Y)      (Abs(X)>Abs(Y)?Abs(X):Abs(Y))
#define MAX3(X, Y, Z)   (MAX2(MAX2(X,Y),Z))

#include <QFileDialog>
#include <STEPControl_Reader.hxx>
#include <TColStd_HSequenceOfTransient.hxx>
#include <TopTools_DataMapOfShapeInteger.hxx>
#include <vtkWorldPointPicker.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkObjectFactory.h>
#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_ShapeDataSource.hxx>
#include <IVtkTools_ShapeObject.hxx>
#include <IVtkTools_ShapePicker.hxx>
#include <IVtkTools_SubPolyDataFilter.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>
#include <vtkLookupTable.h>
#include <vtkTextActor.h>

#include <vtkLegendBoxActor.h>
#include <vtkSphereSource.h>
#include <array>
#include <vtkPlaneSource.h>
#include <vtkNamedColors.h>
#include <vtkCubeSource.h>
#include <QtConcurrent/QtConcurrent>
#include <QProgressDialog>
#include <QProgressBar>
#include <QComboBox>
#include <QSlider>

#include <OSD.hxx>

CadPreset1Widget::CadPreset1Widget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, QToolBox *reportWidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    reportInfo = reportWidget;
    console = consoleWidget;
}

CadPreset1Widget::~CadPreset1Widget(){

}

void CadPreset1Widget::loadMainWindow(){
    createAreaViewers();
    centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(0);
    menu->updateMenu(Globals::instance()->getTabCad(), this);
    importProperties();
}

void CadPreset1Widget::createAreaViewers(){
    if(vtkViewer != 0){
        vtkViewer->reset();
        vtkViewer->update();
    }
}

void CadPreset1Widget::
initFacesListArea(){
    table_widget = new QTableWidget();
    //table_widget->setSelectionMode(QAbstractItemView::SingleSelection);
    table_widget->setRowCount(list_assigned_faces->size());
    table_widget->setColumnCount(1);
    table_widget->setFixedWidth(100);
    table_widget->setFixedHeight(table_widget->verticalHeader()->sectionSize(0) * (table_widget->rowCount()+1));
    table_widget->verticalHeader()->hide();
    QStringList horzHeaders;
    horzHeaders << "List of faces";
    table_widget->setHorizontalHeaderLabels(horzHeaders);
    table_widget->horizontalHeader()->setDisabled(true);

    for (int i=0; i<table_widget->rowCount(); i++){
        QTableWidgetItem *newItem = new QTableWidgetItem("Face: " + QString::number(i));
        newItem->setFlags(newItem->flags() ^ Qt::ItemIsEditable);
        table_widget->setItem(i, 0, newItem);
    }

    connect(table_widget, SIGNAL(cellClicked(int,int)), this, SLOT(showGeometryFaces(int)));
    faces_layout->addWidget(table_widget);
    QPushButton *btn_addGroup = new QPushButton("Group");
    connect(btn_addGroup, SIGNAL(clicked(bool)), this, SLOT(addPhysicalGroup()));
    faces_layout->addWidget(new QLabel("Press Ctrl key\nto select multiple"));
    faces_layout->addWidget(btn_addGroup);
    faces_layout->addStretch();
}

void CadPreset1Widget::clearLayout(QLayout *layout){
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}

void CadPreset1Widget::openImportDialog(){
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__ << endl;

    vtkViewer->reset();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Descomentar los siguiente" << endl;
    QString pathFileName =
            QFileDialog::getOpenFileName(this, tr("Import geometry"), "", tr("STEP files (*.step *.stp)"));
    if (pathFileName == NULL)
        return;

    //QString pathFileName = "C:/Users/siserte/Desktop/pieza2.step" ;
    //QString pathFileName = "C:/Users/siserte/Desktop/component8.step" ;
    //QString pathFileName = "C:/Users/siserte/Desktop/Decantador_escalado.STEP" ;

    STEPControl_Reader reader;
    reader.ReadFile(pathFileName.toStdString().c_str());

    //clearLayout(faces_layout);

    reader.NbRootsForTransfer();
    reader.TransferRoots();

    aShape = reader.OneShape();
    BRepTools::Clean(aShape);

    clearLayout(faces_layout);

    QString path = Globals::instance()->getAppProjectPath();
    QDir dir(path);
    dir.setNameFilters(QStringList() << "*.step" << "*.STEP" << "*.Step" << "*.stp" << "*.STP" << "*.Stp");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
        dir.remove(dirFile);

    QString dstFile = Globals::instance()->getAppProjectPath() + "geometry.step";
    QFile::copy(pathFileName, dstFile);
    QFile file(Globals::instance()->getAppProjectPath()+"/infoSavedCAD.ini");
    file.open(QFile::WriteOnly|QFile::Truncate);
    file.close();

    initColorList();
    showGeometryFaces();
    initFacesListArea();
    cnt_groups=0;
    slider_opacity->setDisabled(false);

    QSettings *CADsettings = new QSettings(file.fileName(), QSettings::IniFormat);
    CADsettings->setValue("CADFile/path", dstFile);
    CADsettings->setValue("PhysicalGroups/count", QString::number(cnt_groups));
}

void CadPreset1Widget::loadImportDialog(QString fileName){
    vtkViewer->reset();

    QSettings *CADsettings = new QSettings(fileName, QSettings::IniFormat);
    QString pathFileName = Globals::instance()->getAppProjectPath() + "geometry.step";
    OSD::SetSignal(false);

    STEPControl_Reader reader;
    IFSelect_ReturnStatus status = reader.ReadFile(pathFileName.toStdString().c_str());
    if (status == IFSelect_RetError || status == IFSelect_RetFail || status == IFSelect_RetVoid)
        qDebug() << "ERROR STEP file:" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << pathFileName << endl;
    Standard_Integer nbr;
    try {
        nbr = reader.NbRootsForTransfer();
    } catch (Standard_Failure const& theFailure) {
      //esto ha pasado cuando una geometría se mueve de ordenador.
      //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << theFailure.GetMessageString() << endl;
      std::cerr << "STEP import failed: " << theFailure.GetMessageString() << "\n";
      QMessageBox *messageBox = new QMessageBox();
      messageBox->setIcon(QMessageBox::Warning);
      messageBox->setWindowTitle("Error! Geometry is corrupted!");
      messageBox->setText(theFailure.GetMessageString());
      messageBox->setStandardButtons(QMessageBox::Close);
      messageBox->exec();
      menu->goToHomeView();
    }

    reader.TransferRoots();
    aShape = reader.OneShape();
    BRepTools::Clean(aShape);

    clearLayout(faces_layout);
    initColorList();
    initFacesListArea();

    int cnt_groups = CADsettings->value("PhysicalGroups/count", 0).toInt();

    for (int i = 0; i < cnt_groups; i++){
        QString name = CADsettings->value("PG" + QString::number(i+1) + "/name").toString();
        QString faces = CADsettings->value("PG" + QString::number(i+1) + "/faces").toString();
        QString type = CADsettings->value("PG" + QString::number(i+1) + "/type").toString();
        loadPhysicalGroup(name, faces, type);

        //Give color to the faces
        QStringList listFaces = faces.split(";");
        for (const auto& face : listFaces)
            list_assigned_faces->replace(face.toInt(), true);
        int firstFace = listFaces.first().toInt();
        for (const auto& face : listFaces)
            faces_color_list->replace(face.toInt(), firstFace+1); //cero is for not assigned faces

    }
    showGeometryFaces();
    slider_opacity->setDisabled(false);
}

void CadPreset1Widget::loadPhysicalGroup(QString name, QString faces, QString type){
    QLineEdit *line_name = new QLineEdit();
    line_name->setObjectName("line_name");
    line_name->setText(name);

    QLineEdit *line_faces = new QLineEdit();
    line_faces->setObjectName("line_faces");
    line_faces->setText(faces);

    QPushButton *btn_validate = new QPushButton("Update");
    btn_validate->setObjectName("btn_validate");
    connect(btn_validate, SIGNAL(clicked(bool)), this, SLOT(updateGroupName()));
    //btn_validate->setFixedWidth(150);
    QPushButton *btn_remove = new QPushButton();
    connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(removeGroup()));
    btn_remove->setObjectName("btn_remove");
    btn_remove->setFixedWidth(27);
    btn_remove->setIcon(QIcon(":/Resources/remove.png"));
    btn_remove->setIconSize(QSize(17,17));

    QHBoxLayout *layout_buttons = new QHBoxLayout();
    layout_buttons->addWidget(btn_validate);
    layout_buttons->addWidget(btn_remove);

    QComboBox* combo_group_type = new QComboBox();
    combo_group_type->setObjectName("group_type");
    combo_group_type->addItem("wall");
    combo_group_type->addItem("patch");
    //combo_group_type->addItem("symmetry");
    combo_group_type->setCurrentText(type);

    QWidget *widget = new QWidget();
    widget->setStyleSheet("background-color: #CCCCCC");
    QVBoxLayout *layout_physicalGroup = new QVBoxLayout();
    layout_physicalGroup->addWidget(new QLabel("Name:"));
    layout_physicalGroup->addWidget(line_name);
    layout_physicalGroup->addWidget(new QLabel("Type:"));
    layout_physicalGroup->addWidget(combo_group_type);
    layout_physicalGroup->addWidget(new QLabel("Faces: " + faces));
    layout_physicalGroup->addWidget(line_faces);
    layout_physicalGroup->addLayout(layout_buttons);
    widget->setLayout(layout_physicalGroup);
    layout_groups->addWidget(widget);
    line_faces->setVisible(false);
}

void CadPreset1Widget::showGeometryFaces(int id){
    vtkViewer->reset();

    TopExp_Explorer exp;
    vtkSmartPointer<vtkPoints> centerPoints = vtkSmartPointer<vtkPoints>::New();
    vtkIdType pid;
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkStringArray> labels = vtkSmartPointer<vtkStringArray>::New();
    labels->SetName("labels");

    int i=0;
    for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next(), i++) {
        TopoDS_Face aFace = TopoDS::Face(exp.Current());

        GProp_GProps shellProps;
        BRepGProp::SurfaceProperties(aFace, shellProps);
        gp_Pnt centerOfMass = shellProps.CentreOfMass();
        //qDebug() << "Face: " << QString::number(i) << " - " << QString::number(centerOfMass.X()) << " / " << QString::number(centerOfMass.Y()) << " / " << QString::number(centerOfMass.Z());
        pid = centerPoints->InsertNextPoint(centerOfMass.X(), centerOfMass.Y(), centerOfMass.Z());
        vertices->InsertNextCell(1,&pid);
        /*
        Standard_Real umin, umax, vmin, vmax;
        BRepTools::UVBounds(aFace, umin, umax, vmin, vmax);
        BRepTools::UVBounds(aCurrentFace,umin, umax, vmin, vmax);
        Handle(Geom_Surface) aSurface = BRep_Tool::Surface(aFace);
        GeomLProp_SLProps props(aSurface, umin, vmin, 1, 0.01);
        aSurface.
        gp_Dir normal = props.Normal();
        qDebug() << "Face: " << QString::number(i) << " - " << QString::number(umin) << " / " << QString::number(umax) << " / " << QString::number(vmin) << " / " << QString::number(vmax);
        */
        //char str[20];
        //sprintf(str, "Face: %d", i);
        //labels->InsertNextValue(str);
        labels->InsertNextValue(QString::number(i).toStdString());

        BRep_Builder aBuilder;
        TopoDS_Compound aCompound;
        aBuilder.MakeCompound(aCompound);
        aBuilder.Add(aCompound,aFace);

        IVtkOCC_Shape::Handle shapeImpl = new IVtkOCC_Shape(aCompound);
        vtkSmartPointer<IVtkTools_ShapeDataSource> DS = vtkSmartPointer<IVtkTools_ShapeDataSource>::New();
        DS->SetShape(shapeImpl);

        vtkSmartPointer<vtkPolyDataMapper> Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        Mapper->SetInputConnection(DS->GetOutputPort());
        vtkSmartPointer<vtkActor> Actor = vtkSmartPointer<vtkActor>::New();
        Actor->SetMapper(Mapper);
        Actor->GetProperty()->SetOpacity(slider_opacity->value() / 100.0);

        vtkSmartPointer<vtkLookupTable> Table = IVtkTools::InitLookupTable();
        IVtkTools::InitShapeMapper(Mapper, Table);
        if (i == id) {
            IVtkTools::SetLookupTableColor(Table, MT_ShadedFace,
                                           color_list->last()[0],
                    color_list->last()[1],
                    color_list->last()[2]);
            Actor->GetProperty()->SetOpacity(1);
        } else {
            IVtkTools::SetLookupTableColor(Table, MT_ShadedFace,
                                           color_list->at(faces_color_list->at(i))[0],
                    color_list->at(faces_color_list->at(i))[1],
                    color_list->at(faces_color_list->at(i))[2]);
        }
        vtkViewer->add(Actor);
    }

    vtkSmartPointer<vtkPolyData> point_poly = vtkSmartPointer<vtkPolyData>::New();
    point_poly->SetPoints(centerPoints);
    point_poly->SetVerts(vertices);
    point_poly->GetPointData()->AddArray(labels);

    vtkSmartPointer<vtkTextProperty> tprop = vtkSmartPointer<vtkTextProperty>::New();
    tprop->SetFontSize(10);
    tprop->SetFontFamily( vtkTextProperty::GetFontFamilyFromString( "Arial" ) );
    tprop->SetColor( 0.0, 0.8, 0.2 );

    vtkSmartPointer<vtkPointSetToLabelHierarchy> pointSetToLabelHierarchyFilter = vtkSmartPointer<vtkPointSetToLabelHierarchy>::New();
    pointSetToLabelHierarchyFilter->SetInputData(point_poly);
    pointSetToLabelHierarchyFilter->SetLabelArrayName("labels");
    //pointSetToLabelHierarchyFilter->SetPriorityArrayName("sizes"); //en caso de solaparse dos etiquetas, se muestra la mayor por id
    pointSetToLabelHierarchyFilter->GetTextProperty()->SetColor(0,0,0);
    pointSetToLabelHierarchyFilter->GetTextProperty()->SetFontSize(14);
    pointSetToLabelHierarchyFilter->GetTextProperty()->BoldOn();
    //pointSetToLabelHierarchyFilter->Update();

    double labelRatio = 0.05;
    int iteratorType = vtkLabelHierarchy::QUEUE;

    vtkSmartPointer<vtkLabelPlacementMapper> labelMapper = vtkSmartPointer<vtkLabelPlacementMapper>::New();
    labelMapper->SetInputConnection(pointSetToLabelHierarchyFilter->GetOutputPort());
    labelMapper->SetIteratorType(iteratorType);
    labelMapper->SetMaximumLabelFraction(labelRatio);
    vtkSmartPointer<vtkActor2D> ActorText = vtkSmartPointer<vtkActor2D>::New();
    ActorText->SetMapper(labelMapper);
    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //put the coordinate axis to the left corner (remove if we want that the axis button works)
    vtkOrientationMarkerWidget *widget = vtkOrientationMarkerWidget::New();
    widget->SetDefaultRenderer(vtkViewer->m_renderer);
    widget->SetOrientationMarker(vtkViewer->axes);
    widget->SetInteractor(vtkViewer->GetRenderWindow()->GetInteractor());
    widget->EnabledOn();
    vtkViewer->setShowAxes(false);
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    generateViewerLegend();

    vtkViewer->add(ActorText); // == vtkViewer->m_renderer->AddActor(Actor);
    if(id < 0)
        vtkViewer->zoomToBox();
    vtkViewer->update();
}

void CadPreset1Widget::showFacesValidationError(QString aux){
    QMessageBox *errorBox = new QMessageBox();
    errorBox->setIcon(QMessageBox::Warning);
    errorBox->setWindowTitle("Error while parsing the file");
    errorBox->setText("The physical group cannot be validated.\n" + aux);
    errorBox->setStandardButtons(QMessageBox::Close);
    errorBox->exec();
}

void CadPreset1Widget::validateGroup(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    //Check the name field
    QLineEdit *line_name = (QLineEdit*)w->findChild<QWidget*>("line_name");
    if (line_name->text() == ""){
        showFacesValidationError("A group name is required.");
        return;
    }

    //Check and parse the faces field
    QLineEdit *line_faces = (QLineEdit*)w->findChild<QWidget*>("line_faces");
    if (line_faces->text() == ""){
        showFacesValidationError("No face selected.");
        return;
    }

    bool ok = true;
    QStringList faces = line_faces->text().split(";");
    for (const auto& face : faces) {
        if (list_assigned_faces->at(face.toInt())){
            showFacesValidationError("Face " + face + " is already assigned.");
            ok=false;
            break;
        }
    }

    if (! ok){
        line_faces->setText("none");
        QPushButton *btn_remove = (QPushButton*)w->findChild<QWidget*>("btn_remove");
        btn_remove->click();
        return;
    }

    QPushButton *btn_validate = (QPushButton*)w->findChild<QWidget*>("btn_validate");
    disconnect(btn_validate, SIGNAL(clicked(bool)), 0, 0);
    connect(btn_validate, SIGNAL(clicked(bool)), this, SLOT(updateGroupName()));

    //Update the label with not assigned faces
    for (const auto& face : faces)
        list_assigned_faces->replace(face.toInt(), true);

    //At this point the physical group has been correctly validated

    int firstFace = faces.first().toInt();
    for (const auto& face : faces){
        faces_color_list->replace(face.toInt(), firstFace+1); //cero is for not assigned faces
    }

    cnt_groups++;
    showGeometryFaces();
}

void CadPreset1Widget::updateGroupName(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    //Check the name field
    QLineEdit *line_name = (QLineEdit*)w->findChild<QWidget*>("line_name");
    if (line_name->text() == ""){
        showFacesValidationError("A group name is required.");
        return;
    }

    showGeometryFaces();
}


void CadPreset1Widget::writeSTLs(){
    QDir dir(Globals::instance()->getAppProjectPath() + "/constant/triSurface");
    dir.setNameFilters(QStringList() << "*.*");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
        dir.remove(dirFile);

    QString settingsFilePath = Globals::instance()->getAppProjectPath()+"/infoSavedCAD.ini";
    QFile file(settingsFilePath);
    file.open(QFile::Truncate);
    file.close();
    QSettings *CADsettings = new QSettings(settingsFilePath, QSettings::IniFormat);
    //CADsettings->setValue("CADFile/path", Globals::instance()->getAppProjectPath() + "geometry.step");

    int cnt=0;
    list_faces_names = new QList<QString>();
    for (int i = 0; i < layout_groups->count(); ++i) {
        QWidget *widget = layout_groups->itemAt(i)->widget();
        if (widget != NULL) {
            QLineEdit *line_name = (QLineEdit*)widget->findChild<QWidget*>("line_name");
            QLineEdit *line_faces = (QLineEdit*)widget->findChild<QWidget*>("line_faces");
            QComboBox *combo_group = (QComboBox*)widget->findChild<QWidget*>("group_type");
            QStringList faces = line_faces->text().split(";");

            BRep_Builder aBuilder;
            TopoDS_Compound aCompound;
            aBuilder.MakeCompound(aCompound);
            TopExp_Explorer exp;

            for (const auto& face : faces){
                int i=0;
                for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next(), i++) {
                    if(face.toInt() == i){
                        TopoDS_Face aFace = TopoDS::Face(exp.Current());
                        aBuilder.Add(aCompound,aFace);
                    }
                }
            }
            QString path = Globals::instance()->getAppProjectPath() + "/constant/triSurface/" + line_name->text() + ".stl";
            OSD_Path osd_path(path.toStdString().c_str());
            Handle(StlMesh_Mesh) stl_mesh = new StlMesh_Mesh;
            BRepMesh_IncrementalMesh* brep_mesh = new BRepMesh_IncrementalMesh(aCompound, 0.01, true, 2, true);
            StlTransfer::RetrieveMesh(brep_mesh->Shape(), stl_mesh);
            RWStl::WriteAscii(stl_mesh, osd_path);

            list_faces_names->append(line_name->text());
            str_blockmesh = str_blockmesh + "\t" + line_name->text() + "{\n\t\ttype " + combo_group->currentText() + ";\n\t\tfaces\t();}\n";
            str_snappy = str_snappy + "\t" + line_name->text() + ".stl {\n\t\tname\t" + line_name->text() + ";\n\t\ttype\ttriSurfaceMesh; }\n";

            cnt++;
            CADsettings->setValue("PG" + QString::number(cnt) + "/name", line_name->text());
            CADsettings->setValue("PG" + QString::number(cnt) + "/faces", line_faces->text());
            CADsettings->setValue("PG" + QString::number(cnt) + "/type", combo_group->currentText());
        }
    }
    CADsettings->setValue("PhysicalGroups/count", cnt);

    //Default
    BRep_Builder aBuilder;
    TopoDS_Compound aCompound;
    aBuilder.MakeCompound(aCompound);
    TopExp_Explorer exp;

    int i=0;
    for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next(), i++) {
        if(faces_color_list->at(i) == 0){
            TopoDS_Face aFace = TopoDS::Face(exp.Current());
            aBuilder.Add(aCompound,aFace);
        }
    }

    QString path = Globals::instance()->getAppProjectPath() + "/constant/triSurface/Default.stl";
    OSD_Path osd_path(path.toStdString().c_str());
    Handle(StlMesh_Mesh) stl_mesh = new StlMesh_Mesh;
    BRepMesh_IncrementalMesh* brep_mesh = new BRepMesh_IncrementalMesh(aCompound, 0.01, true, 2, true);
    StlTransfer::RetrieveMesh(brep_mesh->Shape(), stl_mesh);
    RWStl::WriteAscii(stl_mesh, osd_path);

    str_blockmesh += "\tDefault{\n\t\ttype wall;\n\t\tfaces\t();}\n";
    str_snappy += "\tDefault.stl {\n\t\tname\tDefault;\n\t\ttype\ttriSurfaceMesh; }\n";

    // All
    QString cadpath = Globals::instance()->getAppProjectPath() + "/constant/triSurface/cad.stl";
    OSD_Path osd_cadpath(cadpath.toStdString().c_str());
    stl_mesh = new StlMesh_Mesh;
    brep_mesh = new BRepMesh_IncrementalMesh(aShape, 0.01, true, 2, true);
    StlTransfer::RetrieveMesh(brep_mesh->Shape(), stl_mesh);
    RWStl::WriteAscii(stl_mesh, osd_cadpath);
}

void CadPreset1Widget::createBlockMeshDictFile(){
    QString path = Globals::instance()->getAppProjectPath() + "/constant/polyMesh/blockMeshDict";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n\n";
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"constant/polyMesh\";\n\tobject\tblockMeshDict;}"
                  "\n#include\t\"../../interfaceGUI\"\nconvertToMeters\t1;"
                  "\nvertices\t($V1 $V2 $V3 $V4 $V5 $V6 $V7 $V8);"
                  "\nboundary\t(\n\twalls{\n\t\ttype wall;\n\t\tfaces\t("
                  "\n\t\t\t(0 4 7 3)"
                  "\n\t\t\t(4 5 6 7)"
                  "\n\t\t\t(2 6 5 1)"
                  "\n\t\t\t(1 5 4 0)"
                  "\n\t\t\t(0 3 2 1)"
                  "\n\t\t\t(3 7 6 2));}\n";
        stream << str_blockmesh;
        stream << ");\nblocks\t(\n\thex (0 1 2 3 4 5 6 7) $NODES simpleGrading (1 1 1)\n);" << endl;
        file.close();
    }
}

void CadPreset1Widget::createSnappyHexMeshDictFile(){
    QString path = Globals::instance()->getAppProjectPath() + "/system/snappyHexMeshDict";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n\n";
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"system\";\n\tobject\tsnappyHexMeshDict;}"
                  "\n#include\t\"../snappyGUI\""
                  "\n#include\t\"../snappyOrders\""
                  "\n#include\t\"../interfaceGUI\"\n"
                  "\ncastellatedMesh\t$step1;"
                  "\nsnap\t$step2;"
                  "\naddLayers\t$step3;\n"
                  "\ngeometry {\n";
        stream << str_snappy;
        stream << "}\ncastellatedMeshControls {\n\tlocationInMesh\t($locationX $locationY $locationZ);\n\trefinementSurfaces {\n";
        for (int i=0; i<list_faces_names->size(); i++)
            stream << "\t\t" << list_faces_names->at(i) << " {\n\t\t\tlevel\t($min_" << list_faces_names->at(i) << " $max_" << list_faces_names->at(i) << "); }\n";
        stream << "\t\tDefault {\n\t\t\tlevel\t($min_Default $max_Default); }\n";
        stream << "\t}\n\t";
        //stream << "refinementRegions {\n\t\trefinementBox {\n\t\t\tmode inside;\n\t\t\tlevels ((1E15 $regionLevel));\n\t\t}\n\t}\n\t";
        stream << "refinementRegions { }\n\t";
        stream << "features ();\n";
        stream << "\tminRefinementCells	0;\n\tmaxGlobalCells	10000000;\n\tresolveFeatureAngle	5;\n\tnCellsBetweenLevels	1;\n\tmaxLocalCells	500000;\n\tallowFreeStandingZoneFaces	true;\n}\n";
        stream << "snapControls {\n\tnSmoothPatch $nSmooth;\n\ttolerance	$tolerance;\n\tnSolveIter	$nSnap;\n\tnRelaxIter	$nrSnap;\n\tnFeatureSnapIter	$nFeature;\n\timplicitFeatureSnap true;\n\texplicitFeatureSnap false;\n}\n";
        stream << "addLayersControls {\n\tlayers {\n\t\t";
        //stream << "$layers_info";
        for (int i=0; i<list_faces_names->size(); i++)
            stream << list_faces_names->at(i) << " {\n\t\t\tnSurfaceLayers $layers_" << list_faces_names->at(i) << "; }\n\t\t";
        stream << "Default {\n\t\t\tnSurfaceLayers $layers_Default; }\n";
        stream << "\t}\n\t";
        stream << "nSmoothSurfaceNormals	5;\n\tslipFeatureAngle	30.0;\n\tnBufferCellsNoExtrude	0;\n\tnRelaxIter	5;\n\trelativeSizes	false;\n\tminMedianAxisAngle	90.0;\n\tmaxFaceThicknessRatio	0.5;\n\tnSmoothNormals	3;\n\tmaxThicknessToMedialRatio	0.3;\n\tnLayerIter	50;\n\tminThickness	$minThick;\n\tnSmoothThickness	10;\n\tnGrow	10;\n\tnRelaxedIter	20;\n\tconcaveAngle	90.0;\n\tfeatureAngle	60;\n\tfirstLayerThickness	$thick;\n\texpansionRatio	$expansion;\n}\n";
        stream << "meshQualityControls {\n\tminTetQuality	1.0E-20;\n\tminVol	1.0E-14;\n\tmaxInternalSkewness	4.0;\n\tmaxBoundarySkewness	20.0;\n\tmaxConcave	80.0;\n\tminFaceWeight	0.05;\n\tminVolRatio	0.01;\n\tminTwist	0.05;\n\tminArea	-1.0;\n\tmaxNonOrtho	65.0;\n\tminTriangleTwist	-1.0;\n\tminDeterminant	0.01;\n\terrorReduction	0.75;\n\tnSmoothScale	4;\n\trelaxed {\n\t\tmaxNonOrtho	75.0; }\n}\n";
        stream << "mergeTolerance	1e-03;\ndebug	0;";
        file.close();
    }
}

void CadPreset1Widget::resetSettingsFileMesh(){
    QSettings *settings = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);

    settings->beginGroup("MeshSculpt");
    QStringList childKeys = settings->childKeys();
    for(int i=0;i< childKeys.length();i++)
        settings->remove(childKeys[i]);

    for (int i=0; i<list_faces_names->size(); i++) {
        QString str = "min_" + list_faces_names->at(i);
        settings->setValue(str, "1");
        str = "max_" + list_faces_names->at(i);
        settings->setValue(str, "1");
    }

}

void CadPreset1Widget::readyToMesh(){
    /*
    auto dialog = new QProgressDialog();
    dialog->setLabelText("Writing STL files into disk.\nPlease be patient, this may take a while.");
    auto bar = new QProgressBar(dialog);
    bar->setTextVisible(true);
    bar->setMaximum(0);
    bar->setMinimum(0);
    bar->setValue(0);
    dialog->setBar(bar);
    dialog->setWindowFlags(Qt::Dialog | Qt::Desktop);
    int width = QFontMetrics(dialog->font()).width(dialog->labelText()) + 100;
    dialog->resize(width, dialog->height());

    QFuture<void> future = QtConcurrent::run(this, &CadPreset1Widget::writeSTLs);
    QFutureWatcher<void> * futureWatcher = new QFutureWatcher<void>();
    futureWatcher->setFuture(future);
    connect(futureWatcher, SIGNAL (finished()), dialog, SLOT (cancel()));
    dialog->exec();
    */

    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    QStringList dirList = dir.entryList();
    foreach(QString file, dirList){
        if(file.compare("0") != 0 && file.compare(".") != 0 && file.compare("..") != 0 && file.compare("constant") != 0 && file.compare("system") != 0){
            QDir aux = QDir(Globals::instance()->getAppProjectPath() + "\\" + file);
            aux.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
            aux.removeRecursively();
        }
    }

    writeSTLs(); //Here CAD settings are also saved

    createBlockMeshDictFile();
    createSnappyHexMeshDictFile();
    resetSettingsFileMesh();
    createRequiredFiles();
    createControlDictFile();

    menu->goToMeshView();
}

void CadPreset1Widget::createControlDictFile(){
    QFile file(Globals::instance()->getAppProjectPath() + "/system/controlDict");
    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/" << endl;
        stream << "FoamFile{\n"
                  "\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n\tobject\tcontrolDict;\n\tlocation\t\"system\";}\n" << endl;
        stream << "application\tdriftFluxFoam;\nstartFrom\tlatestTime;\nstartTime\t0;\nstopAt\tendTime;\nendTime\t0;\n";
        stream << "deltaT\t0.1;\nwriteControl\tadjustableRunTime;\nwriteInterval\t1;\npurgeWrite\t0;\nwriteFormat\tascii;\nwritePrecision\t7;\nwriteCompression\tuncompressed;\n";
        stream << "timeFormat\tgeneral;\ntimePrecision\t7;\nrunTimeModifiable\tyes;\nadjustTimeStep\ton;\nmaxCo\t5;\nmaxDeltaT\t1;" << endl;

        file.close();
    }
}

void CadPreset1Widget::createRequiredFiles(){
    QFile file_U(Globals::instance()->getAppProjectPath() + "/0/U");
    QFile file_P(Globals::instance()->getAppProjectPath() + "/0/p_rgh");
    QFile file_A(Globals::instance()->getAppProjectPath() + "/0/alpha.sludge");
    QFile file_E(Globals::instance()->getAppProjectPath() + "/0/epsilon");
    QFile file_K(Globals::instance()->getAppProjectPath() + "/0/k");
    QFile file_N(Globals::instance()->getAppProjectPath() + "/0/nut");

    if(file_U.open(QFile::WriteOnly | QFile::Text) &&
            file_P.open(QFile::WriteOnly | QFile::Text) &&
            file_A.open(QFile::WriteOnly | QFile::Text) &&
            file_E.open(QFile::WriteOnly | QFile::Text) &&
            file_K.open(QFile::WriteOnly | QFile::Text) &&
            file_N.open(QFile::WriteOnly | QFile::Text)) {

        QTextStream stream_U(&file_U);
        QTextStream stream_P(&file_P);
        QTextStream stream_A(&file_A);
        QTextStream stream_E(&file_E);
        QTextStream stream_K(&file_K);
        QTextStream stream_N(&file_N);

        stream_U << "/***** File generated by Hydrodeca *****/" << endl;
        stream_U << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolVectorField;\n\tformat\tascii;\n\tobject\tU;\n}\n"
                    "dimensions\t[0 1 -1 0 0 0 0];\n"
                    "internalField\tuniform\t(0 0 0);\n\n"
                    "boundaryField {" << endl;

        stream_P << "/***** File generated by Hydrodeca *****/" << endl;
        stream_P << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tp_rgh;\n}\n"
                    "dimensions\t[1 -1 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        stream_A << "/***** File generated by Hydrodeca *****/" << endl;
        stream_A << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\talpha.sludge;\n}\n"
                    "dimensions\t[0 0 0 0 0 0 0];\n"
                    "internalField\tuniform\t0.0025;\n\n"
                    "boundaryField {" << endl;

        stream_E << "/***** File generated by Hydrodeca *****/" << endl;
        stream_E << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tepsilon;\n}\n"
                    "dimensions\t[0 2 -3 0 0 0 0];\n"
                    "internalField\tuniform\t1.50919e-06;\n\n"
                    "boundaryField {" << endl;

        stream_K << "/***** File generated by Hydrodeca *****/" << endl;
        stream_K << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tk;\n}\n"
                    "dimensions\t[0 2 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0.00015;\n\n"
                    "boundaryField {" << endl;

        stream_N << "/***** File generated by Hydrodeca *****/" << endl;
        stream_N << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tnut;\n\tlocation\t\"0\";\n}\n"
                    "dimensions\t[0 2 -1 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        stream_U << "\twalls {\n\t\ttype\t";
        stream_P << "\twalls {\n\t\ttype\t";;
        stream_A << "\twalls {\n\t\ttype\t";
        stream_K << "\twalls {\n\t\ttype\t";
        stream_E << "\twalls {\n\t\ttype\t";
        stream_N << "\twalls {\n\t\ttype\t";

        stream_U << "fixedValue;\n\t\tvalue\tuniform\t(0 0 0); }\n";
        stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }\n";
        stream_A << "zeroGradient; }\n";
        stream_K << "zeroGradient; }\n";
        stream_E << "zeroGradient; }\n";
        stream_N << "calculated;\n\t\tvalue\tuniform\t0; }\n";

        for (int i=0; i<list_faces_names->size(); i++) {
            QString name = list_faces_names->at(i);

            stream_U << "\t" << name << " {\n\t\ttype\t";
            stream_P << "\t" << name << " {\n\t\ttype\t";
            stream_A << "\t" << name << " {\n\t\ttype\t";
            stream_K << "\t" << name << " {\n\t\ttype\t";
            stream_E << "\t" << name << " {\n\t\ttype\t";
            stream_N << "\t" << name << " {\n\t\ttype\t";

            stream_U << "fixedValue;\n\t\tvalue\tuniform\t(0 0 0); }\n";
            stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }\n";
            stream_A << "zeroGradient; }\n";
            stream_K << "zeroGradient; }\n";
            stream_E << "zeroGradient; }\n";
            stream_N << "calculated;\n\t\tvalue\tuniform\t0; }\n";
        }

        stream_U << "\tDefault {\n\t\ttype\t";
        stream_P << "\tDefault {\n\t\ttype\t";;
        stream_A << "\tDefault {\n\t\ttype\t";
        stream_K << "\tDefault {\n\t\ttype\t";
        stream_E << "\tDefault {\n\t\ttype\t";
        stream_N << "\tDefault {\n\t\ttype\t";

        stream_U << "fixedValue;\n\t\tvalue\tuniform\t(0 0 0); }\n";
        stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }\n";
        stream_A << "zeroGradient; }\n";
        stream_K << "zeroGradient; }\n";
        stream_E << "zeroGradient; }\n";
        stream_N << "calculated;\n\t\tvalue\tuniform\t0; }\n";

        stream_U << "}" << endl;
        stream_P << "}" << endl;
        stream_A << "}" << endl;
        stream_E << "}" << endl;
        stream_K << "}" << endl;
        stream_N << "}" << endl;
    }
}

void CadPreset1Widget::removeGroup(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    QLineEdit *line_faces = (QLineEdit*)w->findChild<QWidget*>("line_faces");
    if(line_faces->text() != "none"){
        QStringList faces = line_faces->text().split(";");

        for (const auto& face : faces) {
            list_assigned_faces->replace(face.toInt(), false);
            faces_color_list->replace(face.toInt(), 0);
        }
    }

    delete w;
    showGeometryFaces();
}

void CadPreset1Widget::generateViewerLegend(){
    vtkSmartPointer<vtkLegendBoxActor> legend = vtkSmartPointer<vtkLegendBoxActor>::New();
    legend->SetNumberOfEntries(std::min(layout_groups->count()+1, list_assigned_faces->count()));
    vtkSmartPointer<vtkCubeSource> legendBox = vtkSmartPointer<vtkCubeSource>::New();
    legendBox->Update();
    int cnt_entry=0;
    for (int i = 0; i < layout_groups->count(); ++i) {
        QWidget *widget = layout_groups->itemAt(i)->widget();
        if (widget != NULL) {
            QLineEdit *line_name = (QLineEdit*)widget->findChild<QWidget*>("line_name");
            //if (! line_name->isEnabled()){ //it means that has been validated
            QLineEdit *line_faces = (QLineEdit*)widget->findChild<QWidget*>("line_faces");
            QStringList faces = line_faces->text().split(";");
            int firstFace = faces.first().toInt();
            legend->SetEntry(cnt_entry++, legendBox->GetOutput(), line_name->text().toStdString().c_str(), color_list->at(firstFace+1));
            //}
        }
    }

    if (cnt_entry < list_assigned_faces->count())
        legend->SetEntry(cnt_entry, legendBox->GetOutput(), "Default", color_list->at(0));

    vtkViewer->add(legend);
}

void CadPreset1Widget::addPhysicalGroup(){
    QLineEdit *line_name = new QLineEdit();
    line_name->setObjectName("line_name");

    QLineEdit *line_faces = new QLineEdit();
    line_faces->setObjectName("line_faces");

    QPushButton *btn_validate = new QPushButton("Update");
    btn_validate->setObjectName("btn_validate");
    connect(btn_validate, SIGNAL(clicked(bool)), this, SLOT(validateGroup()));
    //btn_validate->setFixedWidth(150);
    QPushButton *btn_remove = new QPushButton();
    connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(removeGroup()));
    btn_remove->setObjectName("btn_remove");
    btn_remove->setFixedWidth(27);
    btn_remove->setIcon(QIcon(":/Resources/remove.png"));
    btn_remove->setIconSize(QSize(17,17));

    QHBoxLayout *layout_buttons = new QHBoxLayout();
    layout_buttons->addWidget(btn_validate);
    layout_buttons->addWidget(btn_remove);

    QLabel *label_faces = new QLabel("Faces: ");
    for (int i=0; i<table_widget->rowCount(); i++){
        QTableWidgetItem *item = table_widget->item(i,0);
        if (item->isSelected()){
            int face_id = item->text().split(" ")[1].toInt();
            QString face_str = QString::number(face_id);
            if (line_faces->text() == ""){
                line_faces->setText(face_str);
                label_faces->setText(label_faces->text() + face_str);
            }
            else {
                line_faces->setText(line_faces->text() + ";" + face_str);
                label_faces->setText(label_faces->text() +  ", " + face_str);
            }
        }
    }

    QComboBox* combo_group_type = new QComboBox();
    combo_group_type->setObjectName("group_type");
    combo_group_type->addItem("wall");
    combo_group_type->addItem("patch");
    //combo_group_type->addItem("symmetry");

    QWidget *widget = new QWidget();
    widget->setStyleSheet("background-color: #CCCCCC");
    QVBoxLayout *layout_physicalGroup = new QVBoxLayout();
    layout_physicalGroup->addWidget(new QLabel("Name:"));
    layout_physicalGroup->addWidget(line_name);
    layout_physicalGroup->addWidget(new QLabel("Type:"));
    layout_physicalGroup->addWidget(combo_group_type);
    layout_physicalGroup->addWidget(label_faces);
    layout_physicalGroup->addWidget(line_faces);
    layout_physicalGroup->addLayout(layout_buttons);
    widget->setLayout(layout_physicalGroup);
    layout_groups->addWidget(widget);
    //layout_groups->count();
    line_faces->setVisible(false);
    line_name->setText("undefined"+QString::number(cnt_groups+1));
    btn_validate->click();
}

void CadPreset1Widget::initColorList(){
    faces_color_list = new QList<int>();
    list_assigned_faces = new QList<bool>();
    TopExp_Explorer exp;
    for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next()) {
        faces_color_list->append(0);
        list_assigned_faces->append(false);
    }

    color_list = new QList<double *>();
    double *a = new double[3];
    a[0] = 0.95;
    a[1] = 0.95;
    a[2] = 0.95;
    color_list->append(a);

    /*
    a = new double[3];
    a[0] = 0;
    a[1] = 0;
    a[2] = 1;
    color_list->append(a);
    a = new double[3];
    a[0] = 0;
    a[1] = 1;
    a[2] = 0;
    color_list->append(a);
    a = new double[3];
    a[0] = 1;
    a[1] = 0;
    a[2] = 0;
    color_list->append(a);
    a = new double[3];
    a[0] = 0;
    a[1] = 1;
    a[2] = 1;
    color_list->append(a);
    a = new double[3];
    a[0] = 1;
    a[1] = 0;
    a[2] = 1;
    color_list->append(a);
    a = new double[3];
    a[0] = 1;
    a[1] = 1;
    a[2] = 0;
    color_list->append(a);
    */

    //Change the range init and stride in orther to adjust the number of colors
    for (double i=0.15; i<1; i+=0.22){
        for (double j=0.15; j<1; j+=0.25){
            for (double k=0.15; k<1; k+=0.25){
                double *a = new double[3];
                a[0] = i;
                a[1] = j;
                a[2] = k;
                color_list->append(a);
            }
        }
    }

    a = new double[3];
    a[0] = 1;
    a[1] = 1;
    a[2] = 0;
    color_list->append(a);

    qDebug() << "WARNING: " << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - We have detected " << faces_color_list->size() << " faces and there only are " << color_list->size() - 2 << "colors  available (if faces is greater than colors the program will crash!)" << endl;
}

void CadPreset1Widget::onSliderOpacityStop(){
    showGeometryFaces();
}

void CadPreset1Widget::importProperties(){
    QPushButton *btn_import = new QPushButton("Import geometry");
    btn_import->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btn_import, SIGNAL(clicked(bool)), this, SLOT(openImportDialog()));

    slider_opacity = new QSlider(Qt::Horizontal);
    slider_opacity->setRange(0, 100);
    slider_opacity->setValue(100);
    slider_opacity->setDisabled(true);
    //slider_opacity->setVisible(false);
    connect(slider_opacity, SIGNAL(sliderReleased()),this, SLOT(onSliderOpacityStop()));

    QPushButton *btn_save = new QPushButton("Ready to mesh!");
    btn_save->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btn_save, SIGNAL(clicked(bool)), this, SLOT(readyToMesh()));

    QVBoxLayout* main_layout = new QVBoxLayout();
    layout_groups = new QVBoxLayout();
    main_layout->addWidget(btn_import);
    main_layout->addWidget(new QLabel("Opacity control"));
    main_layout->addWidget(slider_opacity);
    main_layout->addLayout(layout_groups);
    main_layout->addStretch();
    main_layout->addWidget(btn_save);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addLayout(main_layout);

    faces_layout = new QVBoxLayout();
    layout->addLayout(faces_layout);

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    QFile file(Globals::instance()->getAppProjectPath()+"/infoSavedCAD.ini");
    if(file.exists()){
        loadImportDialog(file.fileName());
    }
    //openImportDialog();
}
