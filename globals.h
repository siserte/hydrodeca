#ifndef GLOBAL_H
#define GLOBAL_H

#include <QDate>
#include <QDoubleValidator>
#include <QLayout>
#include <QTableWidget>
#include <QToolBox>
#include <QScrollArea>
#include <QTimer>
#include <QStackedWidget>

#define AUTODEBUG 1
//#define PROJECTNAME "asm1"
//#define PROJECTNAME "EDARTineo"
#define PROJECTNAME "mancomunadaASM1"
#define WIDGETTAB 5

//0 -> simflow; 1 -> docker
#define OFENGINE 1
#define DOCKERIMAGE "ofubuntu-asm"
#define POWERSHELL "C:/Windows/system32/WindowsPowerShell/v1.0/powershell.exe"

extern double locationInMesh[3];

class Globals {

protected:
    static Globals* m_pInstance;

public:
    //Globals * instance();
    static Globals* instance();

    // functions
    void checkProject();
    bool compare_double(double a, double b, double epsilon = 0.001);
    void copyFolder(QString sourceFolder, QString destFolder);
    void readSettingsData();
    bool writeProjectInfoFile();
    void writeSettingsFile();

    // your global setters/getter
    QString getAppPath();
    QString getAppDefaultPath();
    QString getAppProjectPath();
    QString getAppProjectMeshInfoBlockMeshPath();
    QString getAppProjectFoamPath();
    QString getAppProjectInfoPath();
    QString getAppWorkSpace();
    QString getAppWorkSpacePath();
    QString getAppSettingsPath();
    QString getAppCurrentWorkSpacePath();
    QString getCssAreaActions();
    QString getCssAreaButtons();
    QString getCssAreaNav();
    QString getCssAreaNavButtons();
    QString getCssMenu();
    QString getCssMenuSelected();
    QString getCssPushButton();
    QString getCssArrows();
    QString getCssApplyPushButton();
    QString getCssSubMenu();
    QString getCssViewer();
    QString getCssScrollBar();
    QString getCssViewerFont();
    QString getCssLineEdit();
    QString getCssSelect();
    QString getCssToolBox();
    QString getCurrentProjectName();
    QString getCurrentProjectLastModification();
    QString getCurrentProjectCreationDate();
    bool getCurrentProjectCadModel();
    bool getCurrentProjectCfdModel();
    bool getCurrentProjectEdarModel();
    bool getCurrentProjectMeshModel();
    bool getCurrentProjectResultsModel();
    QDoubleValidator* getDoubleValidator(QObject* parent);
    QFont getFontViewer();

    QStackedWidget* getHomeWTab();
    QString getTabHome();
    QString getTabCad();
    QString getTabCfd();
    QString getTabEdar();
    QString getTabMesh();
    QString getTabResults();
    QString getTabHelp();

    void setAppWorkSpace(const QString& val);
    void setCurrentProjectName(const QString& val);
    void setCurrentProjectCadModel(const bool& val);
    void setCurrentProjectCfdModel(const bool& val);
    void setCurrentProjectResultsModel(const bool& val);

    int copyPath(QString, QString);

protected:
    QString app_path;
    QString app_path_settings;
    QString app_path_workspace;
    QString app_default_path;
    QString app_settings_path;
    QString app_workspace;
    QString cp_name;
    QString cp_lastModification;
    QString cp_creationDate;
    bool cp_cadModel;
    bool cp_cfdModel;
    bool cp_edarModel;
    bool cp_meshModel;
    bool cp_resultsModel;
    int num_workspaces;
    QString file_mesh_info_block_mesh;
    QString file_foam;
    QString file_info_project;
    QString tab_home;
    QString tab_cad;
    QString tab_cfd;
    QString tab_edar;
    QString tab_mesh;
    QString tab_results;
    QString tab_analysis;
    QString tab_help;

private:
    Globals();
};
#endif // GLOBAL_H
