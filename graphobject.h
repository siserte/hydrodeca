#ifndef GRAPH_H
#define GRAPH_H

#include <QString>

class GraphObject {

public:
    explicit GraphObject();

public:
    bool visible;

    QString displayMode;

    double p1_x;
    double p1_y;
    double p1_z;

    double p2_x;
    double p2_y;
    double p2_z;

};

#endif // GRAPH_H
