#ifndef CFDWIDGET_H
#define CFDWIDGET_H

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMainWindow>
#include <QProcess>
#include <QProgressDialog>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QTabWidget>
#include <QTreeWidgetItem>
#include <QToolBox>
#include <QWidget>

#include <Geom_TrimmedCurve.hxx>
#include <gp_Ax1.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <menuwidget.h>

#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>

#include <unordered_map>

#include <vtkActor.h>
#include <vtkSmartPointer.h>

#include <vtkviewer.h>

#include <QSettings>
#include <QComboBox>

class CfdWidget : public QWidget {
    Q_OBJECT

public:
    explicit CfdWidget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, QToolBox* reportWidget = 0, QListWidget* console = 0);
    ~CfdWidget();
    void loadMainWindow();

public slots:
    void loadSettingsPropertiesWWTP();
    void loadSolverDriftFlux();
    void saveSolverDriftFlux();

    void loadSolverASM1();
    void saveSolverASM1();

    void loadSettingsBoundariesWWTP();
    void loadSettingsBoundaries();
    void saveSettingsBoundaries();
    void saveBoundariesDriftFlux();
    void saveBoundariesPimple();
    void saveBoundariesASM1();

    void loadSettingsInitialize();
    void saveSettingsInitialize();

    void loadSettingsSolver();
    void saveSettingsSolver();

    void saveSolverPimplePower();
    void saveSolverPimpleHerschel();
    void saveSolverPimpleNewton();

    void saveInitializeASM1();

    void boundaryConditions();
    void driftFluxFoam();
    void pimpleFoam();
    void asm1Foam();
    void showSimulationTab();
    void solverSettingsDriftFluxFoam(QVBoxLayout *);
    void solverSettingsPimpleFoam(QVBoxLayout *);
    void solverSludgePower(QVBoxLayout *);
    void solverSludgeHerschel(QVBoxLayout *);
    void solverSludgeNewtonian(QVBoxLayout *);
    void solverSettingsASM1Foam(QVBoxLayout *);
    void solverProperties();
    void setFields();
    void setFieldsStarted();
    void setFieldsOutput();
    void setFieldsFinished();
    void cancelSetFields();
    void removeBoundary();
    void slotOnBoundTypeChanged(int);
    void slotLoadBoundary();
    void slotOnModeBoundChanged();
    void onSolverChange();
    void onSludgeModelChange();
    void equipmentsConfig();
    void slotSaveEquipment();
    void showInitilizeTab();

private:
    TopoDS_Shape cadmodelRevol;
    vtkActor* cadActor;
    QProcess* processDriftFlux;
    QProcess* processSetFields;
    QProgressDialog* progressBarProcess;
    int progressBarValue;
    void removeDirs(QStringList);
    bool isCancelProcess;

    //void createControlDictFile();
    void loadEquipment(QString);
    void createFVOptionsFile();
    void createTurbulencePropFile();
    void createControlDictFile();
    void copyPimpleFoamFiles();
    void copyASM1FoamFiles();
    void copyDriftFluxFoamFiles();
    void runTopoSet();
    void createASM1KineticPropertiesFile();
    void createAndInitializeDir1();
    QString generateHeader(QString, QString);
    QString createNextASM1Timestep();
    void initASM1();
    void initDriftFlux();
    QWidget * tabASM1kinetics();
    QWidget * tabASM1timestamp();

private:
    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;
    double Qi;
    double Qr;
    double Qe;
    double SST;
    QPushButton* btnInit;
    vtkSmartPointer<vtkLegendBoxActor> legend;

private:
    void createAreaButtons();
    void createAreaActions();
    void createAreaViewers();

private:
    QString m_sSettingsFile;
    QSettings* settings;

    QLineEdit* line_v0;
    QLineEdit* line_rh;
    QLineEdit* line_rp;
    QComboBox* line_model;
    QLineEdit* line_coeficient;
    QLineEdit* line_exponent;
    QLineEdit* line_offset;
    QComboBox* combo_gravity;

    QLineEdit* line_inf_flow;
    QLineEdit* line_inf_sludge;
    QLineEdit* line_recycling;
    QLineEdit* line_effluent;

    QLineEdit* line_height;
    QLineEdit* line_sludge;

    QLineEdit* line_timesteps;
    QLineEdit* line_write;
    QComboBox* line_turbulence;
    QLineEdit* line_lower_tol;
    QLineEdit* line_upper_tol;
    QLineEdit* line_correctors;
    QLineEdit* line_non_orth;

    QComboBox *line_solver;

    QLineEdit* line_numin;
    QLineEdit* line_numax;
    QLineEdit* line_nu0;
    QLineEdit* line_tau0;

    QLineEdit *line_kh;
    QLineEdit *line_kx;
    QLineEdit *line_nh;
    QLineEdit *line_muh;
    QLineEdit *line_ng;
    QLineEdit *line_ks;
    QLineEdit *line_bh;
    QLineEdit *line_koh;
    QLineEdit *line_kno;
    QLineEdit *line_knhh;
    QLineEdit *line_mua;
    QLineEdit *line_ba;
    QLineEdit *line_qam;
    QLineEdit *line_koa;
    QLineEdit *line_knha;

    QLineEdit *line_init_so;
    QLineEdit *line_init_ss;
    QLineEdit *line_init_xba;
    QLineEdit *line_init_xbh;
    QLineEdit *line_init_xi;
    QLineEdit *line_init_si;
    QLineEdit *line_init_xp;
    QLineEdit *line_init_xs;
    QLineEdit *line_init_salk;
    QLineEdit *line_init_snh;
    QLineEdit *line_init_sno;
    QLineEdit *line_init_snd;
    QLineEdit *line_init_xnd;

private:
    QList<bool> *list_assigned_faces;
    QList<double *> *color_list;
    QList<int> *faces_color_list;   //for each face, we assign a color (an index of color_list). Several faces can have the same color.
    int cnt_groups;
    TopoDS_Shape aShape;
    QList<QString> *list_faces_names;
    QVBoxLayout *layoutAreaBoundaries;
    QSettings *settingsBoundaries;
    QSettings *settingsUser;
    QString equips_dir;
    QVBoxLayout *layoutAreaEquipment;

    QComboBox *comboTimesteps;
    QString selectedTimestep;
    void copyPath(QString, QString);

public:
    void initColorList();
    void showGeometryFaces();
    void loadBoundary(bool, int);
    void onBoundTypeChanged(bool, int, int, QWidget*);
    void onModeBoundChanged(int, int, QWidget*);

private:
    void startPimpleFoam();
    void startDriftFluxFoam();
    void startASM1Foam();
    QProcess *processFoam;

public slots:
    void foamOutput();
};

#endif // CFDWIDGET_H
