#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QMainWindow>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QToolBox>
#include <QStackedWidget>

#include <menuwidget.h>
#include <vtkviewer.h>

namespace Ui {
class DefaultWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

protected:

private slots:
    void createNewProject();
    void removeProject();
    void createNewWorkSpace();
    void selectProject();
    void createNewProjectDialog();
    void showLoadProjectDialog();
    void showNewProjectDialog();

    void onNewProjectDialogTextChanged(const QString &text);
    void onNewProjectTextChanged(const QString& text);
    void onNewWorkSpaceDialogTextChanged(const QString &text);
    void onNewWorkSpaceTextChanged(const QString& text);

    void showStatusBarInfo();

    void nextTab();
    void prevTab();
    void resetTab();
    void closeEvent(QCloseEvent *);
    void processOutput();

private:
    Ui::DefaultWindow *ui;
    MenuWidget* menu;
    vtkActor* cadActor;

    /*viewer*/
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QStackedWidget* homeWidget;
    QListWidget* console;


private:
    QScrollArea* createAreaButtons();
    QScrollArea* createAreaActions();
    QTabWidget* createAreaViewers();
    void createStatusBar();
};
#endif // MAINWINDOW_H
