#ifndef HOMEWIDGET_H
#define HOMEWIDGET_H

#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QToolBox>

#include <menuwidget.h>

#include <vtkviewer.h>

class HomeWidget : public QWidget {
    Q_OBJECT

public:
    explicit HomeWidget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, QListWidget* console = 0);
    ~HomeWidget();
    void loadMainWindow();

public slots:
    void createNewProject();
    //void selectProject(const QString& text);
    void selectProject();
    void createNewProjectDialog();
    void createNewWorkSpaceDialog();
    void showLoadProjectDialog();
    void showChangeWorkspaceDialog();
    void showNewProjectDialog();
    void showNewWorkSpaceDialog();

    void onNewProjectDialogTextChanged(const QString &text);
    void onNewProjectTextChanged(const QString& text);
    void onNewWorkSpaceDialogTextChanged(const QString &text);

    void nextTab();
    void prevTab();
    void resetTab();

private:
    vtkActor* cadActor;

private:
    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;
private:
    void createAreaButtons();
    void createAreaActions();
    void createAreaViewers();
};
#endif // HOMEWIDGET_H
