#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QHBoxLayout>
#include <QListWidget>
#include <QMainWindow>
#include <QProcess>
#include <QString>
#include <QToolBox>
#include <QWidget>
#include <QScrollArea>
#include <QStackedWidget>

#include <vtkviewer.h>

class MenuWidget : public QWidget {
    Q_OBJECT

public:
    MenuWidget(QWidget* parent, QWidget* centralwidget);
    ~MenuWidget();
    QHBoxLayout* createMenu();

    QPushButton *btnHome;
    QPushButton *btnWwtp;
    QPushButton *btnCad;
    QPushButton *btnMesh;
    QPushButton *btnCfd;
    QPushButton *btnResults;
    QPushButton *btnHelp;

public slots:
    void goToCadView();
    void goToCfdView();
    void goToEdarView();
    void goToHomeView();
    void goToMeshView();
    void goToResultsView(bool runDriftFlux = false);

    void aboutHelp();
    void onlineTutorialHelp();
    void userGuideHelp();

    void setViewers(VTKViewer* viewer, VTKViewer* graphs);
    void setConsole(QListWidget* console);
    void setHome(QStackedWidget* home);


    void cleanAreas();

    void updateMenu(const QString& currentTab, QWidget* widget);

private:
    QWidget* parent;
    QWidget* parentCentralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;
    QStackedWidget* home;

};

#endif // MENUWIDGET_H
