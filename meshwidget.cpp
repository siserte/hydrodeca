#include "borderlayout.h"
#include "globals.h"
#include "meshwidget.h"
#include "menuwidget.h"
#include "ui_defaultwindow.h"


#include <QCheckBox>
#include <QComboBox>
#include <qDebug>
#include <QDir>
#include <QFile>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLCDNumber>
#include <QMessageBox>
#include <QTextBrowser>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QScrollArea>
#include <QSignalMapper>
#include <QSlider>
#include <QSplitter>
#include <QString>
#include <QThread>
#include <unordered_map>
#include <QVBoxLayout>

#include <AIS_Shape.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBndLib.hxx>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>

#include <BRepLib.hxx>

#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>

#include <BRepPrim_Cone.hxx>
#include <BRepPrimAPI_MakeOneAxis.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepOffsetAPI_ThruSections.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <gp_Ax1.hxx>
#include <gp_Ax3.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_DisplayModeFilter.hxx>
#include <IVtkTools_ShapeDataSource.hxx>

#include <MeshVS_MeshPrsBuilder.hxx>
#include <MeshVS_DrawerAttribute.hxx>

#include <OSD_Path.hxx>

#include <QVTKWidget.h>

#include <RWStl.hxx>

#include <STEPCAFControl_Writer.hxx>

#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <StlMesh_Mesh.hxx>
#include <StlMesh_MeshExplorer.hxx>
#include <StlTransfer.hxx>

#include <TDocStd_Document.hxx>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkCubeSource.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkExecutive.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkInteractorStyleImage.h>
#include <vtkJPEGReader.h>
#include <vtkLookupTable.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkOutlineFilter.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <qprogressdialog.h>
#include <vtkProperty.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStructuredGrid.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkVersion.h>

#include <XCAFApp_Application.hxx>
#include <XCAFDoc_DocumentTool.hxx>
#include <XCAFDoc_ShapeTool.hxx>

#include <XSDRAWSTLVRML_DataSource.hxx>

#include "tier_application/physicalgroupmesh.h"
#include <vtkClipPolyData.h>
#include <vtkPlane.h>
#include <vtkTextProperty.h>
#include <vtkCubeAxesActor2D.h>
#include <vtkLegendScaleActor.h>

#include <vtkPropCollection.h>
#include <vtkAxisActor2D.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkViewport.h>
#include <vtkDecimatePro.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkLODActor.h>
#include <vtkCylinderSource.h>
#include <QDirIterator>
#include <vtkSTLReader.h>
#include <QtMath>

#define MAX2(X, Y)      (Abs(X)>Abs(Y)?Abs(X):Abs(Y))
#define MAX3(X, Y, Z)   (MAX2(MAX2(X,Y),Z))

MeshWidget::MeshWidget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, QToolBox* reportWidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    reportInfo = reportWidget;
    console = consoleWidget;

    isAutoMeshWorking = false;

    vtkViewer->setShowAxes(false);
    vtkViewer->setShowModel(true);
    vtkViewer->setOpacityModel(1.0);
}

MeshWidget::~MeshWidget(){

}

void MeshWidget::loadMainWindow(){
    // Comprobamos si existe una geometria en la carpeta del proyecto
    QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
    QFile file(filename);
    if(!file.exists()){
        QMessageBox::about(this, tr("ERROR MESH TAB"),
                           tr("<p>No se ha encontrado ningun modelo STL en el proyecto. Vuelva a la pestaña de CAD y genere un modelo para continuar.</p>"));
    }
    else{
        resetAreas();
        settingsUser = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
        equips_dir = Globals::instance()->getAppProjectPath() + "equipments/";
        centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);
        menu->updateMenu(Globals::instance()->getTabMesh(), this);

        createAreaButtons();
        createAreaActions();
        createAreaViewers();
        showBase(); //this must be here and cannot be changed
        showSculpt(); //this also (should be improved TODO)
        showBase(); // now move to the desired tab
    }
}

void MeshWidget::createAreaActions(){
    QGridLayout* layout = new QGridLayout();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void MeshWidget::createAreaViewers(){
    QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
    vtkViewer->reset();
    cadActor = vtkViewer->add(filename.toStdString().c_str());
    centerCadActor = cadActor->GetCenter();
    dataSetCadActor = cadActor->GetMapper()->GetInputAsDataSet();

    vtkViewer->update();
    vtkViewer->resetCamera();
    cadActor->GetProperty()->SetColor(0.54,0.46,0.46);

    minBound[0] = cadActor->GetBounds()[0];
    minBound[1] = cadActor->GetBounds()[2];
    minBound[2] = cadActor->GetBounds()[4];

    maxBound[0] = cadActor->GetBounds()[1];
    maxBound[1] = cadActor->GetBounds()[3];
    maxBound[2] = cadActor->GetBounds()[5];

    surface();
}

void MeshWidget::createAreaButtons(){
    // SPACER
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    btnBase = new QPushButton("Base", this);
    btnBase->setStyleSheet(Globals::instance()->getCssPushButton());
    btnBase->setMinimumHeight(50);
    btnBase->setCheckable(true);
    connect(btnBase, SIGNAL(clicked()), this, SLOT(showBase()));

    btnEquip = new QPushButton("Equipments", this);
    btnEquip->setStyleSheet(Globals::instance()->getCssPushButton());
    btnEquip->setMinimumHeight(50);
    btnEquip->setCheckable(true);
    btnEquip->setChecked(false);
    connect(btnEquip, SIGNAL(clicked()), this, SLOT(showEquipments()));

    btnSnappy = new QPushButton("Sculpt", this);
    btnSnappy->setStyleSheet(Globals::instance()->getCssPushButton());
    btnSnappy->setMinimumHeight(50);
    btnSnappy->setDisabled(true);
    btnSnappy->setCheckable(true);
    connect(btnSnappy, SIGNAL(clicked()), this, SLOT(showSculpt()));
    btnSnappy2 = new QPushButton("Snap", this);
    btnSnappy2->setStyleSheet(Globals::instance()->getCssPushButton());
    btnSnappy2->setMinimumHeight(50);
    btnSnappy2->setDisabled(true);
    btnSnappy2->setCheckable(true);
    connect(btnSnappy2, SIGNAL(clicked()), this, SLOT(showSnap()));
    btnSnappy3 = new QPushButton("Layer", this);
    btnSnappy3->setStyleSheet(Globals::instance()->getCssPushButton());
    btnSnappy3->setMinimumHeight(50);
    btnSnappy3->setDisabled(true);
    btnSnappy3->setCheckable(true);
    connect(btnSnappy3, SIGNAL(clicked()), this, SLOT(showLayer()));

    QString str = Globals::instance()->getAppProjectPath() + "/0.2";
    if(QDir(str).exists())
        btnSnappy3->setDisabled(false);
    str = Globals::instance()->getAppProjectPath() + "/0.1";
    if(QDir(str).exists())
        btnSnappy2->setDisabled(false);
    str = Globals::instance()->getAppProjectPath() + "/constant/polyMesh/boundary";
    if(QFileInfo(str).exists())
        btnSnappy->setDisabled(false);


    btnCheckMesh = new QPushButton("", this);
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    btnCheckMesh->setText(settings->value("MeshCheck/check", "Check: N/A").toString());
    btnCheckMesh->setStyleSheet(Globals::instance()->getCssPushButton());
    btnCheckMesh->setMinimumHeight(50);
    btnCheckMesh->setCheckable(true);
    connect(btnCheckMesh, SIGNAL(clicked()), this, SLOT(checkMesh()));

    // Creamos el widget para WestArea
    QHBoxLayout* navArea = new QHBoxLayout();
    navArea->setSpacing(0);
    navArea->setMargin(0);
    navArea->setObjectName("area_nav");
    //navArea->addWidget(btnNext);

    QWidget* navWidget = new QWidget();
    navWidget->setBackgroundRole(QPalette::Midlight);
    navWidget->setStyleSheet(Globals::instance()->getCssArrows());
    navWidget->setLayout(navArea);

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->setMargin(0);
    layoutAreaActions->setSpacing(0);
    layoutAreaActions->addWidget(btnBase);
    layoutAreaActions->addWidget(btnEquip);
    layoutAreaActions->addWidget(btnSnappy);
    layoutAreaActions->addWidget(btnSnappy2);
    layoutAreaActions->addWidget(btnSnappy3);
    layoutAreaActions->addWidget(btnCheckMesh);
    layoutAreaActions->addSpacerItem(my_spacer);
    layoutAreaActions->addWidget(navWidget,Qt::BottomSection);

    if(centralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        QLayoutItem *child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }
    centralWidget->findChild<QScrollArea*>("area_buttons")->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(100);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setStyleSheet(Globals::instance()->getCssAreaButtons());
}

void MeshWidget::showEquipments(){
    if (!QFile::exists(equips_dir))
        QDir().mkdir(equips_dir);

    cnt_equips=0;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    vtkViewer->reset();
    QString filename = Globals::instance()->getAppProjectPath() + "constant/triSurface/cad.stl";
    vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(filename.toStdString().c_str());
    reader->Update();

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(reader->GetOutput());
    vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
    actor->SetMapper(mapper);
    //actor->GetProperty()->SetColor(0.54,0.46,0.46);
    actor->GetProperty()->SetOpacity(0.5);
    vtkViewer->m_renderer->AddActor(actor);
    vtkViewer->update();
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    btnBase->setChecked(false);
    btnEquip->setChecked(true);
    btnSnappy->setChecked(false);
    btnSnappy2->setChecked(false);
    btnSnappy3->setChecked(false);
    btnCheckMesh->setChecked(false);

    layoutAreaEquipment = new QVBoxLayout();

    //QPushButton* btnApply = new QPushButton("Save", this);
    //btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    //btnApply->setMinimumHeight(30);
    //connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSettingsEquipments()));

    QPushButton* btnAddEquip = new QPushButton("Add new equipment");
    btnAddEquip->setFixedWidth(125);
    connect(btnAddEquip, SIGNAL(clicked(bool)), this, SLOT(slotLoadEquipment()));

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->addLayout(layoutAreaEquipment);
    layoutAreaActions->addWidget(btnAddEquip);
    //layoutAreaActions->addWidget(btnApply);
    layoutAreaActions->addStretch();

    loadSettingsEquipment();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

}

void MeshWidget::loadSettingsEquipment(){
    QDirIterator it(equips_dir, QStringList() << "*", QDir::Files);
    while (it.hasNext())
        //qDebug() << it.next();
        loadEquipment(it.next());
}

void MeshWidget::removeEquipment(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    QLineEdit *line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_name");
    if (line_value->text() == ""){
        showEquipmentValidationError("A equipment name is required.");
        return;
    }

    QFile file (equips_dir + line_value->text());
    file.remove();

    delete w;
}

void MeshWidget::slotLoadEquipment(){
    loadEquipment(NULL);
    cnt_equips += 1;
}

void MeshWidget::slotSaveEquipment(){
    QString name;
    double radius, width;
    float minx, miny, minz, maxx, maxy, maxz;

    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    QLineEdit *line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_name");
    if (line_value->text() == ""){
        showEquipmentValidationError("A equipment name is required.");
        return;
    }
    name = line_value->text();
    line_value->setEnabled(false);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << name;

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_radius");
    if (line_value->text() == ""){
        showEquipmentValidationError("A radius is required.");
        return;
    }
    radius = line_value->text().toDouble();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_width");
    if (line_value->text() == ""){
        showEquipmentValidationError("A width is required.");
        return;
    }
    width = line_value->text().toDouble();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_min_x");
    if (line_value->text() == ""){
        showEquipmentValidationError("center X is required.");
        return;
    }
    minx = line_value->text().toFloat();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_min_y");
    if (line_value->text() == ""){
        showEquipmentValidationError("center Y is required.");
        return;
    }
    miny = line_value->text().toFloat();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_min_z");
    if (line_value->text() == ""){
        showEquipmentValidationError("center Z is required.");
        return;
    }
    minz = line_value->text().toFloat();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_max_x");
    if (line_value->text() == ""){
        showEquipmentValidationError("rotation X is required.");
        return;
    }
    maxx = line_value->text().toFloat();
    /*
    line_value = (QLineEdit*)w->findChild<QWidget*>("line_max_y");
    if (line_value->text() == ""){
        showEquipmentValidationError("rotation Y is required.");
        return;
    }
    maxy = line_value->text().toFloat();
    */
    line_value = (QLineEdit*)w->findChild<QWidget*>("line_max_z");
    if (line_value->text() == ""){
        showEquipmentValidationError("rotation Z is required.");
        return;
    }
    maxz = line_value->text().toFloat();

    QSettings *setfile_equip = new QSettings(equips_dir + name, QSettings::IniFormat);
    setfile_equip->setValue("name", name);
    setfile_equip->setValue("radius", QString::number(radius));
    setfile_equip->setValue("width", QString::number(width));
    setfile_equip->setValue("centerx", QString::number(minx));
    setfile_equip->setValue("centery", QString::number(miny));
    setfile_equip->setValue("centerz", QString::number(minz));
    setfile_equip->setValue("inclination", QString::number(maxx));
    setfile_equip->setValue("azimuth", QString::number(maxz));
    setfile_equip->setValue("saved", QString::number(1));

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    vtkSmartPointer<vtkCylinderSource> source = vtkSmartPointer<vtkCylinderSource>::New();
    source->SetResolution(25);
    source->SetCenter(minx, miny, minz);
    source->SetRadius(radius);
    source->SetHeight(width);
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(source->GetOutputPort());
    if (vtkLocationMesh)
        vtkViewer->m_renderer->RemoveActor(vtkLocationMesh);
    vtkLocationMesh = vtkSmartPointer<vtkActor>::New();
    vtkLocationMesh->SetMapper(mapper);
    vtkLocationMesh->GetProperty()->SetColor(1,0,0);
    vtkLocationMesh->GetProperty()->SetOpacity(1);
    vtkLocationMesh->SetOrigin(minx, miny, minz);
    double tecta = 90-maxx;
    double phi = maxz-90;
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Min: " << QString::number(tecta) << " :: " << QString::number(phi) << endl;
    vtkLocationMesh->RotateWXYZ(tecta, 1, 0, 0);
    vtkLocationMesh->RotateWXYZ(phi, 0, 0, 1);
    //vtkLocationMesh->RotateX(maxx);
    //vtkLocationMesh->RotateY(maxy);
    //vtkLocationMesh->RotateZ(maxz);
    //double *xrange = vtkLocationMesh->GetXRange();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Xrange: " << xrange[0] << " :: " << xrange[1] << endl;
    //double *bounds = vtkLocationMesh->GetBounds();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Min: " << bounds[0] << " :: " << bounds[2] << " :: " << bounds[4] << endl;
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Max: " << bounds[1] << " :: " << bounds[3] << " :: " << bounds[5] << endl;
    //double vecOri[3];
    //vtkLocationMesh->GetOrientation(vecOri);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Vector Orientation: " << "(" << vecOri[0] << ", " << vecOri[1] << ", " << vecOri[2] << ")" << endl;
    vtkViewer->m_renderer->AddActor(vtkLocationMesh);
    vtkViewer->update();

    double *center = vtkLocationMesh->GetCenter();
    double THRESHOLD = 0.0001;
    double ux = qSin(qDegreesToRadians(maxx)) * qCos(qDegreesToRadians(maxz));
    if (std::abs(ux) < THRESHOLD) ux = 0;
    double uy = qSin(qDegreesToRadians(maxx)) * qSin(qDegreesToRadians(maxz));
    if (std::abs(uy) < THRESHOLD) uy = 0;
    double uz = qCos(qDegreesToRadians(maxx));
    if (std::abs(uz) < THRESHOLD) uz = 0;
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Center: " << "(" << center[0] << ", " << center[1] << ", " << center[2] << ")" << endl;
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " Vector director: " << "(" << ux << ", " << uy << ", " << uz << ")" << endl;

    setfile_equip->setValue("ux", QString::number(ux, 'f', 2));
    setfile_equip->setValue("uy", QString::number(uy, 'f', 2));
    setfile_equip->setValue("uz", QString::number(uz, 'f', 2));

    setfile_equip->setValue("minx", QString::number(center[0] + (ux * width / 2), 'f', 2));
    setfile_equip->setValue("miny", QString::number(center[1] + (uy * width / 2), 'f', 2));
    setfile_equip->setValue("minz", QString::number(center[2] + (uz * width / 2), 'f', 2));
    setfile_equip->setValue("maxx", QString::number(center[0] - (ux * width / 2), 'f', 2));
    setfile_equip->setValue("maxy", QString::number(center[1] - (uy * width / 2), 'f', 2));
    setfile_equip->setValue("maxz", QString::number(center[2] - (uz * width / 2), 'f', 2));
}

void MeshWidget::createTopoSetDictFile(){
    QString path = Globals::instance()->getAppProjectPath() + "/system/topoSetDict";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"system\";\n\tobject\ttopoSetDict;}"
                  "\nactions\n(" << endl;

        QString name, radius, p1x, p1y, p1z, p2x, p2y, p2z;
        QDirIterator it(equips_dir, QStringList() << "*", QDir::Files);
        while (it.hasNext()){
            QSettings *setfile_equip = new QSettings(it.next(), QSettings::IniFormat);
            name = setfile_equip->value("name").toString();
            p1x = setfile_equip->value("minx").toString();
            p1y = setfile_equip->value("miny").toString();
            p1z = setfile_equip->value("minz").toString();
            p2x = setfile_equip->value("maxx").toString();
            p2y = setfile_equip->value("maxy").toString();
            p2z = setfile_equip->value("maxz").toString();
            radius = setfile_equip->value("radius").toString();

            stream << "\t{" << endl;
            stream << "\t\tname\t" << name << "_CellSet;\n";
            stream << "\t\ttype\tcellSet;\n";
            stream << "\t\taction\tnew;\n";
            stream << "\t\tsource\tcylinderToCell;\n";
            stream << "\t\tsourceInfo {\n";
            stream << "\t\t\tp1\t(" << p1x << " " << p1y << " " << p1z << ");\n";
            stream << "\t\t\tp2\t(" << p2x << " " << p2y << " " << p2z << ");\n";
            stream << "\t\t\tradius\t" << radius << "; }\n\t}" << endl;
            stream << "\t{" << endl;
            stream << "\t\tname\t" << name << ";\n";
            stream << "\t\ttype\tcellZoneSet;\n";
            stream << "\t\taction\tnew;\n";
            stream << "\t\tsource\tsetToCellZone;\n";
            stream << "\t\tsourceInfo { set\t" << name << "_CellSet; }\n\t}" << endl;
        }
        stream << ");";
    }
}

void MeshWidget::showEquipmentValidationError(QString aux){
    QMessageBox *errorBox = new QMessageBox();
    errorBox->setIcon(QMessageBox::Warning);
    errorBox->setWindowTitle("Error when validating");
    errorBox->setText("The equipment cannot be validated.\n" + aux);
    errorBox->setStandardButtons(QMessageBox::Close);
    errorBox->exec();
}

void MeshWidget::loadEquipment(QString pathfile){
    QPushButton *btn_remove = new QPushButton();
    connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(removeEquipment()));
    btn_remove->setFixedWidth(20);
    btn_remove->setIcon(QIcon(":/Resources/remove.png"));
    btn_remove->setIconSize(QSize(17,17));

    QLineEdit *lineEquipName = new QLineEdit();
    lineEquipName->setObjectName("line_equip_name");

    QComboBox* comboEquipType = new QComboBox();
    comboEquipType->setObjectName("combo_equip_type");
    //comboEquipType->setFixedWidth(100);
    comboEquipType->addItem("Impeller");

    QLineEdit *lineRadius = new QLineEdit();
    lineRadius->setObjectName("line_equip_radius");
    QLineEdit *lineWidth = new QLineEdit();
    lineWidth->setObjectName("line_equip_width");

    QLineEdit *lineMinX = new QLineEdit();
    lineMinX->setObjectName("line_min_x");
    QLineEdit *lineMinY = new QLineEdit();
    lineMinY->setObjectName("line_min_y");
    QLineEdit *lineMinZ = new QLineEdit();
    lineMinZ->setObjectName("line_min_z");
    QLineEdit *lineMaxX = new QLineEdit();
    lineMaxX->setObjectName("line_max_x");
    QLineEdit *lineMaxY = new QLineEdit();
    lineMaxY->setObjectName("line_max_y");
    QLineEdit *lineMaxZ = new QLineEdit();
    lineMaxZ->setObjectName("line_max_z");

    QSettings *setfile_equip;
    if (pathfile != NULL){
        setfile_equip = new QSettings(pathfile, QSettings::IniFormat);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << setfile_equip->fileName() << endl;
    } else {
        setfile_equip = new QSettings(equips_dir + "__no__exists__", QSettings::IniFormat);
    }
    lineEquipName->setText(setfile_equip->value("name", "equipment"+QString::number(cnt_equips+1)).toString());
    int saved = setfile_equip->value("saved", 0).toInt();
    if (saved)
        lineEquipName->setEnabled(false);
    lineRadius->setText(setfile_equip->value("radius", 1).toString());
    lineWidth->setText(setfile_equip->value("width", 1).toString());
    lineMinX->setText(setfile_equip->value("centerx", 1).toString());
    lineMinY->setText(setfile_equip->value("centery", 1).toString());
    lineMinZ->setText(setfile_equip->value("centerz", 1).toString());
    lineMaxX->setText(setfile_equip->value("inclination", 2).toString());
    //lineMaxY->setText(setfile_equip->value("rotationy", 2).toString());
    lineMaxZ->setText(setfile_equip->value("azimuth", 2).toString());

    QPushButton *btn_update = new QPushButton();
    btn_update->setText("Show/Update");
    connect(btn_update, SIGNAL(clicked(bool)), this, SLOT(slotSaveEquipment()));

    QWidget *lineA = new QWidget;
    lineA->setFixedHeight(2);
    lineA->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    lineA->setStyleSheet(QString("background-color: #c0c0c0;"));
    QWidget *lineB = new QWidget;
    lineB->setFixedHeight(2);
    lineB->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    lineB->setStyleSheet(QString("background-color: #c0c0c0;"));

    int cnt = 0;
    QGridLayout *layout_coordinates = new QGridLayout();
    layout_coordinates->addWidget (new QLabel("Identifier:"), cnt, 0, Qt::AlignRight);
    layout_coordinates->addWidget (lineEquipName, cnt, 1, 1, 3);
    layout_coordinates->addWidget(lineA, ++cnt, 0, 1, 4);
    layout_coordinates->addWidget (new QLabel("Coordinates"), ++cnt, 0, Qt::AlignCenter);
    layout_coordinates->addWidget (new QLabel("X"), cnt, 1, Qt::AlignCenter);
    layout_coordinates->addWidget (new QLabel("Y"), cnt, 2,Qt::AlignCenter);
    layout_coordinates->addWidget (new QLabel("Z"), cnt, 3,Qt::AlignCenter);
    layout_coordinates->addWidget (new QLabel("Center:"), ++cnt, 0, Qt::AlignCenter);
    layout_coordinates->addWidget (lineMinX, cnt, 1);
    layout_coordinates->addWidget (lineMinY, cnt, 2);
    layout_coordinates->addWidget (lineMinZ, cnt, 3);
    layout_coordinates->addWidget(lineB, ++cnt, 0, 1, 4);
    //layout_coordinates->addWidget (new QLabel("Spheric:"), ++cnt, 0, Qt::AlignCenter);
    layout_coordinates->addWidget (new QLabel("Inclination:"), ++cnt, 0, Qt::AlignRight);
    layout_coordinates->addWidget (lineMaxX, cnt, 1);
    layout_coordinates->addWidget (new QLabel("Azimuth:"), cnt, 2, Qt::AlignRight);
    layout_coordinates->addWidget (lineMaxZ, cnt, 3);
    //layout_coordinates->addWidget (lineMaxY, cnt, 2);
    //layout_coordinates->addWidget (lineMaxZ, cnt, 3);
    layout_coordinates->addWidget (new QLabel("Radius:"), ++cnt, 0, Qt::AlignRight);
    layout_coordinates->addWidget (lineRadius, cnt, 1, 1, 1);
    layout_coordinates->addWidget (new QLabel("Width:"), cnt, 2, Qt::AlignRight);
    layout_coordinates->addWidget (lineWidth, cnt, 3, 1, 1);
    layout_coordinates->addWidget (new QLabel("Type:"), ++cnt, 0, Qt::AlignRight);
    layout_coordinates->addWidget (comboEquipType, cnt, 1, 1, 2);
    layout_coordinates->addWidget (btn_update, ++cnt, 0, Qt::AlignCenter);
    layout_coordinates->addWidget (btn_remove, cnt, 3, Qt::AlignRight);

    QWidget *wEquipment = new QWidget();
    wEquipment->setObjectName("equipment_box");
    wEquipment->setStyleSheet("background-color: cyan");
    wEquipment->setLayout(layout_coordinates);

    layoutAreaEquipment->addWidget(wEquipment);
}

void MeshWidget::resetAreas(){
    // SPACER
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Creamos el widget para WestArea
    QHBoxLayout* navArea = new QHBoxLayout(this);
    navArea->setSpacing(0);
    navArea->setMargin(0);
    navArea->setObjectName("area_nav");
    //navArea->addWidget(btnNext);

    QWidget* navWidget = new QWidget(this);
    navWidget->setBackgroundRole(QPalette::Midlight);
    navWidget->setStyleSheet(Globals::instance()->getCssArrows());
    navWidget->setLayout(navArea);

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->addSpacerItem(my_spacer);
    layoutAreaActions->addWidget(navWidget,Qt::BottomSection);
    //layoutAreaActions->addSpacerItem(my_spacer2);

    QLayoutItem *child;
    if(centralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        while ((child = centralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }
    centralWidget->findChild<QScrollArea*>("area_buttons")->setLayout(layoutAreaActions);

    //Clean Area Actions
    if(centralWidget->findChild<QScrollArea*>("area_actions")->layout() != NULL){
        child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_actions")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_actions")->layout();
    }

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    //centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(400);

    //Clear Area Viewers
    vtkViewer->reset();
    vtkViewer->update();
    vtkViewer->resetCamera();
}

void MeshWidget::calculateBase(){
    // Create a cube
    vtkSmartPointer<vtkCubeSource> cubeSource = vtkSmartPointer<vtkCubeSource>::New();

    double center[3];
    center[0] = minBound[0] + (0.5 * (maxBound[0] - minBound[0]));
    center[1] = minBound[1] + (0.5 * (maxBound[1] - minBound[1]));
    center[2] = minBound[2] + (0.5 * (maxBound[2] - minBound[2]));

    cubeSource->SetCenter(center);
    cubeSource->SetXLength(maxBound[0] - minBound[0]);
    cubeSource->SetYLength(maxBound[1] - minBound[1]);
    cubeSource->SetZLength(maxBound[2] - minBound[2]);

    // Create a mapper and actor
    vtkSmartPointer<vtkPolyDataMapper> mapperCube = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapperCube->SetInputConnection(cubeSource->GetOutputPort());

    vtkSmartPointer<vtkActor> actorCube = vtkSmartPointer<vtkActor>::New();
    actorCube->SetMapper(mapperCube);
    actorCube->GetProperty()->SetOpacity(0);

    vtkPolyData* cube = cubeSource->GetOutput();

    // Create the outline
    vtkSmartPointer<vtkOutlineFilter> outline =  vtkSmartPointer<vtkOutlineFilter>::New();
    outline->SetInputData(cube);

    vtkSmartPointer<vtkPolyDataMapper> outlineMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    outlineMapper->SetInputConnection(outline->GetOutputPort());

    vtkSmartPointer<vtkActor> outlineActor =  vtkSmartPointer<vtkActor>::New();
    outlineActor->SetMapper(outlineMapper);
    outlineActor->GetProperty()->SetColor(0,0,0);

    vtkViewer->add(actorCube);
    vtkViewer->add(outlineActor);
    vtkViewer->update();
}

void MeshWidget::loadSettingsBase(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_origin_x->setText(settings->value("MeshBase/origin-x", QString::number(minBound[0])).toString());
    line_origin_y->setText(settings->value("MeshBase/origin-y", QString::number(minBound[1])).toString());
    line_origin_z->setText(settings->value("MeshBase/origin-z", QString::number(minBound[2])).toString());
    line_length_x->setText(settings->value("MeshBase/length-x", QString::number(maxBound[0] - minBound[0])).toString());
    line_length_y->setText(settings->value("MeshBase/length-y", QString::number(maxBound[1] - minBound[1])).toString());
    line_length_z->setText(settings->value("MeshBase/length-z", QString::number(maxBound[2] - minBound[2])).toString());
    line_regular->setChecked(settings->value("MeshBase/regular", true).toBool());
    line_resolution->setText(settings->value("MeshBase/resolution", "0.5").toString());

    double resolution = line_resolution->text().toDouble();
    int n_x = int((maxBound[0] - minBound[0])/(resolution));
    int n_y = int((maxBound[1] - minBound[1])/(resolution));
    int n_z = int((maxBound[2] - minBound[2])/(resolution));

    line_nodes_x->setText(settings->value("MeshBase/nodes-x", QString::number(n_x)).toString());
    line_nodes_y->setText(settings->value("MeshBase/nodes-y", QString::number(n_y)).toString());
    line_nodes_z->setText(settings->value("MeshBase/nodes-z", QString::number(n_z)).toString());
    label_nodes_resolution->setText("(" + QString::number(n_x*n_y*n_z) + " cells)");
}

void MeshWidget::saveSettingsBase(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("MeshBase/origin-x", line_origin_x->text());
    settings->setValue("MeshBase/origin-y", line_origin_y->text());
    settings->setValue("MeshBase/origin-z", line_origin_z->text());
    settings->setValue("MeshBase/length-x", line_length_x->text());
    settings->setValue("MeshBase/length-y", line_length_y->text());
    settings->setValue("MeshBase/length-z", line_length_z->text());
    settings->setValue("MeshBase/regular", line_regular->isChecked());
    settings->setValue("MeshBase/resolution", line_resolution->text());
    settings->setValue("MeshBase/nodes-x", line_nodes_x->text());
    settings->setValue("MeshBase/nodes-y", line_nodes_y->text());
    settings->setValue("MeshBase/nodes-z", line_nodes_z->text());
}

void MeshWidget::createSnappyHexMeshDictFile(){
    QMapIterator<QString, physicalGroupMesh*> iter(*groups);

    QString path = Globals::instance()->getAppProjectPath() + "/system/snappyHexMeshDict";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"system\";\n\tobject\tsnappyHexMeshDict;}"
                  "\n#include\t\"../snappyGUI\""
                  "\n#include\t\"../snappyOrders\""
                  "\n#include\t\"../interfaceGUI\"\n"
                  "\ncastellatedMesh\t$step1;"
                  "\nsnap\t$step2;"
                  "\naddLayers\t$step3;\n"
                  "\ngeometry {" << endl;
        while (iter.hasNext()) {
            iter.next();
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << iter.key() << endl;
            if(iter.value()->getType() == 0)
                stream << "\t" << iter.key() << ".stl {\n\t\tname\t" << iter.key() << ";\n\t\ttype\ttriSurfaceMesh; }" << endl;
            else{
                QSettings *setfile_equip = new QSettings(equips_dir + iter.value()->getName(), QSettings::IniFormat);
                //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << setfile_equip->fileName() << endl;
                //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << setfile_equip->value("name") << endl;
                stream << "\t" << setfile_equip->value("name").toString() <<
                          " {\n\t\ttype searchableCylinder;\n" <<
                          "\t\tpoint1\t(" << setfile_equip->value("minx").toString() << " " <<  setfile_equip->value("miny").toString() << " " << setfile_equip->value("minz").toString() << ");\n" <<
                          "\t\tpoint2\t(" << setfile_equip->value("maxx").toString() << " " <<  setfile_equip->value("maxy").toString() << " " << setfile_equip->value("maxz").toString() << ");\n" <<
                          "\t\tradius\t" << setfile_equip->value("radius").toString() << "; }" << endl;
            }
        }
        stream << "}\ncastellatedMeshControls {" << endl;
        //stream << "\tlocationInMesh\t(" << QString::number(x) << " " << QString::number(y) << " " << QString::number(z) << ");" << endl;
        stream << "\tlocationInMesh\t($locationX $locationY $locationZ);" << endl;
        stream << "\trefinementSurfaces {" << endl;
        iter.toFront();
        while (iter.hasNext()) {
            iter.next();
            if(iter.value()->getType() == 0)
                stream << "\t\t" << iter.key() << " {\n\t\t\tlevel\t(" << iter.value()->getRefinementMin() << " " << iter.value()->getRefinementMax() << "); }\n";
        }
        stream << "\t}" << endl;
        stream << "\trefinementRegions {\n";
        iter.toFront();
        while (iter.hasNext()) {
            iter.next();
            if(iter.value()->getType() == 1)
                stream << "\t\t" << iter.value()->getName() <<
                          " {\n\t\t\tmode inside;\n" <<
                          "\t\t\tlevels\t((" << iter.value()->getRefinementMin() << " " << iter.value()->getRefinementMax() << ")); }\n";
        }
        stream << "\t}" << endl;
        stream << "\tfeatures ();" << endl;
        stream << "\tminRefinementCells\t0;" << endl;
        stream << "\tmaxGlobalCells\t10000000;" << endl;
        stream << "\tresolveFeatureAngle\t5;" << endl;
        stream << "\tnCellsBetweenLevels\t1;" << endl;
        stream << "\tmaxLocalCells\t500000;" << endl;
        stream << "\tallowFreeStandingZoneFaces\ttrue;\n}" << endl;

        stream << "snapControls {\n\t";
        stream << "nSmoothPatch\t$nSmooth;\n\t";
        stream << "tolerance\t$tolerance;\n\t";
        stream << "nSolveIter\t$nSnap;\n\t";
        stream << "nRelaxIter\t$nrSnap;\n\t";
        stream << "nFeatureSnapIter\t$nFeature;\n\t";
        stream << "implicitFeatureSnap\ttrue;\n\t";
        stream << "explicitFeatureSnap\tfalse;\n}\n";

        stream << "addLayersControls {\n\tlayers {\n";
        iter.toFront();
        while (iter.hasNext()) {
            iter.next();
            int aux = iter.value()->getLayers();
            if (aux > 0)
                stream << "\t\t" << iter.key() << " {\n\t\t\tnSurfaceLayers\t" << aux << "; }\n";
        }
        stream << "\t}\n\t";
        stream << "nSmoothSurfaceNormals\t5;\n\t";
        stream << "slipFeatureAngle\t30.0;\n\t";
        stream << "nBufferCellsNoExtrude\t0;\n\t";
        stream << "nRelaxIter\t5;\n\t";
        stream << "relativeSizes\tfalse;\n\t";
        stream << "minMedianAxisAngle\t90.0;\n\t";
        stream << "maxFaceThicknessRatio\t0.5;\n\t";
        stream << "nSmoothNormals\t3;\n\t";
        stream << "maxThicknessToMedialRatio\t0.3;\n\t";
        stream << "nLayerIter\t50;\n\t";
        stream << "minThickness\t$minThick;\n\t";
        stream << "nSmoothThickness\t10;\n\t";
        stream << "nGrow\t10;\n\t";
        stream << "nRelaxedIter\t20;\n\t";
        stream << "concaveAngle\t90.0;\n\t";
        stream << "featureAngle\t60;\n\t";
        stream << "firstLayerThickness\t$thick;\n\t";
        stream << "expansionRatio\t$expansion;\n}\n";

        stream << "meshQualityControls {\n\tminTetQuality	1.0E-20;\n\tminVol	1.0E-14;\n\tmaxInternalSkewness	4.0;\n\tmaxBoundarySkewness	20.0;\n\tmaxConcave	80.0;\n\tminFaceWeight	0.05;\n\tminVolRatio	0.01;\n\tminTwist	0.05;\n\tminArea	-1.0;\n\tmaxNonOrtho	65.0;\n\tminTriangleTwist	-1.0;\n\tminDeterminant	0.01;\n\terrorReduction	0.75;\n\tnSmoothScale	4;\n\trelaxed {\n\t\tmaxNonOrtho	75.0; }\n}\n";
        stream << "mergeTolerance	1e-03;\ndebug	0;";
        file.close();
    }
}

void MeshWidget::showBase(){
    //Check one of the files created by OpenFoam during the base process (boundary, faces, neighbour, owner, points)
    QString str = Globals::instance()->getAppProjectPath() + "/constant/polyMesh/boundary";
    if(QFileInfo(str).exists()){
        blockMeshFinished();
    }

    btnBase->setChecked(true);
    btnEquip->setChecked(false);
    btnSnappy->setChecked(false);
    btnSnappy2->setChecked(false);
    btnSnappy3->setChecked(false);
    btnCheckMesh->setChecked(false);

    centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);
    // Ponemos el cubo por defecto
    calculateBase();
    vtkViewer->update();

    // Creamos la lista temporal de signals para poder pasar argumentos
    QSignalMapper *originSignalMapper = new QSignalMapper(this);
    QSignalMapper *lengthSignalMapper = new QSignalMapper(this);

    QLabel* l_origin = new QLabel("Box origin (meters)");
    label_origin_x = new QLabel("x: ");
    label_origin_x->setAlignment(Qt::AlignRight);
    label_origin_y = new QLabel("y: ");
    label_origin_y->setAlignment(Qt::AlignRight);
    label_origin_z = new QLabel("z: ");
    label_origin_z->setAlignment(Qt::AlignRight);

    line_origin_x = new QLineEdit;
    line_origin_x->setValidator(Globals::instance()->getDoubleValidator(this));
    //line_origin_x->setText(QString::number(minBound[0]));
    line_origin_x->setAlignment(Qt::AlignRight);
    line_origin_x->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_origin_x->setMaximumWidth(70);
    //connect(line_origin_x, SIGNAL(returnPressed()), originSignalMapper, SLOT(map()));
    //originSignalMapper->setMapping(line_origin_x, "line_origin_x");

    line_origin_y = new QLineEdit;
    line_origin_y->setValidator(Globals::instance()->getDoubleValidator(this));
    //line_origin_y->setText(QString::number(minBound[1]));
    line_origin_y->setAlignment(Qt::AlignRight);
    line_origin_y->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_origin_y->setMaximumWidth(70);
    //connect(line_origin_y, SIGNAL(returnPressed()), originSignalMapper, SLOT(map()));
    //originSignalMapper->setMapping(line_origin_y, "line_origin_y");

    line_origin_z = new QLineEdit;
    line_origin_z->setValidator(Globals::instance()->getDoubleValidator(this));
    //line_origin_z->setText(QString::number(minBound[2]));
    line_origin_z->setAlignment(Qt::AlignRight);
    line_origin_z->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_origin_z->setMaximumWidth(70);
    //connect(line_origin_z, SIGNAL(returnPressed()), originSignalMapper, SLOT(map()));
    //originSignalMapper->setMapping(line_origin_z, "line_origin_z");

    QHBoxLayout* layoutOrigin = new QHBoxLayout();
    layoutOrigin->setMargin(0);
    layoutOrigin->setSpacing(0);
    layoutOrigin->addWidget(label_origin_x,Qt::AlignRight);
    layoutOrigin->addWidget(line_origin_x,Qt::AlignRight);
    layoutOrigin->addWidget(label_origin_y,Qt::AlignRight);
    layoutOrigin->addWidget(line_origin_y,Qt::AlignRight);
    layoutOrigin->addWidget(label_origin_z,Qt::AlignRight);
    layoutOrigin->addWidget(line_origin_z,Qt::AlignRight);

    QWidget* widgetOrigin = new QWidget();
    widgetOrigin->setLayout(layoutOrigin);
    //connect(originSignalMapper, SIGNAL(mapped(QString)), this, SLOT(onChangeOrigin(const QString&)));



    QLabel* l_length = new QLabel("Box length (meters)");
    label_length_x = new QLabel("x: ");
    label_length_x->setAlignment(Qt::AlignRight);
    label_length_y = new QLabel("y: ");
    label_length_y->setAlignment(Qt::AlignRight);
    label_length_z = new QLabel("z: ");
    label_length_z->setAlignment(Qt::AlignRight);

    line_length_x = new QLineEdit;
    line_length_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_length_x->setAlignment(Qt::AlignRight);
    line_length_x->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_length_x->setMaximumWidth(70);
    //connect(line_length_x, SIGNAL(returnPressed()), lengthSignalMapper, SLOT(map()));
    //lengthSignalMapper->setMapping(line_length_x, "line_length_x");

    line_length_y = new QLineEdit;
    line_length_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_length_y->setAlignment(Qt::AlignRight);
    line_length_y->setMaximumWidth(70);
    line_length_y->setStyleSheet(Globals::instance()->getCssLineEdit());
    //connect(line_length_y, SIGNAL(returnPressed()), lengthSignalMapper, SLOT(map()));
    //lengthSignalMapper->setMapping(line_length_y, "line_length_y");

    line_length_z = new QLineEdit;
    line_length_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_length_z->setAlignment(Qt::AlignRight);
    line_length_z->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_length_z->setMaximumWidth(70);
    //connect(line_length_z, SIGNAL(returnPressed()), lengthSignalMapper, SLOT(map()));
    //lengthSignalMapper->setMapping(line_length_z, "line_length_z");

    QHBoxLayout* layoutLength = new QHBoxLayout();
    layoutLength->setMargin(0);
    layoutLength->setSpacing(0);
    layoutLength->addWidget(label_length_x,Qt::AlignRight);
    layoutLength->addWidget(line_length_x,Qt::AlignRight);
    layoutLength->addWidget(label_length_y,Qt::AlignRight);
    layoutLength->addWidget(line_length_y,Qt::AlignRight);
    layoutLength->addWidget(label_length_z,Qt::AlignRight);
    layoutLength->addWidget(line_length_z,Qt::AlignRight);

    QWidget* widgetLength = new QWidget();
    widgetLength->setLayout(layoutLength);
    //connect(lengthSignalMapper, SIGNAL(mapped(QString)), this, SLOT(onChangeLength(const QString&)));



    label_regular = new QLabel("Regular cells");
    line_regular = new QCheckBox;
    line_regular->setChecked(true);
    connect(line_regular, SIGNAL(stateChanged(int)),this, SLOT(onRegularCellsChange(int)));

    QHBoxLayout* layoutRegularCells = new QHBoxLayout();
    layoutRegularCells->setMargin(0);
    layoutRegularCells->addWidget(label_regular);
    layoutRegularCells->addWidget(line_regular);
    layoutRegularCells->setAlignment(Qt::AlignLeft);

    QWidget* widgetRegularCells = new QWidget();
    widgetRegularCells->setLayout(layoutRegularCells);

    line_resolution = new QLineEdit("");
    connect(line_resolution, SIGNAL(returnPressed()),this, SLOT(updateSlider()));
    label_resolution = new QLabel("Resolution");
    line_slider = new QSlider(Qt::Horizontal);
    line_slider->setRange(1, 1000);
    connect(line_slider, SIGNAL(sliderReleased()),this, SLOT(onSliderStop()));

    QLabel* lMin = new QLabel("0.001 m");
    QLabel* lMax = new QLabel("1 m");

    QHBoxLayout* layoutSlider = new QHBoxLayout();
    layoutSlider->addWidget(lMin);
    layoutSlider->addWidget(line_slider);
    layoutSlider->addWidget(lMax);
    QWidget* widgetSlider = new QWidget();
    widgetSlider->setLayout(layoutSlider);
    widgetSlider->setMaximumHeight(50);

    line_nodes_x = new QLineEdit;
    line_nodes_x->setValidator(Globals::instance()->getDoubleValidator(this));
    line_nodes_x->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_nodes_x->setDisabled(true);
    line_nodes_x->setAlignment(Qt::AlignRight);
    line_nodes_x->setMaximumWidth(70);

    line_nodes_y = new QLineEdit;
    line_nodes_y->setValidator(Globals::instance()->getDoubleValidator(this));
    line_nodes_y->setDisabled(true);
    line_nodes_y->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_nodes_y->setMaximumWidth(70);
    line_nodes_y->setAlignment(Qt::AlignRight);

    line_nodes_z = new QLineEdit;
    line_nodes_z->setValidator(Globals::instance()->getDoubleValidator(this));
    line_nodes_z->setDisabled(true);
    line_nodes_z->setAlignment(Qt::AlignRight);
    line_nodes_z->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_nodes_z->setMaximumWidth(70);

    label_nodes_x = new QLabel("x:");
    label_nodes_y = new QLabel("y:");
    label_nodes_z = new QLabel("z:");

    label_nodes_x->setAlignment(Qt::AlignRight);
    label_nodes_y->setAlignment(Qt::AlignRight);
    label_nodes_z->setAlignment(Qt::AlignRight);

    label_nodes_resolution = new QLabel("(N/A cells)");
    label_nodes_resolution->setAlignment(Qt::AlignRight);

    QHBoxLayout* layoutNodes = new QHBoxLayout();
    layoutNodes->setMargin(0);
    layoutNodes->setSpacing(0);
    layoutNodes->addWidget(label_nodes_x,Qt::AlignRight);
    layoutNodes->addWidget(line_nodes_x,Qt::AlignRight);
    layoutNodes->addWidget(label_nodes_y,Qt::AlignRight);
    layoutNodes->addWidget(line_nodes_y,Qt::AlignRight);
    layoutNodes->addWidget(label_nodes_z,Qt::AlignRight);
    layoutNodes->addWidget(line_nodes_z,Qt::AlignRight);
    layoutNodes->setAlignment(Qt::AlignRight);

    QHBoxLayout* layoutResolution = new QHBoxLayout();
    layoutResolution->setMargin(0);
    layoutResolution->setSpacing(0);
    layoutResolution->addWidget(label_nodes_resolution,Qt::AlignRight);
    layoutResolution->setAlignment(Qt::AlignRight);


    QWidget* widgetNodes = new QWidget();
    widgetNodes->setLayout(layoutNodes);
    widgetNodes->setLayout(layoutResolution);

    QWidget* widgetResolution = new QWidget();
    widgetResolution->setLayout(layoutResolution);

    QLabel* l_nodes_length = new QLabel("Node Length");
    l_nodes_length->setMaximumWidth(200);
    QLabel* l_num_length = new QLabel("0.5 m");
    l_num_length->setObjectName("l_num_length");

    QPushButton* btnBlockMesh = new QPushButton("RUN", this);
    btnBlockMesh->setObjectName("btnBlockMesh");
    btnBlockMesh->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnBlockMesh->setMinimumHeight(30);
    connect(btnBlockMesh, SIGNAL(clicked()), this, SLOT(blockMesh()));

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__ << endl;

    QLabel* l_n = new QLabel("Number of nodes");

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(l_origin);
    layout->addWidget(widgetOrigin);
    layout->addWidget(l_length);
    layout->addWidget(widgetLength);
    layout->addWidget(widgetRegularCells);
    layout->addWidget(label_resolution);
    layout->addWidget(line_resolution);
    layout->addWidget(widgetSlider);
    layout->addWidget(l_n);
    layout->addWidget(widgetNodes);
    layout->addWidget(widgetResolution);
    layout->addWidget(btnBlockMesh);
    layout->addSpacerItem(my_spacer);

    loadSettingsBase();
    line_slider->setValue(line_resolution->text().toDouble()*1000);

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void MeshWidget::getFacesNames(){
    list_faces_names = new QList<QString>();
    QDir dir = QDir(Globals::instance()->getAppProjectPath() + "/constant/triSurface");
    QStringList dirList = dir.entryList();
    foreach(QString file, dirList){
        if(file.compare(".") != 0 && file.compare("..") != 0 && file.compare("cad.stl") != 0 && file.compare("cad.eMesh") != 0){
            list_faces_names->append(file.split(".")[0]);
        }
    }
}

void MeshWidget::pasteTextLayers()
{
    QLineEdit *line = qobject_cast<QLineEdit*>(sender());
    QWidget* w = qobject_cast<QWidget *>(line->parent());

    QLineEdit *line_layers = (QLineEdit*)w->findChild<QWidget*>("face_layers");
    physicalGroupMesh *group = groups->value(w->objectName());
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << line_layers->text().toInt() << endl;
    group->setLayers(line_layers->text().toInt());
}

void MeshWidget::pasteText()
{
    QLineEdit *line = qobject_cast<QLineEdit*>(sender());
    QWidget* w = qobject_cast<QWidget *>(line->parent());

    QLineEdit *line_min = (QLineEdit*)w->findChild<QWidget*>("face_min");
    QLineEdit *line_max = (QLineEdit*)w->findChild<QWidget*>("face_max");
    physicalGroupMesh *group = groups->value(w->objectName());
    group->setRefinementMin(line_min->text().toInt());
    group->setRefinementMax(line_max->text().toInt());
}

void MeshWidget::changeLocationInMesh(){
    //Check the dir "0.1" created by OpenFoam during the sculpt process
    /*QString str = Globals::instance()->getAppProjectPath() + "/0.1";
    if(QDir(str).exists()){
        vtkViewer->reset();
        snappyHexMeshStep1Finished();
    } else {
        return;
    }*/

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("MeshSculpt/locationX", line_location_x->text());
    settings->setValue("MeshSculpt/locationY", line_location_y->text());
    settings->setValue("MeshSculpt/locationZ", line_location_z->text());

    // Create a sphere
    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetCenter(line_location_x->text().toFloat(), line_location_y->text().toFloat(), line_location_z->text().toFloat());
    sphereSource->SetRadius(0.5);

    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    //mapper->SetInputData(point);
    mapper->SetInputConnection(sphereSource->GetOutputPort());

    if (vtkLocationMesh)
        vtkViewer->m_renderer->RemoveActor(vtkLocationMesh);

    vtkLocationMesh = vtkSmartPointer<vtkActor>::New();
    vtkLocationMesh->SetMapper(mapper);
    vtkLocationMesh->GetProperty()->SetColor(1,0,0);
    vtkLocationMesh->GetProperty()->SetOpacity(1);

    vtkViewer->m_renderer->AddActor(vtkLocationMesh);
    vtkViewer->update();
}

void MeshWidget::showSculpt(){
    //Check the dir "0.1" created by OpenFoam during the sculpt process
    QString str = Globals::instance()->getAppProjectPath() + "/0.1";
    if(QDir(str).exists()){
        snappyHexMeshStep1Finished();
    }

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    btnBase->setChecked(false);
    btnEquip->setChecked(false);
    btnSnappy->setChecked(true);
    btnSnappy2->setChecked(false);
    btnSnappy3->setChecked(false);
    btnCheckMesh->setChecked(false);


    line_location_x = new QLineEdit(settings->value("MeshSculpt/locationX", 0).toString());
    line_location_x->setFixedWidth(30);
    line_location_y = new QLineEdit(settings->value("MeshSculpt/locationY", 0).toString());
    line_location_y->setFixedWidth(30);
    line_location_z = new QLineEdit(settings->value("MeshSculpt/locationZ", 0).toString());
    line_location_z->setFixedWidth(30);

    QHBoxLayout* layout_location = new QHBoxLayout();
    layout_location->addSpacing(10);
    layout_location->addWidget(new QLabel("X:"));
    layout_location->addSpacing(10);
    layout_location->addWidget(line_location_x);
    layout_location->addSpacing(10);
    layout_location->addWidget(new QLabel("Y:"));
    layout_location->addSpacing(10);
    layout_location->addWidget(line_location_y);
    layout_location->addSpacing(10);
    layout_location->addWidget(new QLabel("Z:"));
    layout_location->addSpacing(10);
    layout_location->addWidget(line_location_z);
    layout_location->addSpacing(10);
    layout_location->addSpacing(10);

    layout_sculpt = new QVBoxLayout();

    layout_sculpt->addWidget(new QLabel("Location in mesh"));
    layout_sculpt->addLayout(layout_location);

    QPushButton* btnLocation = new QPushButton("Apply", this);
    //btnRun->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    //btnRun->setMinimumHeight(30);
    connect(btnLocation, SIGNAL(clicked()), this, SLOT(changeLocationInMesh()));

    layout_sculpt->addWidget(btnLocation);
    layout_sculpt->addSpacing(20);
    layout_sculpt->addWidget(new QLabel("Refinement surfaces"));

    //groups = new QList<physicalGroupMesh*>();
    groups = new QMap<QString, physicalGroupMesh*>();
    getFacesNames();
    for(int i=0; i<list_faces_names->size(); i++){
        QWidget *widget_face_sculpt = new QWidget();
        QVBoxLayout *layout_face_sculpt = new QVBoxLayout();
        widget_face_sculpt->setLayout(layout_face_sculpt);

        QWidget *widget_face_params = new QWidget();
        //widget_face_params->setObjectName("face_params");
        QHBoxLayout *layout_minmax = new QHBoxLayout();
        widget_face_params->setLayout(layout_minmax);
        layout_minmax->addStretch();
        layout_minmax->addWidget(new QLabel("Minimum:"));
        layout_minmax->addSpacing(10);
        QLineEdit *line_min = new QLineEdit();
        line_min->setObjectName("face_min");
        line_min->setFixedWidth(30);
        layout_minmax->addWidget(line_min);
        layout_minmax->addSpacing(20);
        layout_minmax->addWidget(new QLabel("Maximum:"));
        layout_minmax->addSpacing(10);
        QLineEdit *line_max = new QLineEdit();
        line_max->setObjectName("face_max");
        line_max->setFixedWidth(30);
        layout_minmax->addWidget(line_max);

        //if(list_faces_names->at(i) == "Default"){
        //    line_min->setText(QString::number(0));
        //    line_max->setText(QString::number(0));
        //} else {
        QString str = "MeshSculpt/min_" + list_faces_names->at(i);
        line_min->setText(settings->value(str, "0").toString());
        str = "MeshSculpt/max_" + list_faces_names->at(i);
        line_max->setText(settings->value(str, "0").toString());
        //}

        //connect(line_max,SIGNAL(editingFinished()),this,SLOT(pasteText()));
        connect(line_min,SIGNAL(textChanged(QString)),this,SLOT(pasteText()));
        connect(line_max,SIGNAL(textChanged(QString)),this,SLOT(pasteText()));

        QLabel* label_face = new QLabel(list_faces_names->at(i));
        widget_face_params->setObjectName(list_faces_names->at(i));
        //label_face->setObjectName("face_name");
        layout_face_sculpt->addWidget(label_face);
        layout_face_sculpt->addWidget(widget_face_params);
        layout_sculpt->addWidget(widget_face_sculpt);
        if (list_faces_names->at(i) == "Default")
            widget_face_sculpt->setVisible(false);
        physicalGroupMesh *group = new physicalGroupMesh(list_faces_names->at(i), line_min->text().toInt(), line_max->text().toInt());
        groups->insert(list_faces_names->at(i), group);
    }

    layout_sculpt->addWidget(new QLabel("Refinement equipments"));

    QDirIterator it(equips_dir, QStringList() << "*", QDir::Files);
    while (it.hasNext()){
        QSettings *setfile_equip = new QSettings(it.next(), QSettings::IniFormat);
        QString equip_name = setfile_equip->value("name").toString();

        QWidget *widget_face_sculpt = new QWidget();
        QVBoxLayout *layout_face_sculpt = new QVBoxLayout();
        widget_face_sculpt->setLayout(layout_face_sculpt);

        QWidget *widget_face_params = new QWidget();
        QHBoxLayout *layout_minmax = new QHBoxLayout();
        widget_face_params->setLayout(layout_minmax);

        layout_minmax->addStretch();
        layout_minmax->addWidget(new QLabel("Minimum:"));
        layout_minmax->addSpacing(10);
        QLineEdit *line_min = new QLineEdit();
        line_min->setObjectName("face_min");
        line_min->setFixedWidth(30);
        layout_minmax->addWidget(line_min);
        layout_minmax->addSpacing(20);
        layout_minmax->addWidget(new QLabel("Maximum:"));
        layout_minmax->addSpacing(10);
        QLineEdit *line_max = new QLineEdit();
        line_max->setObjectName("face_max");
        line_max->setFixedWidth(30);
        layout_minmax->addWidget(line_max);

        QString str = "MeshSculpt/min_" + equip_name;
        line_min->setText(settings->value(str, "0").toString());
        str = "MeshSculpt/max_" + equip_name;
        line_max->setText(settings->value(str, "0").toString());

        connect(line_min,SIGNAL(textChanged(QString)),this,SLOT(pasteText()));
        connect(line_max,SIGNAL(textChanged(QString)),this,SLOT(pasteText()));

        QLabel* label_face = new QLabel(equip_name);
        widget_face_params->setObjectName(equip_name);
        layout_face_sculpt->addWidget(label_face);
        layout_face_sculpt->addWidget(widget_face_params);
        layout_sculpt->addWidget(widget_face_sculpt);
        //if (list_faces_names->at(i) == "Default")
        //    widget_face_sculpt->setVisible(false);
        physicalGroupMesh *group = new physicalGroupMesh(equip_name, line_min->text().toInt(), line_max->text().toInt());
        group->setType(1);
        groups->insert(equip_name, group);
    }

    //SLUDGE REGION
    QWidget *widget_face_sculpt = new QWidget();
    QVBoxLayout *layout_face_sculpt = new QVBoxLayout();
    widget_face_sculpt->setLayout(layout_face_sculpt);

    layout_sculpt->addWidget(widget_face_sculpt);

    //BUTTON RUN
    QPushButton* btnRun = new QPushButton("RUN", this);
    btnRun->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnRun->setMinimumHeight(30);
    connect(btnRun, SIGNAL(clicked()), this, SLOT(snappyHexMeshStep1()));

    layout_sculpt->addWidget(btnRun);
    layout_sculpt->addStretch();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout_sculpt);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void MeshWidget::loadSettingsSnap(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_snap_it->setText(settings->value("MeshSnap/snap", "300").toString());
    line_relax_it->setText(settings->value("MeshSnap/relax", "5").toString());
    line_feature_it->setText(settings->value("MeshSnap/feat", "10").toString());
    line_smooth_it->setText(settings->value("MeshSnap/smooth", "3").toString());
    line_tolerance->setText(settings->value("MeshSnap/tolerance", "1").toString());
}

void MeshWidget::saveSettingsSnap(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("MeshSnap/snap", line_snap_it->text());
    settings->setValue("MeshSnap/relax", line_relax_it->text());
    settings->setValue("MeshSnap/feat", line_feature_it->text());
    settings->setValue("MeshSnap/smooth", line_smooth_it->text());
    settings->setValue("MeshSnap/tolerance", line_tolerance->text());
}

void MeshWidget::showSnap(){
    //Check the dir "0.2" created by OpenFoam during the snap process
    QString str = Globals::instance()->getAppProjectPath() + "/0.2";
    if(QDir(str).exists()){
        snappyHexMeshStep2Finished();
    }

    btnBase->setChecked(false);
    btnSnappy->setChecked(false);
    btnSnappy2->setChecked(true);
    btnSnappy3->setChecked(false);
    btnCheckMesh->setChecked(false);

    QPushButton* btnStep1 = new QPushButton("RUN", this);
    btnStep1->setObjectName("btnBlockMesh");
    btnStep1->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnStep1->setMinimumHeight(30);
    connect(btnStep1, SIGNAL(clicked()), this, SLOT(snappyHexMeshStep2()));

    QLabel* l_n = new QLabel("Number of snapping iterations");
    line_snap_it = new QLineEdit;
    line_snap_it->setValidator(Globals::instance()->getDoubleValidator(this));
    line_snap_it->setAlignment(Qt::AlignRight);
    line_snap_it->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_snap_it->setMaximumWidth(70);

    QHBoxLayout* layoutIn = new QHBoxLayout();
    layoutIn->setMargin(0);
    layoutIn->addWidget(line_snap_it);
    layoutIn->setAlignment(Qt::AlignRight);
    QWidget* widgetIn = new QWidget();
    widgetIn->setLayout(layoutIn);

    QLabel* l_nr = new QLabel("Number of relaxation iterations");
    line_relax_it = new QLineEdit;
    line_relax_it->setValidator(Globals::instance()->getDoubleValidator(this));
    line_relax_it->setAlignment(Qt::AlignRight);
    line_relax_it->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_relax_it->setMaximumWidth(70);

    QHBoxLayout* layoutInr = new QHBoxLayout();
    layoutInr->setMargin(0);
    layoutInr->addWidget(line_relax_it);
    layoutInr->setAlignment(Qt::AlignRight);
    QWidget* widgetInr = new QWidget();
    widgetInr->setLayout(layoutInr);

    QLabel* l_f = new QLabel("Number of feature snapping iterations");
    line_feature_it = new QLineEdit;
    line_feature_it->setValidator(Globals::instance()->getDoubleValidator(this));
    line_feature_it->setAlignment(Qt::AlignRight);
    line_feature_it->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_feature_it->setMaximumWidth(70);

    QHBoxLayout* layoutF = new QHBoxLayout();
    layoutF->setMargin(0);
    layoutF->addWidget(line_feature_it);
    layoutF->setAlignment(Qt::AlignRight);
    QWidget* widgetF = new QWidget();
    widgetF->setLayout(layoutF);

    QLabel* l_s = new QLabel("Number of smoothing iterations");
    line_smooth_it = new QLineEdit;
    line_smooth_it->setValidator(Globals::instance()->getDoubleValidator(this));
    line_smooth_it->setAlignment(Qt::AlignRight);
    line_smooth_it->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_smooth_it->setMaximumWidth(70);

    QHBoxLayout* layoutS = new QHBoxLayout();
    layoutS->setMargin(0);
    layoutS->addWidget(line_smooth_it);
    layoutS->setAlignment(Qt::AlignRight);
    QWidget* widgetS = new QWidget();
    widgetS->setLayout(layoutS);

    QLabel* l_t = new QLabel("Tolerance");
    line_tolerance = new QLineEdit;
    line_tolerance->setValidator(Globals::instance()->getDoubleValidator(this));
    line_tolerance->setAlignment(Qt::AlignRight);
    line_tolerance->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_tolerance->setMaximumWidth(70);

    QHBoxLayout* layoutT = new QHBoxLayout();
    layoutT->setMargin(0);
    layoutT->addWidget(line_tolerance);
    layoutT->setAlignment(Qt::AlignRight);
    QWidget* widgetT = new QWidget();
    widgetT->setLayout(layoutT);

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(l_n,Qt::AlignRight);
    layout->addWidget(widgetIn,Qt::AlignRight);
    layout->addWidget(l_nr,Qt::AlignRight);
    layout->addWidget(widgetInr,Qt::AlignRight);
    layout->addWidget(l_f,Qt::AlignRight);
    layout->addWidget(widgetF,Qt::AlignRight);
    layout->addWidget(l_s,Qt::AlignRight);
    layout->addWidget(widgetS,Qt::AlignRight);
    layout->addWidget(l_t,Qt::AlignRight);
    layout->addWidget(widgetT,Qt::AlignRight);
    layout->addWidget(btnStep1,Qt::AlignRight);
    layout->addSpacerItem(my_spacer);

    loadSettingsSnap();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void MeshWidget::showLayer(){
    //Check the dir "0.3" created by OpenFoam during the layer process
    QString str = Globals::instance()->getAppProjectPath() + "/0.3";
    if(QDir(str).exists()){
        //progressBarProcess = new QProgressDialog(parent);
        //progressBarProcess->setLabelText("Loading...");
        snappyHexMeshStep3Finished();
    }

    btnBase->setChecked(false);
    btnSnappy->setChecked(false);
    btnSnappy2->setChecked(false);
    btnSnappy3->setChecked(true);
    btnCheckMesh->setChecked(false);

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    QDoubleValidator *validator= new QDoubleValidator(this);
    validator->setBottom(0.00);
    validator->setDecimals(4);
    validator->setTop(100.00);
    validator->setNotation(QDoubleValidator::StandardNotation);

    QVBoxLayout *layout_faces = new QVBoxLayout();
    layout_faces->addWidget(new QLabel("Number of layers per face"));
    for(int i=0; i<list_faces_names->size(); i++){
        QWidget *widget_face_layers = new QWidget();
        widget_face_layers->setObjectName(list_faces_names->at(i));
        QHBoxLayout *layout_face_layers = new QHBoxLayout();
        widget_face_layers->setLayout(layout_face_layers);
        layout_face_layers->addWidget(new QLabel(list_faces_names->at(i)));
        QLineEdit *line_layers = new QLineEdit();
        line_layers->setObjectName("face_layers");
        line_layers->setFixedWidth(60);
        layout_face_layers->addStretch();
        layout_face_layers->addWidget(line_layers);
        layout_faces->addWidget(widget_face_layers);

        connect(line_layers,SIGNAL(textChanged(QString)),this,SLOT(pasteTextLayers()));

        if(list_faces_names->at(i) == "Default"){
            line_layers->setText(QString::number(0));
            widget_face_layers->setVisible(false);
        } else {
            QString str = "MeshLayer/layers_" + list_faces_names->at(i);
            line_layers->setText(settings->value(str, "0").toString());
        }
    }

    line_thickness_first = new QLineEdit;
    line_thickness_first->setValidator(validator);
    line_thickness_first->setAlignment(Qt::AlignRight);
    line_thickness_first->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_thickness_first->setMaximumWidth(70);
    line_thickness_first->setText(settings->value("MeshLayer/thickfirst", "0.4").toString());

    /*
    QLabel* l_minThick = new QLabel("Minimum thickness");

    line_thickness_min = new QLineEdit;
    line_thickness_min->setValidator(validator);
    line_thickness_min->setAlignment(Qt::AlignRight);
    line_thickness_min->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_thickness_min->setMaximumWidth(70);


    QHBoxLayout* layoutMinThick = new QHBoxLayout();
    layoutMinThick->setMargin(0);
    layoutMinThick->addWidget(line_thickness_min);
    layoutMinThick->setAlignment(Qt::AlignRight);
    QWidget* widgetMinThick = new QWidget();
    widgetMinThick->setLayout(layoutMinThick);
    */

    line_expansion = new QLineEdit;
    line_expansion->setValidator(validator);
    line_expansion->setAlignment(Qt::AlignRight);
    line_expansion->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_expansion->setMaximumWidth(70);
    line_expansion->setText(settings->value("MeshLayer/expansion", "1").toString());

    QPushButton* btnStep1 = new QPushButton("RUN", this);
    btnStep1->setObjectName("btnBlockMesh");
    btnStep1->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnStep1->setMinimumHeight(30);
    connect(btnStep1, SIGNAL(clicked()), this, SLOT(snappyHexMeshStep3()));

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(layout_faces);
    layout->addWidget(new QLabel("First layer thickness"));
    layout->addWidget(line_thickness_first);
    layout->addWidget(new QLabel("Expansion ratio"));
    layout->addWidget(line_expansion);
    layout->addWidget(btnStep1);
    layout->addStretch();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! SLOTS
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void MeshWidget::onRegularCellsChange(int value){
    line_nodes_x->setEnabled(value == 0);
    line_nodes_y->setEnabled(value == 0);
    line_nodes_z->setEnabled(value == 0);
    line_slider->setEnabled((value != 0));
}

void MeshWidget::updateSlider(){
    int aux = line_resolution->text().toDouble() * 1000;
    line_slider->setValue(aux);

    double length_node = line_slider->value()/1000.0;
    int n_x = int((maxBound[0] - minBound[0])/(length_node));
    int n_y = int((maxBound[1] - minBound[1])/(length_node));
    int n_z = int((maxBound[2] - minBound[2])/(length_node));

    size_t cells = n_x*n_y*n_z;
    line_nodes_x->setText(QString::number(n_x));
    line_nodes_y->setText(QString::number(n_y));
    line_nodes_z->setText(QString::number(n_z));
    label_nodes_resolution->setText("("+ QString::number(cells) +" cells)");
}

void MeshWidget::onSliderStop(){
    double length_node = line_slider->value()/1000.0;
    int n_x = int((maxBound[0] - minBound[0])/(length_node));
    int n_y = int((maxBound[1] - minBound[1])/(length_node));
    int n_z = int((maxBound[2] - minBound[2])/(length_node));

    /*int n_x = int((maxBound[0] - minBound[0])/2);
    int n_y = int((maxBound[1] - minBound[1])/2);
    int n_z = int((maxBound[2] - minBound[2])/2);*/

    line_resolution->setText(QString::number(length_node));
    line_nodes_x->setText(QString::number(n_x));
    line_nodes_y->setText(QString::number(n_y));
    line_nodes_z->setText(QString::number(n_z));
    label_nodes_resolution->setText("("+QString::number(n_x*n_y*n_z)+" cells)");
}

void MeshWidget::onChangeLength(const QString& idPoint) {
    // Actualizamos el punto
    QString newValue = centralWidget->findChild<QLineEdit*>(idPoint)->text();

    QString data = idPoint;
    QStringList strings = data.split("_");
    bool modified = false;

    if(strings.at(1).compare("x") == 0 && !Globals::instance()->compare_double((maxBound[0] - minBound[0]), newValue.toDouble())){
        modified = true;
        maxBound[0] = minBound[0] + newValue.toDouble();
    }
    else if(strings.at(1).compare("y") == 0 && !Globals::instance()->compare_double((maxBound[1] - minBound[1]), newValue.toDouble())){
        modified = true;
        maxBound[1] = minBound[1] + newValue.toDouble();
    }
    else if(strings.at(1).compare("z") == 0 && !Globals::instance()->compare_double((maxBound[2] - minBound[2]), newValue.toDouble())){
        modified = true;
        maxBound[2] = minBound[2] + newValue.toDouble();
    }

    if(modified){
        vtkViewer->reset();
        QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
        vtkViewer->add(filename.toStdString().c_str());
        calculateBase();

        // Si esta activo el tamaño de celdas regular
        if(line_regular->isChecked()){
            onSliderStop();
        }

        vtkViewer->update();
    }
    else{
        QMessageBox::about(this, tr("Origin Error"),
                           tr("<p>Not Modified Point</p>"));
    }
}

void MeshWidget::onChangeOrigin(const QString& idPoint) {
    // Actualizamos el punto
    QString newValue = centralWidget->findChild<QLineEdit*>(idPoint)->text();

    QString data = idPoint;
    QStringList strings = data.split("_");
    bool modified = false;

    if(strings.at(1).compare("x") == 0 && !Globals::instance()->compare_double(minBound[0], newValue.toDouble())){
        modified = true;
        minBound[0] = newValue.toDouble();
        maxBound[0] = minBound[0] + line_length_x->text().toDouble();
    }
    else if(strings.at(1).compare("y") == 0 && !Globals::instance()->compare_double(minBound[1], newValue.toDouble())){
        modified = true;
        minBound[1] = newValue.toDouble();
        maxBound[1] = minBound[1] + line_length_y->text().toDouble();
    }
    else if(strings.at(1).compare("z") == 0 && !Globals::instance()->compare_double(minBound[2], newValue.toDouble())){
        modified = true;
        minBound[2] = newValue.toDouble();
        maxBound[2] = minBound[2] + line_length_z->text().toDouble();
    }

    if(modified){
        vtkViewer->reset();
        QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
        vtkViewer->add(filename.toStdString().c_str());
        calculateBase();
        vtkViewer->update();
    }
    else{
        QMessageBox::about(this, tr("Origin Error"),
                           tr("<p>Not Modified Point</p>"));
    }
}

void MeshWidget::blockMesh(){
    btnCheckMesh->setText("Check: N/A");
    btnSnappy->setDisabled(false);
    btnSnappy2->setDisabled(true);
    btnSnappy3->setDisabled(true);
    menu->btnCfd->setDisabled(true);
    saveSettingsBase();

    if(!isAutoMeshWorking){
        QPushButton* btnCancel = new QPushButton("CANCEL", this);
        btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelBlockMesh()));

        progressBarValue=0;
        progressBarProcess = new QProgressDialog(parent);
        progressBarProcess->setWindowModality(Qt::WindowModal);
        progressBarProcess->setLabelText("Meshing base ...");
        progressBarProcess->setCancelButton(btnCancel);
        progressBarProcess->setRange(0,100);
        progressBarProcess->setFixedWidth(600);
        progressBarProcess->setFixedHeight(100);
        progressBarProcess->setMinimumDuration(0);
        progressBarProcess->show();
    }

    // Creamos el fichero interfaceGUI.txt
    // Actualizamos la informacion del fichero settings.txt
    // Escribir a fichero interfaceGUI
    QFile file(Globals::instance()->getAppProjectPath()+"\\interfaceGUI");

    /*
    int nodes_x = centralWidget->findChild<QLineEdit* >("line_nodes_x")->text().toInt();
    int nodes_y = centralWidget->findChild<QLineEdit* >("line_nodes_y")->text().toInt();
    int nodes_z = centralWidget->findChild<QLineEdit* >("line_nodes_z")->text().toInt();
*/

    minBound[0] = line_origin_x->text().toDouble();
    minBound[1] = line_origin_y->text().toDouble();
    minBound[2] = line_origin_z->text().toDouble();
    maxBound[0] = line_origin_x->text().toDouble() + line_length_x->text().toDouble();
    maxBound[1] = line_origin_y->text().toDouble() + line_length_y->text().toDouble();
    maxBound[2] = line_origin_z->text().toDouble() + line_length_z->text().toDouble();

    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);

        stream << "V1 (" << minBound[0] << " " << minBound[1] << " " << minBound[2] <<");" << endl;
        stream << "V2 (" << maxBound[0] << " " << minBound[1] << " " << minBound[2] <<");" << endl;
        stream << "V3 (" << maxBound[0] << " " << maxBound[1] << " " << minBound[2] <<");" << endl;
        stream << "V4 (" << minBound[0] << " " << maxBound[1] << " " << minBound[2] <<");" << endl;
        stream << "V5 (" << minBound[0] << " " << minBound[1] << " " << maxBound[2] <<");" << endl;
        stream << "V6 (" << maxBound[0] << " " << minBound[1] << " " << maxBound[2] <<");" << endl;
        stream << "V7 (" << maxBound[0] << " " << maxBound[1] << " " << maxBound[2] <<");" << endl;
        stream << "V8 (" << minBound[0] << " " << maxBound[1] << " " << maxBound[2] <<");" << endl;
        stream << "NODES (" << line_nodes_x->text() << " " << line_nodes_y->text() << " " << line_nodes_z->text() << ");" << endl;

        file.flush();
        file.close();
    }

    QFile fileInfoBlockMesh(Globals::instance()->getAppProjectMeshInfoBlockMeshPath());
    if (fileInfoBlockMesh.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileInfoBlockMesh);
        stream << "ORIGIN " << line_origin_x->text() << "," << line_origin_y->text() << "," << line_origin_z->text() << endl;
        stream << "LENGTH " << line_length_x->text() << "," << line_length_y->text() << "," << line_length_z->text() << endl;
        stream << "NODES " << line_nodes_x->text() << "," << line_nodes_y->text() << "," << line_nodes_z->text() << endl;
        stream << "NUMNODES " << line_resolution->text() << endl;
        stream << "REGULARCELLS " << line_regular->isChecked() << endl;
        stream << "SLIDERRESOLUTION " << line_resolution->text() << endl;
        fileInfoBlockMesh.flush();
        fileInfoBlockMesh.close();
    }

    // Eliminamos de la carpeta constants\polyMesh todo menos blockMeshDict
    QDir dir = QDir(Globals::instance()->getAppProjectPath() + "\\constant\\polyMesh");
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList(QDir::Files);

    foreach(QString file, dirList){
        if(file.compare("blockMeshDict")){
            dir.remove(file);
        }
    }

    // Ejecutamos el comando blockMesh
    processBlockMesh = new QProcess();
    connect(processBlockMesh, SIGNAL(started()), this, SLOT(blockMeshStarted()));
    //connect(processBlockMesh, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(stateChanged(QProcess::ProcessState)));
    //connect(processBlockMesh, SIGNAL(error(QProcess::ProcessError)), this, SLOT(error(QProcess::ProcessError)));
    connect(processBlockMesh, SIGNAL(readyReadStandardOutput()),this,SLOT(blockMeshOutput()));
    connect(processBlockMesh, SIGNAL(finished(int)), this, SLOT(blockMeshFinished()));

    processBlockMesh->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processBlockMesh->setProcessChannelMode(QProcess::MergedChannels);
    //processBlockMesh->setProcessChannelMode(QProcess::ForwardedChannels);
    //processBlockMesh->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/blockMesh.output");
    //processBlockMesh->setStandardErrorFile(Globals::instance()->getAppProjectPath() + "/blockMesh.error");

    if(OFENGINE == 0){
        processBlockMesh->start("blockMesh");
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__ << endl;
        //processBlockMesh->waitForStarted();
        //processBlockMesh->waitForFinished();

        if (processBlockMesh->waitForStarted() == false) {
            qDebug() << "Error starting cmd.exe process";
            qDebug() << processBlockMesh->errorString();
            return;
        }
        // Show process output
        processBlockMesh->waitForReadyRead();
        qDebug() << processBlockMesh->readAllStandardOutput();
        processBlockMesh->write("dir");
        processBlockMesh->waitForBytesWritten();
        processBlockMesh->waitForReadyRead();
        qDebug() << processBlockMesh->readAllStandardOutput();

    } else if (OFENGINE == 1){
        QString script = "blockMesh.sh";
        QFile fileScriptBlock(Globals::instance()->getAppProjectPath() + script);
        if (fileScriptBlock.open(QFile::WriteOnly|QFile::Truncate)) {
            QTextStream stream(&fileScriptBlock);
            stream << "#!/bin/bash" << endl;
            stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
            stream << "/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/bin/blockMesh" << endl;
            fileScriptBlock.close();

            QString commands = "docker run --rm --user user --workdir /tmp/project -v "
                    + Globals::instance()->getAppProjectPath()
                    + ":/tmp/project "
                    + DOCKERIMAGE
                    + " /tmp/project/" + script;
            processBlockMesh->start(POWERSHELL, commands.split(" "));

            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__ << endl;

            script = "snappyHexMesh.sh";
            QFile fileScriptSnappy(Globals::instance()->getAppProjectPath() + script);
            if (fileScriptSnappy.open(QFile::WriteOnly|QFile::Truncate)) {
                QTextStream stream(&fileScriptSnappy);
                stream << "#!/bin/bash" << endl;
                stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
                stream << "export OMPI_MCA_btl_vader_single_copy_mechanism=none" << endl;
                stream << "snappyHexMesh" << endl;
                //stream << "decomposePar" << endl;
                //stream << "mpirun -np 4 snappyHexMesh -parallel" << endl;
                //stream << "reconstructParMesh" << endl;
                fileScriptSnappy.close();
            }
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << processBlockMesh->arguments() << endl;
        }
    }

    QFile parDictFile(Globals::instance()->getAppProjectPath() + "system/decomposeParDict");
    if(parDictFile.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&parDictFile);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"system\";\n\tobject\tdecomposeParDict;}" << endl;
        stream << "numberOfSubdomains\t4;" << endl;
        stream << "method\tsimple;" << endl;
        stream << "simpleCoeffs\n{\n";
        stream << "\tn\t( 2 2 1 );\n";
        stream << "\tdelta\t0.001;\n}" << endl;
        stream << "hierarchicalCoeffs\n{\n";
        stream << "\tn\t( 1 1 1 );\n";
        stream << "\tdelta\t0.001;\n";
        stream << "\torder\txyz;\n}" << endl;
        stream << "metisCoeffs\n{\n";
        stream << "\tprocessorWeights ( 1 1 1 1 );\n}" << endl;
        stream << "manualCoeffs\n{\n";
        stream << "\tdataFile\t\"\";\n}" << endl;
        stream << "distributed\tno;" << endl;
        stream << "roots\t();" << endl;

        parDictFile.close();
    }
}

void MeshWidget::stateChanged(QProcess::ProcessState state){
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << state << endl;
}

void MeshWidget::checkMesh(){
    btnCheckMesh->setText("Check: ...");
    btnCheckMesh->setDisabled(true);
    processCheckMesh = new QProcess();
    connect(processCheckMesh, SIGNAL(started()), this, SLOT(checkMeshStarted()));
    connect(processCheckMesh,SIGNAL(readyReadStandardOutput()),this,SLOT(checkMeshOutput()));
    connect(processCheckMesh, SIGNAL(finished(int)), this, SLOT(checkMeshFinished()));

    processCheckMesh->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processCheckMesh->setProcessChannelMode(QProcess::MergedChannels);
    //processCheckMesh->start("checkMesh");
    //centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(3);

    QString script = "checkMesh.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "checkMesh" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processCheckMesh->start(POWERSHELL, commands.split(" "));
}

void MeshWidget::checkMeshStarted(){
    isCancelProcess = false;
}

void MeshWidget::checkMeshOutput(){
    QString text;
    text.append(processCheckMesh->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    line->setFlags(Qt::ItemIsEnabled);
    console->addItem(line);
    console->scrollToBottom();
}

void MeshWidget::checkMeshFinished(){
    QStringList pieces = console->item(console->count()-1)->text().split( "\r\n\r\n" );
    if (pieces.value(pieces.length() - 3) == "Mesh OK.") {
        btnCheckMesh->setText("Check: Pass");
    } else {
        btnCheckMesh->setText("Check: Fail");
    }
    settings->setValue("MeshCheck/check", btnCheckMesh->text());
    btnCheckMesh->setDisabled(false);
    btnCheckMesh->setChecked(false);
}


void MeshWidget::blockMeshStarted(){
    isCancelProcess = false;
    //Remove files from older executions
    QStringList list_excluded_dirs;
    list_excluded_dirs << "processor" << "0" << "profiles" << "constant" << "equipments" << "system";
    resetDirectoriesExclude(list_excluded_dirs);
    createControlDictFile();
    runCheck = 0;
}

void MeshWidget::blockMeshOutput(){
    if(!isAutoMeshWorking){
        if(progressBarValue < 75) {
            progressBarValue += 1;
        }
        progressBarProcess->setValue(progressBarValue);
    }
    QString text;
    text.append(processBlockMesh->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();

    QFile outputfile(Globals::instance()->getAppProjectPath() + "/blockMesh.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Append)) {
        QTextStream stream(&outputfile);
        stream << text;
    }
}

void MeshWidget::cancelBlockMesh(){
    processBlockMesh->kill();
    isCancelProcess = true;
}

void MeshWidget::blockMeshFinished(){
    if(!isCancelProcess){
        vtkViewer->reset();
        QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
        vtkViewer->add(filename.toStdString().c_str());
        vtkViewer->readOpenFoam(0, "solid color", false, true, false, false);
        vtkViewer->update();

        if(!isAutoMeshWorking){
            progressBarProcess->setValue(90);
            QThread::sleep(1);
            progressBarProcess->setValue(100);
        }
        btnSnappy->setDisabled(false);
    }
}

void MeshWidget::surface(){
    if(!isAutoMeshWorking){
        QPushButton* btnCancel = new QPushButton("CANCEL", this);
        btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelSurface()));
        progressBarValue=0;
        progressBarProcess = new QProgressDialog(parent);
        progressBarProcess->setLabelText("Extracting features...");
        progressBarProcess->setFixedWidth(600);
        progressBarProcess->setFixedHeight(100);
        progressBarProcess->setValue(progressBarValue);
        progressBarProcess->setRange(0,100);
        progressBarProcess->setCancelButton(btnCancel);
    }

    // Ejecutamos el comando surface
    processSurface = new QProcess();
    connect(processSurface, SIGNAL(started()), this, SLOT(surfaceStarted()));
    connect(processSurface,SIGNAL(readyReadStandardOutput()),this,SLOT(surfaceOutput()));
    connect(processSurface, SIGNAL(finished(int)), this, SLOT(surfaceFinished()));

    processSurface->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processSurface->setProcessChannelMode(QProcess::MergedChannels);
    //processSurface->start("surfaceFeatureExtract");

    QString script = "surfaceExtract.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "surfaceFeatureExtract" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processSurface->start(POWERSHELL, commands.split(" "));
}

void MeshWidget::surfaceStarted(){
    isCancelProcess = false;
}

void MeshWidget::cancelSurface(){
    processSurface->kill();
    isCancelProcess = true;
}

void MeshWidget::surfaceOutput(){
    if(!isAutoMeshWorking){
        if(progressBarValue < 75) {
            progressBarValue++;
        }

        progressBarProcess->setValue(progressBarValue);
    }
    QString text;
    text.append(processSurface->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();
}

void MeshWidget::surfaceFinished(){
    if(!isCancelProcess){
        if(!isAutoMeshWorking){
            progressBarProcess->setValue(90);
            QThread::sleep(1);
            progressBarProcess->setValue(100);
        }
    }
}

void MeshWidget::copyFoamFiles(){
    QString file_name;
    QString path_set = Globals::instance()->getAppPath() + "/settings/filesSolvers/common/";
    QString path_proj = Globals::instance()->getAppProjectPath();

    file_name = "system/fvSchemes" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSolution";
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
}

void MeshWidget::snappyHexMeshStep1(){
    //Remove files from older executions
    QStringList list_excluded_dirs;
    list_excluded_dirs << "processor" << "0" << "profiles" << "constant" << "equipments" << "system";
    resetDirectoriesExclude(list_excluded_dirs);
    createControlDictFile();

    copyFoamFiles();

    btnCheckMesh->setText("Check: N/A");
    btnSnappy2->setDisabled(false);
    btnSnappy3->setDisabled(true);
    //saveSettingsSculpt();

    if(!isAutoMeshWorking){
        //surface();
        QPushButton* btnCancel = new QPushButton("CANCEL", this);
        btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelSnappyHexMeshStep1()));
        progressBarValue=0;
        progressBarProcess = new QProgressDialog(parent);
        progressBarProcess->setLabelText("Sculpting...");
        progressBarProcess->setFixedWidth(600);
        progressBarProcess->setFixedHeight(100);
        progressBarProcess->setValue(progressBarValue);
        progressBarProcess->setCancelButton(btnCancel);
    }

    double maxBound[3];
    double minBound[3];
    double* bounds = vtkViewer->bounds();
    maxBound[0] = bounds[1];
    maxBound[1] = bounds[3];
    maxBound[2] = bounds[5];

    minBound[0] = bounds[0];
    minBound[1] = bounds[2];
    minBound[2] = bounds[4];

    //double height  = 4;
    //maxBound[1] = height*1.1;
    QFile file(Globals::instance()->getAppProjectPath()+"\\snappyGUI");
    if (file.open(QFile::WriteOnly|QFile::Truncate)){
        QTextStream stream(&file);

        m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
        settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
        QMapIterator<QString, physicalGroupMesh*> i(*groups);
        while (i.hasNext()) {
            i.next();
            stream << "min_" << i.key() << "\t" << i.value()->getRefinementMin() << ";\n";
            stream << "max_" << i.key() << "\t" << i.value()->getRefinementMax() << ";\n";
            settings->setValue("MeshSculpt/min_" + i.key(), i.value()->getRefinementMin());
            settings->setValue("MeshSculpt/max_" + i.key(), i.value()->getRefinementMax());
            stream << "layers_" << i.key() << "\t" << i.value()->getLayers() << ";\n";
        }

        stream << "C1 " << "(" << minBound[0] << " " << minBound[1] << " " << minBound[2] << ");" << endl;
        stream << "C2 " << "(" << maxBound[0] << " " << maxBound[1] << " " << maxBound[2] << ");" << endl;
        stream << "regionLevel\t4;" << endl;
        stream << "implicit " << "true" << ";" << endl;
        stream << "explicit " << "false" << ";" << endl;
        stream << "nSnap " << "0" << ";" << endl;
        stream << "nrSnap " << "0" << ";" << endl;
        stream << "nFeature " << "0" << ";" << endl;
        stream << "nSmooth " << "0" << ";" << endl;
        stream << "tolerance " << "0" << ";" << endl;
        stream << "expansion " << "0" << ";" << endl;
        stream << "thick " << "0" << ";" << endl;
        stream << "minThick " << "0" << ";" << endl;
        stream << "nLayers " << "0" << ";" << endl;
        stream << "timeStep " << "0.1" << ";" << endl;

        stream << "locationX " << settings->value("MeshSculpt/locationX", 0).toString() << ";" << endl;
        stream << "locationY " << settings->value("MeshSculpt/locationY", 0).toString() << ";" << endl;
        stream << "locationZ " << settings->value("MeshSculpt/locationZ", 0).toString() << ";" << endl;

        file.flush();
        file.close();
    }

    QFile file2(Globals::instance()->getAppProjectPath()+"\\snappyOrders");
    if (file2.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file2);
        stream << "step1\ttrue;" << endl;
        stream << "step2\tfalse;" << endl;
        stream << "step3\tfalse;" << endl;

        file2.flush();
        file2.close();
    }

    createSnappyHexMeshDictFile();

    processSnappyStep1 = new QProcess();
    connect(processSnappyStep1, SIGNAL(started()), this, SLOT(snappyHexMeshStep1Started()));
    connect(processSnappyStep1,SIGNAL(readyReadStandardOutput()),this,SLOT(snappyHexMeshStep1Output()));
    connect(processSnappyStep1, SIGNAL(finished(int)), this, SLOT(snappyHexMeshStep1Finished()));

    processSnappyStep1->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processSnappyStep1->setProcessChannelMode(QProcess::MergedChannels);
    processSnappyStep1->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/snappyStep1.output");
    //processSnappyStep1->start("snappyHexMesh");

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/snappyHexMesh.sh";
    processSnappyStep1->start(POWERSHELL, commands.split(" "));

    createTopoSetDictFile();
}

void MeshWidget::snappyHexMeshStep1Started(){
    console->clear();
    isCancelProcess = false;
    runCheck = 0;
}

void MeshWidget::cancelSnappyHexMeshStep1(){
    processSnappyStep1->kill();
    isCancelProcess = true;
}

void MeshWidget::snappyHexMeshStep1Output(){
    if(!isAutoMeshWorking){
        if(progressBarValue < 75) {
            progressBarValue++;
        }

        progressBarProcess->setValue(progressBarValue);
    }
    QString text;
    text.append(processSnappyStep1->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();
}

void MeshWidget::snappyHexMeshStep1Finished(){
    if(!isCancelProcess){
        // Pasamos a la pantalla de CFD
        Globals::instance()->setCurrentProjectCfdModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();

        vtkViewer->reset();
        //vtkViewer->readOpenFoam(0.1, "solid color", false, false, false, true);
        //Esta línea es substituida por el siguiente bloque que corresponde a la función
        //readOpenFoam pero aquí la tenemos disponible para hacer pruebas.

        /***************** PRUEBA LOCA ***********************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/

        vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
        reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
        reader->Update();
        reader->SetTimeValue(0.1);
        reader->CreateCellToPointOn();
        reader->SetCreateCellToPoint(1);
        reader->DecomposePolyhedraOn();
        reader->DecomposePolyhedraOff();
        reader->ReadZonesOn();
        reader->Update();

        vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
        gf->SetInputConnection(0, reader->GetOutputPort(0));

        //improved visualization
        vtkSmartPointer<vtkDecimatePro> deci = vtkSmartPointer<vtkDecimatePro>::New();
        deci->SetInputConnection(0, gf->GetOutputPort(0));
        deci->SetTargetReduction(0.5);
        deci->PreserveTopologyOff();
        deci->SplittingOn();
        deci->BoundaryVertexDeletionOn();
        vtkSmartPointer<vtkSmoothPolyDataFilter> smoother = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
        smoother->SetInputConnection(0,deci->GetOutputPort(0));
        smoother->SetNumberOfIterations(50);
        vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
        normals->SetInputConnection(0, smoother->GetOutputPort(0));
        normals->FlipNormalsOn();
        vtkSmartPointer<vtkPolyDataMapper> smoothMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        smoothMapper->SetInputConnection(0, normals->GetOutputPort(0));
        vtkSmartPointer<vtkActor> smoothActor = vtkSmartPointer<vtkActor>::New();
        smoothActor->SetMapper(smoothMapper);
        smoothActor->GetProperty()->SetColor(0.54,0.46,0.46);
        smoothActor->GetProperty()->SetRepresentationToSurface();
        smoothActor->GetProperty()->SetLineWidth(2);
        smoothActor->GetProperty()->EdgeVisibilityOn();
        smoothActor->GetProperty()->SetOpacity(0.75);

        //original visualization
        vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
        block0->GetCellData()->SetActiveScalars("solid color");
        block0->GetPointData()->SetActiveScalars("solid color");
        vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
        mapper->SetInputData(block0);
        //vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
        actor->SetMapper(mapper);
        actor->GetProperty()->SetColor(0.54,0.46,0.46);
        actor->GetProperty()->SetRepresentationToSurface();
        actor->GetProperty()->SetLineWidth(2);
        actor->GetProperty()->EdgeVisibilityOn();
        actor->GetProperty()->SetOpacity(0.5);
        vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
        hueLut->SetHueRange(0.667,0.0);
        hueLut->Build();
        //mapper->SetLookupTable(hueLut);


        // Clip a plane
        vtkSmartPointer<vtkClipPolyData> clip = vtkSmartPointer<vtkClipPolyData>::New();
        clip->SetValue(0);
        clip->GenerateClippedOutputOn();
        clip->SetInputConnection(gf->GetOutputPort());
        vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
        plane->SetNormal(1.0, 0.0, 0.0);
        clip->SetClipFunction(plane);
        //mapper->SetInputConnection(clip->GetOutputPort());

        /*
        //
        // //////////////////////////////////////////////////////////////////////////////////////////
        // Scale bar
        vtkSmartPointer<vtkTextProperty> tprop =  vtkSmartPointer<vtkTextProperty>::New();
        tprop->SetColor(0,0,0);
        tprop->ShadowOn();

        vtkSmartPointer<vtkCubeAxesActor2D> axes =  vtkSmartPointer<vtkCubeAxesActor2D>::New();
        axes->SetInputConnection(gf->GetOutputPort());
        axes->SetCamera(vtkViewer->m_renderer->GetActiveCamera());
        axes->SetLabelFormat("%6.4g m.");
        axes->SetFlyModeToOuterEdges();
        //axes->SetFontFactor(0.8);
        axes->SetAxisTitleTextProperty(tprop);
        axes->SetXLabel("");
        axes->SetAxisLabelTextProperty(tprop);
        axes->SetYAxisVisibility(0);
        axes->SetZAxisVisibility(0);
        vtkViewer->m_renderer->AddViewProp(axes);
        //
        */

        vtkSmartPointer<vtkLegendScaleActor> legendScaleActor = vtkSmartPointer<vtkLegendScaleActor>::New();
        legendScaleActor->GetLegendLabelProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendTitleProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendLabelProperty()->SetFontSize(legendScaleActor->GetLegendLabelProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetFontSize(legendScaleActor->GetLegendTitleProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetLineOffset(-25);
        legendScaleActor->GetLegendLabelProperty()->SetLineOffset(-25);
        legendScaleActor->AllAxesOff();
        vtkViewer->m_renderer->AddActor(legendScaleActor);


        vtkViewer->m_renderer->AddActor(actor);
        //vtkViewer->m_renderer->AddActor(smoothActor);

        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/
        /*****************************************************/

        vtkViewer->update();

        if(!isAutoMeshWorking){
            progressBarProcess->setValue(90);
            //QThread::sleep(1);
            progressBarProcess->setValue(100);
        }
        btnSnappy2->setDisabled(false);
        menu->btnCfd->setDisabled(false);
    }
}

void MeshWidget::resetDirectoriesExclude(QStringList dirs){
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList dirList = dir.entryList();

    foreach(QString filename, dirList){
        if(! dirs.contains(filename)){
            QDir aux = QDir(Globals::instance()->getAppProjectPath() + filename);
            aux.removeRecursively();
        }
    }
}

void MeshWidget::createControlDictFile(){
    QFile file(Globals::instance()->getAppProjectPath()+"/system/controlDict");

    if (file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);

        stream << "/***** File generated by Hydrodeca *****/" << endl;
        stream << "FoamFile {\n"
                  "\tversion\t2.0;\n\tformat\tascii;\n\tclass\tdictionary;\n\tlocation\t\"system\";\n\tobject\tcontrolDict; }" << endl;
        stream << "application\tpimpleFoam;" << endl;
        stream << "startFrom\tlatestTime;" << endl;
        stream << "startTime\t0;" << endl;
        stream << "stopAt\tendTime;" << endl;
        stream << "endTime\t1000;" << endl;
        stream << "deltaT\t0.1;" << endl;
        stream << "writeControl\tadjustableRunTime;" << endl;
        stream << "writeInterval\t1;" << endl;
        stream << "purgeWrite\t0;" << endl;
        stream << "writeFormat\tascii;" << endl;
        stream << "writePrecision\t6;" << endl;
        stream << "writeCompression\tuncompressed;" << endl;
        stream << "timeFormat\tgeneral;" << endl;
        stream << "timePrecision\t6;" << endl;
        stream << "runTimeModifiable\tyes;" << endl;
        stream << "adjustTimeStep\ton;" << endl;
        stream << "maxCo\t5;" << endl;
        stream << "maxDeltaT\t1;" << endl;

        file.flush();
        file.close();
    }
}

void MeshWidget::snappyHexMeshStep2(){
    //Remove files from older executions
    QStringList list_excluded_dirs;
    list_excluded_dirs << "processor" << "0" << "profiles" << "constant" << "equipments" << "system" << "0.1";

    resetDirectoriesExclude(list_excluded_dirs);
    createControlDictFile();

    btnCheckMesh->setText("Check: N/A");
    btnSnappy3->setDisabled(false);
    saveSettingsSnap();
    if(!isAutoMeshWorking){
        QPushButton* btnCancel = new QPushButton("CANCEL", this);
        btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelSnappyHexMeshStep2()));
        progressBarValue=0;
        progressBarProcess = new QProgressDialog(parent);
        progressBarProcess->setLabelText("Snapping...");
        progressBarProcess->setValue(progressBarValue);
        progressBarProcess->setFixedWidth(600);
        progressBarProcess->setFixedHeight(100);
        progressBarProcess->setCancelButton(btnCancel);
    }

    QFile file(Globals::instance()->getAppProjectPath()+"\\snappyGUI");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);

        QMapIterator<QString, physicalGroupMesh*> i(*groups);
        while (i.hasNext()) {
            i.next();
            stream << "min_" << i.key() << "\t" << i.value()->getRefinementMin() << ";\n";
            stream << "max_" << i.key() << "\t" << i.value()->getRefinementMax() << ";\n";
            stream << "layers_" << i.key() << "\t" << i.value()->getLayers() << ";\n";
        }

        stream << "C1 " << "(" << 0 << " " << 0 << " " << 0 << ");" << endl;
        stream << "C2 " << "(" << 0 << " " << 0 << " " << 0 << ");" << endl;
        stream << "regionLevel " << "0" << ";" <<endl;

        stream << "implicit true;" <<endl;
        stream << "explicit false;" << endl;
        stream << "nSnap " << line_snap_it->text()<< ";" << endl;
        stream << "nrSnap " << line_relax_it->text()<< ";" << endl;
        stream << "nFeature " << line_feature_it->text()<< ";" << endl;
        stream << "nSmooth " << line_smooth_it->text()<< ";" << endl;
        stream << "tolerance " << line_tolerance->text()<< ";" << endl;

        stream << "expansion " << "0" << ";" << endl;
        stream << "thick " << "0" << ";" << endl;
        stream << "minThick " << "0" << ";" << endl;
        stream << "nLayers " << "0" << ";" << endl;
        stream << "timeStep " << "0.1" << ";" << endl;

        stream << "locationX " << locationInMesh[0] << ";" << endl;
        stream << "locationY " << locationInMesh[1] << ";" << endl;
        stream << "locationZ " << locationInMesh[2] << ";" << endl;

        file.flush();
        file.close();
    }

    QFile file2(Globals::instance()->getAppProjectPath()+"\\snappyOrders");
    if (file2.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file2);
        stream << "step1\tfalse;" << endl;
        stream << "step2\ttrue;" << endl;
        stream << "step3\tfalse;" << endl;

        file2.flush();
        file2.close();
    }

    processSnappyStep2 = new QProcess();
    connect(processSnappyStep2, SIGNAL(started()), this, SLOT(snappyHexMeshStep2Started()));
    connect(processSnappyStep2,SIGNAL(readyReadStandardOutput()),this,SLOT(snappyHexMeshStep2Output()));
    connect(processSnappyStep2, SIGNAL(finished(int)), this, SLOT(snappyHexMeshStep2Finished()));

    processSnappyStep2->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processSnappyStep2->setProcessChannelMode(QProcess::MergedChannels);
    processSnappyStep2->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/snappyStep2.output");
    //processSnappyStep2->start("snappyHexMesh");

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/snappyHexMesh.sh";
    processSnappyStep2->start(POWERSHELL, commands.split(" "));
}

void MeshWidget::snappyHexMeshStep2Started(){
    console->clear();
    isCancelProcess = false;
    runCheck = 0;
}

void MeshWidget::snappyHexMeshStep2Output(){
    if(!isAutoMeshWorking){
        if(progressBarValue < 75) {
            progressBarValue++;
        }

        progressBarProcess->setValue(progressBarValue);
    }

    QString text;
    text.append(processSnappyStep2->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();
}

void MeshWidget::cancelSnappyHexMeshStep2(){
    processSnappyStep2->kill();
    isCancelProcess = true;
}

void MeshWidget::snappyHexMeshStep2Finished(){
    if(!isCancelProcess){
        Globals::instance()->setCurrentProjectCfdModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();
        vtkViewer->reset();
        vtkViewer->readOpenFoam(0.2, "solid color", false, false, false, true);
        vtkViewer->update();

        // Pasamos a la pestaña de CFD
        //menu->goToCfdView();
        if(!isAutoMeshWorking){
            progressBarProcess->setValue(90);
            QThread::sleep(1);
            progressBarProcess->setValue(100);
        }
        isAutoMeshWorking = false;
        btnSnappy3->setDisabled(false);
    }
}

void MeshWidget::snappyHexMeshStep3(){
    //Remove files from older executions
    QStringList list_excluded_dirs;
    list_excluded_dirs << "0.2" << "processor" << "0" << "profiles" << "constant" << "equipments" << "system" << "0.1";
    resetDirectoriesExclude(list_excluded_dirs);
    createControlDictFile();

    btnCheckMesh->setText("Check: N/A");
    if(!isAutoMeshWorking){
        QPushButton* btnCancel = new QPushButton("CANCEL", this);
        btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelSnappyHexMeshStep3()));
        progressBarValue=0;
        progressBarProcess = new QProgressDialog(parent);
        progressBarProcess->setLabelText("Adding layers...");
        progressBarProcess->setValue(progressBarValue);
        progressBarProcess->setFixedWidth(600);
        progressBarProcess->setFixedHeight(100);
        progressBarProcess->setCancelButton(btnCancel);
    }

    createSnappyHexMeshDictFile();

    QFile file(Globals::instance()->getAppProjectPath()+"\\snappyGUI");
    if (file.open(QFile::WriteOnly|QFile::Truncate)){
        QTextStream stream(&file);

        //QString str_layers = "";
        QMapIterator<QString, physicalGroupMesh*> i(*groups);
        while (i.hasNext()) {
            i.next();
            stream << "min_" << i.key() << "\t" << i.value()->getRefinementMin() << ";\n";
            stream << "max_" << i.key() << "\t" << i.value()->getRefinementMax() << ";\n";
            //stream << "layers_" << i.key() << "\t" << i.value()->getLayers() << ";\n";
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << i.key() << " - " << i.value()->getLayers() << endl;
            int aux = i.value()->getLayers();
            settings->setValue("MeshLayer/layers_" + i.key(), aux);
            //if (aux > 0)
            //    str_layers += "\t" + i.key() + "{ nSurfaceLayers\t" + QString::number(aux) + "; }";
        }
        //str_layers += "\n";
        //stream << "layers_info\t" << str_layers << ";" << endl;
        stream << "C1 " << "(" << 0 << " " << 0 << " " << 0 << ");" << endl;
        stream << "C2 " << "(" << 0 << " " << 0 << " " << 0 << ");" << endl;
        stream << "regionLevel " << "0" << ";" <<endl;
        stream << "implicit true;" <<endl;
        stream << "explicit false;" << endl;
        stream << "nSnap " << "0" << ";" <<endl;
        stream << "nrSnap " << "0" << ";" <<endl;
        stream << "nFeature " << "0" << ";" <<endl;
        stream << "nSmooth " << "0" << ";" <<endl;
        stream << "tolerance " << "0" << ";" <<endl;

        stream << "expansion " << line_expansion->text()<< ";" << endl;
        stream << "thick " << line_thickness_first->text()<< ";" << endl;
        stream << "minThick " << line_thickness_first->text()<< ";" << endl;
        //stream << "nLayers " << line_layers->text()<< ";" << endl;

        stream << "locationX " << locationInMesh[0] << ";" << endl;
        stream << "locationY " << locationInMesh[1] << ";" << endl;
        stream << "locationZ " << locationInMesh[2] << ";" << endl;

        settings->setValue("MeshLayer/thickfirst", line_thickness_first->text());
        settings->setValue("MeshLayer/expansion", line_expansion->text());

        file.flush();
        file.close();
    }

    QFile file2(Globals::instance()->getAppProjectPath()+"\\snappyOrders");
    if (file2.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file2);
        stream << "step1\tfalse;" << endl;
        stream << "step2\tfalse;" << endl;
        stream << "step3\ttrue;" << endl;

        file2.flush();
        file2.close();
    }

    QFile file3(Globals::instance()->getAppProjectPath()+"\\controlDictGUI");
    if (file3.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file3);
        stream << "timeStep " << "0.1" << ";" << endl;
        file3.flush();
        file3.close();
    }

    processSnappyStep3 = new QProcess();
    connect(processSnappyStep3, SIGNAL(started()), this, SLOT(snappyHexMeshStep3Started()));
    connect(processSnappyStep3,SIGNAL(readyReadStandardOutput()),this,SLOT(snappyHexMeshStep3Output()));
    connect(processSnappyStep3, SIGNAL(finished(int)), this, SLOT(snappyHexMeshStep3Finished()));

    processSnappyStep3->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processSnappyStep3->setProcessChannelMode(QProcess::MergedChannels);
    //processSnappyStep3->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/snappyStep3.output");
    //processSnappyStep3->start("snappyHexMesh");

    QString path = "C:/Windows/system32/WindowsPowerShell/v1.0/powershell.exe";
    QString commands = "docker run --rm --user user --workdir /tmp/project -v " + Globals::instance()->getAppProjectPath() + ":/tmp/project ofubuntu /tmp/project/snappyHexMesh.sh";
    processSnappyStep3->start(path, commands.split(" "));
}

void MeshWidget::snappyHexMeshStep3Started(){
    console->clear();
    isCancelProcess = false;
    runCheck = 0;
}

void MeshWidget::snappyHexMeshStep3Output(){
    if(!isAutoMeshWorking){
        if(progressBarValue < 75) {
            progressBarValue++;
        }

        progressBarProcess->setValue(progressBarValue);
    }

    QString text;
    text.append(processSnappyStep3->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();
}

void MeshWidget::cancelSnappyHexMeshStep3(){
    processSnappyStep3->kill();
    isCancelProcess = true;
}

void MeshWidget::snappyHexMeshStep3Finished(){
    if(!isCancelProcess){
        Globals::instance()->setCurrentProjectCfdModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();
        vtkViewer->reset();

        vtkViewer->readOpenFoam(0.3, "solid color", false, false, false, true);
        //Esta línea es substituida por el siguiente bloque que corresponde a la función
        //readOpenFoam pero aquí la tenemos disponible para hacer pruebas.
        /*
        vtkSmartPointer<vtkPOpenFOAMReader> reader = vtkSmartPointer<vtkPOpenFOAMReader>::New();
        reader->SetFileName(Globals::instance()->getAppProjectFoamPath().toStdString().c_str());
        reader->Update();
        reader->SetTimeValue(0.3);
        reader->CreateCellToPointOn();
        reader->SetCreateCellToPoint(1);
        reader->DecomposePolyhedraOn();
        reader->DecomposePolyhedraOff();
        reader->ReadZonesOn();
        reader->Update();
        vtkSmartPointer<vtkCompositeDataGeometryFilter> gf = vtkSmartPointer<vtkCompositeDataGeometryFilter>::New();
        gf->SetInputConnection(0, reader->GetOutputPort(0));
        vtkUnstructuredGrid* block0 = vtkUnstructuredGrid::SafeDownCast(reader->GetOutput()->GetBlock(0));
        block0->GetCellData()->SetActiveScalars("solid color");
        block0->GetPointData()->SetActiveScalars("solid color");

        vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
        mapper->SetInputData(block0);

        // Clip a plane
        vtkSmartPointer<vtkClipPolyData> clip = vtkSmartPointer<vtkClipPolyData>::New();
        clip->SetValue(0);
        clip->GenerateClippedOutputOn();
        clip->SetInputConnection(gf->GetOutputPort());
        vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
        plane->SetNormal(1.0, 0.0, 0.0);
        clip->SetClipFunction(plane);
        mapper->SetInputConnection(clip->GetOutputPort());

        vtkSmartPointer<vtkLegendScaleActor> legendScaleActor = vtkSmartPointer<vtkLegendScaleActor>::New();
        legendScaleActor->GetLegendLabelProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendTitleProperty()->SetColor(0,0,0);
        legendScaleActor->GetLegendLabelProperty()->SetFontSize(legendScaleActor->GetLegendLabelProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetFontSize(legendScaleActor->GetLegendTitleProperty()->GetFontSize() * 2);
        legendScaleActor->GetLegendTitleProperty()->SetLineOffset(-25);
        legendScaleActor->GetLegendLabelProperty()->SetLineOffset(-25);
        legendScaleActor->AllAxesOff();
        vtkViewer->m_renderer->AddActor(legendScaleActor);

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);
        actor->GetProperty()->SetColor(0.54,0.46,0.46);
        actor->GetProperty()->SetRepresentationToSurface();
        actor->GetProperty()->SetLineWidth(2);
        actor->GetProperty()->EdgeVisibilityOn();
        actor->GetProperty()->SetOpacity(1);

        vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
        hueLut->SetHueRange(0.667,0.0);
        hueLut->Build();
        mapper->SetLookupTable(hueLut);
        vtkViewer->m_renderer->AddActor(actor);
        */


        vtkViewer->update();

        // Pasamos a la pestaña de CFD
        //menu->goToCfdView();
        if(!isAutoMeshWorking){
            progressBarProcess->setValue(90);
            QThread::sleep(1);
            progressBarProcess->setValue(100);
        }
        isAutoMeshWorking = false;
        btnCheckMesh->setDisabled(false);
    }
}
