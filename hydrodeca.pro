#-------------------------------------------------
#
# Project created by QtCreator 2016-03-24T18:01:01
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HydroDeca
TEMPLATE = app
RC_FILE = hydrodeca.rc

SOURCES += main.cpp \
    borderlayout.cpp \
    globals.cpp \
    menuwidget.cpp \
    vtkviewer.cpp \
    resultswidget.cpp \
    meshwidget.cpp \
    cfdwidget.cpp \
    cadwidget.cpp \
    mainwindow.cpp \
    homewidget.cpp \
    edarwidget.cpp \
    graphobject.cpp \
    tier_application/ComputerLayersModel.cpp \
    tier_application/ComputerVesilind.cpp \
    tier_application/ComputerLayersModel.cpp \
    tier_application/ConvertColors.cpp \
    tier_data_access/StringMethods.cpp \
    tier_data_access/TextFileRead.cpp \
    tier_application/boxlinefield.cpp \
    tier_application/fileeditor.cpp \
    tier_application/physicalgroupmesh.cpp \
    tier_application/resultentity.cpp

HEADERS  += \
    borderlayout.h \
    globals.h \
    menuwidget.h \
    vtkviewer.h \
    resultswidget.h \
    meshwidget.h \
    cfdwidget.h \
    cadwidget.h \
    mainwindow.h \
    homewidget.h \
    edarwidget.h \
    graphobject.h \
    tier_application/ComputerVesilind.h \
    tier_application/ComputerLayersModel.h \
    tier_application/ConvertColors.h \
    tier_data_access/StringMethods.h \
    tier_data_access/TextFileRead.h \
    tier_application/boxlinefield.h \
    tier_application/fileeditor.h \
    tier_application/linenumberarea.h \
    tier_application/physicalgroupmesh.h \
    tier_application/resultentity.h

FORMS    += \
    defaultwindow.ui

INCLUDEPATH += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\inc
INCLUDEPATH += "C:\Program Files (x86)\VTK\include\vtk-6.1"

DEFINES +=WNT WIN32

## LIBRERIAS OPENCASCADE
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\FWOSPlugin.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\PTKernel.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBin.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBinL.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBinTObj.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBinXCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBO.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBool.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKCDF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKDCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKBRep.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKDraw.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKernel.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKFeat.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKFillet.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKG2d.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKG3d.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKGeomAlgo.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKGeomBase.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKHLR.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKIGES.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKIVtk.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKIVtkDraw.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKLCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKMath.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKMesh.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKMeshVS.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKNIS.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKOffset.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKOpenGl.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKPCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKPLCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKPrim.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKPShape.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKQADraw.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKService.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKShapeSchema.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKShHealing.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKStdLSchema.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKStdSchema.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKSTEP.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKSTEP209.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKSTEPAttr.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKSTEPBase.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKSTL.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKTObj.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKTObjDRAW.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKTopAlgo.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKTopTest.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKV3d.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKViewerTest.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKVoxel.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKVRML.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXCAFSchema.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXDEDRAW.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXDEIGES.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXDESTEP.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXMesh.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXml.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXmlL.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXmlTObj.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXmlXCAF.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXSBase.lib
LIBS += C:\OpenCASCADE6.9.1-vc12-64\opencascade-6.9.1\win64\vc12\lib\TKXSDRAW.lib

## LIBRERIAS VTK
LIBS += "C:\Program Files (x86)\VTK\lib\vtkalglib-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkChartsCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonColor-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonComputationalGeometry-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonDataModel-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonExecutionModel-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonMath-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonMisc-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonSystem-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkCommonTransforms-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkDICOMParser-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkDomainsChemistry-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkexoIIc-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkexpat-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersAMR-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersExtraction-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersFlowPaths-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersGeneral-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersGeneric-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersGeometry-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersHybrid-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersHyperTree-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersImaging-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersModeling-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersParallel-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersParallelImaging-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersProgrammable-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersSelection-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersSMP-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersSources-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersStatistics-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersTexture-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkFiltersVerdict-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkfreetype-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkftgl-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkGeovisCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkgl2ps-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkGUISupportQt-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkGUISupportQtOpenGL-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkhdf5_hl-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkhdf5-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingColor-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingFourier-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingGeneral-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingHybrid-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingMath-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingMorphological-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingSources-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingStatistics-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkImagingStencil-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkInfovisCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkInfovisLayout-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkInteractionImage-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkInteractionStyle-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkInteractionWidgets-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOAMR-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOEnSight-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOExodus-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOExport-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOGeometry-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOImage-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOImport-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOInfovis-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOLegacy-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOLSDyna-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOMINC-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOMovie-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIONetCDF-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOParallel-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOPLY-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOSQL-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOVideo-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOXML-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkIOXMLParser-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkjpeg-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkjsoncpp-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtklibxml2-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkmetaio-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkNetCDF_cxx-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkNetCDF-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkoggtheora-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkParallelCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkpng-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkproj4-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingAnnotation-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingContext2D-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingFreeType-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingFreeTypeOpenGL-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingGL2PS-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingImage-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingLabel-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingLIC-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingLOD-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingOpenGL-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingQt-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingVolume-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingVolumeAMR-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkRenderingVolumeOpenGL-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtksqlite-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtksys-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtktiff-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkverdict-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkViewsContext2D-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkViewsCore-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkViewsGeovis-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkViewsInfovis-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkViewsQt-6.1.lib"
LIBS += "C:\Program Files (x86)\VTK\lib\vtkzlib-6.1.lib"

RESOURCES += \
    resources.qrc

DISTFILES += \

CONFIG += resources_big
