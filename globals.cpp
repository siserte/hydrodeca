#include "globals.h"
//#include "fluidlauncher.h"

#include <QObject>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFont>
#include <QHeaderView>
#include <QIODevice>
#include <QLabel>
#include <QStandardPaths>
#include <QString>
#include <QTextStream>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <string>
#include <sstream>
#include <vector>

#include<QDoubleValidator>

#define DEFAULT_INPUT_TIMEOUT 10000
#define SIZING_FACTOR_HEIGHT 6/10
#define SIZING_FACTOR_WIDTH 6/10

double locationInMesh[3];

Globals* Globals::m_pInstance  = NULL;
Globals* Globals::instance() {
    if (!m_pInstance)
        m_pInstance = new Globals();
    return m_pInstance;
}

int Globals::copyPath(QString src, QString dst) {
    QDir dir(src);
    if (! dir.exists())
        return 1;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files))
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);

    return 0;
}

Globals::Globals() {
    app_path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    app_path_settings = app_path +"/settings";
    app_path_workspace = app_path + "/workspace";
    app_default_path = app_path_settings + "\\_default";
    app_settings_path = app_path_settings + "\\settings.txt";
    app_workspace = "";
    cp_cadModel = false;
    cp_cfdModel = false;
    cp_creationDate = "";
    cp_edarModel = false;
    cp_lastModification = "";
    cp_meshModel = false;
    cp_name = "";
    cp_resultsModel = false;

    file_info_project = "info.txt";
    file_foam = "foam.foam";
    file_mesh_info_block_mesh = "mesh_info";

    num_workspaces = 0;

    tab_home = "HOME";
    tab_edar = "WWTP";
    tab_cad = "CAD";
    tab_cfd = "CFD";
    tab_mesh = "MESH";
    tab_results = "RESULTS";
    tab_analysis = "ANALYSIS";
    tab_help = "HELP";

    readSettingsData();
    checkProject();
}

/********************************************************************************
 * FUNCTIONS
 ********************************************************************************/

void Globals::checkProject(){
    //qDebug() << "Start check" <<endl;
    // Miramos si tiene Modelo EDAR

    // Miramos si tiene Modelo CAD
    //QString fileCadModel = getAppProjectPath() + "geometry_preset_1.txt";
    QString fileCadModel = getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
    //qDebug() << "fileCadModel " << fileCadModel <<endl;
    QFile fileCad(fileCadModel);
    cp_cadModel = fileCad.exists();

    //qDebug() << "cad: " << cp_cadModel << " path: " << fileCadModel << endl;


    // Miramos si tiene Modelo CFD
    QString fileCfdModel = getAppProjectPath() + "\\dummyCFD";
    QFile fileCfd(fileCfdModel);
    cp_cfdModel = fileCfd.exists();

    // Miramos si tiene Modelo MESH
    QString fileMeshModel = getAppProjectPath() + "\\constant\\polyMesh\\points";
    QFile fileMesh(fileMeshModel);
    cp_meshModel = fileMesh.exists();

    // Miramos si tiene Modelo RESULTS
    QDir dir(getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    cp_resultsModel = (dir.count() > 3);

    // Miramos si tiene Modelo ANALYSIS



    writeProjectInfoFile();
    //qDebug() << "End check" <<endl;
}

bool Globals::compare_double(double a, double b, double epsilon){
    return std::abs(a - b) < epsilon;
}

void Globals::copyFolder(QString sourceFolder, QString destFolder) {
    QDir sourceDir(sourceFolder);
    if(!sourceDir.exists())
        return;

    QDir destDir(destFolder);
    if(!destDir.exists()) {
        destDir.mkdir(destFolder);
    }

    QStringList files = sourceDir.entryList(QDir::Files);
    for(int i = 0; i< files.count(); i++) {
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        QFile::copy(srcName, destName);
    }

    files.clear();
    files = sourceDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for(int i = 0; i< files.count(); i++) {
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        copyFolder(srcName, destName);
    }
}


void Globals::readSettingsData(){
    // Miramos si existe el fichero de settings.txt (contiene informacion del proyecto actual)
    QFile file(app_settings_path);
    bool existsProject = false;
    bool existsWorkSpace = false;

    if(file.exists()){
        if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream in(&file);
            QString line = in.readLine();
            int numLine = 0;

            while (!line.isNull()) {
                if(numLine == 0){
                    app_workspace = line;
                    existsWorkSpace = true;
                }
                if(numLine == 1){
                    cp_name = line;
                    existsProject = true;
                    break;
                }

                line = in.readLine();
                numLine++;
            }
        }

        // Cerramos el fichero de settings
        file.close();
    }

    // Miramos el numero de workspaces existentes
    QDir dir = QDir(app_path_workspace);
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList();
    num_workspaces = dirList.count();

    // Conseguimos los datos del proyecto actual
    QFile file_project(getAppProjectInfoPath());

    if(existsWorkSpace && existsProject && file_project.exists()){
        if(file_project.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream in(&file_project);
            QString line = in.readLine();
            int numLine = 0;

            while (!line.isNull()) {
                if(numLine == 0){
                    cp_name = line;
                }
                if(numLine == 1){
                    cp_creationDate = line;
                }
                if(numLine == 2){
                    cp_lastModification = line;
                }
                if(numLine == 3){
                    cp_edarModel = (line == "true" ? true : false);
                }
                if(numLine == 4){
                    cp_cadModel = (line == "true" ? true : false);
                }
                if(numLine == 5){
                    cp_cfdModel = (line == "true" ? true : false);
                }
                if(numLine == 6){
                    cp_meshModel = (line == "true" ? true : false);
                }
                if(numLine == 7){
                    cp_resultsModel = (line == "true" ? true : false);
                    break;
                }

                line = in.readLine();
                numLine++;
            }
        }

        // Cerramos el fichero de settings
        file_project.close();
    }
}

bool Globals::writeProjectInfoFile() {
    //qDebug() << "Start info" <<endl;
    // Actualizamos la informacion del fichero del proyecto info.txt
    if(cp_name != ""){
        QFile file(getAppProjectInfoPath());
        if (file.open(QFile::WriteOnly|QFile::Truncate)) {
            QTextStream stream(&file);
            QDateTime date = QDateTime::currentDateTime();

            stream << cp_name << endl;
            stream << (cp_creationDate == ""?date.toString("yyyy-MM-dd hh:mm:ss"):cp_creationDate) << endl;
            stream << date.toString("yyyy-MM-dd hh:mm:ss") << endl;
            stream << (cp_edarModel?"true":"false") << endl;
            stream << (cp_cadModel?"true":"false") << endl;
            stream << (cp_cfdModel?"true":"false") << endl;
            stream << (cp_meshModel?"true":"false") << endl;
            stream << (cp_resultsModel?"true":"false") << endl;

            file.resize(file.pos());
            file.flush();
            file.close();
        }
        //qDebug() << "Start end" <<endl;
        return true;
    }
    //qDebug() << "Start end" <<endl;
    return false;
}

void Globals::writeSettingsFile() {
    // Actualizamos la informacion del fichero de la app settings.txt
    QFile file(app_settings_path);
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);

        stream << app_workspace << endl;
        stream << " " << endl;

        file.resize(file.pos());
        file.flush();
        file.close();
    }
}

QStackedWidget* Globals::getHomeWTab(){
    QStackedWidget *home = new QStackedWidget();
    QPalette background =  home->palette();
    background.setColor(QPalette::Background, Qt::white);
    home->setAutoFillBackground(true);
    home->setPalette(background);

    return home;
}

/********************************************************************************
 * GETTERS AND SETTERS
 ********************************************************************************/
QString Globals::getAppPath(){
    return app_path;
}
QString Globals::getAppDefaultPath(){
    return app_default_path;
}
QString Globals::getAppProjectPath(){
    return app_path_workspace + "/" + cp_name + "/";
}

QString Globals::getAppProjectMeshInfoBlockMeshPath(){
    return app_path_workspace + "\\" + app_workspace + "\\" + cp_name + "\\" + file_mesh_info_block_mesh;
}
QString Globals::getAppProjectInfoPath(){
    return app_path_workspace + "\\" + app_workspace + "\\" + cp_name + "\\" + file_info_project;
}
QString Globals::getAppProjectFoamPath(){
    return app_path_workspace + "\\" + app_workspace + "\\" + cp_name + "\\" + file_foam;
}
QString Globals::getAppWorkSpacePath(){
    return app_path_workspace ;
}
QString Globals::getAppSettingsPath(){
    return app_path_settings ;
}
QString Globals::getAppCurrentWorkSpacePath(){
    return app_path_workspace + "\\" + app_workspace + "\\";
}
QString Globals::getAppWorkSpace(){
    return app_workspace;
}
QString Globals::getCssMenu(){
    return "QPushButton { "
           "background-color: rgb(5,83,151); "
           "color: #ffffff; "
           "font-size: 14px; "
           "padding: 13px; "
           "font-weight: bold; "
           "border-right: 0.5px solid rgb(84,119,149); "
           "border-top: 1px solid rgb(0,0,0); "
           "border-left: 0px solid rgb(84,119,149); "
           "border-bottom: 4px solid rgb(5,83,151);} "
           "QPushButton:disabled { "
            //           "background-color: #444444;} "
            "color: rgb(4,62,114);"
            "background-color: rgb(5,83,151); "
            //"color: #ffffff; "
            "font-size: 14px; "
            "padding: 13px; "
            "font-weight: bold; "
            "border-right: 0.5px solid rgb(84,119,149); "
            "border-top: 1px solid rgb(0,0,0); "
            "border-left: 0px solid rgb(84,119,149); "
            "border-bottom: 4px solid rgb(5,83,151);} "
            "QPushButton:hover { "
            "background-color: rgb(5,83,151); "
            "color: white; "
            "padding: 13px; "
            "border-right: 0.5px solid rgb(84,119,149); "
            "border-top: 1px solid rgb(0,0,0); "
            "border-left: 0px solid rgb(84,119,149); "
            "border-bottom: 4px solid rgb(255,255,255)}";
}
QString Globals::getCssMenuSelected(){
    return "QPushButton { "
           "background-color: rgb(5,90,164); "
           "color: #ffffff; "
           "font-size: 14px; "
           "padding: 13px; "
           "font-weight: bold; "
           "border-right: 0.5px solid rgb(84,119,149); "
           "border-top: 1px solid rgb(0,0,0); "
           "border-left: 0px solid rgb(84,119,149); "
           "border-bottom: 4px solid rgb(255,255,255);} "
           "QPushButton:hover { "
           "background-color: rgb(5,90,164); "
           "color: #ffffff; "
           "padding: 13px; "
           "border-bottom: 4px solid rgb(255,255,255); "
           "border-left: 0 px solid rgb(84,119,149); "
           "border-right: 0.5px solid rgb(84,119,149); "
           "border-top: 1px solid rgb(0,0,0)}";
}
QString Globals::getCssSubMenu(){
    return "QMenu {"
           "background-color: rgb(5,83,151); "
           "color: white; "
           "spacing: 0px; "
           "margin: 0px;} "
           "QMenu::item:selected {"
           "background-color: rgb(255,255,255); "
           "color: rgb(5,83,151);}";
}
QString Globals::getCssAreaActions(){
    return "background-color: rgb(237,237,237); "
           "color: black; "
           "font-size: 14px;";
}
QString Globals::getCssAreaButtons(){
    return "background-color: rgb(172,164,161); "
           "color: black; "
           "font-size: 14px;";
}
QString Globals::getCssAreaNav(){
    return "background-color: rgb(252,252,252); "
           "color: black; "
           "font-size: 14px;";
}
QString Globals::getCssAreaNavButtons(){
    return "QPushButton{ "
           "background-color: #000000; border: 0.5px solid gray;"
           "border-radius: 5px; "
           "background-color: #ffffff;}"
           "QPushButton:hover { "
           "font-size: 20 pt;"
           "background-color: rgb(210,210,210);} "
           "QPushButton:checked { "
           "background-color: rgb(237,237,237);} "
           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(235,235,235);} "
           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(235,235,235);}; ";
    //"QPushButton:disabled{ "
    // "background-color: #444444; };";
}
QString Globals::getCssViewer(){
    return "QTabWidget::pane { "
           "position: absolute; "
           "top: -0.5em; "
           "spacing: 0px; "
           "font: calibri ;"
           "color: black; "
           "font-weight: bold; "
           "margin: 0px;} "

           "QTabBar::tab { "
           "font: calibri ;"
           "color: black; "
           "font-weight: bold;} "

           "QTabWidget:tab-bar {"
           "spacing: 0px; "
           "color: black; "
           "font: calibri ;"
           "font-weight: bold; "
           "margin: 0px;"
           "border-top: 2px solid #C2C7CB;}";
}

QString Globals::getCssScrollBar(){
    return "QScrollBar:vertical {"
           "border: 0px solid grey;"
           "width: 12px;"
           "height: 12px;"
           "background: rgb(160,160,160);}"

           "QScrollBar::handle:vertical { "
           "background: black; "
           "min-width: 20px;}";
}


QString Globals::getCssViewerFont(){
    return "color: black; "
           "font-weight: bold;";
}
QString Globals::getCssArrows(){
    return "QPushButton{ "
           "background-color: rgb(172,164,161);"
           "font-size: 20 pt;"
           "font-weight: bold;"
           "color: #444444;"
           "text-align: left;"
           "border-radius: 8px;}"

           "QPushButton:hover { "
           "font-size: 20 pt;"
           "background-color: rgb(172,164,161);}"

           "QPushButton:checked { "
           "background-color: rgb(172,164,161);}"

           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(172,164,161);}"

           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(172,164,161);}";
}
QString Globals::getCssPushButton(){
    return "QPushButton{ "
           "background-color: rgb(172,164,161);"
           "font-size: 20 pt;"
           "font-weight: bold;"
           "color: #444444;"
           "text-align: left;"
           "padding-left: 14 px;"
           "border-radius: 0px;} "

           "QPushButton:hover { "
           "font-size: 20 pt;"
           "background-color: rgb(210,210,210);} "

           "QPushButton:checked { "
           "background-color: rgb(237,237,237);} "

           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(235,235,235);} "

           "QPushButton:pressed { "
           "font-size: 20 pt;"
           "background-color: rgb(235,235,235);} ";
}
QString Globals::getCssApplyPushButton(){
    return "QPushButton{ "
           "background-color: #3cbaa2; border: 0.5px solid gray;"
           "font: bold;"
           "font-weight: bold;"
           "color: #ffffff;"
           "border-radius: 0px; "
           "background-color: rgb(60,60,60);} "

           "QPushButton:checked { "
           "background-color: rgb(240,240,240);} "

           "QPushButton:disabled { "
           "color: gray;"
           "background-color:  rgb(230,230,230); "
           "font-size: 14px; "
           "font-weight: bold;} "

           "QPushButton:pressed { "
           "background-color:  rgb(255,255,255);} ";
}
QString Globals::getCssLineEdit(){
    return "QLineEdit{ "
           "color: black; "
           "background-color: white;} "

           "QLineEdit::disabled{ "
           "color: gray; "
           "background-color: rgb(213,213,213);} ";
}

QString Globals::getCssSelect(){
    return "QComboBox{ "
           "selection-background-color: rgb(100,100,100); "
           "background-color: white;} "

           "QComboBox QAbstractItemView{"
           "selection-background-color: rgb(100,100,100); "
           "background-color: rgb(255,255,255);} ";
}

QString Globals::getCssToolBox(){
    return  "QToolBox::tab {"
            "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
            "stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,"
            "stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);"
            "border-radius: 5px;"
            "color: darkgray;} "

            "QToolBox::tab:selected { "
            "color: black;}";
}

QFont Globals::getFontViewer(){
    return QFont("Calibri", 13);
}
QString Globals::getCurrentProjectName(){
    return cp_name;
}
QString Globals::getCurrentProjectLastModification(){
    return cp_lastModification;
}
QString Globals::getCurrentProjectCreationDate(){
    return cp_creationDate;
}
bool Globals::getCurrentProjectCadModel(){
    return cp_cadModel;
}
bool Globals::getCurrentProjectCfdModel(){
    return cp_cfdModel;
}
bool Globals::getCurrentProjectEdarModel(){
    return cp_edarModel;
}
bool Globals::getCurrentProjectMeshModel(){
    return cp_meshModel;
}
bool Globals::getCurrentProjectResultsModel(){
    return cp_resultsModel;
}
QDoubleValidator* Globals::getDoubleValidator(QObject *parent ) {
    return new QDoubleValidator(-1000, 1000, 4, parent);
}
QString Globals::getTabHome(){
    return tab_home;
}
QString Globals::getTabCad(){
    return tab_cad;
}
QString Globals::getTabCfd(){
    return tab_cfd;
}
QString Globals::getTabEdar(){
    return tab_edar;
}
QString Globals::getTabMesh(){
    return tab_mesh;
}
QString Globals::getTabResults(){
    return tab_results;
}
QString Globals::getTabHelp(){
    return tab_help;
}
void Globals::setAppWorkSpace(const QString& val){
    app_workspace = val;
}
void Globals::setCurrentProjectName(const QString& val){
    cp_name = val;
}
void Globals::setCurrentProjectCadModel(const bool& val){
    cp_cadModel = val;
}
void Globals::setCurrentProjectCfdModel(const bool& val){
    cp_cfdModel = val;
}
void Globals::setCurrentProjectResultsModel(const bool& val){
    cp_resultsModel = val;
}
