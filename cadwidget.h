#ifndef CADPRESET1WIDGET_H
#define CADPRESET1WIDGET_H

#include <QLineEdit>
#include <QListWidget>
#include <QMainWindow>
#include <QWidget>
#include <menuwidget.h>
#include <TopoDS_Compound.hxx>
#include <StlMesh.hxx>
#include <StlMesh_Mesh.hxx>
#include <vtkActor.h>
#include <QList>
#include <QTableWidget>
#include <QSlider>

class CadPreset1Widget : public QWidget {
    Q_OBJECT

public:
    explicit CadPreset1Widget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, QToolBox* reportWidget = 0, QListWidget* consoleWidget = 0);
    ~CadPreset1Widget();
    void loadMainWindow();

    void initFacesListArea();
    void showFacesValidationError(QString);
    void clearLayout(QLayout *);
    void writeSTLs();
    void createBlockMeshDictFile();
    void createSnappyHexMeshDictFile();
    void resetSettingsFileMesh();
    void loadImportDialog(QString);
    void loadPhysicalGroup(QString, QString, QString);
    void createRequiredFiles();
    void createControlDictFile();

public slots:
    void importProperties();
    void openImportDialog();
    void addPhysicalGroup();
    void validateGroup();
    void showGeometryFaces(int id=-1);
    void removeGroup();
    void initColorList();
    void generateViewerLegend();
    void readyToMesh();
    void updateGroupName();
    void onSliderOpacityStop();

private:
    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    Handle(StlMesh_Mesh) theStlMesh;
    QToolBox* reportInfo;
    QListWidget* console;
    void createAreaViewers();

    QVBoxLayout *layout_groups;
    TopoDS_Shape aShape;
    QList<bool> *list_assigned_faces;
    QList<double *> *color_list;
    QList<int> *faces_color_list;   //for each face, we assign a color (an index of color_list). Several faces can have the same color.
    QVBoxLayout* faces_layout;
    QTableWidget *table_widget;
    int cnt_groups; //total number of created groups (including the removed) used for naming undefined groups.
    QList<QString> *list_faces_names;
    QString str_blockmesh;
    QString str_snappy;
    QSlider *slider_opacity;
};

#endif // CADPRESET1WIDGET_H
