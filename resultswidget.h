#ifndef RESULTSWIDGET_H
#define RESULTSWIDGET_H

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QLineEdit>
#include <QMainWindow>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QTabWidget>
#include <QToolBox>
#include <QWidget>

#include <QVTKWidget.h>

#include <menuwidget.h>

#include <Geom_TrimmedCurve.hxx>
#include <gp_Ax1.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>

#include <unordered_map>

#include <QProcess>

#include <vtkActor.h>
#include <vtkSmartPointer.h>

#include <vtkviewer.h>

#include <QComboBox>
#include <QSlider>
#include <QStandardItemModel>
#include "tier_application/resultentity.h"
#include <QModelIndex>
#include <QSettings>

class ResultsWidget : public QWidget {
    Q_OBJECT

public:
    explicit ResultsWidget(QWidget* parent = 0, QWidget* centralwidget = 0, VTKViewer* vtk = 0, VTKViewer* graphs = 0, MenuWidget* menuwidget = 0, bool runDriftFlux = false, QToolBox* reportWidget = 0, QListWidget* console = 0);
    ~ResultsWidget();
    void loadMainWindow();

    void updateChartViewer();
    bool isSimulationRunning();
    void saveLine();

public slots:
    void stopOngoingSimulation();

    //void addGraph(const QString&);
    void addPlane();
    //    void showSludge();
    void nextStepTime();
    void onChangeDisplayOption(const QString&);
    void onChangeComboTimestep(const QString&);
    void onChangeComboMetrics();
    void onChangeSliderTimestep(const int);
    //void showAreaGraph();
    void showAreaPlane();

    void onSliderOpacityStop();
    void onShowModelChange(int value);
    void onShowSludgeChange(int value);

    void startDriftFluxFoam();
    void driftFluxFoamFinished();
    void cancelDriftFluxFoam();
    void driftFluxFoamOutput();
    void driftFluxFoamStarted();

    void startPimpleFoam();
    void processFoamOutput();

    void onRefresh();
    void onPause();
    void onPauseEnd();
    void onPlay();
    void onStop();
    void onNextTimestep();
    void onPrevTimestep();

    void showPrevTimestep();
    void showNextTimestep();
    void showSequenceTimesteps();

    //void showDomain();
    void updatePlaneClicked();
    void updateLineClicked();
    void onBasicPlaneChanged(int);
    void removePlane();
    //void showIsosurface();
    void onChangeComboEntity(int);

    void showEntity(ResultEntity*);
    void onSaveEntity();
    void onEntityChanged(QModelIndex);
    void onModelDataChanged(QStandardItem*);
    //QStandardItemModel* getVolumeEntities();
    void onChangedDataFromEntity(int);
    void loadEntity(int);
    void onTabCentralChanged(int);

    void showSurfaceObject();
    void showVolumeObject();

private:
    bool boolShowSurface;
    bool boolShowVolume;
    bool boolShowPlane;
    bool boolShowLine;

    double origin[3];
    double normal[3];

    QString displayMode;
    bool isShowPlane;
    bool isShowGraph;

    double p1[3];
    double p2[3];
    double stepTime;
    QTimer* stepTimeTimer;
    bool runningSimulation;
    QStringList list_excluded_dirs;
    void updateComboTimesteps();
    void addMetrics();
    bool checkMetricInTimestep(QString, QString);

private:
    MenuWidget* menu;
    QWidget* parent;
    QWidget* centralWidget;
    VTKViewer* vtkViewer;
    VTKViewer* vtkGraphs;
    QToolBox* reportInfo;
    QListWidget* console;

    QProcess* processDriftFlux;
    QProcess* processFoam;
    bool isCancelProcess;

    int currentNumFolders;

private:
    void createAreaButtons();

    void updateViewers(int);

    QWidget *showVolumeTab();
    QWidget *showPlaneTab();
    QWidget *showLineTab();
    QWidget *showSurfaceTab();

    QLineEdit *line_volume_value;
    QLineEdit *line_surface_value;

private:
    QPushButton* btnPlay;
    QPushButton* btnPause;
    QPushButton* btnRefresh;
    QPushButton *btnNextstep;
    QPushButton *btnStop;
    QPushButton *btnPrevstep;

    bool stopMedia;
    bool reproducing;

    //QSlider *slider_opacity;
    QVBoxLayout *lPlanes;
    QVBoxLayout *layoutAreaGraphs;

    QSlider *sliderTimestep;
    int totalSteps;
    int firstStep;
    int lastStep;
    int singleStep;
    QListView *listview_entities;
    QStandardItemModel *model_entities;
    QGridLayout *layout_bottomleft;
    QWidget *widget_slider;
    QSettings *settingsUser;
    QTabWidget *tabCentral;

    QComboBox *comboTimestep;
    QSlider *slider_base_opacity;
    QComboBox *combo_metric;
};

#endif // RESULTSWIDGET_H
