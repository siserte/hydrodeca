#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#define vtkFloatingPointType double

#include "borderlayout.h"
#include "mainwindow.h"
#include "menuwidget.h"
#include "globals.h"
#include "ui_defaultwindow.h"

#include <QComboBox>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSignalMapper>
#include <QTextStream>
#include <QToolBox>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::DefaultWindow) {
    ui->setupUi(this);

    this->setAttribute(Qt::WA_AlwaysShowToolTips,true);

    QDir().mkdir(Globals::instance()->getAppWorkSpacePath());

    //if(QDir(Globals::instance()->getAppWorkSpacePath()).exists()){
    menu = new MenuWidget(this, ui->centralwidget);

    QFrame* line = new QFrame();
    line->setFrameShape(QFrame::VLine);
    line->setLineWidth(0);

    QFrame* line2 = new QFrame();
    line2->setFrameShape(QFrame::VLine);
    line2->setLineWidth(0);

    QFrame* line3 = new QFrame();
    line3->setFrameShape(QFrame::HLine);
    line3->setLineWidth(1);

    // QColor colorMenu = QColor::setRgb(5,83,151);

    QPalette palette2 = line3->palette();
    palette2.setColor(QPalette::WindowText, QColor::fromRgb(5,83,151));
    line3->setPalette(palette2);


    QPalette palette = line->palette();
    palette.setColor(QPalette::WindowText, Qt::gray);
    line->setPalette(palette);
    line2->setPalette(palette);

    BorderLayout* layout = new BorderLayout;
    layout->setSpacing(0);
    layout->addWidget(createAreaButtons(), BorderLayout::West);
    //layout->addWidget(line,BorderLayout::West);
    layout->addWidget(createAreaActions(), BorderLayout::West);
    layout->addWidget(line2,BorderLayout::West);
    layout->addWidget(createAreaViewers(), BorderLayout::Center);
    //layout->addWidget(line,BorderLayout::North);
    layout->add(menu->createMenu(), BorderLayout::North);
    layout->addWidget(line3,BorderLayout::North);
    layout->addWidget(vtkViewer->getControlArea(), BorderLayout::East);
    ui->centralwidget->setLayout(layout);

    QString str = "HydroSludge";
    setWindowTitle(str);

    menu->setConsole(console);
    menu->setViewers(vtkViewer, vtkGraphs);
    menu->setHome(homeWidget);

    ui->statusbar->hide();
    //createStatusBar();

    /* }
    else{
        // Creamos la carpeta que contendra todos los proyectos
        QDir().mkdir(Globals::instance()->getAppWorkSpacePath());
    }*/

    if (AUTODEBUG == 1){
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Remove the line below" << endl;
        selectProject();
    }

    if (OFENGINE == 1) {
        QProcess* pCheckDocker = new QProcess();
        //connect(processSnappyStep1, SIGNAL(started()), this, SLOT(snappyHexMeshStep1Started()));
        //connect(pCheckDocker,SIGNAL(readyReadStandardOutput()),this,SLOT(snappyHexMeshStep1Output()));
        //connect(processSnappyStep1, SIGNAL(finished(int)), this, SLOT(snappyHexMeshStep1Finished()));

        //processSnappyStep1->setWorkingDirectory(Globals::instance()->getAppProjectPath());
        pCheckDocker->setProcessChannelMode(QProcess::MergedChannels);

        QString commands = "docker ps";

        pCheckDocker->start(POWERSHELL, commands.split(" "));
        pCheckDocker->waitForFinished();
        QString output = pCheckDocker->readAllStandardOutput();
        if (output.split(" ")[0] != "CONTAINER"){
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< "Docker not running" << endl;
            QMessageBox *messageBox = new QMessageBox();
            messageBox->setIcon(QMessageBox::Warning);
            messageBox->setWindowTitle("Warning! Docker is not running!");
            messageBox->setText("Hydrodeca couldn't find the Docker daemon running. It means that meshing and simulation is not enabled\n\nERROR message:\n" + output);
            messageBox->setStandardButtons(QMessageBox::Close);
            messageBox->exec();
        } else {
            //commands = "docker ps -a | findstr " + $DOCKERIMAGE + " | Measure-Object | Select-Object -expand count";
            commands = "docker ps -a | findstr " + QString(DOCKERIMAGE);
            pCheckDocker->start(POWERSHELL, commands.split(" "));
            pCheckDocker->waitForFinished();
            QString output = pCheckDocker->readAllStandardOutput();
            if (output == NULL){
                //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< "Containers have not found" << endl;
            } else {
                //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< output << endl;
                QStringList lines = output.split("\n");
                for (int i = 0; i < lines.count()-1; i++){
                    QStringList fields = lines[i].split(" ");
                    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< "Stopping container: " << fields[0] << endl;
                    QProcess* process = new QProcess();
                    QString commands = "docker stop " + fields[0];
                    process->start(POWERSHELL, commands.split(" "));
                    process->waitForFinished();
                }
            }
        }
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< output << endl;
    }
}

MainWindow::~MainWindow(){
    delete ui;
}

/********************************************************************************
 * GUI FUNCTIONS
 ********************************************************************************/
void MainWindow::createStatusBar(){
    QPushButton* btnStatusBarProject = new QPushButton("info", this);
    btnStatusBarProject->setObjectName("btnStatusBarProject");
    connect(btnStatusBarProject, SIGNAL(clicked()), this, SLOT(showStatusBarInfo()));

    ui->statusbar->addWidget(btnStatusBarProject);
}

void MainWindow::showStatusBarInfo() {
    QString edarModel = Globals::instance()->getCurrentProjectEdarModel()?"True":"False";
    QString cadModel = Globals::instance()->getCurrentProjectCadModel()?"True":"False";
    QString cfdModel = Globals::instance()->getCurrentProjectCfdModel()?"True":"False";
    QString meshModel = Globals::instance()->getCurrentProjectMeshModel()?"True":"False";
    QString resultsModel = Globals::instance()->getCurrentProjectResultsModel()?"True":"False";

    QString textInfo = "<strong>INFORMATION</strong> <br> <br>";
    textInfo += "<strong>Workspace:</strong> " + Globals::instance()->getAppWorkSpace() + "<br>";
    textInfo +="<strong>Project:</strong> " + Globals::instance()->getCurrentProjectName() + "<br>";
    textInfo += "<strong>Cre. Date:</strong> " + Globals::instance()->getCurrentProjectCreationDate() + "<br>";
    textInfo += "<strong>Mod. Date:</strong> " + Globals::instance()->getCurrentProjectLastModification() + "<br>";
    textInfo += "<strong>Edar Model:</strong> " + edarModel + "<br>";
    textInfo += "<strong>Cad Model:</strong> " + cadModel + "<br>";
    textInfo += "<strong>Cfd Model:</strong> " + cfdModel + "<br>";
    textInfo += "<strong>Mesh Model:</strong> " + meshModel + "<br>";
    textInfo += "<strong>Results Model:</strong> " + resultsModel + "<br>";



    QMessageBox* msgBox = new QMessageBox();
    msgBox->setIcon(QMessageBox::Information);
    msgBox->setWindowTitle("CURRENT PROJECT INFO");
    msgBox->setModal(true);
    msgBox->setFixedWidth(700);
    msgBox->setFixedHeight(600);
    msgBox->setStyleSheet("border: none;");
    msgBox->setText(textInfo);
    msgBox->show();
}

QScrollArea* MainWindow::createAreaButtons(){
    // SPACER
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Creamos el widget para WestArea
    QVBoxLayout *grid = new QVBoxLayout;
    grid->setSpacing(0);
    grid->setMargin(0);

    // Ponemos los datos del proyecto actual
    QVBoxLayout* layoutDataArea = new QVBoxLayout();
    layoutDataArea->setSpacing(0);
    layoutDataArea->setMargin(0);

    QLabel* workspace = new QLabel("WorkSpace");
    workspace->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* name = new QLabel("Project");
    name->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* cre_date = new QLabel("Cre. Date");
    cre_date->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* mod_date = new QLabel("Mod. Date");
    mod_date->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* edar = new QLabel("Edar Model");
    edar->setStyleSheet("font-size: 18px; font-weight: bold;");
    QLabel* cad = new QLabel("Cad Model");
    cad->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* cfd = new QLabel("Cfd Model");
    cfd->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* mesh = new QLabel("Mesh Model");
    mesh->setStyleSheet("font-size: 14px; font-weight: bold;");
    QLabel* results = new QLabel("Results Model");
    results->setStyleSheet("font-size: 14px; font-weight: bold;");
    layoutDataArea->addWidget(workspace);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getAppWorkSpace()));
    layoutDataArea->addWidget(name);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectName()));
    layoutDataArea->addWidget(cre_date);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCreationDate()));
    layoutDataArea->addWidget(mod_date);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectLastModification()));
    layoutDataArea->addWidget(edar);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectEdarModel()?"True":"False"));
    layoutDataArea->addWidget(cad);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCadModel()?"True":"False"));
    layoutDataArea->addWidget(cfd);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectCfdModel()?"True":"False"));
    layoutDataArea->addWidget(mesh);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectMeshModel()?"True":"False"));
    layoutDataArea->addWidget(results);
    layoutDataArea->addWidget(new QLabel(Globals::instance()->getCurrentProjectResultsModel()?"True":"False"));

    QGroupBox* currentProjectDataArea = new QGroupBox();
    currentProjectDataArea->setStyleSheet("border: none;");
    currentProjectDataArea->setLayout(layoutDataArea);

    QPushButton* btnPrev = new QPushButton;
    btnPrev->setObjectName("btnPrev");
    btnPrev->setIcon(QIcon(":/Resources/back-24838.png"));
    btnPrev->setFixedSize(30,30);
    btnPrev->setIconSize(QSize(30,30));
    connect(btnPrev, SIGNAL(clicked()), this, SLOT(prevTab()));

    QPushButton* btnNext = new QPushButton;
    btnNext->setObjectName("btnNext");
    btnNext->setIcon(QIcon(":/Resources/right-24837.png"));
    btnNext->setFixedSize(30,30);
    btnNext->setIconSize(QSize(30,30));
    connect(btnNext, SIGNAL(clicked()), this, SLOT(nextTab()));

    // Creamos el widget para WestArea
    QHBoxLayout* navArea = new QHBoxLayout();
    navArea->setSpacing(0);
    navArea->setMargin(0);
    navArea->setObjectName("area_nav");
    //navArea->addWidget(btnPrev);
    //navArea->addWidget(btnNext);

    QWidget* navWidget = new QWidget();
    navWidget->setBackgroundRole(QPalette::Midlight);
    navWidget->setStyleSheet(Globals::instance()->getCssAreaNav());
    navWidget->setStyleSheet(Globals::instance()->getCssPushButton());
    navWidget->setLayout(navArea);

    grid->addSpacerItem(my_spacer);
    //grid->addWidget(navWidget);
    //grid->addSpacerItem(my_spacer2);

    // Cambiamos el contenido de Area Buttons
    QScrollArea* westArea = new QScrollArea(this);
    westArea->setObjectName("area_buttons");
    westArea->setMaximumWidth(10);
    westArea->setBackgroundRole(QPalette::Midlight);
    westArea->setStyleSheet(Globals::instance()->getCssAreaActions());
    westArea->setFrameShape(QFrame::NoFrame);
    westArea->setLayout(grid);

    return westArea;
}

void MainWindow::nextTab(){
    findChild<QTabWidget*>("area_viewers")->setCurrentIndex(2);
    menu->goToEdarView();
}

void MainWindow::prevTab(){

}

void MainWindow::resetTab(){

}

QScrollArea* MainWindow::createAreaActions(){
    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QLabel* lNameNewProject = new QLabel("New project");
    QLineEdit* iNameNewProject = new QLineEdit;
    iNameNewProject->setObjectName("iNameNewProject");
    iNameNewProject->setStyleSheet(Globals::instance()->getCssLineEdit());

    QPushButton* btnCreateNewProject = new QPushButton("APPLY", this);
    btnCreateNewProject->setObjectName("btnCreateNewProject");
    //btnCreateNewProject->setMaximumHeight(30);
    btnCreateNewProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnCreateNewProject->setMinimumHeight(30);
    connect(btnCreateNewProject, SIGNAL(clicked()), this, SLOT(createNewProject()));

    // Miramos si existe el fichero de settings.txt (contiene informacion del proyecto actual)
    QLabel* lSelectProject = new QLabel("Existing project");
    QDir dir = QDir(Globals::instance()->getAppCurrentWorkSpacePath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList();

    QComboBox* cbSelectProject = new QComboBox(this);
    cbSelectProject->setObjectName("cbSelectProject");
    cbSelectProject->setMaximumHeight(20);
    cbSelectProject->setStyleSheet(Globals::instance()->getCssSelect());

    cbSelectProject->addItem(" ");

    foreach(QString file, dirList){
        if(QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + file).exists()){
            cbSelectProject->addItem(file);
        }
    }

    cbSelectProject->setCurrentText(" ");

    // Creamos el boton de
    QPushButton* btnLoadProject = new QPushButton("LOAD PROJECT", this);
    btnLoadProject->setObjectName("btnLoadProject");
    btnLoadProject->setMinimumHeight(20);
    btnLoadProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btnLoadProject, SIGNAL(clicked()), this, SLOT(selectProject()));

    QPushButton* btnRemoveProject = new QPushButton("DELETE PROJECT", this);
    btnRemoveProject->setObjectName("btnRemoveProject");
    btnRemoveProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnRemoveProject->setMinimumHeight(20);
    connect(btnRemoveProject, SIGNAL(clicked()), this, SLOT(removeProject()));

    QSpacerItem * my_spacer2 = new QSpacerItem(0,300, QSizePolicy::Expanding, QSizePolicy::Expanding);

    layoutAreaActions->addWidget(lNameNewProject);
    layoutAreaActions->addWidget(iNameNewProject);
    //layoutAreaActions->addWidget(cbTypeProject);
    layoutAreaActions->addWidget(btnCreateNewProject);
    layoutAreaActions->addSpacerItem(my_spacer2);
    layoutAreaActions->addWidget(lSelectProject);
    layoutAreaActions->addWidget(cbSelectProject);
    layoutAreaActions->addWidget(btnLoadProject);
    layoutAreaActions->addWidget(btnRemoveProject);
    layoutAreaActions->addSpacerItem(my_spacer);

    // Cambiamos el contenido de Area Actions
    QScrollArea* areaActions = new QScrollArea(this);
    areaActions->setWidgetResizable(true);
    areaActions->setObjectName("area_actions");
    areaActions->setMinimumWidth(300);
    areaActions->setBackgroundRole(QPalette::Midlight);
    areaActions->setStyleSheet(Globals::instance()->getCssAreaActions());
    areaActions->setFrameShape(QFrame::NoFrame);
    areaActions->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    areaActions->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    areaActions->setLayout(layoutAreaActions);


    return areaActions;
}

QTabWidget* MainWindow::createAreaViewers(){
    vtkViewer = new VTKViewer();
    vtkGraphs = new VTKViewer();
    homeWidget = Globals::instance()->getHomeWTab();

    console = new QListWidget();
    console->setStyleSheet("QListWidget {background-color: #000000;}");
    console->scrollToBottom();

    QTabWidget* tabs = new QTabWidget(this);
    tabs->setObjectName("area_viewers");
    tabs->setTabPosition(QTabWidget::East);
    tabs->setFont(Globals::instance()->getFontViewer());
    tabs->setStyleSheet(Globals::instance()->getCssViewerFont());
    tabs->setStyleSheet(Globals::instance()->getCssViewer());

    tabs->addTab(homeWidget, "OVERVIEW");
    tabs->addTab(vtkViewer, "3D VIEWER");
    tabs->addTab(vtkGraphs, "GRAPHS");
    tabs->addTab(console, "CONSOLE");

    return tabs;
}

/********************************************************************************
 * PRIVATE FUNCTIONS
 ********************************************************************************/



/********************************************************************************
 * SLOTS FUNCTIONS
 ********************************************************************************/
void MainWindow::onNewProjectTextChanged(const QString &text) {
    if(text.length() >= 3 && !findChild<QPushButton*>("btnCreateNewProject")->isEnabled()){
        findChild<QPushButton*>("btnCreateNewProject")->setEnabled(true);
    }
    else if(text.length() < 3 && findChild<QPushButton*>("btnCreateNewProject")->isEnabled()){
        findChild<QPushButton*>("btnCreateNewProject")->setEnabled(false);
    }
}

void MainWindow::createNewProject(){
    //TODO Cogemos el tipo de proyecto
    //qDebug() << "Create  Project" << endl;



    // Deshabilitamos el boton de crear proyecto
    findChild<QPushButton*>("btnCreateNewProject")->setEnabled(false);

    // Comprobamos que no exista el proyecto en el sistema
    QString newProjectName = findChild<QLineEdit*>("iNameNewProject")->text();

    if(!QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName).exists()){
        QDir().mkdir(Globals::instance()->getAppCurrentWorkSpacePath() + "/" + newProjectName);
        Globals::instance()->setCurrentProjectName(newProjectName);

        //Globals::instance()->checkProject();
        //Globals::instance()->writeSettingsFile();
        //Globals::instance()->writeProjectInfoFile();
        //QFile::copy(Globals::instance()->getAppDefaultPath() + "\\preset\\geometry_preset_1.txt", filename);

        QString src = Globals::instance()->getAppSettingsPath() + "/recommendations.ini";
        if (!QFile::exists(src))
            qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - recommentations.ini does not exist" << endl;
        QString dst = Globals::instance()->getAppProjectPath();
        if (! QFile::copy(src, dst))
            qDebug() << endl << src << " - " << dst << endl;

        src = Globals::instance()->getAppSettingsPath() + "/filesSolvers/common/";
        if (! Globals::instance()->copyPath(src, dst))
            qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - solver common files cannot be copied" << endl;
        else
            return;

        //Globals::instance()->copyFolder(Globals::instance()->getAppDefaultPath()+"\\openfoam\\case_1", Globals::instance()->getAppProjectPath());

        QString str = "HydroSludge - " + Globals::instance()->getCurrentProjectName();
        setWindowTitle(str);

        // Cambiamos a la pestaña de cad
        menu->goToEdarView();
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newProjectName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }
}

void MainWindow::removeProject(){
    QString currentItem = findChild<QComboBox*>("cbSelectProject")->currentText();
    //QFile file = Globals::instance()->getAppLibraryRheologyPath() + "\\" + currentItem;
    QDir dir = QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + currentItem);
    //dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    dir.removeRecursively();

    QDir dirAll = QDir(Globals::instance()->getAppCurrentWorkSpacePath());
    dirAll.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList dirList = dirAll.entryList();



    findChild<QComboBox*>("cbSelectProject")->clear();
    findChild<QComboBox*>("cbSelectProject")->addItem(" ");
    foreach(QString file, dirList){
        if(QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + file).exists()){
            findChild<QComboBox*>("cbSelectProject")->addItem(file);
        }
    }

    findChild<QComboBox*>("cbSelectProject")->setCurrentText(" ");

}

void MainWindow::onNewWorkSpaceTextChanged(const QString &text) {
    if(text.length() >= 3 && !findChild<QPushButton*>("btnCreateNewWorkSpace")->isEnabled()){
        findChild<QPushButton*>("btnCreateNewWorkSpace")->setEnabled(true);
    }
    else if(text.length() < 3 && findChild<QPushButton*>("btnCreateNewWorkSpace")->isEnabled()){
        findChild<QPushButton*>("btnCreateNewWorkSpace")->setEnabled(false);
    }
}

void MainWindow::createNewWorkSpace(){
    // Deshabilitamos el boton de crear proyecto
    /*findChild<QPushButton*>("btnCreateNewWorkSpace")->setEnabled(false);

    // Comprobamos que no exista el proyecto en el sistema
    QString newWorkSpaceName = findChild<QLineEdit*>("iNameNewWorkSpace")->text();

    if(!QDir(Globals::instance()->getAppWorkSpacePath() + "\\" + newWorkSpaceName).exists()){
        // Creamos la carpeta del proyecto
        QDir().mkdir(Globals::instance()->getAppWorkSpacePath() + "\\" + newWorkSpaceName);
        //qDebug() << "New workspace" << Globals::instance()->getAppWorkSpacePath() + "\\" + newWorkSpaceName <<endl;
        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setAppWorkSpace(newWorkSpaceName);
        Globals::instance()->writeSettingsFile();

        QLabel* lNameNewProject = new QLabel("New project");
        QLineEdit* iNameNewProject = new QLineEdit;
        iNameNewProject->setObjectName("iNameNewProject");

        QComboBox* cbTypeProject = new QComboBox(this);
        cbTypeProject->setObjectName("cbTypeProject");
        cbTypeProject->setMaximumHeight(30);
        cbTypeProject->addItem("Circular");
        cbTypeProject->addItem("Hopper");
        cbTypeProject->addItem("Lamella");
        cbTypeProject->addItem("Rectangular");
        QPushButton* btnCreateNewProject = new QPushButton("APPLY", this);
        btnCreateNewProject->setObjectName("btnCreateNewProject");
        btnCreateNewProject->setMinimumHeight(20);
        btnCreateNewProject->setStyleSheet(Globals::instance()->getCssApplyPushButton());
        connect(btnCreateNewProject, SIGNAL(clicked()), this, SLOT(createNewProject()));

        QVBoxLayout* layoutAreaActions = new QVBoxLayout();
        QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);

        layoutAreaActions->addWidget(lNameNewProject);
        layoutAreaActions->addWidget(iNameNewProject);
        layoutAreaActions->addWidget(cbTypeProject);
        layoutAreaActions->addWidget(btnCreateNewProject);
        //layoutAreaActions->addWidget(btnRemoveProject);
        layoutAreaActions->addSpacerItem(my_spacer);

        if(findChild<QScrollArea*>("area_actions")->layout() != NULL){
            QLayoutItem* child;
            while ((child = findChild<QScrollArea*>("area_actions")->layout()->takeAt(0)) != 0) {
                delete child->widget();
            }
            delete findChild<QScrollArea*>("area_actions")->layout();

            findChild<QScrollArea*>("area_actions")->setLayout(layoutAreaActions);
        }

        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Message</h2>"
                                  "<p>The workspace " + newWorkSpaceName + " has been successfully created. </p>" +
                                  "<p>Please, create a new project to start.</p>");
        msgBox.setStyleSheet("QLabel{width: 400px; min-width: 400px; max-width: 400px;}");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newWorkSpaceName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }*/
}

void MainWindow::selectProject() {

    if (AUTODEBUG){
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Uncomment the previous line and delete the next" << endl;
        Globals::instance()->setCurrentProjectName(PROJECTNAME);
    }else{
        QComboBox* select = ui->centralwidget->findChild<QComboBox*>("cbSelectProject");
        Globals::instance()->setCurrentProjectName(select->currentText());
    }

    Globals::instance()->checkProject();
    Globals::instance()->writeSettingsFile();

    QString str = "HydroSludge - " + Globals::instance()->getCurrentProjectName();
    setWindowTitle(str);

    if (AUTODEBUG){
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Set the default tab" << endl;

        if(WIDGETTAB == 1)
            menu->goToEdarView();
        else if(WIDGETTAB == 2)
            menu->goToCadView();
        else if(WIDGETTAB == 3)
            menu->goToMeshView();
        else if(WIDGETTAB == 4)
            menu->goToCfdView();
        else if(WIDGETTAB == 5)
            menu->goToResultsView();
    } else{
        menu->goToEdarView();
    }
}

void MainWindow::showLoadProjectDialog() {
    QString dir = QFileDialog::getExistingDirectory(this,
                                                    "Select Project",
                                                    Globals::instance()->getAppCurrentWorkSpacePath(),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(Globals::instance()->getCurrentProjectName() != dir){ // Cambiar
        Globals::instance()->setCurrentProjectName(Globals::instance()->getCurrentProjectName());
        Globals::instance()->checkProject();
        menu->goToEdarView();
    }
    else{
        QMessageBox::about(this, tr("ERROR LOAD PROJECT"),
                           tr("<p>El proyecto seleccionado ya esta en uso</p>"));
    }
}

void MainWindow::showNewProjectDialog() {
    QLineEdit* iNameDialogNewProject = new QLineEdit;
    iNameDialogNewProject->setObjectName("iNameDialogNewProject");
    QObject::connect(iNameDialogNewProject, SIGNAL(textChanged(QString)), this, SLOT(onNewProjectDialogTextChanged(QString)));

    QPushButton* btnCreateDialogNewProject = new QPushButton("NEW PROJECT", this);
    btnCreateDialogNewProject->setObjectName("btnCreateDialogNewProject");
    btnCreateDialogNewProject->setDisabled(true);
    QObject::connect(btnCreateDialogNewProject, SIGNAL(clicked()), this, SLOT(createNewProjectDialog()));

    QFormLayout* layoutNewArea = new QFormLayout();
    layoutNewArea->addRow("NAME", iNameDialogNewProject);
    layoutNewArea->addWidget(btnCreateDialogNewProject);

    QScrollArea* createNewProjectArea = new QScrollArea(this);
    createNewProjectArea->setFrameShape(QFrame::NoFrame);
    createNewProjectArea->setLayout(layoutNewArea);

    QGridLayout *grid = new QGridLayout;
    //grid->addWidget(createNewProjectArea, 1, 0);

    QDialog* dialog = new QDialog(this);
    dialog->setObjectName("dialogNewProject");
    dialog->setLayout(grid);
    dialog->exec();
}

void MainWindow::createNewProjectDialog(){
    // Comprobamos que no exista el proyecto en el sistema
    QString newProjectName = findChild<QLineEdit*>("iNameDialogNewProject")->text();

    if(!QDir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName).exists()){
        // Creamos la carpeta del proyecto
        QDir().mkdir(Globals::instance()->getAppCurrentWorkSpacePath() + "\\" + newProjectName);

        // Actualizamos la informacion del fichero settings.txt
        Globals::instance()->setCurrentProjectName(newProjectName);
        Globals::instance()->checkProject();
        //Globals::instance()->setCurrentProjectCadModel(true);
        Globals::instance()->writeSettingsFile();
        Globals::instance()->writeProjectInfoFile();

        // Escondemos el dialog de nuevo proyecto
        QDialog* dialogNewProject = findChild<QDialog*>("dialogNewProject");
        dialogNewProject->hide();

        // Cambiamos a la pestaña de cad
        menu->goToEdarView();
    }
    else{
        QMessageBox msgBox;
        msgBox.setInformativeText("<h2>Error Message</h2>"
                                  "<p>Could not create the " + newProjectName + " project because there is another one with the same name" +
                                  "<p>Please select another name.");
        msgBox.exec();
    }
}

void MainWindow::onNewProjectDialogTextChanged(const QString &text) {
    if(text.length() >= 3 && !findChild<QPushButton*>("btnCreateDialogNewProject")->isEnabled()){
        findChild<QPushButton*>("btnCreateDialogNewProject")->setEnabled(true);
    }
    else if(text.length() < 3 && findChild<QPushButton*>("btnCreateDialogNewProject")->isEnabled()){
        findChild<QPushButton*>("btnCreateDialogNewProject")->setEnabled(false);
    }
}

void MainWindow::onNewWorkSpaceDialogTextChanged(const QString &text) {
    if(text.length() >= 3 && !findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->isEnabled()){
        findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->setEnabled(true);
    }
    else if(text.length() < 3 && findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->isEnabled()){
        findChild<QPushButton*>("btnCreateDialogNewWorkSpace")->setEnabled(false);
    }
}

void MainWindow::processOutput(){
    QProcess *process = qobject_cast<QProcess*>(sender());
    QString text;
    text.append(process->readAllStandardOutput());
    qDebug() << "\t(Sergio): " << text << endl;
}

void MainWindow::closeEvent(QCloseEvent *event){
    //second attempt (working)
    /*
    QString script = "terminateFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "list=$(ps -ef | grep driftFluxFoam | tr -s \" \" | cut -f2 -d\" \")" << endl;
        stream << "for i in $list; do kill $i; done" << endl;
        stream << "list=$(ps -ef | grep blockMesh | tr -s \" \" | cut -f2 -d\" \")" << endl;
        stream << "for i in $list; do kill $i; done" << endl;
        stream << "list=$(ps -ef | grep checkMesh | tr -s \" \" | cut -f2 -d\" \")" << endl;
        stream << "for i in $list; do kill $i; done" << endl;
        stream << "list=$(ps -ef | grep snappyHexMesh | tr -s \" \" | cut -f2 -d\" \")" << endl;
        stream << "for i in $list; do kill $i; done" << endl;
        stream << "list=$(ps -ef | grep surfaceFeatureExtract | tr -s \" \" | cut -f2 -d\" \")" << endl;
        stream << "for i in $list; do kill $i; done" << endl;
        //stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        //stream << "reconstructPar" << endl;
        fileScript.close();
    }

    QProcess* process = new QProcess();
    QString path = "C:/Windows/system32/WindowsPowerShell/v1.0/powershell.exe";

    QString commands = "docker exec $(cat " + Globals::instance()->getAppProjectPath() + "container_id) /tmp/project/terminateFoam.sh";
    process->start(path, commands.split(" "));
    */

    if (OFENGINE == 1) {
        //third attempt (working)
        QProcess* process = new QProcess();

        QString commands = "docker stop $(cat " + Globals::instance()->getAppProjectPath() + "container_id)";
        process->start(POWERSHELL, commands.split(" "));
    }

}
