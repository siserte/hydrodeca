#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include "borderlayout.h"
#include "edarwidget.h"
#include "globals.h"
#include "menuwidget.h"
#include "ui_defaultwindow.h"

#include <QComboBox>
#include <QCheckBox>
#include <qDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QSize>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QMessageBox>
#include <QSpinBox>
#include <QTextBrowser>
#include <QTableWidget>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QScrollArea>
#include <QSignalMapper>
#include <QSplitter>
#include <QString>
#include <unordered_map>
#include <QLayoutItem>
#include <QVBoxLayout>

#include "tier_application/ComputerLayersModel.h"
#include "tier_application/ComputerVesilind.h"
#include "tier_application/ConvertColors.h"
#include "tier_data_access/TextFileRead.h"
#include "tier_data_access/StringMethods.h"

#include <AIS_Shape.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBndLib.hxx>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepCheck_Analyzer.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>

#include <BRepLib.hxx>

#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>

#include <BRepPrim_Cone.hxx>
#include <BRepPrimAPI_MakeOneAxis.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepOffsetAPI_ThruSections.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <gp_Ax1.hxx>
#include <gp_Ax3.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_DisplayModeFilter.hxx>
#include <IVtkTools_ShapeDataSource.hxx>

#include <MeshVS_MeshPrsBuilder.hxx>
#include <MeshVS_DrawerAttribute.hxx>

#include <OSD_Path.hxx>

#include <QVTKWidget.h>

#include <RWStl.hxx>

#include <STEPCAFControl_Writer.hxx>

#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <StlMesh_Mesh.hxx>
#include <StlMesh_MeshExplorer.hxx>
#include <StlTransfer.hxx>

#include <TDocStd_Document.hxx>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkDoubleArray.h>
#include <vtkExecutive.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkInteractorStyleImage.h>
#include <vtkJPEGReader.h>
#include <vtkLabelHierarchy.h>
#include <vtkLabelPlacementMapper.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPointData.h>
#include <vtkPointSetToLabelHierarchy.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkPolyDataReader.h>
#include <vtkProperty.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkQImageToImageSource.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStringArray.h>
#include <vtkStructuredGrid.h>
#include <vtkTextProperty.h>
#include <vtkTextRenderer.h>
#include <vtkVariantArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkVersion.h>
#include <vtkAxis.h>

#include <vtkTable.h>
#include <vtkFloatArray.h>
#include <vtkContextView.h>
#include <vtkChartXY.h>
#include <vtkContextScene.h>
#include <vtkContextActor.h>
#include <vtkPlot.h>
#include <vtkPlotPoints.h>
#include <vtkPen.h>

#include <vtkPNGReader.h>

#include <XCAFApp_Application.hxx>
#include <XCAFDoc_DocumentTool.hxx>
#include <XCAFDoc_ShapeTool.hxx>

#include <XSDRAWSTLVRML_DataSource.hxx>

#include <QFileDialog>
#include "tier_application/boxlinefield.h"
#include "tier_application/fileeditor.h"
#include <QLayoutItem>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

//#include <thread>
//#include <chrono>
//std::this_thread::sleep_for(std::chrono::milliseconds(2000));


#define MAX2(X, Y)      (Abs(X)>Abs(Y)?Abs(X):Abs(Y))
#define MAX3(X, Y, Z)   (MAX2(MAX2(X,Y),Z))


EdarWidget::EdarWidget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, QToolBox* reportWidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    reportInfo = reportWidget;
    console = consoleWidget;

    vtkViewer->setShowAxes(false);
    vtkViewer->setShowModel(true);
    vtkViewer->setOpacityModel(1.0);
    lineArraysCreated=false;
}

EdarWidget::~EdarWidget(){

}

void EdarWidget::loadMainWindow(){
    statePoint = vtkSmartPointer <vtkContextActor>::New();
    model10 = vtkSmartPointer <vtkContextActor>::New();
    settlingCurve = vtkSmartPointer<vtkContextActor>::New();

    menu->updateMenu(Globals::instance()->getTabEdar(), this);

    menu->btnHome->setDisabled(true);
    menu->btnCad->setDisabled(false);
    menu->btnMesh->setDisabled(false);
    menu->btnCfd->setDisabled(false);
    menu->btnCfd->setDisabled(false);

    createAreaButtons();
    createAreaActions();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Cambiar la pestaña por defecto" << endl;
    designProperties();
    //settlingProperties();
    //dataInputProperties();
    //applyProcess();

    centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);
}

void EdarWidget::createAreaActions(){
    QGridLayout* layout = new QGridLayout();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(400);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(400);
}

void EdarWidget::createAreaButtons(){
    QPushButton *btnDesign = new QPushButton("Design", this);
    connect(btnDesign, SIGNAL(clicked()), this, SLOT(designProperties()));
    btnDesign->setStyleSheet(Globals::instance()->getCssPushButton());
    QFont font = btnDesign->font();
    font.setPointSize(22);
    btnDesign->setFont(font);
    btnDesign->setMinimumHeight(50);
    btnDesign->setCheckable(true);
    btnDesign->setAutoExclusive(true);
    btnDesign->setChecked(true);
    QPushButton *btnSettling = new QPushButton("Settling", this);
    connect(btnSettling, SIGNAL(clicked()), this, SLOT(settlingProperties()));
    btnSettling->setStyleSheet(Globals::instance()->getCssPushButton());
    btnSettling->setMinimumHeight(50);
    btnSettling->setCheckable(true);
    btnSettling->setAutoExclusive(true);
    QPushButton *btnRheology = new QPushButton("Rheology", this);
    connect(btnRheology, SIGNAL(clicked()), this, SLOT(rheologyProperties()));
    btnRheology->setStyleSheet(Globals::instance()->getCssPushButton());
    btnRheology->setMinimumHeight(50);
    btnRheology->setCheckable(true);
    btnRheology->setAutoExclusive(true);
    QPushButton *btnDataInput = new QPushButton("Data input", this);
    connect(btnDataInput, SIGNAL(clicked()), this, SLOT(dataInputProperties()));
    btnDataInput->setStyleSheet(Globals::instance()->getCssPushButton());
    btnDataInput->setMinimumHeight(50);
    btnDataInput->setCheckable(true);
    btnDataInput->setAutoExclusive(true);
    QPushButton *btnProcess = new QPushButton("Process", this);
    connect(btnProcess, SIGNAL(clicked()), this, SLOT(processProperties()));
    btnProcess->setStyleSheet(Globals::instance()->getCssPushButton());
    btnProcess->setMinimumHeight(50);
    btnProcess->setCheckable(true);
    btnProcess->setAutoExclusive(true);

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->setMargin(0);
    layoutAreaActions->setSpacing(0);
    layoutAreaActions->addWidget(btnDesign);
    layoutAreaActions->addWidget(btnSettling);
    layoutAreaActions->addWidget(btnDataInput);
    layoutAreaActions->addWidget(btnRheology);
    layoutAreaActions->addWidget(btnProcess);
    layoutAreaActions->addStretch();

    if(centralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        QLayoutItem *child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }
    centralWidget->findChild<QScrollArea*>("area_buttons")->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(100);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setStyleSheet(Globals::instance()->getCssAreaButtons());
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! SLOTS
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void EdarWidget::setImageInViewer(int option, char *filepath = ""){

    vtkViewer->reset();

    if (option > 0){
        float offset;
        int scale;
        if (option == 1) { //Design
            offset = 0.5;
            scale = 900;
        } else if (option == 2) { //Process
            filepath = strdup(":/Resources/Process.png");
            offset = 0.8;
            scale = 900;
        }

        int *size = vtkViewer->GetRenderWindow()->GetSize();

        QImage img(filepath);
        QImage imgScaled = img.scaled(scale, scale, Qt::KeepAspectRatio, Qt::SmoothTransformation);

        vtkSmartPointer<vtkQImageToImageSource> imageReader = vtkSmartPointer<vtkQImageToImageSource>::New();
        imageReader->SetQImage(&imgScaled);
        imageReader->Update();

        vtkSmartPointer<vtkImageMapper> imageMapper = vtkSmartPointer<vtkImageMapper>::New();
        imageMapper->SetInputConnection(imageReader->GetOutputPort());
        imageMapper->SetColorWindow(255);
        imageMapper->SetColorLevel(127.5);

        vtkSmartPointer<vtkActor2D> imageActor2D = vtkSmartPointer<vtkActor2D>::New();
        imageActor2D->SetMapper(imageMapper);
        imageActor2D->SetPosition(0.5 * size[0] - imgScaled.width() * 0.5, offset * size[1] - imgScaled.height() * 0.5);

        vtkViewer->add(imageActor2D);
    }

    vtkViewer->update();
}

void EdarWidget::loadSettingsDataInput(int index){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    flowTabbar->setCurrentIndex(index);
    if (index == 0) { //Transcient
        flow_profiles->setCurrentText(settings->value("DataInput/flow_file", "").toString());
        if (flow_profiles->count()) {
            func1->setCurrentIndex(settings->value("DataInput/flow_filter1", "0").toInt());
            func2->setCurrentIndex(settings->value("DataInput/flow_filter2", "0").toInt());
            val1->setText(settings->value("DataInput/flow_value1", "0").toString());
            val2->setText(settings->value("DataInput/flow_value2", "0").toString());
        }
        checkRemoveOutliers->setChecked(settings->value("DataInput/outliers", "0").toInt());

    } else {
        line_caudal->setText(settings->value("DataInput/caudal", "300").toString());
        line_solids->setText(settings->value("DataInput/solids", "50").toString());
    }
}

void EdarWidget::saveSettingsDataInput(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    int index = flowTabbar->currentIndex();
    settings->setValue("DataInput/type", index);

    if (index == 0) { //Transcient
        settings->setValue("DataInput/flow_file", flow_profiles->currentText());
        settings->setValue("DataInput/flow_filter1", func1->currentIndex());
        settings->setValue("DataInput/flow_filter2", func2->currentIndex());
        settings->setValue("DataInput/flow_value1", val1->text());
        settings->setValue("DataInput/flow_value2", val2->text());
        settings->setValue("DataInput/outliers", checkRemoveOutliers->isChecked());
    } else {
        settings->setValue("DataInput/caudal", line_caudal->text());
        settings->setValue("DataInput/solids", line_solids->text());
    }
}

int EdarWidget::parseFlowFile(std::string stream){
    bool ok;
    std::string line;
    int lineIndex=1;
    std::istringstream f(stream);
    QString qsAux;
    double dobAux;

    while (std::getline(f, line)) {
        qsAux = QString::fromStdString(line);
        dobAux = qsAux.toDouble(&ok);
        if ((!ok) && (qsAux != "")){ //if empty we'll use 0
            showFileParserError(lineIndex, "Wrong value: " + qsAux);
            return lineIndex;
        }
        lineIndex++;
    }
    return 0;
}

void EdarWidget::openFlowFileDialog(){
    QString pathFileName =
            QFileDialog::getOpenFileName(this,
                                         tr("Import flow profile"), "",
                                         tr("Flow profile files (*.csv)"));

    QString fileName = QFileInfo(pathFileName).fileName();
    QFile file(pathFileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        std::string str = stream.readAll().toStdString();
        if (!parseFlowFile(str)){
            QString dst = Globals::instance()->getAppProjectPath() + "/flow_profiles/" + fileName;
            QFile::copy(pathFileName, dst);
            updateFlowProfilesList();
            flow_profiles->setCurrentText(fileName);
        }
        file.close();
    }
}

void EdarWidget::updateFlowProfilesList(){
    QDir path(Globals::instance()->getAppProjectPath() + "/flow_profiles");
    QStringList files = path.entryList(QDir::Files);
    flow_profiles->clear();
    flow_profiles->addItems(files);
}

void EdarWidget::createFilterFields(){
    flow_layout->addWidget(new QLabel("Filter configuration:"));

    func1 = new QComboBox();
    val1 = new QLineEdit();
    func2 = new QComboBox();
    val2 = new QLineEdit();
    //func3 = new QComboBox();
    //val3 = new QLineEdit();

    func1->setFixedWidth(175);
    val1->setFixedWidth(50);
    func2->setFixedWidth(175);
    val2->setFixedWidth(50);
    //func3->setFixedWidth(175);
    //val3->setFixedWidth(50);

    func1->addItem("No filter");
    func1->addItem("Moving Average");

    func2->addItem("No filter");
    func2->addItem("Moving Average");

    QHBoxLayout *filter1 = new QHBoxLayout();
    filter1->addWidget(new QLabel("1. "));
    filter1->addWidget(func1);
    filter1->addWidget(val1);
    QHBoxLayout *filter2 = new QHBoxLayout();
    filter2->addWidget(new QLabel("2. "));
    filter2->addWidget(func2);
    filter2->addWidget(val2);

    QVBoxLayout *filters = new QVBoxLayout();
    filters->addLayout(filter1);
    filters->addLayout(filter2);

    flow_layout->addLayout(filters);
}

void EdarWidget::movingAverage(int amplitud, int color){
    int len = flowValues->size();
    double *mm = new double [len]();
    double *sums = new double[len]();

    for (int i = (amplitud / 2); i < len - (amplitud / 2); i++) {
        for (int j = 0; j <= amplitud; j++) {
            sums[i] += flowValues->at(i + (amplitud / 2) - j);
        }
        mm[i] = sums[i] / (amplitud + 1);
    }

    for (int i = 0; i < (amplitud / 2); i++) {
        mm[i] = mm[amplitud / 2];
    }

    for (int i = len - (amplitud/2); i < len; i++) {
        mm[i] = mm[len - (amplitud / 2)-1];
    }

    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->GetColumn(0)->SetName("Samples");
    table->GetColumn(1)->SetName("Flow");

    table->SetNumberOfRows(len);

    for (int i=0; i < len; i++) {
        table->SetValue(i, 0, i);
        table->SetValue(i, 1, mm[i]);
    }

    //Adding another line to the chart
    vtkPlot *line = flowChart->AddPlot(vtkChart::LINE);
    line->SetInputData(table, 0, 1);
    line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
    line->SetColor(color, color, color, 255);
    line->SetWidth((color % 3) * 2);
}

void EdarWidget::checkFilters(){
    if (func2->currentIndex() == 1)
        movingAverage(val2->text().toInt(), 80);

    if (func1->currentIndex() == 1)
        movingAverage(val1->text().toInt(), 160);
}

void EdarWidget::getFlowValuesFromFile(){
    flowValues->clear();
    double sum=0;
    std::string str, fileLine;
    double value;
    QFile file(Globals::instance()->getAppProjectPath() + "/flow_profiles/" + flow_profiles->currentText());
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        str = stream.readAll().toStdString();
        std::istringstream f(str);
        while (std::getline(f, fileLine)) {
            value = QString::fromStdString(fileLine).toDouble();
            flowValues->append(value);
            sum += value;
        }
        file.close();
    }
    flowAverage = sum / flowValues->size();
}

void EdarWidget::removeOutliers(){
    double *stdDvt = new double [flowValues->size()];
    double sum=0, min, max;

    for (int i = 0; i < flowValues->size(); i++)
    {
        stdDvt[i] = pow((flowValues->at(i) - flowAverage),2);
        sum += stdDvt[i];
    }
    flowStd = sqrt(sum / flowValues->size());

    min = flowAverage - 1.96 * flowStd;
    max = flowAverage + 1.96 * flowStd;

    int w = 0;
    for (int i = 0; i < flowValues->size(); i++)
    {
        if (flowValues->at(i) >= min && flowValues->at(i) <= max) {
            flowValues->replace(i-w, flowValues->at(i));
        } else {
            w = w + 1;
        }
    }
}

void EdarWidget::showFlowChart(){
    getFlowValuesFromFile();

    if (checkRemoveOutliers->isChecked()){
        removeOutliers();
    }

    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->GetColumn(0)->SetName("Samples");
    table->GetColumn(1)->SetName("Flow");

    table->SetNumberOfRows(flowValues->size());

    for (int i=0; i < flowValues->size(); i++){
        table->SetValue(i, 0, i);
        table->SetValue(i, 1, flowValues->at(i));
    }

    vtkSmartPointer<vtkContextView> view = vtkSmartPointer<vtkContextView>::New();
    view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    flowChart = vtkSmartPointer<vtkChartXY>::New();
    view->GetScene()->AddItem(flowChart);
    vtkPlot *line = flowChart->AddPlot(vtkChart::LINE);
    line->SetWidth(1.0);
    line->SetColor(0, 255, 0, 255);
    line->SetInputData(table, 0, 1);

    flowChart->GetTitleProperties()->SetBold(true);
    flowChart->GetTitleProperties()->SetFontSize(20);
    flowChart->GetTitleProperties()->SetShadow(true);
    flowChart->SetAutoSize(false);
    flowChart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Samples");
    flowChart->GetAxis(vtkAxis::LEFT)->SetTitle("Flow");

    int *size = vtkViewer->GetRenderWindow()->GetSize();
    int margin = 75;
    flowChart->SetSize(vtkRectf(margin-50, margin-50, size[0]-margin, size[1]-margin));
    flowChart->SetTitle("Flow over the time");

    vtkSmartPointer<vtkContextScene> chartScene = vtkSmartPointer<vtkContextScene>::New();
    chartScene->AddItem(flowChart.GetPointer());
    settlingCurve->SetScene(chartScene.GetPointer());

    checkFilters();

    flowChart->Update();
    vtkViewer->add(settlingCurve);
    vtkViewer->update();
}

void EdarWidget::clearFlowLayout(){
    QLayoutItem *item, *item2;
    while((item = flow_layout->takeAt(0))) {
        if (item->layout()) {
            while((item2 = item->layout()->takeAt(0))) {
                if (item2->layout()) {
                    clearLayout(item2->layout());
                    delete item2->layout();
                }
            }
            delete item->layout();
        }
    }
}

void EdarWidget::onFlowProfileChanged(){
    clearFlowLayout();
    createFilterFields();
    showFlowChart();
    checkRemoveOutliers->setDisabled(false);
    btnApplyFilters->setDisabled(false);
}

void EdarWidget::showDataInputTranscient(QVBoxLayout *layout){
    flow_profiles = new QComboBox();
    flow_profiles->setStyleSheet(Globals::instance()->getCssSelect());

    updateFlowProfilesList();

    QLabel *flow_title = new QLabel("Flow profile:");
    QPushButton *flow_import = new QPushButton("Import flow from a file");
    flow_import->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(flow_import, SIGNAL(clicked(bool)), this, SLOT(openFlowFileDialog()));

    layout->addWidget(flow_title);
    layout->addWidget(flow_profiles);
    layout->addWidget(flow_import);
    layout->addSpacing(10);
}

void EdarWidget::showDataInputStationary(QVBoxLayout *layout){
    label_caudal = new QLabel("Flow (m3/h)");
    line_caudal = new QLineEdit("");
    label_solids = new QLabel("Solid (mg/l)");
    line_solids = new QLineEdit("");

    layout->addWidget(label_caudal);
    layout->addWidget(line_caudal);
    layout->addWidget(label_solids);
    layout->addWidget(line_solids);
}

void EdarWidget::changeFlowTab(){
    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(dataInputBaseLayout(flowTabbar->currentIndex()));
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

QLayout* EdarWidget::dataInputBaseLayout(int index){
    QFile sheetFile(":/Styles/QTabBar.css");
    sheetFile.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(sheetFile.readAll());

    flowTabbar = new QTabBar();
    flowTabbar->setStyleSheet(styleSheet);
    flowTabbar->addTab("Transcient");   //0
    flowTabbar->addTab("Stationary");   //1
    flowTabbar->setCurrentIndex(index);
    connect(flowTabbar, SIGNAL(currentChanged(int)), this, SLOT(changeFlowTab()));

    btnSaveDataInput = new QPushButton("Save", this);
    connect(btnSaveDataInput, SIGNAL(clicked()), this, SLOT(saveSettingsDataInput()));

    btnApplyFilters = new QPushButton("Update chart", this);
    btnApplyFilters->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btnApplyFilters, SIGNAL(clicked()), this, SLOT(showFlowChart()));

    flow_layout = new QVBoxLayout();
    QVBoxLayout *tab_layout = new QVBoxLayout();
    QHBoxLayout *outliersLayout = new QHBoxLayout();
    tab_layout->addWidget(flowTabbar);
    if(index==0){
        flowValues = new QList<double>;
        showDataInputTranscient(tab_layout);
        connect(flow_profiles, SIGNAL(currentTextChanged(QString)), this, SLOT(onFlowProfileChanged()));

        outliersLayout->addWidget(new QLabel("Filter outliers:"));
        checkRemoveOutliers = new QCheckBox();
        outliersLayout->addWidget(checkRemoveOutliers);
        outliersLayout->addStretch();

        if (flow_profiles->count()){
            createFilterFields();
        } else {
            checkRemoveOutliers->setDisabled(true);
            btnApplyFilters->setDisabled(true);
        }

    }else if(index==1){
        showDataInputStationary(tab_layout);
    }

    loadSettingsDataInput(index);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(tab_layout);
    layout->addSpacing(10);
    layout->addLayout(flow_layout);
    layout->addLayout(outliersLayout);
    layout->addWidget(btnApplyFilters);
    layout->addSpacing(20);
    layout->addWidget(btnSaveDataInput);
    layout->addStretch();

    return layout;
}

void EdarWidget::dataInputProperties(){
    QString dirname = Globals::instance()->getAppProjectPath() + "/flow_profiles/";
    if (!QDir(dirname).exists())
        QDir().mkdir(dirname);

    setImageInViewer(0);

    //We need to free the memory of these arrays before deleting the sludge_layout
    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    int index = settings->value("DataInput/type", 0).toInt();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(dataInputBaseLayout(index));
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void EdarWidget::toolBoxChanged()
{
    int index = designInfo->currentIndex();
    char *filepath;

    if (index == 0){ //General
        filepath = strdup(":/Resources/General.png");
        //designInfo->setMinimumHeight(600);

    } else if (index == 1) { //Input
        filepath = strdup(":/Resources/InputMixer.png");
        //designInfo->setMinimumHeight(500);

    } else if(index == 2){ //Feed
        filepath = strdup(":/Resources/FeedWell.png");
        //designInfo->setMinimumHeight(250);

    } else if (index == 3){ // Scraper
        filepath = strdup(":/Resources/Scrapers.png");
        //designInfo->setMinimumHeight(400);
    }

    setImageInViewer(1, filepath);
}

void EdarWidget::designProperties(){    
    designInfo = new QToolBox();
    designInfo->addItem(getInfoDesignGeneralTab(), "GENERAL");
    designInfo->addItem(getInfoDesignInputMixedLiquorTab(), "INPUT MIXED LIQUOR");
    designInfo->addItem(getInfoDesignFeedwellTab(), "FEED WELL");
    designInfo->addItem(getInfoDesignScraperTab(), "SCRAPER");
    designInfo->setStyleSheet(Globals::instance()->getCssToolBox());

    connect(designInfo,SIGNAL(currentChanged(int)),this,SLOT(toolBoxChanged()));
    loadSettingsDesign();
    btnSaveDesignGeneral = new QPushButton("Save", this);
    connect(btnSaveDesignGeneral, SIGNAL(clicked()), this, SLOT(saveSettingsDesign()));

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(designInfo);
    layout->addWidget(btnSaveDesignGeneral);
    //QSpacerItem * my_spacer = new QSpacerItem(0,2000, QSizePolicy::Expanding, QSizePolicy::Expanding);
    //layout->addWidget(designInfo, 0 , 0, Qt::AlignTop);
    //layout->addItem(my_spacer, 1, 0, Qt::AlignTop);
    //layout->addWidget(btnSaveDesignGeneral, 2, 0, Qt::AlignTop);
    layout->addStretch();

    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    toolBoxChanged();
}

QWidget* EdarWidget::getInfoDesignFeedwellTab(){
    BoxLineField *box_inner_feedwell = new BoxLineField(18, "Diameter Inner", &line_diameter_inner_feedwell);
    BoxLineField *box_height_feedwell = new BoxLineField(-8, "Height", &line_height_feedwell);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(box_inner_feedwell);
    layout->addLayout(box_height_feedwell);
    layout->addStretch();

    QWidget* feedwellTab = new QWidget();
    feedwellTab->setLayout(layout);

    return feedwellTab;
}

void EdarWidget::loadSettingsDesign(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_diameter_clarifier->setText(settings->value("Design/diameter_clarifier", "28").toString());
    line_diameter_liquor->setText(settings->value("Design/diameter_liquor", "0.5").toString());
    line_diameter_concrete->setText(settings->value("Design/diameter_concrete", "1.2").toString());
    line_diameter_upper_sludge->setText(settings->value("Design/diameter_upper_sludge", "3.3").toString());
    line_diameter_base_sludge->setText(settings->value("Design/diameter_base_sludge", "3.3").toString());
    line_draught->setText(settings->value("Design/draught", "3.6").toString());
    line_height_sludge->setText(settings->value("Design/height_sludge", "0.85").toString());
    line_height_well->setText(settings->value("Design/height_well", "0.73").toString());
    line_length_bridge->setText(settings->value("Design/length_bridge", "17.20").toString());
    line_length_scrapers->setText(settings->value("Design/length_scrapers", "12.56").toString());
    line_width_inner_wall->setText(settings->value("Design/width_inner_wall", "0.30").toString());
    line_width_sink->setText(settings->value("Design/width_sink", "0.60").toString());
    line_width_drain->setText(settings->value("Design/width_drain", "0.60").toString());
    line_height_slab->setText(settings->value("Design/height_slab", "1.45").toString());
    line_height_inner_wall->setText(settings->value("Design/height_inner_wall", "0.30").toString());
    line_height_outer_wall->setText(settings->value("Design/height_outer_wall", "0.30").toString());

    line_windows->setText(settings->value("Liquor/windows", "4").toString());
    line_height_window->setText(settings->value("Liquor/height_window", "1.08").toString());
    line_width_inner_window->setText(settings->value("Liquor/width_inner_window", "0.23").toString());
    line_width_outer_window->setText(settings->value("Liquor/width_outer_window", "0.54").toString());
    line_thickness_window->setText(settings->value("Liquor/thickness_window", "0.35").toString());
    line_geometry_window->setCurrentText(settings->value("Liquor/geometry_window", "Trapezoidal").toString());
    line_deflector_window->setChecked(settings->value("Liquor/deflector_window", true).toBool());

    line_type->setCurrentText(settings->value("Scraper/type", "Rectas").toString());
    line_form->setCurrentText(settings->value("Scraper/form", "C").toString());
    line_angle->setText(settings->value("Scraper/angle", "0").toString());

    line_diameter_inner_feedwell->setText(settings->value("Feedwell/diameter_inner", "4.8").toString());
    line_height_feedwell->setText(settings->value("Feedwell/height", "1.53").toString());
    //line_diameter_min_feedwell->setText(settings->value("Feedwell/diameter_min", "5.6").toString());
    //line_height_min_feedwell->setText(settings->value("Feedwell/height_min", "1.53").toString());
    //line_diameter_max_feedwell->setText(settings->value("Feedwell/diameter_max", "8.4").toString());
}

void EdarWidget::saveSettingsDesign(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("Design/diameter_clarifier", line_diameter_clarifier->text());
    settings->setValue("Design/diameter_liquor", line_diameter_liquor->text());
    settings->setValue("Design/diameter_concrete", line_diameter_concrete->text());
    settings->setValue("Design/diameter_upper_sludge", line_diameter_upper_sludge->text());
    settings->setValue("Design/diameter_base_sludge", line_diameter_base_sludge->text());
    settings->setValue("Design/diameter_draught", line_draught->text());
    settings->setValue("Design/height_sludge", line_height_sludge->text());
    settings->setValue("Design/height_well", line_height_well->text());
    settings->setValue("Design/length_bridge", line_length_bridge->text());
    settings->setValue("Design/length_scrapers", line_length_scrapers->text());
    settings->setValue("Design/width_inner_wall", line_width_inner_wall->text());
    settings->setValue("Design/width_sink", line_width_sink->text());
    settings->setValue("Design/width_drain", line_width_drain->text());
    settings->setValue("Design/height_inner_wall", line_height_inner_wall->text());
    settings->setValue("Design/height_slab", line_height_slab->text());
    settings->setValue("Design/height_outer_wall", line_height_outer_wall->text());

    settings->setValue("Liquor/windows", line_windows->text());
    settings->setValue("Liquor/height_window", line_height_window->text());
    settings->setValue("Liquor/width_inner_window", line_width_inner_window->text());
    settings->setValue("Liquor/width_outer_window", line_width_outer_window->text());
    settings->setValue("Liquor/thickness_window", line_thickness_window->text());
    settings->setValue("Liquor/geometry_window", line_geometry_window->currentText());
    settings->setValue("Liquor/deflector_window", line_deflector_window->isChecked());

    settings->setValue("Scraper/type", line_type->currentText());
    settings->setValue("Scraper/form", line_form->currentText());
    settings->setValue("Scraper/angle", line_angle->text());

    settings->setValue("Feedwell/diameter_inner", line_diameter_inner_feedwell->text());
    settings->setValue("Feedwell/height", line_height_feedwell->text());
    //settings->setValue("Feedwell/diameter_min", line_diameter_min_feedwell->text());
    //settings->setValue("Feedwell/height_min", line_height_min_feedwell->text());
    //settings->setValue("Feedwell/diameter_max", line_diameter_max_feedwell->text());

    settings->sync();
}

void EdarWidget::loadSettingsSettling(int index){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    if(index == 0){
        sludge_profiles->setCurrentText(settings->value("Settling/sludge_file").toString());
        vesilind_v0 = settings->value("Settling/v0", 0).toDouble();
        vesilind_rh = settings->value("Settling/rh", 0).toDouble();
    } else {
        line_sedimentation_model->setCurrentIndex(settings->value("model", "0").toInt());
        line_v0->setText(settings->value("Settling/v0", "0.001908").toString());
        line_rh->setText(settings->value("Settling/rh", "1.899").toString());
        line_rt->setText(settings->value("Settling/rt", "5").toString());
        line_weight_max->setText(settings->value("Settling/wmax", "8.4").toString());
        line_weight_min->setText(settings->value("Settling/wmin", "8.4").toString());

        //onSettlingTypeInputTextChanged();
        //updateSliderV0();
        //updateSliderRh();

    }
}

void EdarWidget::saveSettingsSettling(){

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    int index = tabbar->currentIndex();
    settings->setValue("Settling/mode", index);

    if (index == 0) { //experimental
        settings->setValue("Settling/sludge_file", sludge_profiles->currentText());
        if (lineArraysCreated) {
            int n = concentrationList->size();
            settings->setValue("Settling/concentrations", n);
            for (int i = 0; i < n; i++){
                settings->setValue("Settling/lower_c" + QString::number(i), adjustmentLower[i]);
                settings->setValue("Settling/upper_c" + QString::number(i), adjustmentUpper[i]);
            }
            settings->setValue("Settling/v0", QString::number(vesilind_v0, 'f', 6));
            settings->setValue("Settling/rh", QString::number(vesilind_rh, 'f', 6));
        }

    } else if (index == 1) { //manual
        settings->setValue("Settling/model", line_sedimentation_model->currentIndex());
        settings->setValue("Settling/v0", line_v0->text());
        settings->setValue("Settling/rh", line_rh->text());
        settings->setValue("Settling/rt", line_rt->text());
        settings->setValue("Settling/wmax", line_weight_max->text());
        settings->setValue("Settling/wmin", line_weight_min->text());
    }
    //settings->sync();
}

QWidget* EdarWidget::getInfoDesignGeneralTab(){
    BoxLineField *box_clarifier = new BoxLineField(1, "Clarifier diameter", &line_diameter_clarifier);
    BoxLineField *box_liquor = new BoxLineField(3, "Diameter Input Mixed Liquor", &line_diameter_liquor);
    BoxLineField *box_concrete = new BoxLineField(-4, "Diameter Concrete Support", &line_diameter_concrete);
    BoxLineField *box_upper_sludge = new BoxLineField(-5, "Diameter Upper Sludge", &line_diameter_upper_sludge);
    BoxLineField *box_base_sludge = new BoxLineField(4, "Diameter Base Sludge", &line_diameter_base_sludge);
    BoxLineField *box_draught = new BoxLineField(5, "Draught", &line_draught);
    BoxLineField *box_sludge = new BoxLineField(-9, "Start Height Sludge Pond", &line_height_sludge);
    BoxLineField *box_well_height = new BoxLineField(-10, "Well Height", &line_height_well);
    BoxLineField *box_width_inner_wall = new BoxLineField(0, "B.Width Inner Wall Landfill", &line_width_inner_wall);
    BoxLineField *box_sink_channel = new BoxLineField(0, "C.Width Sink Channel Space Landfill", &line_width_sink);
    BoxLineField *box_drain_channel = new BoxLineField(0, "D.Width Drain Channel Space Landfill", &line_width_drain);
    BoxLineField *box_slab_channel = new BoxLineField(0, "F.Width Slab Channel Space Landfill", &line_height_slab);
    BoxLineField *box_height_outer_wall = new BoxLineField(0, "G.Height Outer Wall Weir", &line_height_outer_wall);
    BoxLineField *box_height_inner_wall = new BoxLineField(0, "H.Height Inner Wall Weir", &line_height_inner_wall);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(box_clarifier);
    layout->addLayout(box_liquor);
    layout->addLayout(box_concrete);
    layout->addLayout(box_upper_sludge);
    layout->addLayout(box_base_sludge);
    layout->addLayout(box_draught);
    layout->addLayout(box_sludge);
    layout->addLayout(box_well_height);
    layout->addLayout(box_width_inner_wall);
    layout->addLayout(box_sink_channel);
    layout->addLayout(box_drain_channel);
    layout->addLayout(box_slab_channel);
    layout->addLayout(box_height_outer_wall);
    layout->addLayout(box_height_inner_wall);
    layout->addStretch();

    QWidget* generalTab = new QWidget();
    generalTab->setLayout(layout);

    return generalTab;
}

QWidget* EdarWidget::getInfoDesignInputMixedLiquorTab(){
    BoxLineField *box_windows = new BoxLineField(-11, "Number of Windows", &line_windows);
    BoxLineField *box_height_window = new BoxLineField(-12, "Height Window", &line_height_window);
    BoxLineField *box_width_inner_window = new BoxLineField(-13, "Width Inner Window", &line_width_inner_window);
    BoxLineField *box_width_outer_window = new BoxLineField(-14, "Width Outer Window", &line_width_outer_window);
    BoxLineField *box_thickness_window = new BoxLineField(-15, "Thickness Window", &line_thickness_window);

    line_geometry_window= new QComboBox();
    line_geometry_window->addItem("Cilíndrica");
    line_geometry_window->addItem("Cuadrada");
    line_geometry_window->addItem("Trapezoidal");
    line_geometry_window->addItem("Otro");
    line_deflector_window= new QCheckBox();

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(box_windows);
    layout->addLayout(box_height_window);
    layout->addLayout(box_width_inner_window);
    layout->addLayout(box_width_outer_window);
    layout->addLayout(box_thickness_window);
    layout->addWidget(new QLabel("16.Geometry Window"));
    layout->addWidget(line_geometry_window);
    layout->addWidget(new QLabel("17.Deflectors on Window"));
    layout->addWidget(line_deflector_window);
    layout->addStretch();

    QWidget* inputMixedLiquorTab = new QWidget();
    inputMixedLiquorTab->setLayout(layout);
    inputMixedLiquorTab->setStyleSheet(Globals::instance()->getCssScrollBar());

    return inputMixedLiquorTab;
}

QWidget* EdarWidget::getInfoDesignScraperTab(){
    line_type = new QComboBox();
    line_type->addItem("Curvadas");
    line_type->addItem("Rectas");
    line_form = new QComboBox();
    line_form->addItem("A");
    line_form->addItem("B");
    line_form->addItem("C");
    line_form->addItem("D");

    BoxLineField *box_bridge = new BoxLineField(1, "Length Bridge", &line_length_bridge);
    BoxLineField *box_scraper = new BoxLineField(3, "Length Scrapers", &line_length_scrapers);
    BoxLineField *box_angle = new BoxLineField(-26, "Angle", &line_angle);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(new QLabel("24.Type"));
    layout->addWidget(line_type);
    layout->addWidget(new QLabel("25.Form"));
    layout->addWidget(line_form);
    layout->addLayout(box_angle);
    layout->addLayout(box_bridge);
    layout->addLayout(box_scraper);
    layout->addStretch();

    QWidget* designScraperTab = new QWidget();
    designScraperTab->setLayout(layout);

    return designScraperTab;
}

void EdarWidget::loadSettingsRheology(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_rheology_type->setCurrentText(settings->value("Rheology/type", "Bingham").toString());
    line_tau->setText(settings->value("Rheology/tau", "0.0022").toString());
    line_k->setText(settings->value("Rheology/k", "2.0").toString());
    line_n->setText(settings->value("Rheology/n", "5.0").toString());
}

void EdarWidget::saveSettingsRheology(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("Rheology/type", line_rheology_type->currentText());
    settings->setValue("Rheology/tau", line_tau->text());
    settings->setValue("Rheology/k", line_k->text());
    settings->setValue("Rheology/n", line_n->text());
}

void EdarWidget::rheologyProperties(){
    label_rheology_type  = new QLabel("Type");
    line_rheology_type = new QComboBox();
    line_rheology_type->addItem("Bingham");
    line_rheology_type->addItem("Herschel-Buckley");

    label_tau = new QLabel("tau");
    line_tau = new QLineEdit("");
    label_k = new QLabel("k");
    line_k = new QLineEdit("");
    label_n = new QLabel("n");
    line_n = new QLineEdit("");

    QPushButton* btnSaveRheology = new QPushButton("Save", this);
    connect(btnSaveRheology, SIGNAL(clicked()), this, SLOT(saveSettingsRheology()));

    loadSettingsRheology();

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(label_rheology_type);
    layout->addWidget(line_rheology_type);
    layout->addWidget(label_tau);
    layout->addWidget(line_tau);
    layout->addWidget(label_k);
    layout->addWidget(line_k);
    layout->addWidget(label_n);
    layout->addWidget(line_n);
    layout->addWidget(btnSaveRheology);
    layout->addStretch();

    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    //areaActionWidget->setMaximumHeight(250);
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    setImageInViewer(0);
}

void EdarWidget::onSettlingTypeInputTextChanged() {
    line_rt->setDisabled((line_sedimentation_model->currentText() == "Vesilind"));
    /*
        line_rt->setDisabled((true));
    QString s_model = centralWidget->findChild<QComboBox*>("cb_vsedimentation_model")->currentText();
    bool check = true;
    if(s_model != "Vesilind")
        check = false;
    //qDebug()  << "Set type: " << check <<endl;
    centralWidget->findChild<QLineEdit*>("i_rt")->setDisabled(check);
*/
}

void EdarWidget::showSettlingsManual(QVBoxLayout *layout){
    line_sedimentation_model  = new QComboBox(this);
    label_sedimentation_model= new QLabel("Type (m/s)");
    line_sedimentation_model->addItem("Vesilind");
    line_sedimentation_model->addItem("Takács");
    //QObject::connect(line_sedimentation_model, SIGNAL(currentIndexChanged(int)), this, SLOT(onSettlingTypeInputTextChanged()));
    box_v0 = new BoxLineField("v0 (m/s)", &line_v0);
    box_rh = new BoxLineField("rh (kg/m3)", &line_rh);
    line_rt = new QLineEdit;
    label_rt= new QLabel("rt (kg/m3)");
    line_weight_max = new QLineEdit;
    label_weight_max = new QLabel("Max. Weight (mg/l)");
    line_weight_min = new QLineEdit;
    label_weight_min = new QLabel("Min. Weight (mg/l)");

    slider_v0 = new QSlider(Qt::Horizontal);
    slider_v0->setRange(0, 1000);
    connect(slider_v0, SIGNAL(sliderReleased()),this, SLOT(onSliderV0Stop()));
    connect(line_v0, SIGNAL(textChanged(QString)), this, SLOT(updateSliderV0()));

    slider_rh = new QSlider(Qt::Horizontal);
    slider_rh->setRange(0, 1000);
    connect(slider_rh, SIGNAL(sliderReleased()),this, SLOT(onSliderRhStop()));
    connect(line_rh, SIGNAL(textChanged(QString)), this, SLOT(updateSliderRh()));

    layout->addWidget(label_sedimentation_model);
    layout->addWidget(line_sedimentation_model);
    layout->addLayout(box_v0);
    layout->addWidget(slider_v0);
    layout->addLayout(box_rh);
    layout->addWidget(slider_rh);
    //layout->addWidget(label_rt);
    //layout->addWidget(line_rt);
    //layout->addWidget(label_weight_max);
    //layout->addWidget(line_weight_max);
    //layout->addWidget(label_weight_min);
    //layout->addWidget(line_weight_min);
}

void EdarWidget::changeSludgeFile(){
    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    clearLayout(sludge_layout);
    btn_show_tabs->setDisabled(false);
}

void EdarWidget::showSettlingsExperimental(QVBoxLayout *layout){
    sludge_profiles = new QComboBox();
    sludge_profiles->setStyleSheet(Globals::instance()->getCssSelect());

    updateSludgeProfilesList();

    QLabel *sludge_title = new QLabel("Sludge profile:");
    QPushButton *sludge_import = new QPushButton("Import sludge properties from a file");
    sludge_import->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(sludge_import, SIGNAL(clicked(bool)), this, SLOT(openFileDialog()));
    QPushButton *sludge_new = new QPushButton("Create a new sludge profile");
    sludge_new->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(sludge_new, SIGNAL(clicked(bool)), this, SLOT(openCreateWindow()));
    QPushButton *sludge_edit = new QPushButton("Edit sludge profile");
    sludge_edit->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(sludge_edit, SIGNAL(clicked(bool)), this, SLOT(openEditWindow()));

    btn_show_tabs = new QPushButton("Start with the adjustment");
    btn_show_tabs->setDisabled(true);
    btn_show_tabs->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btn_show_tabs, SIGNAL(clicked(bool)), this, SLOT(createConcentrationTabs()));
    if (sludge_profiles->count() > 0)
        btn_show_tabs->setDisabled(false);

    layout->addWidget(sludge_title);
    layout->addWidget(sludge_profiles);
    layout->addWidget(sludge_import);
    layout->addWidget(sludge_new);
    layout->addWidget(sludge_edit);
    layout->addSpacing(10);
    layout->addWidget(btn_show_tabs);
}

void EdarWidget::changeSettlingsTab(){
    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(settlingBaseLayout(tabbar->currentIndex()));
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void EdarWidget::updateSludgeProfilesList(){
    QDir path(Globals::instance()->getAppProjectPath() + "/sludge_profiles");
    QStringList files = path.entryList(QDir::Files);
    sludge_profiles->clear();
    sludge_profiles->addItems(files);
}

QLayout* EdarWidget::settlingBaseLayout(int index){
    QFile sheetFile(":/Styles/QTabBar.css");
    sheetFile.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(sheetFile.readAll());

    tabbar = new QTabBar();
    tabbar->setStyleSheet(styleSheet);
    tabbar->addTab("Experimental");//0
    tabbar->addTab("Manual");   //1
    tabbar->setCurrentIndex(index);
    connect(tabbar, SIGNAL(currentChanged(int)), this, SLOT(changeSettlingsTab()));

    btnSaveSettling = new QPushButton("Save", this);
    connect(btnSaveSettling, SIGNAL(clicked()), this, SLOT(saveSettingsSettling()));

    sludge_layout = new QVBoxLayout();
    QVBoxLayout *tab_layout = new QVBoxLayout();
    tab_layout->addWidget(tabbar);
    if(index==0){
        showSettlingsExperimental(tab_layout);
        loadSettingsSettling(index);
        connect(sludge_profiles, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSludgeFile()));
    }else if(index==1){
        showSettlingsManual(tab_layout);
        loadSettingsSettling(index);
    }

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(tab_layout);
    layout->addSpacing(10);
    layout->addLayout(sludge_layout);
    layout->addSpacing(10);
    layout->addWidget(btnSaveSettling);
    layout->addStretch();

    return layout;
}

void EdarWidget::settlingProperties(){
    QString dirname = Globals::instance()->getAppProjectPath() + "/sludge_profiles/";
    if (!QDir(dirname).exists())
        QDir().mkdir(dirname);

    setImageInViewer(0);

    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    int index = settings->value("Settling/mode", 0).toInt();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(settlingBaseLayout(index));
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void EdarWidget::openEditWindow(){
    editorWindow = new QWidget();
    editor = new FileEditor();
    sludge_filename = new QLineEdit();
    sludge_filename->setText(sludge_profiles->currentText().split(".")[0]);
    QPushButton *btnSave = new QPushButton("Save", this);
    connect(btnSave, SIGNAL(clicked()), this, SLOT(saveSludge()));

    QString filename = Globals::instance()->getAppProjectPath() + "/sludge_profiles/" + sludge_profiles->currentText();
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        while (!stream.atEnd()) {
            QString line = stream.readLine();
            if(!stream.atEnd())
                editor->insertPlainText(line + "\n");
            else
                editor->insertPlainText(line);
        }
        file.close();
    } else {
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Error opening the file: " << filename << endl;
    }

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(new QLabel("Example:\nConcentration 1, Concetration 2, Concentration 3\ntime1,height1,time2,height2,time3,height3\n..."));
    layout->addWidget(editor);
    layout->addWidget(new QLabel("Sludge profile name:"));
    layout->addWidget(sludge_filename);
    layout->addWidget(btnSave);

    editorWindow->setLayout(layout);
    editorWindow->show();
}

void EdarWidget::openCreateWindow(){
    editorWindow = new QWidget();
    editor = new FileEditor();
    sludge_filename = new QLineEdit();
    QPushButton *btnSave = new QPushButton("Save", this);
    connect(btnSave, SIGNAL(clicked()), this, SLOT(saveSludge()));

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(new QLabel("Format data (comma separated): 1st row are headers; rest of rows are samples.\nFor example:\nConcentration 1, Concetration 2, Concentration 3\ntime1-height1,time2-height2,time3-height3\ntime1-height1,time2-height2,time3-height3\ntime1-height1,time2-height2,time3-height3\n..."));
    //layout->addWidget(new QLabel("Insert data one sample per line and separated by a semicolon.\nFor example:\n1;0.333\n2;0.435\n..."));
    layout->addWidget(editor);
    layout->addWidget(new QLabel("Sludge profile name (without extension):"));
    layout->addWidget(sludge_filename);
    layout->addWidget(btnSave);

    editorWindow->setLayout(layout);
    editorWindow->show();
}


void EdarWidget::showAllConcentrationsChart(){
    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->GetColumn(0)->SetName("Time");

    //Get the longest vector
    int maxTime = 0;
    std::string s = "Concentration: ";
    for (int i = 0; i < concentrationList->size(); i++) {
        table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
        table->GetColumn(i+1)->SetName((s + std::to_string(i+1)).c_str());
        if (concentrationTimes->at(i)->last() > maxTime)
            maxTime = concentrationTimes->at(i)->last();
    }
    maxTime += 1;
    table->SetNumberOfRows(maxTime);

    //Initilize the values to cero
    for(int i = 0; i < maxTime; i++) {
        table->SetValue(i, 0, i);
        for (int j = 0; j < concentrationList->size(); j++) {
            table->SetValue(i, j+1, 0);
        }
    }

    //Fill the table with the data
    double height;
    int nSamples, time;
    for (int i = 0; i < concentrationList->size(); i++) {
        nSamples = concentrationTimes->at(i)->size();
        for (int sample = 0; sample < nSamples; sample++) {
            time = concentrationTimes->at(i)->at(sample);
            height = concentrationValues->at(i)->at(sample);
            table->SetValue(time, i+1, height);
        }
    }

    vtkSmartPointer<vtkContextView> view = vtkSmartPointer<vtkContextView>::New();
    view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    vtkSmartPointer<vtkChartXY> chart = vtkSmartPointer<vtkChartXY>::New();
    view->GetScene()->AddItem(chart);
    vtkPlot *line;
    //line->SetInputData(table, 0, 1);

    //line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
    //line->SetColor(7,60,60,255);
    //line->SetWidth(3.0);

    for (int i = 1; i <= concentrationList->size(); i++){
        //Adding another line to the chart
        line = chart->AddPlot(vtkChart::POINTS);
        line->SetInputData(table, 0, i);
        //line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
        line->SetColor(0, (i*50)%255, 0);
        vtkPlotPoints::SafeDownCast(line)->SetMarkerStyle((i%4)+1);
        //NONE,CROSS,PLUS,SQUARE,CIRCLE,DIAMOND
        //line->SetWidth(3.0);
    }

    chart->GetTitleProperties()->SetBold(true);
    chart->GetTitleProperties()->SetFontSize(20);
    chart->GetTitleProperties()->SetShadow(true);
    chart->SetAutoSize(false);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Time");
    chart->GetAxis(vtkAxis::LEFT)->SetTitle("Height");
    chart->SetShowLegend(true);

    int *size = vtkViewer->GetRenderWindow()->GetSize();
    int margin = 100;
    chart->SetSize(vtkRectf(margin-50, margin-50, size[0]-margin*2, size[1]-margin*2));
    chart->SetTitle("Settling curve");

    vtkSmartPointer<vtkContextScene> chartScene = vtkSmartPointer<vtkContextScene>::New();
    chartScene->AddItem(chart.GetPointer());
    settlingCurve->SetScene(chartScene.GetPointer());

    chart->Update();
    vtkViewer->add(settlingCurve);
    vtkViewer->update();
}

void EdarWidget::showConcentrationChart(){
    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table->GetColumn(0)->SetName("Time");
    table->GetColumn(1)->SetName("Height");

    int concentrationIndex = concentrationTabs->currentIndex();

    int time;
    double height;
    int nSeconds = (concentrationTimes->at(concentrationIndex)->last())+1;
    table->SetNumberOfRows(nSeconds);
    int nSamples = concentrationTimes->at(concentrationIndex)->size();

    for(int i = 0; i < nSeconds; i++) {
        table->SetValue(i, 0, i);
        table->SetValue(i, 1, 0);
    }

    for (int sample = 0; sample < nSamples; sample++) {
        time = concentrationTimes->at(concentrationIndex)->at(sample);
        height = concentrationValues->at(concentrationIndex)->at(sample);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__
        //         << " - " << time
        //         << " - " << height;
        table->SetValue(time, 1, height);
    }

    auto table2 = vtkSmartPointer<vtkTable>::New();
    table2->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table2->AddColumn(vtkSmartPointer<vtkDoubleArray>::New());
    table2->GetColumn(0)->SetName("X");
    table2->GetColumn(1)->SetName("Y");
    //int ini = concentrationTimes->at(concentrationTabs->currentIndex())->first();
    //int fin = concentrationTimes->at(concentrationTabs->currentIndex())->last();
    int ini = adjustmentLower[concentrationIndex];
    int fin = adjustmentUpper[concentrationIndex];
    int cntRows = 0;
    for (int sample = 0; sample < nSamples; sample++) {
        int time = concentrationTimes->at(concentrationIndex)->at(sample);
        if (time >= ini && time <= fin){
            cntRows++;
        }
    }

    table2->SetNumberOfRows(cntRows);
    double m=linearRegression[concentrationIndex];
    double b = interceptArray[concentrationIndex];
    for (int i = 0, sample = 0; sample < nSamples; sample++) {
        time = concentrationTimes->at(concentrationIndex)->at(sample);
        if (time >= ini && time <= fin){
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << time << " - " ;
            table2->SetValue(i, 0, time);
            table2->SetValue(i, 1, (m*time + b));
            i++;
        }
    }

    //***********************************************************************************************

    vtkSmartPointer<vtkContextView> view = vtkSmartPointer<vtkContextView>::New();
    view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    vtkSmartPointer<vtkChartXY> chart = vtkSmartPointer<vtkChartXY>::New();
    view->GetScene()->AddItem(chart);
    vtkPlot *line = chart->AddPlot(vtkChart::POINTS);
    line->SetInputData(table, 0, 1);

    //line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
    //line->SetColor(7,60,60,255);
    //line->SetWidth(3.0);

    //Adding another line to the chart
    line = chart->AddPlot(vtkChart::LINE);
    line->SetInputData(table2, 0, 1);
    line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
    line->SetColor(0, 255, 0, 255);
    line->SetWidth(6.0);

    chart->GetTitleProperties()->SetBold(true);
    chart->GetTitleProperties()->SetFontSize(20);
    chart->GetTitleProperties()->SetShadow(true);
    chart->SetAutoSize(false);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Time");
    chart->GetAxis(vtkAxis::LEFT)->SetTitle("Height");

    int *size = vtkViewer->GetRenderWindow()->GetSize();
    int margin = 100;
    chart->SetSize(vtkRectf(margin-50, margin-50, size[0]-margin*2, size[1]-margin*2));
    chart->SetTitle("Settling curve");

    vtkSmartPointer<vtkContextScene> chartScene = vtkSmartPointer<vtkContextScene>::New();
    chartScene->AddItem(chart.GetPointer());
    settlingCurve->SetScene(chartScene.GetPointer());

    chart->Update();
    vtkViewer->add(settlingCurve);
    vtkViewer->update();
}

void EdarWidget::concentrationTabChanged(){
    showConcentrationChart();
}

double slope(const std::vector<int>& x, const std::vector<double>& y, double *intercept) {
    const auto n    = x.size();
    const auto s_x  = std::accumulate(x.begin(), x.end(), 0.0);
    const auto s_y  = std::accumulate(y.begin(), y.end(), 0.0);
    const auto s_xx = std::inner_product(x.begin(), x.end(), x.begin(), 0.0);
    const auto s_xy = std::inner_product(x.begin(), x.end(), y.begin(), 0.0);
    const auto slope    = (n * s_xy - s_x * s_y) / (n * s_xx - s_x * s_x);
    *intercept = (s_y - slope * s_x) / n;
    return slope;
}

void vesilind(QList<double> *SST_exp, double *fit, double *v0, double *rh)
{
    double xsum = 0, x2sum = 0, ysum = 0, xysum = 0;    //variables for sums/sigma of xi,yi,xi^2,xiyi etc

    for (int i = 0; i<SST_exp->size(); i++) {
        xsum += SST_exp->at(i);                 //calculate sigma(xi)
        ysum += log(abs(fit[i]));                    //calculate sigma(yi)
        x2sum += pow(SST_exp->at(i), 2);        //calculate sigma(x^2i)
        xysum += SST_exp->at(i) * log(abs(fit[i]));  //calculate sigma(xi*yi)
    }

    *rh = (SST_exp->size()*xysum - xsum*ysum) / (SST_exp->size()*x2sum - xsum*xsum);    //calculate slope(or the the power of exp)
    double b = (x2sum*ysum - xsum*xysum) / (x2sum*SST_exp->size() - xsum*xsum);         //calculate intercept
    *v0 = pow(2.71828, b);                                                              //since b=ln(c)
}

void EdarWidget::getLinearRegresion(int index){
    int iniTime = lineLowerArray[index].text().toInt();
    int finTime = lineUpperArray[index].text().toInt();
    int iniIndex=0;
    int finIndex=(concentrationTimes->at(index)->size())-1;

    if (finTime <= iniTime){
        return;
    }
    int i, currTime=0, prevTime=-1;
    for (i = 1; i < concentrationTimes->at(index)->size(); i++){
        currTime = concentrationTimes->at(index)->at(i);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << iniTime << " - " << currTime;
        if (currTime > iniTime){
            prevTime = concentrationTimes->at(index)->at(i-1);
            iniTime = prevTime;
            iniIndex = i-1;
            break;
        }
    }
    for (; i < concentrationTimes->at(index)->size(); i++){
        currTime = concentrationTimes->at(index)->at(i);
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << finTime << " - " << currTime;
        if (currTime > finTime){
            prevTime = concentrationTimes->at(index)->at(i-1);
            finTime = prevTime;
            finIndex = i-1;
            break;
        }
    }

    adjustmentLower[index]=iniTime;
    adjustmentUpper[index]=finTime;

    std::vector<int> timesVec;
    std::vector<double> heightsVec;
    for(int i = iniIndex; i <= finIndex; i++) {
        timesVec.push_back(concentrationTimes->at(index)->toVector().toStdVector()[i]);
        heightsVec.push_back(concentrationValues->at(index)->toVector().toStdVector()[i]);
    }

    double intercept;
    double res = slope(timesVec, heightsVec, &intercept);
    linearRegression[index] = res;
    interceptArray[index] = intercept;

    //for(auto const& value: timesVec)
    //    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << value;
}

void EdarWidget::updateConcentrationTab(){

    getLinearRegresion(concentrationTabs->currentIndex());

    vesilind(concentrationList, linearRegression, &vesilind_v0, &vesilind_rh);
    label_v0->setText("Vesilind:\n\tv0 (m/s): " + QString::number(vesilind_v0));
    label_rh->setText("\trh (kg/m3): "  + QString::number(vesilind_rh));

    showConcentrationChart();
}

QWidget *EdarWidget::showConcentrationTab(int index){
    lineLowerArray[index].setText(QString::number(adjustmentLower[index]));
    lineUpperArray[index].setText(QString::number(adjustmentUpper[index]));

    double intercept;
    double res = slope(concentrationTimes->at(index)->toVector().toStdVector(), concentrationValues->at(index)->toVector().toStdVector(), &intercept);
    linearRegression[index] = res;
    interceptArray[index] = intercept;

    QVBoxLayout *lowerLimitLayout = new QVBoxLayout();
    lowerLimitLayout->addWidget(new QLabel("Lower limit"));
    lowerLimitLayout->addWidget(&lineLowerArray[index]);
    QVBoxLayout *upperLimitLayout = new QVBoxLayout();
    upperLimitLayout->addWidget(new QLabel("Upper limit"));
    upperLimitLayout->addWidget(&lineUpperArray[index]);

    QPushButton *btnUpdate = new QPushButton("Update", this);
    connect(btnUpdate, SIGNAL(clicked()), this, SLOT(updateConcentrationTab()));

    QHBoxLayout *limitsLayout = new QHBoxLayout();
    limitsLayout->addLayout(lowerLimitLayout);
    limitsLayout->addLayout(upperLimitLayout);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(limitsLayout);
    layout->addWidget(btnUpdate);
    QWidget* tab = new QWidget();
    tab->setLayout(layout);

    return tab;
}

void EdarWidget::createConcentrationTabs(){
    concentrationList = new QList<double>;
    concentrationTimes = new QList<QList<int>*>;
    concentrationValues = new QList<QList<double>*>;
    QStringList listAux;
    std::string line;
    std::string str;
    QString qsAux;
    int index;

    QFile file(Globals::instance()->getAppProjectPath() + "/sludge_profiles/" + sludge_profiles->currentText());
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        str = stream.readAll().toStdString();
        std::istringstream f(str);
        std::getline(f, line);
        qsAux = QString::fromStdString(line);
        listAux = qsAux.split(",");
        int time;
        double height;

        for ( const auto& i : listAux  ){
            concentrationList->append(i.toDouble());
            concentrationTimes->append(new QList<int>);
            concentrationValues->append(new QList<double>);
        }


        adjustmentLower = new double[concentrationList->size()];
        adjustmentUpper = new double[concentrationList->size()];
        linearRegression = new double[concentrationList->size()];
        interceptArray  =new double[concentrationList->size()];
        lineLowerArray = new QLineEdit[concentrationList->size()];
        lineUpperArray = new QLineEdit[concentrationList->size()];

        while (std::getline(f, line)) {
            qsAux = QString::fromStdString(line);
            listAux = qsAux.split(",");
            index=0;
            while(index < (concentrationList->size() * 2)){
                time = listAux[index].toInt();
                height = listAux[index+1].toDouble();
                concentrationTimes->at(index/2)->append(time);
                concentrationValues->at(index/2)->append(height);
                index+=2;
            }
        }
        file.close();

        lineArraysCreated = true;
    }

    //remove list entries with 0's after the first one
    for(int i=0; i < concentrationList->size(); i++){
        int t0s=0;
        QMutableListIterator<int> it(concentrationTimes->at(i)[0]);
        while (it.hasNext()) {
            if (it.next() == 0)
                t0s++;
            if (t0s >= 2)
                it.remove();
        }

        m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
        settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

        adjustmentLower[i]=settings->value("Settling/lower_c" + QString::number(i), concentrationTimes->at(i)->first()).toDouble();
        adjustmentUpper[i]=settings->value("Settling/upper_c" + QString::number(i), concentrationTimes->at(i)->last()).toDouble();
    }

    /* for(int i=0; i < concentrationTimes.size(); i++){
            qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - "
                     << concentrationTimes[i];
    }*/

    concentrationTabs = new QToolBox();
    concentrationTabs->setStyleSheet(Globals::instance()->getCssToolBox());
    connect(concentrationTabs,SIGNAL(currentChanged(int)),this,SLOT(concentrationTabChanged()));

    index=0;
    for (index=0; index < concentrationList->size(); index++){
        concentrationTabs->addItem(showConcentrationTab(index), "Concentration: " + QString::number(concentrationList->at(index)));
    }

    QPushButton *btnShowall = new QPushButton("Show all concetrations");
    connect(btnShowall, SIGNAL(clicked()), this, SLOT(showAllConcentrationsChart()));

    for (int i=0; i<concentrationList->size(); i++)
        getLinearRegresion(i);
    vesilind(concentrationList, linearRegression, &vesilind_v0, &vesilind_rh);
    label_v0 = new QLabel("Vesilind:\n\tv0 (m/s): " + QString::number(vesilind_v0));
    label_rh = new QLabel("\trh (kg/m3): "  + QString::number(vesilind_rh));

    clearLayout(sludge_layout);
    sludge_layout->addWidget(concentrationTabs);
    sludge_layout->addWidget(label_v0);
    sludge_layout->addWidget(label_rh);
    sludge_layout->addWidget(btnShowall);
    sludge_layout->update();
    btn_show_tabs->setDisabled(true );
}

void clearLayout(QLayout *layout){
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}

void EdarWidget::saveSludge(){
    if (sludge_filename->text() == "")
        return;

    if (!parseSludgeFile(editor->toPlainText().toStdString())) {
        QString dirname = Globals::instance()->getAppProjectPath() + "/sludge_profiles/";
        QString filename = dirname + sludge_filename->text().split(".")[0] + ".csv";
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly)) {
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << editor->toPlainText() << endl;
            QTextStream stream(&file);
            stream << editor->toPlainText() << endl;
            file.close();
        } else {
            qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Error opening the file: " << filename << endl;
        }

        updateSludgeProfilesList();
        sludge_profiles->setCurrentText(sludge_filename->text() + ".csv");
        editorWindow->close();
        delete(editorWindow);
    } else {
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Error while saving the file" << endl;
    }

    createConcentrationTabs();

}

void EdarWidget::showFileParserError(int line, QString aux){
    QMessageBox *errorBox = new QMessageBox();
    errorBox->setIcon(QMessageBox::Warning);
    errorBox->setWindowTitle("Error while parsing the file");
    errorBox->setText("Please check line: " + QString::number(line) + "\nHelp: " + aux);
    errorBox->setStandardButtons(QMessageBox::Close);
    errorBox->exec();
}

int EdarWidget::parseSludgeFile(std::string stream){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << QString::fromStdString(stream) << endl;
    bool ok;
    std::string line;
    int lineIndex=1;
    int cnt=0;
    std::istringstream f(stream);
    QString qsAux;
    double dobAux;
    QStringList listAux;

    if (std::getline(f, line)){
        qsAux = QString::fromStdString(line);
        listAux = qsAux.split(",");

        for ( const auto& i : listAux  )
        {
            dobAux = i.toDouble(&ok);
            if ((!ok) && (i != "")){ //if empty we'll use 0
                showFileParserError(lineIndex, "Wrong value: " + i);
                return lineIndex;
            }
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << listAux << " - " << dobAux << " - " << ok << endl;
        }
    }

    cnt = listAux.size();
    lineIndex++;

    //remaining lines
    while (std::getline(f, line)) {
        qsAux = QString::fromStdString(line);
        listAux = qsAux.split(",");
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << listAux << " - " << listAux.size() << endl;
        if(listAux.size() != cnt * 2){
            showFileParserError(lineIndex, "Not correct number of values");
            return lineIndex;
        } else {
            for ( const auto& i : listAux  )
            {
                dobAux = i.toDouble(&ok);
                if ((!ok) && (i != "")){ //if empty we'll use 0
                    showFileParserError(lineIndex, "Wrong value: " + i);
                    return lineIndex;
                }
            }
        }
        lineIndex++;
    }
    return 0;
}

void EdarWidget::openFileDialog(){
    QString pathFileName =
            QFileDialog::getOpenFileName(this,
                                         tr("Import sludge profile"), "",
                                         tr("Sludge profile files (*.csv)"));

    QString fileName = QFileInfo(pathFileName).fileName();
    QFile file(pathFileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        std::string str = stream.readAll().toStdString();
        if (!parseSludgeFile(str)){
            QString dst = Globals::instance()->getAppProjectPath() + "/sludge_profiles/" + fileName;
            //qDebug() << file.size() << QString::fromStdString(str) << endl << fileName << endl << dst;
            QFile::copy(pathFileName, dst);
            updateSludgeProfilesList();
            sludge_profiles->setCurrentText(fileName);
        }
        file.close();
    }
}

void EdarWidget::updateSliderV0(){
    slider_v0->setValue(box_v0->getValue()*100000);
}

void EdarWidget::updateSliderRh(){
    slider_rh->setValue(box_rh->getValue()*100);
}

void EdarWidget::onSliderV0Stop(){
    line_v0->setText(QString::number(slider_v0->value()/100000.0));
}

void EdarWidget::onSliderRhStop(){
    line_rh->setText(QString::number(slider_rh->value()/100.0));
}

vtkSmartPointer<vtkChartXY> EdarWidget::updateModel10Layers(){

    ComputerLayersModel nn(4.0,Qi+Qr,Qr/Qi,diam,SST,v0*3600,rh,10);
    double* ele = nn.rk4vec_test();

    // Create a table with some points in it
    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    vtkSmartPointer<vtkTable> tablePoints = vtkSmartPointer<vtkTable>::New();
    vtkSmartPointer<vtkFloatArray> arrX = vtkSmartPointer<vtkFloatArray>::New();
    arrX->SetName("Model 10 layers");
    table->AddColumn(arrX);
    vtkSmartPointer<vtkFloatArray> arrC = vtkSmartPointer<vtkFloatArray>::New();
    arrC->SetName("M10Curve");
    table->AddColumn(arrC);

    vtkSmartPointer<vtkFloatArray> arrS = vtkSmartPointer<vtkFloatArray>::New();
    arrS->SetName("M10Overflow");
    table->AddColumn(arrS);

    vtkSmartPointer<vtkFloatArray> arrU = vtkSmartPointer<vtkFloatArray>::New();
    arrU->SetName("M10Underflow");
    table->AddColumn(arrU);

    /*vtkSmartPointer<vtkFloatArray> arrXPoints = vtkSmartPointer<vtkFloatArray>::New();
    arrXPoints->SetName("M10Concentration (kg/m^3)");
    tablePoints->AddColumn(arrXPoints);

    vtkSmartPointer<vtkFloatArray> arrP = vtkSmartPointer<vtkFloatArray>::New();
    arrP->SetName("M10Point");
    tablePoints->AddColumn(arrP);*/

    // Fill in the table with some example values
    int numPoints = 10;//sizeof(ele)/sizeof(*ele);
    table->SetNumberOfRows(numPoints);
    for (int i = 0; i < numPoints; ++i){
        table->SetValue(i, 1, i+1 );
        table->SetValue(i, 0, ele[i]);
    }

    // Intersection Point
    /*tablePoints->SetNumberOfRows(1);
    tablePoints->SetValue(0, 0, SST);
    tablePoints->SetValue(0, 1, Qi/A*SST);*/
    // Check intersection of the curve with the point
    table->Modified();
    // Set up the view
    /*vtkSmartPointer<vtkContextView> view = vtkSmartPointer<vtkContextView>::New();
    view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    // Add multiple line plots, setting the colors etc
    view->GetScene()->AddItem(chart);*/
    vtkSmartPointer<vtkChartXY> chart = vtkSmartPointer<vtkChartXY>::New();

    vtkPlot *line = chart->AddPlot(vtkChart::LINE);
    //vtkPlot *lineO = chart->AddPlot(vtkChart::LINE);

    line->SetInputData(table, 0, 1);
    //lineO->SetInputData(table, 0, 2);
    //lineO->GetPen()->SetLineType(vtkPen::DASH_LINE);
    line->SetColor(7,60,60,255);
    //lineO->SetColor(102,178,255,255);
    line->SetWidth(3.0);
    line = chart->AddPlot(vtkChart::LINE);
    chart->GetTitleProperties()->SetBold(true);
    chart->GetTitleProperties()->SetFontSize(20);
    chart->GetTitleProperties()->SetShadow(true);
    chart->SetShowLegend(true);
    chart->SetAutoSize(false);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Concentration (kg/m3)");
    chart->GetAxis(vtkAxis::LEFT)->SetTitle("Layer");

    int *size = vtkViewer->GetRenderWindow()->GetSize();
    chart->SetSize(vtkRectf(0.0, 0.0, 0.5*size[0], 0.60*size[1]));
    chart->SetTitle("10 Layers Model");

    chart->Update();

    return chart;
}

void EdarWidget::loadSettingsProcess(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_proc_diam->setText(settings->value("Process/diameter", "30").toString());
    double area = M_PI*pow((line_proc_diam->text().toDouble())*0.5, 2.0);
    line_proc_area->setText(settings->value("Process/area", area).toString());
    line_proc_qi->setText(settings->value("Process/qi", "109.5").toString());
    line_proc_qr->setText(settings->value("Process/qr", "372").toString());
    line_proc_tanks->setText(settings->value("Process/tanks", "1").toString());
    double va = line_proc_qr->text().toDouble()/area;
    line_proc_va->setText(settings->value("Process/va", va).toString());
    line_proc_sst->setText(settings->value("Process/sst", "3").toString());
}

void EdarWidget::saveSettingsProcess(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    settings->setValue("Process/diameter", line_proc_diam->text());
    double area = M_PI*pow((line_proc_diam->text().toDouble())*0.5, 2.0);
    line_proc_area->setText(QString::number(area));
    settings->setValue("Process/area", line_proc_area->text());
    settings->setValue("Process/qi", line_proc_qi->text());
    settings->setValue("Process/qr", line_proc_qr->text());
    settings->setValue("Process/tanks", line_proc_tanks->text());
    double va = line_proc_qr->text().toDouble()/area;
    line_proc_va->setText(QString::number(va));
    settings->setValue("Process/va", line_proc_va->text());
    settings->setValue("Process/sst", line_proc_sst->text());

    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();

    applyProcess();
}

void EdarWidget::processProperties(){
    line_proc_diam= new QLineEdit("");
    label_proc_diam= new QLabel("Diameter (m)");
    line_proc_area= new QLineEdit("");
    label_proc_area = new QLabel("Area (m2)");
    line_proc_qi= new QLineEdit("");
    label_proc_qi= new QLabel("Qi (m3/h)");
    line_proc_qr= new QLineEdit("");
    label_proc_qr = new QLabel("Qr (m3/h)");
    line_proc_tanks= new QLineEdit("");
    label_proc_tanks = new QLabel("Number of settling tanks");
    line_proc_va= new QLineEdit("");
    label_proc_va = new QLabel("VA (m3/m2/h)");
    line_proc_sst= new QLineEdit("");
    label_proc_sst= new QLabel("SST (kg/m3)");

    QPushButton* btnSaveProcess = new QPushButton("Save", this);
    connect(btnSaveProcess, SIGNAL(clicked()), this, SLOT(saveSettingsProcess()));

    slider_qi = new QSlider(Qt::Horizontal);
    slider_qi->setRange(0, 1000);
    //slider_qi->setValue(line_proc_qi->text().toDouble());
    connect(slider_qi, SIGNAL(sliderReleased()),this, SLOT(onSliderQiStop()));
    connect(line_proc_qi, SIGNAL(textChanged(QString)), this, SLOT(updateSliderQi()));

    slider_qr = new QSlider(Qt::Horizontal);
    slider_qr->setRange(0, 1000);
    //slider_qr->setValue(line_proc_qr->text().toDouble());
    connect(slider_qr, SIGNAL(sliderReleased()),this, SLOT(onSliderQrStop()));
    connect(line_proc_qr, SIGNAL(textChanged(QString)), this, SLOT(updateSliderQr()));

    loadSettingsProcess();

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(label_proc_diam);
    layout->addWidget(line_proc_diam);
    layout->addWidget(label_proc_area);
    layout->addWidget(line_proc_area);
    layout->addWidget(label_proc_qi);
    layout->addWidget(line_proc_qi);
    layout->addWidget(slider_qi);
    layout->addWidget(label_proc_qr);
    layout->addWidget(line_proc_qr);
    layout->addWidget(slider_qr);
    layout->addWidget(label_proc_tanks);
    layout->addWidget(line_proc_tanks);
    layout->addWidget(label_proc_va);
    layout->addWidget(line_proc_va);
    layout->addWidget(label_proc_sst);
    layout->addWidget(line_proc_sst);
    layout->addWidget(btnSaveProcess);
    layout->addStretch();

    if(lineArraysCreated){
        delete [] lineLowerArray;
        delete [] lineUpperArray;
        lineArraysCreated=false;
    }

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    //areaActionWidget->setMaximumHeight(450);
    areaActionWidget->setLayout(layout);

    setImageInViewer(2);

    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();
    applyProcess();

    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void EdarWidget::updateSliderQi(){
    slider_qi->setValue(line_proc_qi->text().toDouble());

    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();
    applyProcess();
}

void EdarWidget::updateSliderQr(){
    slider_qr->setValue(line_proc_qr->text().toDouble());

    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();
    applyProcess();
}

void EdarWidget::onSliderQrStop(){
    line_proc_qr->setText(QString::number(slider_qr->value()));

    line_proc_va->setText(QString::number(line_proc_qr->text().toDouble()/line_proc_area->text().toDouble()));
    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();
    applyProcess();
}

void EdarWidget::onSliderQiStop(){
    line_proc_qi->setText(QString::number(slider_qi->value()));

    Qi = line_proc_qi->text().toDouble()/line_proc_tanks->text().toDouble();
    Qr = line_proc_qr->text().toDouble()/line_proc_tanks->text().toDouble();
    A = line_proc_area->text().toDouble();
    SST = line_proc_sst->text().toDouble();
    diam = line_proc_diam->text().toDouble();
    applyProcess();
}

void EdarWidget::applyProcess(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    //v0 = settings->value("Settling/v0", "0.001908").toFloat();
    //rh = settings->value("Settling/rh", "1.899").toFloat();
    //rt = settings->value("Settling/rt", "5").toFloat();
    v0 = settings->value("Settling/v0", "0").toFloat();
    rh = settings->value("Settling/rh", "0").toFloat();
    rt = settings->value("Settling/rt", "0").toFloat();

    // Create a table with some points in it
    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    vtkSmartPointer<vtkTable> tablePoints = vtkSmartPointer<vtkTable>::New();
    vtkSmartPointer<vtkFloatArray> arrX = vtkSmartPointer<vtkFloatArray>::New();
    arrX->SetName("Concentration (kg/m^3)");
    table->AddColumn(arrX);
    vtkSmartPointer<vtkFloatArray> arrC = vtkSmartPointer<vtkFloatArray>::New();
    arrC->SetName("Curve");
    table->AddColumn(arrC);

    vtkSmartPointer<vtkFloatArray> arrS = vtkSmartPointer<vtkFloatArray>::New();
    arrS->SetName("Overflow");
    table->AddColumn(arrS);

    vtkSmartPointer<vtkFloatArray> arrU = vtkSmartPointer<vtkFloatArray>::New();
    arrU->SetName("Underflow");
    table->AddColumn(arrU);

    vtkSmartPointer<vtkFloatArray> arrXPoints = vtkSmartPointer<vtkFloatArray>::New();
    arrXPoints->SetName("Concentration (kg/m^3)");
    tablePoints->AddColumn(arrXPoints);

    vtkSmartPointer<vtkFloatArray> arrP = vtkSmartPointer<vtkFloatArray>::New();
    arrP->SetName("Point");
    tablePoints->AddColumn(arrP);

    double SRL = (Qi+Qr)/A*SST;
    double Va = Qr/A;

    // Fill in the table with some example values
    int numPoints = 100;
    float inc = 10.0 / (numPoints-1);
    table->SetNumberOfRows(numPoints);
    for (int i = 0; i < numPoints; ++i){
        table->SetValue(i, 0, i * inc);
        table->SetValue(i, 1, 3600*(v0*exp(-rh*i*inc)-v0*exp(-rt*i*inc))*i*inc);
        table->SetValue(i, 2, Qi/A*i*inc);
        if(SRL-Va*i*inc>=0){
            table->SetValue(i, 3, SRL-Va*i*inc);
        }
        else{
            table->SetValue(i, 3, 0.0);
        }
    }

    // Intersection Point
    tablePoints->SetNumberOfRows(1);
    tablePoints->SetValue(0, 0, SST);
    tablePoints->SetValue(0, 1, Qi/A*SST);
    // Check intersection of the curve with the point
    double yMax = 3600*(v0*exp(-rh*SST)-v0*exp(-rt*SST))*SST;
    table->Modified();
    // Set up the view
    vtkSmartPointer<vtkContextView> view = vtkSmartPointer<vtkContextView>::New();
    view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    vtkSmartPointer<vtkContextView> view2 = vtkSmartPointer<vtkContextView>::New();
    view2->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    // Add multiple line plots, setting the colors etc
    vtkSmartPointer<vtkChartXY> chart = vtkSmartPointer<vtkChartXY>::New();
    vtkSmartPointer<vtkChartXY> chart2 = vtkSmartPointer<vtkChartXY>::New();
    view->GetScene()->AddItem(chart);
    view2->GetScene()->AddItem(chart2);

    vtkPlot *line = chart->AddPlot(vtkChart::LINE);
    vtkPlot *lineO = chart->AddPlot(vtkChart::LINE);
    vtkPlot *lineU = chart->AddPlot(vtkChart::LINE);
    vtkPlot *points = chart->AddPlot(vtkChart::POINTS);
    vtkPlot *line2 = chart2->AddPlot(vtkChart::LINE);

    line->SetInputData(table, 0, 1);
    lineO->SetInputData(table, 0, 2);
    lineU->SetInputData(table, 0, 3);
    points->SetInputData(tablePoints, 0, 1);
    if (yMax>(Qi/A*SST)){
        points->SetColor(51,255,51,255);
    }
    else {
        points->SetColor(255,0,0,255);
    }
    points->SetWidth(5.0);
    line->GetPen()->SetLineType(vtkPen::SOLID_LINE);
    lineO->GetPen()->SetLineType(vtkPen::DASH_LINE);
    lineU->GetPen()->SetLineType(vtkPen::DASH_LINE);
    line->SetColor(7,60,60,255);
    lineU->SetColor(204,102,0,255);
    lineO->SetColor(102,178,255,255);
    line->SetWidth(3.0);
    line = chart->AddPlot(vtkChart::LINE);
    line2->SetInputData(table, 0, 1);
    line2 = chart2->AddPlot(vtkChart::LINE);
    chart->GetTitleProperties()->SetBold(true);
    chart->GetTitleProperties()->SetFontSize(20);
    chart->GetTitleProperties()->SetShadow(true);
    chart->SetShowLegend(true);
    chart->SetAutoSize(false);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Concentration (kg/m3)");
    chart->GetAxis(vtkAxis::LEFT)->SetTitle("Loading Rate (kg/m2/h)");
    chart2->SetAutoSize(false);

    int *size = vtkViewer->GetRenderWindow()->GetSize();
    chart->SetSize(vtkRectf(0.0, 0.0, 0.5*size[0], 0.60*size[1]));
    chart->SetTitle("State Point");
    chart2->SetSize(vtkRectf(0.5*size[0], 0.0, 0.45*size[0], 0.60*size[1]));
    chart2->SetTitle("10 capas");

    vtkSmartPointer<vtkContextActor> chartActor = vtkSmartPointer<vtkContextActor>::New();
    vtkSmartPointer<vtkContextScene> chartScene = vtkSmartPointer<vtkContextScene>::New();

    chartScene->AddItem(chart.GetPointer());
    statePoint->SetScene(chartScene.GetPointer());
    vtkSmartPointer<vtkContextActor> chartActor2 = vtkSmartPointer<vtkContextActor>::New();
    vtkSmartPointer<vtkContextScene> chartScene2 = vtkSmartPointer<vtkContextScene>::New();

    chartScene2->AddItem(chart2.GetPointer());
    chartActor2->SetScene(chartScene2.GetPointer());

    vtkSmartPointer<vtkContextScene> chartSceneModel = vtkSmartPointer<vtkContextScene>::New();

    vtkViewer->remove(model10);

    vtkSmartPointer<vtkChartXY>  model10_chart = updateModel10Layers();
    model10_chart->SetSize(vtkRectf(0.5*size[0], 0.0, 0.45*size[0], 0.60*size[1]));
    chartSceneModel->AddItem(model10_chart.GetPointer());

    model10->SetScene(chartSceneModel.GetPointer());

    vtkViewer->add(model10);

    vtkViewer->remove(statePoint);
    chart->Update();
    vtkViewer->add(statePoint);
    vtkViewer->update();
}

void EdarWidget::clearLayout(QLayout *layout){
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}


void EdarWidget::remove(QLayout* layout){
    QLayoutItem* child;
    while(layout->count()!=0){
        child = layout->takeAt(0);
        if(child->layout() != 0){
            remove(child->layout());
        }
        else if(child->widget() != 0){
            delete child->widget();
        }

        delete child;
    }
}
