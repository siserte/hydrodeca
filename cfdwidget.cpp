#include "borderlayout.h"
#include "CfdWidget.h"
#include "globals.h"
#include "menuwidget.h"
#include "ui_defaultwindow.h"

#include <QCheckBox>
#include <QComboBox>
#include <qDebug>
#include <QDir>
#include <QFile>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QTextBrowser>
#include <QThread>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QScrollArea>
#include <QSignalMapper>
#include <QSplitter>
#include <QString>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVBoxLayout>

#include <unordered_map>
#include <AIS_Shape.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBndLib.hxx>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>

#include <BRepLib.hxx>

#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>

#include <BRepPrim_Cone.hxx>
#include <BRepPrimAPI_MakeOneAxis.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>

#include <BRepOffsetAPI_ThruSections.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <gp_Ax1.hxx>
#include <gp_Ax3.hxx>
#include <gp_Pln.hxx>
#include <gp_Pnt.hxx>

#include <IVtkOCC_Shape.hxx>
#include <IVtkTools_DisplayModeFilter.hxx>
#include <IVtkTools_ShapeDataSource.hxx>

#include <MeshVS_MeshPrsBuilder.hxx>
#include <MeshVS_DrawerAttribute.hxx>

#include <OSD_Path.hxx>

#include <QVTKWidget.h>

#include <RWStl.hxx>

#include <STEPCAFControl_Writer.hxx>

#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <StlMesh_Mesh.hxx>
#include <StlMesh_MeshExplorer.hxx>
#include <StlTransfer.hxx>

#include <TDocStd_Document.hxx>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkCompositeDataGeometryFilter.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkDistancePolyDataFilter.h>
#include <vtkExecutive.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkImageViewer.h>
#include <vtkInformationVector.h>
#include <vtkInformationDoubleVectorKey.h>
#include <vtkInteractorStyleImage.h>
#include <vtkJPEGReader.h>
#include <vtkLookupTable.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataReader.h>
#include <vtkProperty.h>
#include <vtkPOpenFOAMReader.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkStructuredGrid.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkVersion.h>

#include <XCAFApp_Application.hxx>
#include <XCAFDoc_DocumentTool.hxx>
#include <XCAFDoc_ShapeTool.hxx>

#include <XSDRAWSTLVRML_DataSource.hxx>

#include <QSettings>
#include <STEPControl_Reader.hxx>
#include <BRepTools.hxx>
#include <vtkStringArray.h>
#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>
#include <vtkCellArray.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkPointSetToLabelHierarchy.h>
#include <vtkLabelHierarchy.h>
#include <vtkLabelPlacementMapper.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkLegendBoxActor.h>
#include <vtkCubeSource.h>
#include <QDirIterator>
#include <vtkSTLReader.h>
#include <vtkLODActor.h>
#include <vtkCylinderSource.h>

#include <vtkArrowSource.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

#define MAX2(X, Y)      (Abs(X)>Abs(Y)?Abs(X):Abs(Y))
#define MAX3(X, Y, Z)   (MAX2(MAX2(X,Y),Z))

CfdWidget::CfdWidget(QWidget* widget, QWidget* centralwidget, VTKViewer* vtk, VTKViewer* graphs, MenuWidget* menuwidget, QToolBox *reportWidget, QListWidget* consoleWidget) : QWidget(widget) {
    parent = widget;
    centralWidget = centralwidget;
    vtkViewer = vtk;
    vtkGraphs = graphs;
    menu = menuwidget;
    reportInfo = reportWidget;
    console = consoleWidget;

    vtkViewer->setShowAxes(true);
    vtkViewer->setShowModel(true);
    vtkViewer->setOpacityModel(1.0);
}

CfdWidget::~CfdWidget(){

}

void CfdWidget::loadMainWindow(){
    // Comprobamos si existe una geometria en la carpeta del proyecto
    QString filename = Globals::instance()->getAppProjectPath() + "\\constant\\triSurface\\cad.stl";
    QFile file(filename);
    if(!file.exists()){
        QMessageBox::about(this, tr("ERROR CFD TAB"),
                           tr("<p>No se ha encontrado ninguna malla en el proyecto. Vuelva a la pestaña de MESH y genere la malla.</p>"));
    }
    else{
        settingsUser = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
        equips_dir = Globals::instance()->getAppProjectPath() + "equipments/";
        settings = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
        createAreaButtons();
        createAreaActions();
        createAreaViewers();
        centralWidget->findChild<QTabWidget*>("area_viewers")->setCurrentIndex(1);
        //  parent->setWindowTitle(tr(Globals::instance()->getTabCfd().toStdString().c_str()));
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Set the correct option here" << endl;
        //propertiesMixedLiquor();
        solverProperties();
        //boundaryConditions();
        // Actualizamos el menu
        menu->updateMenu(Globals::instance()->getTabCfd(), this);
    }
}

void CfdWidget::showGeometryFaces(){
    vtkViewer->reset();

    TopExp_Explorer exp;
    int i=0;
    for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next(), i++) {
        TopoDS_Face aFace = TopoDS::Face(exp.Current());

        BRep_Builder aBuilder;
        TopoDS_Compound aCompound;
        aBuilder.MakeCompound(aCompound);
        aBuilder.Add(aCompound,aFace);

        IVtkOCC_Shape::Handle shapeImpl = new IVtkOCC_Shape(aCompound);
        vtkSmartPointer<IVtkTools_ShapeDataSource> DS = vtkSmartPointer<IVtkTools_ShapeDataSource>::New();
        DS->SetShape(shapeImpl);

        vtkSmartPointer<vtkPolyDataMapper> Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        Mapper->SetInputConnection(DS->GetOutputPort());
        vtkSmartPointer<vtkActor> Actor = vtkSmartPointer<vtkActor>::New();
        Actor->SetMapper(Mapper);
        Actor->GetProperty()->SetOpacity(0.5);

        vtkSmartPointer<vtkLookupTable> Table = IVtkTools::InitLookupTable();
        IVtkTools::InitShapeMapper(Mapper, Table);
        IVtkTools::SetLookupTableColor(Table, MT_ShadedFace,
                                       color_list->at(faces_color_list->at(i))[0],
                color_list->at(faces_color_list->at(i))[1],
                color_list->at(faces_color_list->at(i))[2]);
        vtkViewer->add(Actor);
    }

    //put the coordinate axis to the left corner (remove if we want that the axis button works)
    vtkOrientationMarkerWidget *widget = vtkOrientationMarkerWidget::New();
    widget->SetDefaultRenderer(vtkViewer->m_renderer);
    widget->SetOrientationMarker(vtkViewer->axes);
    widget->SetInteractor(vtkViewer->GetRenderWindow()->GetInteractor());
    widget->EnabledOn();
    vtkViewer->setShowAxes(false);

    vtkViewer->zoomToBox();
    vtkViewer->update();
}

void CfdWidget::createAreaActions(){
    QGridLayout* layout = new QGridLayout();
    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void CfdWidget::initColorList(){
    faces_color_list = new QList<int>();
    list_assigned_faces = new QList<bool>();

    TopExp_Explorer exp;
    for(exp.Init(aShape, TopAbs_FACE); exp.More(); exp.Next()) {
        faces_color_list->append(0);
        list_assigned_faces->append(false);
    }

    color_list = new QList<double *>();
    double *a = new double[3];
    a[0] = 0.95;
    a[1] = 0.95;
    a[2] = 0.95;
    color_list->append(a);

    for (double i=0.25; i<1; i+=0.25){
        for (double j=0.25; j<1; j+=0.25){
            for (double k=0.25; k<1; k+=0.25){
                double *a = new double[3];
                a[0] = i;
                a[1] = j;
                a[2] = k;
                color_list->append(a);
            }
        }
    }
}

void CfdWidget::createAreaViewers(){
    if(vtkViewer != 0){
        vtkViewer->reset();
        //vtkViewer->readOpenFoam(2, "solid color", false, false, false, true);


        QFile file(Globals::instance()->getAppProjectPath()+"/infoSavedCAD.ini");
        if(! file.exists()){
            //ESTO NO DEBERÍA PASAR
            qDebug() << "(ERROR):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - infoSavedCAD.ini not found " << endl;
            return;
        }

        QSettings *CADsettings = new QSettings(file.fileName(), QSettings::IniFormat);
        QString pathFileName = Globals::instance()->getAppProjectPath() + "geometry.step";

        STEPControl_Reader reader;
        reader.ReadFile(pathFileName.toStdString().c_str());
        reader.NbRootsForTransfer();
        reader.TransferRoots();
        aShape = reader.OneShape();
        BRepTools::Clean(aShape);

        initColorList();

        cnt_groups = CADsettings->value("PhysicalGroups/count").toInt();
        list_faces_names = new QList<QString>();
        legend = vtkSmartPointer<vtkLegendBoxActor>::New();
        legend->SetNumberOfEntries(cnt_groups+1);
        vtkSmartPointer<vtkCubeSource> legendBox = vtkSmartPointer<vtkCubeSource>::New();
        legendBox->Update();
        for (int i = 0; i < cnt_groups; i++){
            QString faces = CADsettings->value("PG" + QString::number(i+1) + "/faces").toString();
            QString name = CADsettings->value("PG" + QString::number(i+1) + "/name").toString();
            QStringList listFaces = faces.split(";");
            for (const auto& face : listFaces)
                list_assigned_faces->replace(face.toInt(), true);
            int firstFace = listFaces.first().toInt();
            for (const auto& face : listFaces)
                faces_color_list->replace(face.toInt(), firstFace+1); //cero is for not assigned faces
            legend->SetEntry(i, legendBox->GetOutput(), name.toStdString().c_str(), color_list->at(firstFace+1));
            list_faces_names->append(name);
        }
        legend->SetEntry(cnt_groups, legendBox->GetOutput(), "Default", color_list->at(0));

        showGeometryFaces();
        //vtkViewer->add(legend);
        vtkViewer->update();
        vtkViewer->resetCamera();
    }
}

void CfdWidget::createAreaButtons(){
    QPushButton* btnProperties = new QPushButton("Solver", this);
    btnProperties->setStyleSheet(Globals::instance()->getCssPushButton());
    btnProperties->setMinimumHeight(50);
    btnProperties->setDisabled(false);
    btnProperties->setCheckable(true);
    btnProperties->setAutoExclusive(true);
    btnProperties->setChecked(true);
    connect(btnProperties, SIGNAL(clicked()), this, SLOT(solverProperties()));

    QPushButton* btnBC = new QPushButton("Boundaries", this);
    btnBC->setStyleSheet(Globals::instance()->getCssPushButton());
    btnBC->setMinimumHeight(50);
    btnBC->setDisabled(false);
    btnBC->setCheckable(true);
    btnBC->setAutoExclusive(true);
    connect(btnBC, SIGNAL(clicked()), this, SLOT(boundaryConditions()));

    QPushButton* btnEquip = new QPushButton("Equipments", this);
    btnEquip->setStyleSheet(Globals::instance()->getCssPushButton());
    btnEquip->setMinimumHeight(50);
    btnEquip->setDisabled(false);
    btnEquip->setCheckable(true);
    btnEquip->setAutoExclusive(true);
    connect(btnEquip, SIGNAL(clicked()), this, SLOT(equipmentsConfig()));

    btnInit = new QPushButton("Initialize", this);
    //   btnInit->setDisabled(true);
    btnInit->setStyleSheet(Globals::instance()->getCssPushButton());
    btnInit->setMinimumHeight(50);
    btnInit->setDisabled(false);
    btnInit->setCheckable(true);
    btnInit->setAutoExclusive(true);
    connect(btnInit, SIGNAL(clicked()), this, SLOT(showInitilizeTab()));

    QPushButton* btnSim = new QPushButton("Simulation", this);
    btnSim->setStyleSheet(Globals::instance()->getCssPushButton());
    btnSim->setMinimumHeight(50);
    btnSim->setDisabled(false);
    btnSim->setCheckable(true);
    btnSim->setAutoExclusive(true);
    connect(btnSim, SIGNAL(clicked()), this, SLOT(showSimulationTab()));

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->setMargin(0);
    layoutAreaActions->setSpacing(0);
    layoutAreaActions->addWidget(btnProperties);
    layoutAreaActions->addWidget(btnBC);
    layoutAreaActions->addWidget(btnEquip);
    layoutAreaActions->addWidget(btnInit);
    layoutAreaActions->addWidget(btnSim);
    layoutAreaActions->addStretch();

    if(centralWidget->findChild<QScrollArea*>("area_buttons")->layout() != NULL){
        QLayoutItem *child;
        while ((child = centralWidget->findChild<QScrollArea*>("area_buttons")->layout()->takeAt(0)) != 0) {
            delete child->widget();
        }
        delete centralWidget->findChild<QScrollArea*>("area_buttons")->layout();
    }
    centralWidget->findChild<QScrollArea*>("area_buttons")->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setMaximumWidth(100);
    centralWidget->findChild<QScrollArea*>("area_buttons")->setStyleSheet(Globals::instance()->getCssAreaButtons());
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! SLOTS
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void CfdWidget::loadSettingsBoundariesWWTP(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    int n;
    n = settings->value("Process/tanks", 1).toInt();
    double Qi, Qr, Qe, SST, inlet;
    Qi = settings->value("Process/qi", 167).toDouble();
    Qr = settings->value("Process/qr", 333).toDouble();
    SST = settings->value("Process/sst", 1.5).toDouble();
    Qi = Qi/n/14400;
    Qr = Qr/n/14400;
    inlet = Qi + Qr;
    Qe = Qi * (-1);
    SST = SST / 1000;

    line_inf_flow->setText(QString::number(inlet));
    line_recycling->setText(QString::number(Qr * (-1)));
    line_effluent->setText(QString::number(Qe));
    line_inf_sludge->setText(QString::number(SST));
}

void CfdWidget::loadSettingsBoundaries(){
    int cnt = settingsUser->value("CfdBoundaries/cnt",0).toInt();
    if (cnt == 0)
        return;
    else {
        for (int i = 0; i < cnt; i++){
            //QString type = settingsUser->value("CfdBoundaries/b" + QString::number(i) + "_type").toString();
            //QString face = settingsUser->value("CfdBoundaries/b" + QString::number(i) + "_faces").toString();
            loadBoundary(false, i);
            /*
            //?¿?¿¿ Otra cosa rara de QT. Como no muestra en el LineEdit el valor desde loadBoundary, me toca hacerlo ahora.
            if(type == settingsBoundaries->value("Types/t1") || type == settingsBoundaries->value("Types/t2") || type == settingsBoundaries->value("Types/t4")){
                QWidget *widget_boundary = layoutAreaBoundaries->itemAt(i)->widget();
                QWidget *widget_options = widget_boundary->findChild<QWidget*>("options_box");
                QListIterator<QObject *> it(widget_options->children());
                while (it.hasNext()){
                    QObject *o = it.next();
                    if (QLatin1String(o->metaObject()->className()) == "QComboBox"){
                        QComboBox *aux = (QComboBox*) o;
                        QString mode = aux->currentText();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_mode", mode);
                    }  else if (QLatin1String(o->metaObject()->className()) == "QLineEdit"){
                        //else if (o->objectName() == "line_mode"){
                        QLineEdit *aux = (QLineEdit*) o;
                        aux->setText(settingsUser->value("CfdBoundaries/b" + QString::number(i) + "_value", 0).toString());
                        if (type == settingsBoundaries->value("Types/t1")){
                            while (it.hasNext()){
                                QObject *o = it.next();
                                if (QLatin1String(o->metaObject()->className()) == "QLineEdit"){
                                    QLineEdit *aux = (QLineEdit*) o;
                                    aux->setText(settingsUser->value("CfdBoundaries/b" + QString::number(i) + "_sludge", 0).toString());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }*/
        }
    }
}

void CfdWidget::saveBoundariesPimple(){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    QFile file_U(Globals::instance()->getAppProjectPath() + "/0/U");
    QFile file_P(Globals::instance()->getAppProjectPath() + "/0/p");
    QFile file_E(Globals::instance()->getAppProjectPath() + "/0/epsilon");
    QFile file_K(Globals::instance()->getAppProjectPath() + "/0/k");
    QFile file_N(Globals::instance()->getAppProjectPath() + "/0/nut");

    if(file_U.open(QFile::WriteOnly | QFile::Text) &&
            file_P.open(QFile::WriteOnly | QFile::Text) &&
            file_E.open(QFile::WriteOnly | QFile::Text) &&
            file_K.open(QFile::WriteOnly | QFile::Text) &&
            file_N.open(QFile::WriteOnly | QFile::Text)) {

        QTextStream stream_U(&file_U);
        QTextStream stream_P(&file_P);
        QTextStream stream_E(&file_E);
        QTextStream stream_K(&file_K);
        QTextStream stream_N(&file_N);


        stream_U << "/***** File generated by Hydrodeca *****/" << endl;
        stream_U << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolVectorField;\n\tformat\tascii;\n\tobject\tU;\n}\n"
                    "dimensions\t[0 1 -1 0 0 0 0];\n"
                    "internalField\tuniform\t(0 0 0);\n\n"
                    "boundaryField {" << endl;

        stream_P << "/***** File generated by Hydrodeca *****/" << endl;
        stream_P << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tp;\n}\n"
                    "dimensions\t[0 2 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        stream_E << "/***** File generated by Hydrodeca *****/" << endl;
        stream_E << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tepsilon;\n}\n"
                    "dimensions\t[0 2 -3 0 0 0 0];\n"
                    "internalField\tuniform\t0.000001;\n\n"
                    "boundaryField {" << endl;

        stream_K << "/***** File generated by Hydrodeca *****/" << endl;
        stream_K << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tk;\n}\n"
                    "dimensions\t[0 2 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0.00001;\n\n"
                    "boundaryField {" << endl;

        stream_N << "/***** File generated by Hydrodeca *****/" << endl;
        stream_N << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tnut;\n\tlocation\t\"0\";\n}\n"
                    "dimensions\t[0 2 -1 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        settingsUser->setValue("CfdBoundaries/cnt", layoutAreaBoundaries->count());
        for (int i = 0; i < layoutAreaBoundaries->count(); ++i) {
            QWidget *widget_boundary = layoutAreaBoundaries->itemAt(i)->widget();
            if (widget_boundary != NULL && widget_boundary->objectName() == "boundary_box") {
                QComboBox *combo_type = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_type");
                QComboBox *combo_faces = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_faces");
                int type = combo_type->currentIndex();
                QString face = combo_faces->currentText();
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_type", QString::number(type));
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_faces", face);

                int mode;
                float flow, vx, vy, vz;
                if(type == 0 || type == 1 || type == 3){
                    QWidget *widget_options = widget_boundary->findChild<QWidget*>("options_box");
                    QComboBox *combo_mode = widget_options->findChild<QComboBox*>("combo_mode");
                    mode = combo_mode->currentIndex();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_mode", QString::number(mode));
                    if (combo_mode->currentIndex() == 0) {//flow
                        QLineEdit *line_flow = widget_options->findChild<QLineEdit*>("line_flow");
                        flow = line_flow->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_flow",line_flow->text());
                    } else { //velocity
                        QLineEdit *line_vx = widget_options->findChild<QLineEdit*>("line_vx");
                        QLineEdit *line_vy = widget_options->findChild<QLineEdit*>("line_vy");
                        QLineEdit *line_vz = widget_options->findChild<QLineEdit*>("line_vz");
                        vx=line_vx->text().toFloat();
                        vy=line_vy->text().toFloat();
                        vz=line_vz->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vx",line_vx->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vy",line_vy->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vz",line_vz->text());
                    }
                }
                stream_U << "\t" << face << " {\n\t\ttype\t";
                stream_P << "\t" << face << " {\n\t\ttype\t";
                stream_K << "\t" << face << " {\n\t\ttype\t";
                stream_E << "\t" << face << " {\n\t\ttype\t";
                stream_N << "\t" << face << " {\n\t\ttype\t";

                if(type == 0){ //inflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow) << "; }" << endl;
                    }
                    stream_K << "fixedValue;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.0001; }" << endl;
                    stream_E << "fixedValue;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\tzeroGradient; }" << endl;
                } else if(type == 1){ //outflow
                    /*
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow*(-1)) << "; }" << endl;
                    }
                    */
                    stream_U << "zeroGradient; }" << endl;
                    stream_K << "zeroGradient; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedValue;\n\t\tvalue\tuniform\t0; }" << endl;
                } else if(type == 3) { //overflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow*(-1)) << "; }" << endl;
                    }
                    stream_K << "zeroGradient; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    //stream_P << "fixedValue;\n\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "zeroGradient; }" << endl;
                } else if(type == 5) { //atmosphere
                    stream_U << "noSlip; }" << endl;
                    stream_K << "kqRWallFunction;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.00001; }" << endl;
                    stream_E << "epsilonWallFunction;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "nutkWallFunction;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\t\tzeroGradient; }" << endl;
                } else { //Wall //deflector not implemented so get these values
                    stream_U << "noSlip; }" << endl;
                    stream_K << "kqRWallFunction;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.00001; }" << endl;
                    stream_E << "epsilonWallFunction;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "nutkWallFunction;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\tzeroGradient; }" << endl;
                }
            }
        }
        stream_U << "}" << endl;
        stream_P << "}" << endl;
        stream_E << "}" << endl;
        stream_K << "}" << endl;
        stream_N << "}" << endl;
    }
}

void CfdWidget::saveBoundariesASM1(){
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
    QString path = Globals::instance()->getAppProjectPath();
    QString location = createNextASM1Timestep();
    qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << location << endl;

    QFile file_salk(path + location + "/Salk");
    QFile file_si(path + location + "/Si");
    QFile file_snd(path + location + "/Snd");
    QFile file_snh(path + location + "/Snh");
    QFile file_sno(path + location + "/Sno");
    QFile file_so(path + location + "/So");
    QFile file_ss(path + location + "/Ss");
    QFile file_xba(path + location + "/Xba");
    QFile file_xbh(path + location + "/Xbh");
    QFile file_xi(path + location + "/Xi");
    QFile file_xnd(path + location + "/Xnd");
    QFile file_xp(path + location + "/Xp");
    QFile file_xs(path + location + "/Xs");

    if(file_salk.open(QFile::WriteOnly | QFile::Text) &&
            file_si.open(QFile::WriteOnly | QFile::Text) &&
            file_snh.open(QFile::WriteOnly | QFile::Text) &&
            file_sno.open(QFile::WriteOnly | QFile::Text) &&
            file_so.open(QFile::WriteOnly | QFile::Text) &&
            file_ss.open(QFile::WriteOnly | QFile::Text) &&
            file_xba.open(QFile::WriteOnly | QFile::Text) &&
            file_xbh.open(QFile::WriteOnly | QFile::Text) &&
            file_xi.open(QFile::WriteOnly | QFile::Text) &&
            file_xnd.open(QFile::WriteOnly | QFile::Text) &&
            file_xp.open(QFile::WriteOnly | QFile::Text) &&
            file_snd.open(QFile::WriteOnly | QFile::Text) &&
            file_xs.open(QFile::WriteOnly | QFile::Text)){

        QTextStream stream_salk(&file_salk);
        stream_salk << generateHeader("Salk", location);
        QTextStream stream_si(&file_si);
        stream_si << generateHeader("Si", location);
        QTextStream stream_snh(&file_snh);
        stream_snh << generateHeader("Snh", location);
        QTextStream stream_so(&file_so);
        stream_so << generateHeader("So", location);
        QTextStream stream_ss(&file_ss);
        stream_ss << generateHeader("Ss", location);
        QTextStream stream_xba(&file_xba);
        stream_xba << generateHeader("Xba", location);
        QTextStream stream_xbh(&file_xbh);
        stream_xbh << generateHeader("Xbh", location);
        QTextStream stream_xi(&file_xi);
        stream_xi << generateHeader("Xi", location);
        QTextStream stream_xp(&file_xp);
        stream_xp << generateHeader("Xp", location);
        QTextStream stream_xs(&file_xs);
        stream_xs << generateHeader("Xs", location);
        QTextStream stream_sno(&file_sno);
        stream_sno << generateHeader("Sno", location);
        QTextStream stream_snd(&file_snd);
        stream_snd << generateHeader("Snd", location);
        QTextStream stream_xnd(&file_xnd);
        stream_xnd << generateHeader("Xnd", location);

        settingsUser->setValue("CfdBoundaries/cnt", layoutAreaBoundaries->count());
        for (int i = 0; i < layoutAreaBoundaries->count(); ++i) {

            QWidget *widget_boundary = layoutAreaBoundaries->itemAt(i)->widget();
            if (widget_boundary != NULL && widget_boundary->objectName() == "boundary_box") {
                QComboBox *combo_type = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_type");
                QComboBox *combo_faces = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_faces");
                int type = combo_type->currentIndex();
                QString face = combo_faces->currentText();
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_type", QString::number(type));
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_faces", face);

                stream_salk << "\t" << face << " {\n\t\ttype\t";
                stream_si << "\t" << face << " {\n\t\ttype\t";
                stream_snh << "\t" << face << " {\n\t\ttype\t";
                stream_so << "\t" << face << " {\n\t\ttype\t";
                stream_ss << "\t" << face << " {\n\t\ttype\t";
                stream_xba << "\t" << face << " {\n\t\ttype\t";
                stream_xbh << "\t" << face << " {\n\t\ttype\t";
                stream_xi << "\t" << face << " {\n\t\ttype\t";
                stream_xp << "\t" << face << " {\n\t\ttype\t";
                stream_xs << "\t" << face << " {\n\t\ttype\t";
                stream_sno << "\t" << face << " {\n\t\ttype\t";
                stream_snd << "\t" << face << " {\n\t\ttype\t";
                stream_xnd << "\t" << face << " {\n\t\ttype\t";

                if(type == 0){
                    QString str_text = widget_boundary->findChild<QLineEdit*>("line_salk")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_salk", str_text);
                    stream_salk << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_si")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_si", str_text);
                    stream_si << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_snh")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_snh", str_text);
                    stream_snh << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_so")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_so", str_text);
                    stream_so << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_ss")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_ss", str_text);
                    stream_ss << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xba")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xba", str_text);
                    stream_xba << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xbh")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xbh", str_text);
                    stream_xbh << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xi")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xi", str_text);
                    stream_xi << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xp")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xp", str_text);
                    stream_xp << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xs")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xs", str_text);
                    stream_xs << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_sno")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_sno", str_text);
                    stream_sno << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_snd")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_snd", str_text);
                    stream_snd << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;

                    str_text = widget_boundary->findChild<QLineEdit*>("line_xnd")->text();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_xnd", str_text);
                    stream_xnd << "fixedValue; \n\t\tvalue\t" << str_text << "; }" << endl;
                } else {
                    stream_salk << "zeroGradient; }" << endl;
                    stream_si << "zeroGradient; }" << endl;
                    stream_snh << "zeroGradient; }" << endl;
                    stream_so << "zeroGradient; }" << endl;
                    stream_ss << "zeroGradient; }" << endl;
                    stream_xba << "zeroGradient; }" << endl;
                    stream_xbh << "zeroGradient; }" << endl;
                    stream_xi << "zeroGradient; }" << endl;
                    stream_xp << "zeroGradient; }" << endl;
                    stream_xs << "zeroGradient; }" << endl;
                    stream_sno << "zeroGradient; }" << endl;
                    stream_snd << "zeroGradient; }" << endl;
                    stream_xnd << "zeroGradient; }" << endl;
                }
            }
        }

        stream_salk << "}" << endl;
        stream_si << "}" << endl;
        stream_snh << "}" << endl;
        stream_so << "}" << endl;
        stream_ss << "}" << endl;
        stream_xba << "}" << endl;
        stream_xbh << "}" << endl;
        stream_xi << "}" << endl;
        stream_xp << "}" << endl;
        stream_xs << "}" << endl;
        stream_sno << "}" << endl;
        stream_snd << "}" << endl;
        stream_xnd << "}" << endl;
    }

    file_salk.close();
    file_si.close();
    file_snh.close();
    file_sno.close();
    file_so.close();
    file_ss.close();
    file_xba.close();
    file_xbh.close();
    file_xi.close();
    file_xnd.close();
    file_xp.close();
    file_snd.close();
    file_xs.close();
}

//TODO - Sergio

/*
    if(file_U.open(QFile::WriteOnly | QFile::Text) &&
            file_P.open(QFile::WriteOnly | QFile::Text) &&
            file_E.open(QFile::WriteOnly | QFile::Text) &&
            file_K.open(QFile::WriteOnly | QFile::Text) &&
            file_N.open(QFile::WriteOnly | QFile::Text)) {

        QTextStream stream_U(&file_U);
        QTextStream stream_P(&file_P);
        QTextStream stream_E(&file_E);
        QTextStream stream_K(&file_K);
        QTextStream stream_N(&file_N);


        settingsUser->setValue("CfdBoundaries/cnt", layoutAreaBoundaries->count());
        for (int i = 0; i < layoutAreaBoundaries->count(); ++i) {
            QWidget *widget_boundary = layoutAreaBoundaries->itemAt(i)->widget();
            if (widget_boundary != NULL && widget_boundary->objectName() == "boundary_box") {
                QComboBox *combo_type = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_type");
                QComboBox *combo_faces = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_faces");
                int type = combo_type->currentIndex();
                QString face = combo_faces->currentText();
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_type", QString::number(type));
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_faces", face);

                int mode;
                float flow, vx, vy, vz;
                if(type == 0 || type == 1 || type == 3){
                    QWidget *widget_options = widget_boundary->findChild<QWidget*>("options_box");
                    QComboBox *combo_mode = widget_options->findChild<QComboBox*>("combo_mode");
                    mode = combo_mode->currentIndex();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_mode", QString::number(mode));
                    if (combo_mode->currentIndex() == 0) {//flow
                        QLineEdit *line_flow = widget_options->findChild<QLineEdit*>("line_flow");
                        flow = line_flow->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_flow",line_flow->text());
                    } else { //velocity
                        QLineEdit *line_vx = widget_options->findChild<QLineEdit*>("line_vx");
                        QLineEdit *line_vy = widget_options->findChild<QLineEdit*>("line_vy");
                        QLineEdit *line_vz = widget_options->findChild<QLineEdit*>("line_vz");
                        vx=line_vx->text().toFloat();
                        vy=line_vy->text().toFloat();
                        vz=line_vz->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vx",line_vx->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vy",line_vy->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vz",line_vz->text());
                    }
                }
                stream_U << "\t" << face << " {\n\t\ttype\t";
                stream_P << "\t" << face << " {\n\t\ttype\t";
                stream_K << "\t" << face << " {\n\t\ttype\t";
                stream_E << "\t" << face << " {\n\t\ttype\t";
                stream_N << "\t" << face << " {\n\t\ttype\t";

                if(type == 0){ //inflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow) << "; }" << endl;
                    }
                    stream_K << "fixedValue;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.0001; }" << endl;
                    stream_E << "fixedValue;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\tzeroGradient; }" << endl;
                } else if(type == 1){ //outflow
                    stream_U << "zeroGradient; }" << endl;
                    stream_K << "zeroGradient; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedValue;\n\t\tvalue\tuniform\t0; }" << endl;
                } else if(type == 3) { //overflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow*(-1)) << "; }" << endl;
                    }
                    stream_K << "zeroGradient; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedValue;\n\t\tvalue\tuniform\t0; }" << endl;
                } else if(type == 5) { //atmosphere
                    stream_U << "noSlip; }" << endl;
                    stream_K << "kqRWallFunction;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.00001; }" << endl;
                    stream_E << "epsilonWallFunction;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "nutkWallFunction;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\t\tzeroGradient; }" << endl;
                } else { //Wall //deflector not implemented so get these values
                    stream_U << "noSlip; }" << endl;
                    stream_K << "kqRWallFunction;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.00001; }" << endl;
                    stream_E << "epsilonWallFunction;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "nutkWallFunction;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "\tzeroGradient; }" << endl;
                }
            }
        }
        stream_U << "}" << endl;
        stream_P << "}" << endl;
        stream_E << "}" << endl;
        stream_K << "}" << endl;
        stream_N << "}" << endl;
    }

    //createAndInitializeDir1();
}*/

void CfdWidget::saveSettingsBoundaries(){
    int solverId = settings->value("SolverSettings/id").toInt();
    if (solverId == 0) { //driftflux
        saveBoundariesDriftFlux();
        createAndInitializeDir1();
    } else if (solverId == 1){ //ASM1
        saveBoundariesASM1();
    } else if (solverId == 2){ //pimple
        saveBoundariesPimple();
        createAndInitializeDir1();
    } else {}
}

QWidget * CfdWidget::tabASM1timestamp(){
    QGridLayout* layout = new QGridLayout();
    layout->addWidget(new QLabel("Select:"), 0, 0);
    layout->addWidget(comboTimesteps, 0, 1);

    QWidget* tab = new QWidget();
    tab->setLayout(layout);

    tab->setMaximumHeight(75);

    return tab;
}

QWidget * CfdWidget::tabASM1kinetics(){
    line_kh = new QLineEdit();
    line_kx = new QLineEdit();
    line_nh = new QLineEdit();
    line_muh = new QLineEdit();
    line_ng = new QLineEdit();
    line_ks = new QLineEdit();
    line_bh = new QLineEdit();
    line_koh = new QLineEdit();
    line_kno = new QLineEdit();
    line_knhh = new QLineEdit();
    line_mua = new QLineEdit();
    line_ba = new QLineEdit();
    line_qam = new QLineEdit();
    line_koa = new QLineEdit();
    line_knha = new QLineEdit();

    QGridLayout* layout = new QGridLayout();
    layout->addWidget(new QLabel("kh:"), 0, 0);
    layout->addWidget(line_kh, 0, 1);
    layout->addWidget(new QLabel("kx:"));
    layout->addWidget(line_kx);
    layout->addWidget(new QLabel("nh:"));
    layout->addWidget(line_nh);
    layout->addWidget(new QLabel("muh:"));
    layout->addWidget(line_muh);
    layout->addWidget(new QLabel("ng:"));
    layout->addWidget(line_ng);
    layout->addWidget(new QLabel("ks:"));
    layout->addWidget(line_ks);
    layout->addWidget(new QLabel("bh:"));
    layout->addWidget(line_bh);
    layout->addWidget(new QLabel("koh:"));
    layout->addWidget(line_koh);
    layout->addWidget(new QLabel("kno:"));
    layout->addWidget(line_kno);
    layout->addWidget(new QLabel("knhh:"));
    layout->addWidget(line_knhh);
    layout->addWidget(new QLabel("mua:"));
    layout->addWidget(line_mua);
    layout->addWidget(new QLabel("ba:"));
    layout->addWidget(line_ba);
    layout->addWidget(new QLabel("qam:"));
    layout->addWidget(line_qam);
    layout->addWidget(new QLabel("koa:"));
    layout->addWidget(line_koa);
    layout->addWidget(new QLabel("knha:"));
    layout->addWidget(line_knha);
    //layout->addWidget(btnApply);

    QWidget* tab = new QWidget();
    tab->setLayout(layout);
    //tab->setMinimumHeight(1750);
    return tab;
}

void CfdWidget::copyPath(QString src, QString dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    QDir dir2(dst);
    if (dir2.exists())
        dir2.removeRecursively();
    dir2.mkpath(dir2.path());

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src + QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

QString CfdWidget::createNextASM1Timestep(){
    double nextTS = selectedTimestep.toDouble() + 0.1;
    QString location = QString::number(nextTS);
    QString path = Globals::instance()->getAppProjectPath();

    copyPath(path + selectedTimestep, path + location);

    return location;
}

QString CfdWidget::generateHeader(QString field, QString location){
    QString header;
    header = "/***** File generated by HydroSludge *****/\n";
    header += "FoamFile{\n";
    header += "\tversion\t2.0;\n";
    header += "\tclass\tvolScalarField;\n";
    //header += "\tarch\t\'LSB;label=32;scalar=64\';\n";
    header += "\tformat\tascii;\n";
    header += "\tobject\t" + field + ";\n";
    header += "\tlocation\t" + location + ";\n}\n";

    header += "#include \"../IncludeInitASM1\"\n";
    header += "dimensions\t[0 0 0 0 0 0 0];\n";
    header += "internalField\tuniform\t$init_" + field + ";\n";
    header += "boundaryField {\n";

    return header;
}

void CfdWidget::saveBoundariesDriftFlux(){
    QFile file_U(Globals::instance()->getAppProjectPath() + "/0/U");
    QFile file_P(Globals::instance()->getAppProjectPath() + "/0/p_rgh");
    QFile file_A(Globals::instance()->getAppProjectPath() + "/0/alpha.sludge");
    QFile file_E(Globals::instance()->getAppProjectPath() + "/0/epsilon");
    QFile file_K(Globals::instance()->getAppProjectPath() + "/0/k");
    QFile file_N(Globals::instance()->getAppProjectPath() + "/0/nut");

    if(file_U.open(QFile::WriteOnly | QFile::Text) &&
            file_P.open(QFile::WriteOnly | QFile::Text) &&
            file_A.open(QFile::WriteOnly | QFile::Text) &&
            file_E.open(QFile::WriteOnly | QFile::Text) &&
            file_K.open(QFile::WriteOnly | QFile::Text) &&
            file_N.open(QFile::WriteOnly | QFile::Text)) {

        QTextStream stream_U(&file_U);
        QTextStream stream_P(&file_P);
        QTextStream stream_A(&file_A);
        QTextStream stream_E(&file_E);
        QTextStream stream_K(&file_K);
        QTextStream stream_N(&file_N);

        stream_U << "/***** File generated by Hydrodeca *****/" << endl;
        stream_U << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolVectorField;\n\tformat\tascii;\n\tobject\tU;\n}\n"
                    "dimensions\t[0 1 -1 0 0 0 0];\n"
                    "internalField\tuniform\t(0 0 0);\n\n"
                    "boundaryField {" << endl;

        stream_P << "/***** File generated by Hydrodeca *****/" << endl;
        stream_P << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tp_rgh;\n}\n"
                    "dimensions\t[1 -1 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        stream_A << "/***** File generated by Hydrodeca *****/" << endl;
        stream_A << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\talpha.sludge;\n}\n"
                    "dimensions\t[0 0 0 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        stream_E << "/***** File generated by Hydrodeca *****/" << endl;
        stream_E << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tepsilon;\n}\n"
                    "dimensions\t[0 2 -3 0 0 0 0];\n"
                    "internalField\tuniform\t1.50919e-07;\n\n"
                    "boundaryField {" << endl;

        stream_K << "/***** File generated by Hydrodeca *****/" << endl;
        stream_K << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tk;\n}\n"
                    "dimensions\t[0 2 -2 0 0 0 0];\n"
                    "internalField\tuniform\t0.0000015;\n\n"
                    "boundaryField {" << endl;

        stream_N << "/***** File generated by Hydrodeca *****/" << endl;
        stream_N << "FoamFile{\n"
                    "\tversion\t2.0;\n\tclass\tvolScalarField;\n\tformat\tascii;\n\tobject\tnut;\n\tlocation\t\"0\";\n}\n"
                    "dimensions\t[0 2 -1 0 0 0 0];\n"
                    "internalField\tuniform\t0;\n\n"
                    "boundaryField {" << endl;

        settingsUser->setValue("CfdBoundaries/cnt", layoutAreaBoundaries->count());
        for (int i = 0; i < layoutAreaBoundaries->count(); ++i) {
            QWidget *widget_boundary = layoutAreaBoundaries->itemAt(i)->widget();
            if (widget_boundary != NULL && widget_boundary->objectName() == "boundary_box") {
                QComboBox *combo_type = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_type");
                QComboBox *combo_faces = (QComboBox*)widget_boundary->findChild<QWidget*>("combo_faces");
                int type = combo_type->currentIndex();
                QString face = combo_faces->currentText();
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_type", QString::number(type));
                settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_faces", face);

                int mode;
                float sludge, flow, vx, vy, vz;
                if(type == 0 || type == 1){ //inflow or outflow
                    QWidget *widget_options = widget_boundary->findChild<QWidget*>("options_box");
                    QComboBox *combo_mode = widget_options->findChild<QComboBox*>("combo_mode");
                    mode = combo_mode->currentIndex();
                    settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_mode", QString::number(mode));
                    if (combo_mode->currentIndex() == 0) {//flow
                        QLineEdit *line_flow = widget_options->findChild<QLineEdit*>("line_flow");
                        flow = line_flow->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_flow",line_flow->text());
                    } else { //velocity
                        QLineEdit *line_vx = widget_options->findChild<QLineEdit*>("line_vx");
                        QLineEdit *line_vy = widget_options->findChild<QLineEdit*>("line_vy");
                        QLineEdit *line_vz = widget_options->findChild<QLineEdit*>("line_vz");
                        vx=line_vx->text().toFloat();
                        vy=line_vy->text().toFloat();
                        vz=line_vz->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vx",line_vx->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vy",line_vy->text());
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_vz",line_vz->text());
                    }
                    if(type==0){ //inflow
                        QLineEdit *line_sludge = widget_options->findChild<QLineEdit*>("line_sludge");
                        sludge=line_sludge->text().toFloat();
                        settingsUser->setValue("CfdBoundaries/b" + QString::number(i) + "_sludge",line_sludge->text());
                    }

                }

                stream_U << "\t" << face << " {\n\t\ttype\t";
                stream_P << "\t" << face << " {\n\t\ttype\t";
                stream_A << "\t" << face << " {\n\t\ttype\t";
                stream_K << "\t" << face << " {\n\t\ttype\t";
                stream_E << "\t" << face << " {\n\t\ttype\t";
                stream_N << "\t" << face << " {\n\t\ttype\t";

                if(type == 0){ //inflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow) << "; }" << endl;
                    }
                    stream_A << "fixedValue;" << endl;
                    stream_A << "\t\tvalue\tuniform\t" << QString::number(sludge) << "; }" << endl;
                    stream_K << "fixedValue;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.0001; }" << endl;
                    stream_E << "fixedValue;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }" << endl;

                } else if(type == 1){ //outflow
                    if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow*(-1)) << "; }" << endl;
                    }
                    stream_A << "zeroGradient; }" << endl;
                    stream_K << "zeroGradient; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "zeroGradient; }" << endl;
                } else if(type == 3) { //overflow
                    /*if(mode == 1){ //velocity
                        stream_U << "fixedValue;" << endl;
                        stream_U << "\t\tvalue\tuniform\t(" << QString::number(vx) << " " << QString::number(vy) << " " << QString::number(vz) << "); }" << endl;
                    } else { //flow
                        stream_U << "flowRateInletVelocity;" << endl;
                        stream_U << "\t\tvolumetricFlowRate\t" << QString::number(flow*(-1)) << "; }" << endl;
                    }*/
                    stream_U << "zeroGradient; }" << endl;
                    stream_A << "zeroGradient; }" << endl;
                    stream_K << "zeroGradient; }" << endl;
                    //stream_K << "fixedValue;" << endl;
                    //stream_K << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_E << "zeroGradient; }" << endl;
                    stream_N << "calculated;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    //stream_P << "fixedValue;\n\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }" << endl;
                } else if(type == 5) { //atmosphere
                    stream_U << "slip; }" << endl;
                    stream_A << "slip; }" << endl;
                    //stream_K << "kqRWallFunction;" << endl;
                    //stream_K << "\t\tvalue\tuniform\t0.0001; }" << endl;
                    stream_K << "slip; }" << endl;
                    //stream_E << "epsilonWallFunction;" << endl;
                    //stream_E << "\t\tvalue\tuniform\t0.000001; }" << endl;
                    stream_E << "slip; }" << endl;
                    //stream_N << "nutkWallFunction;" << endl;
                    //stream_N << "\t\tCmu\t0.09;" << endl;
                    //stream_N << "\t\tkappa\t0.41;" << endl;
                    //stream_N << "\t\tE\t9.8;" << endl;
                    //stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_N << "slip; }" << endl;
                    stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }" << endl;
                } else { //wall || deflector
                    stream_U << "fixedValue;" << endl;
                    stream_U << "\t\tvalue\tuniform\t(0 0 0); }" << endl;
                    stream_A << "zeroGradient; }" << endl;
                    stream_K << "kqRWallFunction;" << endl;
                    stream_K << "\t\tvalue\tuniform\t0.0001; }" << endl;
                    stream_E << "epsilonWallFunction;" << endl;
                    stream_E << "\t\tvalue\tuniform\t0.00001; }" << endl;
                    stream_N << "nutkWallFunction;" << endl;
                    stream_N << "\t\tCmu\t0.09;" << endl;
                    stream_N << "\t\tkappa\t0.41;" << endl;
                    stream_N << "\t\tE\t9.8;" << endl;
                    stream_N << "\t\tvalue\tuniform\t0; }" << endl;
                    stream_P << "fixedFluxPressure;\n\t\tvalue\tuniform\t0; }" << endl;
                }
            }
        }
        stream_U << "}" << endl;
        stream_P << "}" << endl;
        stream_A << "}" << endl;
        stream_E << "}" << endl;
        stream_K << "}" << endl;
        stream_N << "}" << endl;
    }
}

void CfdWidget::createAndInitializeDir1(){
    QString path = Globals::instance()->getAppProjectPath();

    QDir dstDir(path + "1");
    dstDir.removeRecursively();

    copyPath(path + "0", dstDir.path());

    QString meshDir = "0.3/";
    if (!QFile::exists(path + "0.3"))
        meshDir = "0.2/";
    dstDir.~QDir();
    new (&dstDir) QDir(path + "1/polyMesh");
    dstDir.removeRecursively();
    //dstDir.mkpath(dstDir.path());

    QDir srcDir(path + meshDir + "/polyMesh");
    copyPath(srcDir.path(), dstDir.path());
}

void CfdWidget::removeBoundary(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());
    delete w;
}

void CfdWidget::slotOnModeBoundChanged(){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget* w = qobject_cast<QWidget *>(combo->parent());

    QList<QWidget*> childWidgets = w->findChildren<QWidget*>();
    foreach(QWidget* widget, childWidgets)
        if ((widget->parentWidget() == w) && (widget != combo))
            delete widget;

    onModeBoundChanged(combo->currentIndex(), -1, combo->parentWidget());
}

void CfdWidget::onModeBoundChanged(int mode_index, int bound_index, QWidget *w){
    int size_line = 30;
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << bound_index << endl;

    if(mode_index == 1){
        QLineEdit *line_vx = new QLineEdit();
        line_vx->setFixedWidth(size_line);
        line_vx->setObjectName("line_vx");
        line_vx->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_vx", "0").toString());
        QLineEdit *line_vy = new QLineEdit();
        line_vy->setFixedWidth(size_line);
        line_vy->setObjectName("line_vy");
        line_vy->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_vy", "0").toString());
        QLineEdit *line_vz = new QLineEdit();
        line_vz->setFixedWidth(size_line);
        line_vz->setObjectName("line_vz");
        line_vz->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_vz", "0").toString());
        w->layout()->addWidget(new QLabel("X"));
        w->layout()->addWidget(line_vx);
        w->layout()->addWidget(new QLabel("Y"));
        w->layout()->addWidget(line_vy);
        w->layout()->addWidget(new QLabel("Z"));
        w->layout()->addWidget(line_vz);
        w->layout()->addWidget(new QLabel("m/s"));
    } else if (mode_index == 0){
        QLineEdit *line_flow = new QLineEdit();
        line_flow->setObjectName("line_flow");
        line_flow->setFixedWidth(size_line*2);
        line_flow->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_flow", "0.0").toString());
        w->layout()->addWidget(line_flow);
        w->layout()->addWidget(new QLabel("m3/s"));
    }
}

void CfdWidget::slotOnBoundTypeChanged(int type_index){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget *w = qobject_cast<QWidget *>(combo->parent());
    QWidget *wOptions = w->findChild<QWidget*>("options_box");
    qDeleteAll(wOptions->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));

    if(type_index == 0 || type_index == 1 || type_index == 3)
        onBoundTypeChanged(true, -1, type_index, wOptions);
}

void CfdWidget::onBoundTypeChanged(bool notLoad, int bound_index, int type_index, QWidget *wOptions){
    QVBoxLayout *layout_options = (QVBoxLayout*)wOptions->layout();

    if ((type_index == 0)){
        if(settings->value("SolverSettings/id").toInt() == 1){ //ASM1Foam
            QWidget *widget_ASM1_inflow = new QWidget();
            QGridLayout *layout_ASM1_inflow = new QGridLayout();
            widget_ASM1_inflow->setLayout(layout_ASM1_inflow);
            QLineEdit *line_so = new QLineEdit();
            QLineEdit *line_ss = new QLineEdit();
            QLineEdit *line_xba = new QLineEdit();
            QLineEdit *line_xbh = new QLineEdit();
            QLineEdit *line_xi = new QLineEdit();
            QLineEdit *line_si = new QLineEdit();
            QLineEdit *line_xp = new QLineEdit();
            QLineEdit *line_xs = new QLineEdit();
            QLineEdit *line_salk = new QLineEdit();
            QLineEdit *line_snh = new QLineEdit();
            QLineEdit *line_sno = new QLineEdit();
            QLineEdit *line_snd = new QLineEdit();
            QLineEdit *line_xnd = new QLineEdit();

            line_so->setObjectName("line_so");
            line_ss->setObjectName("line_ss");
            line_xba->setObjectName("line_xba");
            line_xbh->setObjectName("line_xbh");
            line_xi->setObjectName("line_xi");
            line_si->setObjectName("line_si");
            line_xp->setObjectName("line_xp");
            line_xs->setObjectName("line_xs");
            line_salk->setObjectName("line_salk");
            line_snh->setObjectName("line_snh");
            line_sno->setObjectName("line_sno");
            line_snd->setObjectName("line_snd");
            line_xnd->setObjectName("line_xnd");

            line_so->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_so", "0").toString());
            line_ss->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_ss", "0").toString());
            line_xba->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xba", "0").toString());
            line_xbh->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xbh", "0").toString());
            line_xi->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xi", "0").toString());
            line_si->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_si", "0").toString());
            line_xp->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xp", "0").toString());
            line_xs->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xs", "0").toString());
            line_salk->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_salk", "0").toString());
            line_snh->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_snh", "0").toString());
            line_sno->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_sno", "0").toString());
            line_snd->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_snd", "0").toString());
            line_xnd->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_xnd", "0").toString());

            layout_ASM1_inflow->addWidget(new QLabel("So (g DQO/m3):"), 0, 0);
            layout_ASM1_inflow->addWidget(line_so, 0, 1);
            layout_ASM1_inflow->addWidget(new QLabel("Ss (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_ss);
            layout_ASM1_inflow->addWidget(new QLabel("Xba (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_xba);
            layout_ASM1_inflow->addWidget(new QLabel("Xbh (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_xbh);
            layout_ASM1_inflow->addWidget(new QLabel("Xi (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_xi);
            layout_ASM1_inflow->addWidget(new QLabel("Si (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_si);
            layout_ASM1_inflow->addWidget(new QLabel("Xp (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_xp);
            layout_ASM1_inflow->addWidget(new QLabel("Xs (g DQO/m3):"));
            layout_ASM1_inflow->addWidget(line_xs);
            layout_ASM1_inflow->addWidget(new QLabel("Salk (mol HCO3/m3:"));
            layout_ASM1_inflow->addWidget(line_salk);
            layout_ASM1_inflow->addWidget(new QLabel("Snh (g N/m3):"));
            layout_ASM1_inflow->addWidget(line_snh);
            layout_ASM1_inflow->addWidget(new QLabel("Sno (g N/m3):"));
            layout_ASM1_inflow->addWidget(line_sno);
            layout_ASM1_inflow->addWidget(new QLabel("Snd (g N/m3):"));
            layout_ASM1_inflow->addWidget(line_snd);
            layout_ASM1_inflow->addWidget(new QLabel("Xnd (g N/m3):"));
            layout_ASM1_inflow->addWidget(line_xnd);

            layout_options->addWidget(widget_ASM1_inflow);
        } else {
            QWidget *widget_value = new QWidget();
            QHBoxLayout *layout_value = new QHBoxLayout();
            widget_value->setLayout(layout_value);

            QComboBox *comboMode = new QComboBox();
            comboMode->setObjectName("combo_mode");
            comboMode->setFixedWidth(75);
            comboMode->addItem("Flow");
            comboMode->addItem("Velocity");
            layout_value->addWidget(comboMode);
            layout_options->addWidget(widget_value);

            int mode_index = 0;
            if(!notLoad){ //it has to be loaded
                mode_index = settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_mode").toInt();
            }
            comboMode->setCurrentIndex(mode_index);
            connect(comboMode, SIGNAL(currentIndexChanged(int)), this, SLOT(slotOnModeBoundChanged()));
            onModeBoundChanged(mode_index, bound_index, widget_value);

            if(settings->value("SolverSettings/id").toInt() == 0){ //driftFluxFoam
                QWidget *widget_sludge = new QWidget();
                QGridLayout *layout_sludge = new QGridLayout();
                widget_sludge->setLayout(layout_sludge);
                QLineEdit *line = new QLineEdit();
                line->setObjectName("line_sludge");
                line->setText(settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_sludge", "0").toString());
                layout_sludge->addWidget(new QLabel("Sludge concentration:"),0 ,0);
                layout_sludge->addWidget(line, 0, 1);
                layout_options->addWidget(widget_sludge);
            }
        }
    } else if (type_index == 1) {
        if ((settings->value("SolverSettings/id").toInt() == 0) || (settings->value("SolverSettings/id").toInt() == 2)){
            QWidget *widget_value = new QWidget();
            QHBoxLayout *layout_value = new QHBoxLayout();
            widget_value->setLayout(layout_value);

            QComboBox *comboMode = new QComboBox();
            comboMode->setObjectName("combo_mode");
            comboMode->setFixedWidth(75);
            comboMode->addItem("Flow");
            comboMode->addItem("Velocity");
            layout_value->addWidget(comboMode);
            layout_options->addWidget(widget_value);

            int mode_index = 0;
            if(!notLoad){ //it has to be loaded
                mode_index = settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_mode").toInt();
            }
            comboMode->setCurrentIndex(mode_index);
            connect(comboMode, SIGNAL(currentIndexChanged(int)), this, SLOT(slotOnModeBoundChanged()));
            onModeBoundChanged(mode_index, bound_index, widget_value);
        }
    } else if (type_index == 3){
        if (settings->value("SolverSettings/id").toInt() == 2){
            QWidget *widget_value = new QWidget();
            QHBoxLayout *layout_value = new QHBoxLayout();
            widget_value->setLayout(layout_value);

            QComboBox *comboMode = new QComboBox();
            comboMode->setObjectName("combo_mode");
            comboMode->setFixedWidth(75);
            comboMode->addItem("Flow");
            comboMode->addItem("Velocity");
            layout_value->addWidget(comboMode);
            layout_options->addWidget(widget_value);

            int mode_index = 0;
            if(!notLoad){ //it has to be loaded
                mode_index = settingsUser->value("CfdBoundaries/b" + QString::number(bound_index) + "_mode").toInt();
            }
            comboMode->setCurrentIndex(mode_index);
            connect(comboMode, SIGNAL(currentIndexChanged(int)), this, SLOT(slotOnModeBoundChanged()));
            onModeBoundChanged(mode_index, bound_index, widget_value);
        }
    }
}

void CfdWidget::slotLoadBoundary(){
    loadBoundary(true, -1);
}

void CfdWidget::loadBoundary(bool notLoad, int index){
    int cnt = settingsBoundaries->value("Types/cnt").toInt();
    QComboBox* comboBoundType = new QComboBox();
    comboBoundType->setObjectName("combo_type");
    comboBoundType->setFixedWidth(100);

    for (int a = 1; a <= cnt; a++)
        comboBoundType->addItem(settingsBoundaries->value("Types/t" + QString::number(a)).toString());

    int type_index = 0;
    if(!notLoad){ //it has to be loaded
        type_index = settingsUser->value("CfdBoundaries/b" + QString::number(index) + "_type").toInt();
    }

    comboBoundType->setCurrentIndex(type_index);
    connect(comboBoundType, SIGNAL(currentIndexChanged(int)), this, SLOT(slotOnBoundTypeChanged(int)));

    QPushButton *btn_remove = new QPushButton();
    connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(removeBoundary()));
    btn_remove->setFixedWidth(20);
    btn_remove->setIcon(QIcon(":/Resources/remove.png"));
    btn_remove->setIconSize(QSize(17,17));

    QComboBox *comboFaces = new QComboBox();
    comboFaces->setObjectName("combo_faces");
    comboFaces->setFixedWidth(125);
    for (int a=0; a < cnt_groups; a++)
        comboFaces->addItem(list_faces_names->at(a));
    comboFaces->addItem("Default");

    if(!notLoad)
        comboFaces->setCurrentText(settingsUser->value("CfdBoundaries/b" + QString::number(index) + "_faces").toString());

    QHBoxLayout *layoutType = new QHBoxLayout();
    layoutType->addWidget(new QLabel("Area name:"));
    layoutType->addWidget(comboBoundType);
    layoutType->addWidget(btn_remove);
    layoutType->addStretch();

    QHBoxLayout *layoutSurface = new QHBoxLayout();
    layoutSurface->addWidget(new QLabel("Surface selection:"));
    layoutSurface->addWidget(comboFaces);
    layoutSurface->addStretch();

    QWidget *wBoundary = new QWidget();
    wBoundary->setObjectName("boundary_box");
    QVBoxLayout *lBoundary = new QVBoxLayout();
    wBoundary->setStyleSheet("background-color: cyan");
    wBoundary->setLayout(lBoundary);
    lBoundary->addLayout(layoutType);
    lBoundary->addLayout(layoutSurface);

    QWidget *wOptions = new QWidget();
    wOptions->setObjectName("options_box");
    QVBoxLayout *lOptions = new QVBoxLayout();
    wOptions->setLayout(lOptions);
    lBoundary->addWidget(wOptions);

    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
    if(type_index == 0 || type_index == 1 || type_index == 3)
        onBoundTypeChanged(notLoad, index, type_index, wOptions);
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    layoutAreaBoundaries->addWidget(wBoundary);

}


void CfdWidget::slotSaveEquipment(){
    double N, W;
    //int combo_index;

    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QWidget* w = qobject_cast<QWidget *>(button->parent());

    QLineEdit *line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_N");
    if (line_value->text() == ""){
        //showEquipmentValidationError("A equipment name is required.");
        return;
    }
    N = line_value->text().toDouble();

    line_value = (QLineEdit*)w->findChild<QWidget*>("line_equip_W");
    if (line_value->text() == ""){
        //showEquipmentValidationError("A equipment name is required.");
        return;
    }
    W = line_value->text().toDouble();

    //combo_index = ((QComboBox*)w->findChild<QWidget*>("combo_equip_point"))->currentIndex();
    QString equipName = ((QLabel*)w->findChild<QWidget*>("label_title"))->text();


    double radius;
    QSettings *setfile_equip = new QSettings(equips_dir + equipName, QSettings::IniFormat);
    radius = setfile_equip->value("radius").toDouble();
    setfile_equip->setValue("Cp", QString::number(2*(N/(1000*3.14*radius))));
    setfile_equip->setValue("Cr", QString::number(2*(W/(1000*3.14*radius))));
    setfile_equip->setValue("N", QString::number(N));
    setfile_equip->setValue("W", QString::number(W));
    //setfile_equip->setValue("point_index", QString::number(combo_index));
}

void CfdWidget::loadEquipment(QString pathfile){
    QLineEdit *lineN = new QLineEdit();
    lineN->setObjectName("line_equip_N");
    QLineEdit *lineW = new QLineEdit();
    lineW->setObjectName("line_equip_W");
    //QComboBox *comboPoint = new QComboBox();

    QSettings *setfile_equip = new QSettings(pathfile, QSettings::IniFormat);

    double p1x, p1y, p1z, p2x, p2y, p2z, cx, cy, cz, radius, width, inclination, azimuth;
    p1x = setfile_equip->value("minx").toDouble();
    p1y = setfile_equip->value("miny").toDouble();
    p1z = setfile_equip->value("minz").toDouble();
    p2x = setfile_equip->value("maxx").toDouble();
    p2y = setfile_equip->value("maxy").toDouble();
    p2z = setfile_equip->value("maxy").toDouble();
    cx = setfile_equip->value("centerx").toDouble();
    cy = setfile_equip->value("centery").toDouble();
    cz = setfile_equip->value("centerz").toDouble();
    radius = setfile_equip->value("radius").toDouble();
    width = setfile_equip->value("width").toDouble();
    inclination = setfile_equip->value("inclination").toDouble();
    azimuth = setfile_equip->value("azimuth").toDouble();

    lineN->setText(setfile_equip->value("C", 1).toString());
    lineW->setText(setfile_equip->value("W", 1).toString());
    //comboPoint->setObjectName("combo_equip_point");
    /*comboPoint->addItem("Reference");
    comboPoint->addItem("Reverse");*/

    QPushButton *btn_update = new QPushButton();
    btn_update->setText("Save");
    connect(btn_update, SIGNAL(clicked(bool)), this, SLOT(slotSaveEquipment()));

    QLabel *labelTitle = new QLabel(setfile_equip->value("name").toString());
    labelTitle->setObjectName("label_title");
    labelTitle->setStyleSheet("font-weight: bold; color: blue");

    int cnt = 0;
    QGridLayout *layout_coordinates = new QGridLayout();
    layout_coordinates->addWidget (labelTitle, cnt, 0, 1, 0, Qt::AlignCenter);
    //layout_coordinates->addWidget (new QLabel("Thrust (N):"), ++cnt, 0, Qt::AlignLeft);
    layout_coordinates->addWidget (new QLabel("Manufacturer Coef.:"), ++cnt, 0, Qt::AlignLeft);
    layout_coordinates->addWidget (lineN, cnt, 1, Qt::AlignLeft);
    layout_coordinates->addWidget (new QLabel("Power (W):"), ++cnt, 0, Qt::AlignLeft);
    layout_coordinates->addWidget (lineW, cnt, 1, Qt::AlignLeft);
    //layout_coordinates->addWidget (new QLabel("Flow intake:"), ++cnt, 0, Qt::AlignLeft);
    //layout_coordinates->addWidget (comboPoint, cnt, 1, Qt::AlignLeft);
    layout_coordinates->addWidget (btn_update, ++cnt, 0, 1, 0, Qt::AlignCenter);

    QWidget *wEquipment = new QWidget();
    //wEquipment->setObjectName("equipment_box");
    wEquipment->setStyleSheet("background-color: white");
    wEquipment->setLayout(layout_coordinates);

    layoutAreaEquipment->addWidget(wEquipment);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    vtkSmartPointer<vtkCylinderSource> source = vtkSmartPointer<vtkCylinderSource>::New();
    source->SetResolution(25);
    source->SetCenter(cx, cy, cz);
    source->SetRadius(radius);
    source->SetHeight(width);
    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(source->GetOutputPort());
    vtkSmartPointer<vtkActor> equipActor = vtkSmartPointer<vtkActor>::New();
    equipActor->SetMapper(mapper);
    equipActor->GetProperty()->SetColor(1,0,0);
    equipActor->GetProperty()->SetOpacity(1);
    equipActor->SetOrigin(cx, cy, cz);
    double tecta = 90-inclination;
    double phi = azimuth-90;
    equipActor->RotateWXYZ(tecta, 1, 0, 0);
    equipActor->RotateWXYZ(phi, 0, 0, 1);
    equipActor->GetProperty()->SetOpacity(0.5);
    vtkViewer->m_renderer->AddActor(equipActor);
    vtkViewer->update();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    std::string strname = setfile_equip->value("name").toString().toStdString();

    vtkSmartPointer<vtkStringArray> labels = vtkSmartPointer<vtkStringArray>::New();
    labels->SetName("labels");

    vtkSmartPointer<vtkPoints> setOfPoints = vtkSmartPointer<vtkPoints>::New();
    vtkIdType pid;
    pid = setOfPoints->InsertNextPoint(p1x, p1y, p1z);
    labels->InsertNextValue(strname + "\nPoint 1");
    //labels->InsertNextValue(strname + "\nPoint " + strPoint1.toStdString());
    pid = setOfPoints->InsertNextPoint(p2x, p2y, p2z);
    labels->InsertNextValue(strname + "\nPoint 2");
    //labels->InsertNextValue(strname + "\nPoint " + strPoint2.toStdString());

    vtkSmartPointer<vtkPolyData> point_poly = vtkSmartPointer<vtkPolyData>::New();
    point_poly->SetPoints(setOfPoints);
    point_poly->GetPointData()->AddArray(labels);

    vtkSmartPointer<vtkPointSetToLabelHierarchy> pointSetToLabelHierarchyFilter = vtkSmartPointer<vtkPointSetToLabelHierarchy>::New();
    pointSetToLabelHierarchyFilter->SetInputData(point_poly);
    pointSetToLabelHierarchyFilter->SetLabelArrayName("labels");
    pointSetToLabelHierarchyFilter->GetTextProperty()->SetColor(0,0,0);
    pointSetToLabelHierarchyFilter->GetTextProperty()->SetFontSize(16);
    //pointSetToLabelHierarchyFilter->GetTextProperty()->BoldOn();

    double labelRatio = 0.05;
    int iteratorType = vtkLabelHierarchy::QUEUE;
    vtkSmartPointer<vtkLabelPlacementMapper> labelMapper = vtkSmartPointer<vtkLabelPlacementMapper>::New();
    labelMapper->SetInputConnection(pointSetToLabelHierarchyFilter->GetOutputPort());
    labelMapper->SetIteratorType(iteratorType);
    labelMapper->SetMaximumLabelFraction(labelRatio);
    vtkSmartPointer<vtkActor2D> ActorText = vtkSmartPointer<vtkActor2D>::New();
    ActorText->SetMapper(labelMapper);
    //vtkViewer->m_renderer->AddActor2D(ActorText);
    //vtkViewer->update();
    */
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    vtkSmartPointer<vtkArrowSource> arrowSource = vtkSmartPointer<vtkArrowSource>::New();
    //arrowSource->SetShaftRadius(0.05);
    //arrowSource->SetTipRadius(0.1);
    //arrowSource->SetTipLength(12.3);

    // Generate a random start and end point
    double startPoint[3], endPoint[3];

    if (comboPoint->currentIndex() == 0){
        startPoint[0] = p1x;
        startPoint[1] = p1y;
        startPoint[2] = p1z;
        endPoint[0] = p2x;
        endPoint[1] = p2y;
        endPoint[2] = p2z;
    } else {
        startPoint[0] = p2x;
        startPoint[1] = p2y;
        startPoint[2] = p2z;
        endPoint[0] = p1x;
        endPoint[1] = p1y;
        endPoint[2] = p1z;
    }

    // Compute a basis
    double normalizedX[3];
    double normalizedY[3];
    double normalizedZ[3];

    // The X axis is a vector from start to end
    vtkMath::Subtract(endPoint, startPoint, normalizedX);
    double length = vtkMath::Norm(normalizedX);
    vtkMath::Normalize(normalizedX);

    // The Z axis is an arbitrary vector cross X
    double arbitrary[3];
    arbitrary[0] = vtkMath::Random(-10,10);
    arbitrary[1] = vtkMath::Random(-10,10);
    arbitrary[2] = vtkMath::Random(-10,10);
    vtkMath::Cross(normalizedX, arbitrary, normalizedZ);
    vtkMath::Normalize(normalizedZ);

    // The Y axis is Z cross X
    vtkMath::Cross(normalizedZ, normalizedX, normalizedY);
    vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();

    // Create the direction cosine matrix
    matrix->Identity();
    for (unsigned int i = 0; i < 3; i++)
    {
        matrix->SetElement(i, 0, normalizedX[i]);
        matrix->SetElement(i, 1, normalizedY[i]);
        matrix->SetElement(i, 2, normalizedZ[i]);
    }

    // Apply the transforms
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->Translate(startPoint);
    transform->Concatenate(matrix);
    transform->Scale(length, length, length);

    // Transform the polydata
    vtkSmartPointer<vtkTransformPolyDataFilter> transformPD = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformPD->SetTransform(transform);
    transformPD->SetInputConnection(arrowSource->GetOutputPort());

    //Create a mapper and actor for the arrow
    vtkSmartPointer<vtkPolyDataMapper> mapperArrow = vtkSmartPointer<vtkPolyDataMapper>::New();
    vtkSmartPointer<vtkActor> actorArrow = vtkSmartPointer<vtkActor>::New();
    //Arrow inside the cylinder
    //mapper2->SetInputConnection(arrowSource->GetOutputPort());
    //actor->SetUserMatrix(transform->GetMatrix());

    //Arrow outside the cylinder
    mapperArrow->SetInputConnection(transformPD->GetOutputPort());

    actorArrow->SetMapper(mapperArrow);
    actorArrow->GetProperty()->SetColor(0,0,0);
    actorArrow->SetScale(1.1);

    /*

    vtkSmartPointer<vtkPolyDataMapper> mapper2 = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper2->SetInputConnection(arrowSource->GetOutputPort());
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper2);
    actor->GetProperty()->SetColor(0,1,0);
    //actor->GetProperty()->SetOpacity(0.5);
    //actor->SetOrigin(p1x, p1y, p1z);
    //actor->SetScale(5.5);

    vtkViewer->m_renderer->AddActor(actorArrow);
    vtkViewer->update();*/
}

void CfdWidget::equipmentsConfig(){
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    vtkViewer->reset();
    QString filename = Globals::instance()->getAppProjectPath() + "constant/triSurface/cad.stl";
    vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(filename.toStdString().c_str());
    reader->Update();

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(reader->GetOutput());
    vtkSmartPointer<vtkLODActor> actor = vtkSmartPointer<vtkLODActor>::New();
    actor->SetMapper(mapper);
    //actor->GetProperty()->SetColor(0.54,0.46,0.46);
    actor->GetProperty()->SetOpacity(0.5);
    vtkViewer->m_renderer->AddActor(actor);
    vtkViewer->update();
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    layoutAreaEquipment = new QVBoxLayout();
    QDirIterator it(equips_dir, QStringList() << "*", QDir::Files);
    while (it.hasNext()){
        QString strEquip = it.next();
        //qDebug() << strEquip;
        loadEquipment(strEquip);
    }

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->addLayout(layoutAreaEquipment);
    //layoutAreaActions->addWidget(btnAddBound);
    //layoutAreaActions->addWidget(btnApply);
    layoutAreaActions->addStretch();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

}

void CfdWidget::boundaryConditions(){
    settingsBoundaries = new QSettings(Globals::instance()->getAppPath() + "/settings/boundaries.ini", QSettings::IniFormat);
    layoutAreaBoundaries = new QVBoxLayout();

    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSettingsBoundaries()));

    QPushButton* btnAddBound = new QPushButton("Add new boundary");
    btnAddBound->setFixedWidth(125);
    connect(btnAddBound, SIGNAL(clicked(bool)), this, SLOT(slotLoadBoundary()));

    QVBoxLayout* layoutAreaActions = new QVBoxLayout();
    layoutAreaActions->addLayout(layoutAreaBoundaries);
    layoutAreaActions->addWidget(btnAddBound);
    layoutAreaActions->addWidget(btnApply);
    layoutAreaActions->addStretch();

    loadSettingsBoundaries();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layoutAreaActions);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);

    showGeometryFaces();
    vtkViewer->add(legend);
    vtkViewer->update();
    //vtkViewer->resetCamera();
}

void CfdWidget::createControlDictFile(){
    QFile file(Globals::instance()->getAppProjectPath()+"/system/controlDict");

    if (file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);

        stream << "/***** File generated by Hydrodeca *****/" << endl;
        stream << "FoamFile {\n"
                  "\tversion\t2.0;\n\tformat\tascii;\n\tclass\tdictionary;\n\tlocation\t\"system\";\n\tobject\tcontrolDict; }" << endl;

        stream << "startFrom\tlatestTime;" << endl;
        stream << "startTime\t0;" << endl;
        stream << "stopAt\tendTime;" << endl;
        stream << "endTime\t" << line_timesteps->text() << ";" << endl;
        stream << "deltaT\t0.02;" << endl;
        stream << "writeControl\tadjustableRunTime;" << endl;
        stream << "writeInterval\t" << line_write->text() << ";" << endl;
        stream << "purgeWrite\t0;" << endl;
        stream << "writeFormat\tascii;" << endl;
        stream << "writePrecision\t6;" << endl;
        stream << "writeCompression\tuncompressed;" << endl;
        stream << "timeFormat\tgeneral;" << endl;
        stream << "timePrecision\t6;" << endl;
        stream << "runTimeModifiable\tyes;" << endl;
        stream << "adjustTimeStep\ton;" << endl;
        stream << "maxCo\t5;" << endl;
        //stream << "maxDeltaT\t1;" << endl;

        file.flush();
        file.close();
    }
}



void CfdWidget::asm1Foam(){
    createControlDictFile();

    QFile file(Globals::instance()->getAppProjectPath()+"/system/controlDict");
    if (file.open(QFile::WriteOnly | QFile::Text | QFile::Append)) {
        QTextStream stream(&file);
        stream << "application\tmy_ASM1Foam;" << endl;

        file.flush();
        file.close();
    }

    copyASM1FoamFiles();

    startASM1Foam();

    menu->goToResultsView(true);
}

void CfdWidget::removeDirs(QStringList excludeList){
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList dirList = dir.entryList();

    foreach(QString filename, dirList){
        if(! excludeList.contains(filename)){
            QDir aux = QDir(Globals::instance()->getAppProjectPath() + filename);
            aux.removeRecursively();
        }

    }
}

void CfdWidget::pimpleFoam(){
    createControlDictFile();

    QFile file(Globals::instance()->getAppProjectPath()+"/system/controlDict");
    if (file.open(QFile::WriteOnly | QFile::Text | QFile::Append)) {
        QTextStream stream(&file);
        stream << "application\tpimpleFoam;" << endl;
        stream << "libs (\n\t\"libOpenFOAM.so\"\n\t\"libsimpleSwakFunctionObjects.so\"\n\t\"libswakFunctionObjects.so\"\n\t\"libgroovyBC.so\"\n\t\"libswakSourceFields.so\"\n\t\"libswakFvOptions.so\"\n\t);" << endl;

        file.flush();
        file.close();
    }

    QStringList list_excluded_dirs;
    list_excluded_dirs << "1" << "sludge_profile" << "processor" << "0" << "profiles" << "constant" << "equipments" << "system" << "0.1" << "0.2" << "0.3";
    removeDirs(list_excluded_dirs);

    copyPimpleFoamFiles();
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    QStringList dirList = dir.entryList();
    if(dirList.contains("equipments")){
        createFVOptionsFile();
    }

    //createTurbulencePropFile();

    startPimpleFoam();

    menu->goToResultsView(true);
}

void CfdWidget::copyASM1FoamFiles(){
    QString file_name;
    QString path_set = Globals::instance()->getAppPath() + "/settings/filesSolvers/ASM1Foam/";
    QString path_proj = Globals::instance()->getAppProjectPath();

    file_name = "constant/diffusionProperties" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSchemes" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSolution";
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/ASM1fvSolution";
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
}

void CfdWidget::copyPimpleFoamFiles(){
    QString file_name;
    QString path_set = Globals::instance()->getAppPath() + "/settings/filesSolvers/pimpleFoam/";
    QString path_proj = Globals::instance()->getAppProjectPath();

    file_name = "constant/turbulenceProperties" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSchemes" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSolution";
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
}

void CfdWidget::copyDriftFluxFoamFiles(){
    QString file_name;
    QString path_set = Globals::instance()->getAppPath() + "/settings/filesSolvers/driftFluxFoam/";
    QString path_proj = Globals::instance()->getAppProjectPath();

    file_name = "constant/turbulenceProperties" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSchemes" ;
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;

    file_name = "system/fvSolution";
    if (QFile::exists(path_proj + file_name))
        QFile::remove(path_proj + file_name);
    if (!QFile::copy(path_set + file_name, path_proj + file_name))
        qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << endl;
}

void CfdWidget::runTopoSet(){
    QProcess *processTopoSet = new QProcess();
    //connect(processSetFields, SIGNAL(started()), this, SLOT(setFieldsStarted()));
    //connect(processSetFields,SIGNAL(readyReadStandardOutput()),this,SLOT(setFieldsOutput()));
    //connect(processSetFields, SIGNAL(finished(int)), this, SLOT(setFieldsFinished()));
    processTopoSet->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processTopoSet->setProcessChannelMode(QProcess::MergedChannels);
    processTopoSet->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/topoSet.output");

    QString script = "topoSet.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "topoSet" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;

    processTopoSet->start(POWERSHELL, commands.split(" "));
    processTopoSet->waitForFinished();
}

void CfdWidget::driftFluxFoam(){

    QFile file(Globals::instance()->getAppProjectPath()+"/system/controlDict");

    if (file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);

        stream << "/***** File generated by Hydrodeca *****/" << endl;
        stream << "FoamFile {\n"
                  "\tversion\t2.0;\n\tformat\tascii;\n\tclass\tdictionary;\n\tlocation\t\"system\";\n\tobject\tcontrolDict; }" << endl;
        stream << "application\tdriftFluxFoam;" << endl;
        stream << "startFrom\tlatestTime;" << endl;
        stream << "startTime\t0;" << endl;
        stream << "stopAt\tendTime;" << endl;
        stream << "endTime\t" << line_timesteps->text() << ";" << endl;
        stream << "deltaT\t1;" << endl;
        stream << "writeControl\tadjustableRunTime;" << endl;
        stream << "writeInterval\t" << line_write->text() << ";" << endl;
        stream << "purgeWrite\t0;" << endl;
        stream << "writeFormat\tascii;" << endl;
        stream << "writePrecision\t6;" << endl;
        stream << "writeCompression\tuncompressed;" << endl;
        stream << "timeFormat\tgeneral;" << endl;
        stream << "timePrecision\t6;" << endl;
        stream << "runTimeModifiable\tyes;" << endl;
        stream << "adjustTimeStep\ton;" << endl;
        stream << "maxCo\t5;" << endl;
        //stream << "maxDeltaT\t1;" << endl;

        file.flush();
        file.close();
    }

    copyDriftFluxFoamFiles();

    // Borramos las carpetas temporales creadas anteriormente
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList();

    foreach(QString file, dirList){
        if(file.compare("equipments") != 0 && file.compare("0") != 0 && file.compare(".") != 0 && file.compare("..") != 0 && file.compare("constant") != 0 && file.compare("system") != 0 && file.compare("0.1") != 0 && file.compare("0.2") != 0 && file.compare("0.3") != 0 && file.compare("1") != 0){
            QDir aux = QDir(Globals::instance()->getAppProjectPath() + "\\" + file);
            aux.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

            aux.removeRecursively();
        }
    }

    startDriftFluxFoam();

    menu->goToResultsView(true);
}

void CfdWidget::loadSettingsInitialize(){
    line_height->setText(settings->value("CfdInitialize/height", "3.0").toString());
    line_sludge->setText(settings->value("CfdInitialize/sludge", "0.002").toString());
}

void CfdWidget::saveSettingsInitialize(){
    settings->setValue("CfdInitialize/height", line_height->text());
    settings->setValue("CfdInitialize/sludge", line_sludge->text());
}

void CfdWidget::showInitilizeTab(){
    if(settings->value("SolverSettings/id").toInt() == 0){
        initDriftFlux();
    } else if(settings->value("SolverSettings/id").toInt() == 1){
        initASM1();
    } else {
        QVBoxLayout* layout = new QVBoxLayout();
        QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
        areaActionWidget->setLayout(layout);
        centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    }
}

void CfdWidget::initDriftFlux(){
    QLabel* l_height = new QLabel("Height (m)");
    line_height = new QLineEdit;
    line_height->setValidator(Globals::instance()->getDoubleValidator(this));
    line_height->setStyleSheet(Globals::instance()->getCssLineEdit());

    QLabel* l_sludge = new QLabel("Sludge concentration ()");
    line_sludge = new QLineEdit;
    line_sludge->setValidator(Globals::instance()->getDoubleValidator(this));
    line_sludge->setStyleSheet(Globals::instance()->getCssLineEdit());

    QPushButton* btnRun = new QPushButton("Run", this);
    btnRun->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnRun->setMinimumHeight(30);
    connect(btnRun, SIGNAL(clicked()), this, SLOT(saveSettingsInitialize()));
    connect(btnRun, SIGNAL(clicked()), this, SLOT(setFields()));

    QSpacerItem * my_spacer = new QSpacerItem(0,1000, QSizePolicy::Expanding, QSizePolicy::Expanding);
    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(l_height);
    layout->addWidget(line_height);
    layout->addWidget(l_sludge);
    layout->addWidget(line_sludge);
    layout->addWidget(btnRun);
    layout->addSpacerItem(my_spacer);

    loadSettingsInitialize();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void CfdWidget::initASM1(){
    QGridLayout *layout_ASM1_inflow = new QGridLayout();
    line_init_so = new QLineEdit();
    line_init_ss = new QLineEdit();
    line_init_xba = new QLineEdit();
    line_init_xbh = new QLineEdit();
    line_init_xi = new QLineEdit();
    line_init_si = new QLineEdit();
    line_init_xp = new QLineEdit();
    line_init_xs = new QLineEdit();
    line_init_salk = new QLineEdit();
    line_init_snh = new QLineEdit();
    line_init_sno = new QLineEdit();
    line_init_snd = new QLineEdit();
    line_init_xnd = new QLineEdit();

    line_init_so->setText(settingsUser->value("InitializeASM1/so", "0").toString());
    line_init_ss->setText(settingsUser->value("InitializeASM1/ss", "0").toString());
    line_init_xba->setText(settingsUser->value("InitializeASM1/xba", "0").toString());
    line_init_xbh->setText(settingsUser->value("InitializeASM1/xbh", "0").toString());
    line_init_xi->setText(settingsUser->value("InitializeASM1/xi", "0").toString());
    line_init_si->setText(settingsUser->value("InitializeASM1/si", "0").toString());
    line_init_xp->setText(settingsUser->value("InitializeASM1/xp", "0").toString());
    line_init_xs->setText(settingsUser->value("InitializeASM1/xs", "0").toString());
    line_init_salk->setText(settingsUser->value("InitializeASM1/salk", "0").toString());
    line_init_snh->setText(settingsUser->value("InitializeASM1/snh", "0").toString());
    line_init_sno->setText(settingsUser->value("InitializeASM1/sno", "0").toString());
    line_init_snd->setText(settingsUser->value("InitializeASM1/snd", "0").toString());
    line_init_xnd->setText(settingsUser->value("InitializeASM1/xnd", "0").toString());

    int cnt=0;
    layout_ASM1_inflow->addWidget(new QLabel("So (g DQO/m3):"), cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_so, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Ss (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_ss, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xba (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xba, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xbh (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xbh, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xi (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xi, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Si (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_si, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xp (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xp, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xs (g DQO/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xs, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Salk (mol HCO3/m3:"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_salk, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Snh (g N/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_snh, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Sno (g N/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_sno, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Snd (g N/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_snd, cnt, 1);
    layout_ASM1_inflow->addWidget(new QLabel("Xnd (g N/m3):"), ++cnt, 0);
    layout_ASM1_inflow->addWidget(line_init_xnd, cnt, 1);

    QPushButton* btnRun = new QPushButton("Run", this);
    btnRun->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    //btnRun->setMinimumHeight(30);
    connect(btnRun, SIGNAL(clicked()), this, SLOT(saveInitializeASM1()));
    //connect(btnRun, SIGNAL(clicked()), this, SLOT(setFields()));

    layout_ASM1_inflow->addWidget(btnRun, ++cnt, 0, 1, 0);

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout_ASM1_inflow);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
}

void CfdWidget::saveInitializeASM1(){
    settings->setValue("InitializeASM1/so", line_init_so->text());
    settings->setValue("InitializeASM1/ss", line_init_ss->text());
    settings->setValue("InitializeASM1/xba", line_init_xba->text());
    settings->setValue("InitializeASM1/xbh", line_init_xbh->text());
    settings->setValue("InitializeASM1/xi", line_init_xi->text());
    settings->setValue("InitializeASM1/si", line_init_si->text());
    settings->setValue("InitializeASM1/xp", line_init_xp->text());
    settings->setValue("InitializeASM1/xs", line_init_xs->text());
    settings->setValue("InitializeASM1/salk", line_init_salk->text());
    settings->setValue("InitializeASM1/snh", line_init_snh->text());
    settings->setValue("InitializeASM1/sno", line_init_sno->text());
    settings->setValue("InitializeASM1/snd", line_init_snd->text());
    settings->setValue("InitializeASM1/xnd", line_init_xnd->text());

    QString path = Globals::instance()->getAppProjectPath();
    QFile file_include(path + "IncludeInitASM1");
    if(file_include.open(QFile::WriteOnly | QFile::Text)){
        QTextStream stream_include(&file_include);
        stream_include << "init_So\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Ss\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Si\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Salk\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Snh\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Sno\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Snd\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xba\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xbh\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xp\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xi\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xs\t" << line_init_so->text() << ";" << endl;
        stream_include << "init_Xnd\t" << line_init_so->text() << ";" << endl;
    }
}

void CfdWidget::setFields(){
    QPushButton* btnCancel = new QPushButton("CANCEL", this);
    btnCancel->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    connect(btnCancel, SIGNAL(clicked()), this, SLOT(cancelSetFields()));
    progressBarValue=0;
    progressBarProcess = new QProgressDialog(parent);
    progressBarProcess->setLabelText("Initializing sludge fields...");
    progressBarProcess->setFixedWidth(600);
    progressBarProcess->setFixedHeight(100);
    progressBarProcess->setValue(progressBarValue);
    progressBarProcess->setRange(0,100);
    progressBarProcess->setCancelButton(btnCancel);


    double maxBound[3];
    double minBound[3];
    double* bounds = vtkViewer->bounds();
    maxBound[0] = bounds[1];
    maxBound[1] = bounds[3];
    maxBound[2] = bounds[5];

    minBound[0] = bounds[0];
    minBound[1] = bounds[2];
    minBound[2] = bounds[4];

    double height = line_height->text().toDouble();
    double sludge = line_sludge->text().toDouble();

    QString gravity = settings->value("SolverDriftFlux/gravity", "Z").toString();

    if (gravity == "X")
        maxBound[0] = height;
    else if (gravity == "Y")
        maxBound[1] = height;
    else if (gravity == "Z")
        maxBound[2] = height;

    QFile file(Globals::instance()->getAppProjectPath()+"/system/setFieldsDict");

    if (file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);

        stream << "/***** File generated by Hydrodeca *****/" << endl;
        stream << "FoamFile {\n"
                  "\tversion\t2.0;\n\tformat\tascii;\n\tclass\tdictionary;\n\tlocation\t\"system\";\n\tobject\tsetFieldsDict; }" << endl;
        stream << "defaultFieldValues\n(\n\tvolScalarFieldValue alpha.sludge 0\n);" << endl;
        stream << "regions\n(\n\tboxToCell {\n\t\tbox ("
               << minBound[0] << " "
               << minBound[1] << " "
               << minBound[2] << ") ("
               << maxBound[0] << " "
               << maxBound[1] << " "
               << maxBound[2] << ");" << endl;
        stream << "\t\tfieldValues\n\t\t(\n\t\t\tvolScalarFieldValue alpha.sludge " << sludge << "\n\t\t);\n\t}" << endl;
        stream << ");";

        file.flush();
        file.close();
    }
    else{
        QMessageBox::about(this, tr("ERROR SAVE BOUNDARY CONDITIONS"),
                           tr("<p>No se ha podido guardar la configuración. Intentalo de nuevo mas tarde.</p>"));
    }

    // Borramos las carpetas temporales creadas anteriormente
    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QStringList dirList = dir.entryList();

    foreach(QString file, dirList){

        if(file.compare("equipments") != 0 && file.compare("0") != 0 && file.compare(".") != 0 && file.compare("..") != 0 && file.compare("constant") != 0 && file.compare("system") != 0 && file.compare("0.1") != 0 && file.compare("0.2") != 0 && file.compare("0.3") != 0 && file.compare("1") != 0){
            QDir aux = QDir(Globals::instance()->getAppProjectPath() + "\\" + file);
            aux.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
            aux.removeRecursively();
        }
    }

    processSetFields = new QProcess();
    connect(processSetFields, SIGNAL(started()), this, SLOT(setFieldsStarted()));
    connect(processSetFields,SIGNAL(readyReadStandardOutput()),this,SLOT(setFieldsOutput()));
    connect(processSetFields, SIGNAL(finished(int)), this, SLOT(setFieldsFinished()));
    processSetFields->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processSetFields->setProcessChannelMode(QProcess::MergedChannels);
    processSetFields->setStandardOutputFile(Globals::instance()->getAppProjectPath() + "/setFields.output");
    //processSetFields->start("setFields");

    QString script = "setFields.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "setFields" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;

    processSetFields->start(POWERSHELL, commands.split(" "));
}

void CfdWidget::setFieldsStarted(){
    isCancelProcess = false;
}

void CfdWidget::setFieldsOutput(){
    if(progressBarValue < 75) {
        progressBarValue++;
    }

    progressBarProcess->setValue(progressBarValue);

    QString text;
    text.append(processSetFields->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    line->setFlags(Qt::ItemIsEnabled);
    console->addItem(line);
    console->scrollToBottom();
}

void CfdWidget::setFieldsFinished(){
    if(!isCancelProcess){
        vtkViewer->reset();
        vtkViewer->readOpenFoam(1, "alpha.sludge", true, false, false, false);
        vtkViewer->update();
        vtkViewer->resetCamera();
        progressBarProcess->setValue(90);
        //QThread::sleep(2);
        progressBarProcess->setValue(100);
    }
}

void CfdWidget::cancelSetFields(){
    processSetFields->kill();
    isCancelProcess = true;
}

void CfdWidget::loadSettingsSolver(){
    line_timesteps->setText(settings->value("CfdSolver/timesteps", "1000").toString());
    line_write->setText(settings->value("CfdSolver/write", "2").toString());

    /*
            line_turbulence->setCurrentText(settings->value("CfdSolver/turbulence", "k-Epsilon").toString());
            line_lower_tol->setText(settings->value("CfdSolver/lower", "1e-07").toString());
            line_upper_tol->setText(settings->value("CfdSolver/upper", "1e-06").toString());
            line_correctors->setText(settings->value("CfdSolver/correctors", "3").toString());
            line_non_orth->setText(settings->value("CfdSolver/non-orthogonal", "0").toString());
            */
}

void CfdWidget::saveSettingsSolver(){
    settings->setValue("CfdSolver/timesteps", line_timesteps->text());
    settings->setValue("CfdSolver/write", line_write->text());

    /*
            settings->setValue("CfdSolver/turbulence", line_turbulence->currentText());
            settings->setValue("CfdSolver/lower", line_lower_tol->text());
            settings->setValue("CfdSolver/upper", line_upper_tol->text());
            settings->setValue("CfdSolver/correctors", line_correctors->text());
            settings->setValue("CfdSolver/non-orthogonal", line_non_orth->text());
            */

    //createControlDictFile();
    //createFVOptionsFile();
}

void CfdWidget::createTurbulencePropFile(){
    QString path = Globals::instance()->getAppProjectPath() + "/constant/turbulenceProperties";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"constant\";\n\tobject\tturbulenceProperties;}" << endl;

        stream << "simulationType\tRAS;" << endl;
        stream << "RAS {" << endl;
        stream << "\tRASModel\tkEpsilon;" << endl;
        stream << "\tturbulence\ton;" << endl;
        stream << "\tprintCoeffs\ton; }" << endl;
    }
    file.flush();
    file.close();
}

void CfdWidget::createFVOptionsFile(){
    QString path = Globals::instance()->getAppProjectPath() + "/constant/fvOptions";
    QFile file(path);

    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{"
                  "\n\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;"
                  "\n\tlocation\t\"constant\";\n\tobject\tfvOptions;}" << endl;

        stream << "options {" << endl;

        QString name;
        QString p1x, p1y, p1z, p2x, p2y, p2z, cx, cy, cz, radius, width, ux, uy, uz, n, w;
        QDirIterator it(equips_dir, QStringList() << "*", QDir::Files);
        while (it.hasNext()){
            QSettings *setfile_equip = new QSettings(it.next(), QSettings::IniFormat);
            name = setfile_equip->value("name").toString();
            p1x = setfile_equip->value("minx").toString();
            p1y = setfile_equip->value("miny").toString();
            p1z = setfile_equip->value("minz").toString();
            p2x = setfile_equip->value("maxx").toString();
            p2y = setfile_equip->value("maxy").toString();
            p2z = setfile_equip->value("maxz").toString();
            cx = setfile_equip->value("centerx").toString();
            cy = setfile_equip->value("centery").toString();
            cz = setfile_equip->value("centerz").toString();
            ux = setfile_equip->value("ux").toString();
            uy = setfile_equip->value("uy").toString();
            uz = setfile_equip->value("uz").toString();
            radius = setfile_equip->value("radius").toString();
            width = setfile_equip->value("width").toString();
            n = setfile_equip->value("N").toString();
            w = setfile_equip->value("W").toString();

            stream << "\t" << name << " {" << endl;

            stream << "\t\ttype\tvectorSwakExplicitSource;" << endl;
            stream << "\t\tactive\ttrue;" << endl;
            stream << "\t\tselectionMode\tall;" << endl;
            stream << "\t\tvectorSwakExplicitSourceCoeffs {" << endl;
            stream << "\t\t\tvariables (" << endl;

            stream << "\t\t\t\t\"X=pos().x;\"" << endl;
            stream << "\t\t\t\t\"xp1=" << p1x << ";\"" << endl;
            stream << "\t\t\t\t\"xp2=" << p2x << ";\"" << endl;
            stream << "\t\t\t\t\"xc=" << cx << ";\"" << endl;
            stream << "\t\t\t\t\"ux=" << ux << ";\"" << endl;

            stream << "\t\t\t\t\"Y=pos().y;\"" << endl;
            stream << "\t\t\t\t\"yp1=" << p1y << ";\"" << endl;
            stream << "\t\t\t\t\"yp2=" << p2y << ";\"" << endl;
            stream << "\t\t\t\t\"yc=" << cy << ";\"" << endl;
            stream << "\t\t\t\t\"uy=" << uy << ";\"" << endl;

            stream << "\t\t\t\t\"Z=pos().z;\"" << endl;
            stream << "\t\t\t\t\"zp1=" << p1z << ";\"" << endl;
            stream << "\t\t\t\t\"zp2=" << p2z << ";\"" << endl;
            stream << "\t\t\t\t\"zc=" << cz << ";\"" << endl;
            stream << "\t\t\t\t\"uz=" << uz << ";\"" << endl;

            stream << "\t\t\t\t\"C=" << n << ";\"" << endl;
            stream << "\t\t\t\t\"F=" << w << ";\"" << endl;
            stream << "\t\t\t\t\"D=" << QString::number(radius.toDouble() * 2) << ";\"" << endl;
            stream << "\t\t\t\t\"L=" << width << ";\"" << endl;
            stream << "\t\t\t\t\"rho=1000;\"" << endl;
            stream << "\t\t\t\t\"" << name << "=4*sqr(C)*F/(sqr(D)*3.1416*L*rho);\""<< endl;

            stream << "\t\t\t\t\"vecDir=vector(ux,uy,uz);\"" << endl;
            stream << "\t\t\t\t\"vecDirNorm=vecDir/mag(vecDir);\"" << endl;
            stream << "\t\t\t);" << endl;

            stream << "\t\t\tselectionMode\tall;" << endl;
            stream << "\t\t\texpressions { ";
            stream << "U \"((sqrt((1-sqr(ux))*sqr(X-xc)+(1-sqr(uy))*sqr(Y-yc)+(1-sqr(uz))*sqr(Z-zc))) > (L/2) && sqrt(sqr((yp2-yp1)*(zp1-Z)-(zp2-zp1)*(yp1-Y))+sqr((zp2-zp1)*(xp1-X)-(xp2-xp1)*(zp1-Z))+sqr((xp2-xp1)*(yp1-Y)-(yp2-yp1)*(xp1-X)))/sqrt(sqr(xp2-xp1)+sqr(yp2-yp1)+sqr(zp2-zp1)) > (D/2)) ? vector(0,0,0) : " << name << "*vecDirNorm\" [0 1 -2 0 0 0 0];";

            stream << "}" << endl;
            stream << "\t\t}\n\t}" << endl;
        }
        stream << "}" << endl;

        file.flush();
        file.close();
    }
}

void CfdWidget::showSimulationTab(){
    QLabel* l_end_time = new QLabel("End Time (s)");
    line_timesteps = new QLineEdit;
    line_timesteps->setValidator(Globals::instance()->getDoubleValidator(this));
    line_timesteps->setStyleSheet(Globals::instance()->getCssLineEdit());

    QLabel* l_interval = new QLabel("Write interval (s)");
    line_write = new QLineEdit;
    line_write->setValidator(Globals::instance()->getDoubleValidator(this));
    line_write->setStyleSheet(Globals::instance()->getCssLineEdit());

    /*
            QLabel* l_turb = new QLabel("Turbulence model");
            line_turbulence = new QComboBox(this);
            line_turbulence->setMaximumHeight(30);
            line_turbulence->addItem("k-Epsilon");
            line_turbulence->addItem("k-Omega");
            line_turbulence->setStyleSheet(Globals::instance()->getCssSelect());

            QLabel* l_downTol = new QLabel("Lower tolerance");
            line_lower_tol = new QLineEdit;
            line_lower_tol->setValidator(Globals::instance()->getDoubleValidator(this));
            line_lower_tol->setStyleSheet(Globals::instance()->getCssLineEdit());

            QLabel* l_upTol = new QLabel("Upper tolerance");
            line_upper_tol = new QLineEdit;
            line_upper_tol->setValidator(Globals::instance()->getDoubleValidator(this));
            line_upper_tol->setStyleSheet(Globals::instance()->getCssLineEdit());

            QLabel* l_corr = new QLabel("Number of correctors");
            line_correctors = new QLineEdit;
            line_correctors->setValidator(Globals::instance()->getDoubleValidator(this));
            line_correctors->setStyleSheet(Globals::instance()->getCssLineEdit());

            QLabel* l_nocorr = new QLabel("Number of non-orthogonal correctors");
            line_non_orth = new QLineEdit;
            line_non_orth->setValidator(Globals::instance()->getDoubleValidator(this));
            line_non_orth->setStyleSheet(Globals::instance()->getCssLineEdit());
            */

    QPushButton* btnRun = new QPushButton("RUN", this);
    btnRun->setObjectName("btnRun");
    btnRun->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnRun->setMinimumHeight(30);
    connect(btnRun, SIGNAL(clicked()), this, SLOT(saveSettingsSolver()));

    if(settings->value("SolverSettings/id").toInt() == 0){
        connect(btnRun, SIGNAL(clicked()), this, SLOT(driftFluxFoam()));
    } else if(settings->value("SolverSettings/id").toInt() == 1){
        connect(btnRun, SIGNAL(clicked()), this, SLOT(asm1Foam()));
    } else {
        connect(btnRun, SIGNAL(clicked()), this, SLOT(pimpleFoam()));
    }

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(l_end_time);
    layout->addWidget(line_timesteps,Qt::AlignRight);
    layout->addWidget(l_interval);
    layout->addWidget(line_write,Qt::AlignRight);
    layout->addWidget(btnRun);
    layout->addStretch();

    loadSettingsSolver();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(200);
}

void CfdWidget::loadSettingsPropertiesWWTP(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);

    line_v0->setText(settings->value("Settling/v0", "0.001980").toString());
    line_rh->setText(settings->value("Settling/rh", "1.53").toString());
    line_rp->setText(settings->value("Settling/rt", "5.6").toString());
}

void CfdWidget::loadSolverDriftFlux(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    line_v0->setText(settings->value("SolverDriftFlux/v0", "-0.001980").toString());
    line_rh->setText(settings->value("SolverDriftFlux/rh", "0.899").toString());
    line_rp->setText(settings->value("SolverDriftFlux/rp", "0").toString());
    line_model->setCurrentText(settings->value("SolverDriftFlux/model", "Bingham").toString());
    line_coeficient->setText(settings->value("SolverDriftFlux/coef", "0.0022").toString());
    line_exponent->setText(settings->value("SolverDriftFlux/exp", "2.0").toString());
    line_offset->setText(settings->value("SolverDriftFlux/offset", "5.0").toString());
    combo_gravity->setCurrentText(settings->value("SolverDriftFlux/gravity", "Z").toString());
}

void CfdWidget::loadSolverASM1(){
    line_kh->setText(settings->value("SolverASM1/kh", "0.00003472").toString());
    line_kx->setText(settings->value("SolverASM1/kx", "0.03").toString());
    line_nh->setText(settings->value("SolverASM1/nh", "0.4").toString());
    line_muh->setText(settings->value("SolverASM1/muh", "0.0000694").toString());
    line_ng->setText(settings->value("SolverASM1/ng", "0.08").toString());
    line_ks->setText(settings->value("SolverASM1/ks", "20").toString());
    line_bh->setText(settings->value("SolverASM1/bh", "0.0000071759").toString());
    line_koh->setText(settings->value("SolverASM1/koh", "0.2").toString());
    line_kno->setText(settings->value("SolverASM1/kno", "0.5").toString());
    line_knhh->setText(settings->value("SolverASM1/knhh", "0.05").toString());
    line_mua->setText(settings->value("SolverASM1/mua", "0.00000925926").toString());
    line_ba->setText(settings->value("SolverASM1/ba", "0.0000017361").toString());
    line_qam->setText(settings->value("SolverASM1/qam", "0.00000092592").toString());
    line_koa->setText(settings->value("SolverASM1/koa", "0.4").toString());
    line_knha->setText(settings->value("SolverASM1/knha", "1").toString());
}

void CfdWidget::saveSolverPimplePower(){
    settings = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
    settings->setValue("SolverSettings/id", 2);
    settings->setValue("SolverSettings/name", "PimpleFoam");
    settings->setValue("SolverPimple/sludge", 0);
    settings->setValue("SolverPimple/numin", line_numin->text());
    settings->setValue("SolverPimple/numax", line_numax->text());
    settings->setValue("SolverPimple/coef", line_coeficient->text());
    settings->setValue("SolverPimple/exp", line_exponent->text());

    QFile file(Globals::instance()->getAppProjectPath() + "constant/transportProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\ttransportProperties;}\n"
                  "transportModel powerLaw;;\n"
                  "powerLawCoeffs\n{\tnuMax\t[ 0 2 -1 0 0 0 0 ] " <<
                  line_numax->text() <<
                  ";\n\tnuMin\t[ 0 2 -1 0 0 0 0 ] " <<
                  line_numin->text() <<
                  ";\n\tk\t[ 0 2 -1 0 0 0 0 ] " <<
                  line_coeficient->text() <<
                  ";\n\tn\t[ 0 0  0 0 0 0 0 ] " <<
                  line_exponent->text() <<
                  ";\n}" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "constant/turbulenceProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\tturbulenceProperties;}\n"
                  "simulationType  RAS;\n"
                  "RAS\n{\tRASModel\tkEpsilon;\n"
                  "\tturbulence\ton;\n"
                  "\tprintCoeffs\ton;\n}" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "system/fvSchemes");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"system\";\n\tobject\tfvSchemes;}\n"
                  "ddtSchemes { default\tEuler;}\n"
                  "gradSchemes { default\tGauss linear;}\n"
                  "divSchemes { default\tnone;\n"
                  "\tdiv(phi,U)      bounded Gauss linearUpwind grad(U);\n"
                  "\tdiv(phi,k)      bounded Gauss upwind;\n"
                  "\tdiv(phi,epsilon) bounded Gauss upwind;\n"
                  "\tdiv(phi,R)      bounded Gauss upwind;\n"
                  "\tdiv(R)          Gauss linear;\n"
                  "\tdiv(phi,nuTilda) bounded Gauss upwind;\n"
                  "\tdiv((nuEff*dev2(T(grad(U))))) Gauss linear; }\n"
                  "laplacianSchemes { default\tGauss linear corrected;}\n"
                  "interpolationSchemes { default\tlinear;}\n"
                  "snGradSchemes { default\tcorrected; }" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "system/fvSolution");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"system\";\n\tobject\tfvSolution;}\n"
                  "solvers {\n"
                  "\tp {\n"
                  "\t\tsolver\tGAMG;\n"
                  "\t\ttolerance\t1e-7;\n"
                  "\t\trelTol\t0.01;\n"
                  "\t\tsmoother\tDICGaussSeidel; }\n"
                  "\tpFinal {\n"
                  "\t\t$p;\n"
                  "\t\trelTol\t0; }\n"
                  "\t\"(U|k|epsilon)\" {\n"
                  "\t\tsolver\tsmoothSolver;\n"
                  "\t\ttolerance\t1e-05;\n"
                  "\t\trelTol\t0.1;\n"
                  "\t\tsmoother\tsymGaussSeidel; }\n"
                  "\t\"(U|k|epsilon)Final\" {\n"
                  "\t\t$U;\n"
                  "\t\trelTol\t0.1; }\n}\n"
                  "PIMPLE {\n"
                  "\tnNonOrthogonalCorrectors 0;\n"
                  "\tnCorrectors         2; }" << endl;
        file.close();
    }

    QString path = Globals::instance()->getAppProjectPath();

    QDir dstDir(path + "1");
    dstDir.removeRecursively();
    dstDir.mkdir(path + "1");

    QDir srcDir(path + "0");
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        QString srcItemPath = srcDir.path() + "/" + info.fileName();
        QString dstItemPath = dstDir.path() + "/" + info.fileName();
        if (info.isDir()) {
        } else if (info.isFile()) {
            if (!QFile::copy(srcItemPath, dstItemPath)) {
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in cpDir";
        }
    }
}


void CfdWidget::saveSolverPimpleNewton(){
    //settings = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
    settings->setValue("SolverSettings/id", 2);
    settings->setValue("SolverSettings/name", "PimpleFoam");
    settings->setValue("SolverPimple/sludge", 2);
    settings->setValue("SolverPimple/nu", line_nu0->text());

    QFile file(Globals::instance()->getAppProjectPath() + "constant/transportProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\ttransportProperties;}\n"
                  "transportModel Newtonian;\n"
                  "nu\t" << line_nu0->text() << ";" << endl;
        file.close();
    }

    QString path = Globals::instance()->getAppProjectPath();

    QDir dstDir(path + "1");
    dstDir.removeRecursively();
    dstDir.mkdir(path + "1");

    QDir srcDir(path + "0");
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        QString srcItemPath = srcDir.path() + "/" + info.fileName();
        QString dstItemPath = dstDir.path() + "/" + info.fileName();
        if (info.isDir()) {
        } else if (info.isFile()) {
            if (!QFile::copy(srcItemPath, dstItemPath)) {
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in cpDir";
        }
    }
}

void CfdWidget::saveSolverPimpleHerschel(){
    settings = new QSettings(Globals::instance()->getAppProjectPath() + "settings.ini", QSettings::IniFormat);
    settings->setValue("SolverSettings/id", 2);
    settings->setValue("SolverSettings/name", "PimpleFoam");
    settings->setValue("SolverPimple/sludge", 1);
    settings->setValue("SolverPimple/nu0", line_nu0->text());
    settings->setValue("SolverPimple/tau0", line_tau0->text());
    settings->setValue("SolverPimple/coef", line_coeficient->text());
    settings->setValue("SolverPimple/exp", line_exponent->text());

    QFile file(Globals::instance()->getAppProjectPath() + "constant/transportProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\ttransportProperties;}\n"
                  "transportModel HerschelBulkley;\n"
                  "HerschelBulkleyCoeffs\n{\tnu0\t[ 0 2 -1 0 0 0 0 ] " <<
                  line_nu0->text() <<
                  ";\n\ttau0\t[ 0 2 -2 0 0 0 0 ] " <<
                  line_tau0->text() <<
                  ";\n\tk\t[ 0 2 -1 0 0 0 0 ] " <<
                  line_coeficient->text() <<
                  ";\n\tn\t[ 0 0  0 0 0 0 0 ] " <<
                  line_exponent->text() <<
                  ";\n}" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "constant/turbulenceProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\tturbulenceProperties;}\n"
                  "simulationType  RAS;\n"
                  "RAS\n{\tRASModel\tkEpsilon;\n"
                  "\tturbulence\ton;\n"
                  "\tprintCoeffs\ton;\n}" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "system/fvSchemes");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"system\";\n\tobject\tfvSchemes;}\n"
                  "ddtSchemes { default\tEuler;}\n"
                  "gradSchemes { default\tGauss linear;}\n"
                  "divSchemes { default\tnone;\n"
                  "\tdiv(phi,U)      bounded Gauss linearUpwind grad(U);\n"
                  "\tdiv(phi,k)      bounded Gauss upwind;\n"
                  "\tdiv(phi,epsilon) bounded Gauss upwind;\n"
                  "\tdiv(phi,R)      bounded Gauss upwind;\n"
                  "\tdiv(R)          Gauss linear;\n"
                  "\tdiv(phi,nuTilda) bounded Gauss upwind;\n"
                  "\tdiv((nuEff*dev2(T(grad(U))))) Gauss linear; }\n"
                  "laplacianSchemes { default\tGauss linear corrected;}\n"
                  "interpolationSchemes { default\tlinear;}\n"
                  "snGradSchemes { default\tcorrected; }" << endl;
        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "system/fvSolution");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"system\";\n\tobject\tfvSolution;}\n"
                  "solvers {\n"
                  "\tp {\n"
                  "\t\tsolver\tGAMG;\n"
                  "\t\ttolerance\t1e-7;\n"
                  "\t\trelTol\t0.01;\n"
                  "\t\tsmoother\tDICGaussSeidel; }\n"
                  "\tpFinal {\n"
                  "\t\t$p;\n"
                  "\t\trelTol\t0; }\n"
                  "\t\"(U|k|epsilon)\" {\n"
                  "\t\tsolver\tsmoothSolver;\n"
                  "\t\ttolerance\t1e-05;\n"
                  "\t\trelTol\t0.1;\n"
                  "\t\tsmoother\tsymGaussSeidel; }\n"
                  "\t\"(U|k|epsilon)Final\" {\n"
                  "\t\t$U;\n"
                  "\t\trelTol\t0.1; }\n}\n"
                  "PIMPLE {\n"
                  "\tnNonOrthogonalCorrectors 0;\n"
                  "\tnCorrectors         2; }" << endl;
        file.close();
    }

    QString path = Globals::instance()->getAppProjectPath();

    QDir dstDir(path + "1");
    dstDir.removeRecursively();
    dstDir.mkdir(path + "1");

    QDir srcDir(path + "0");
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        QString srcItemPath = srcDir.path() + "/" + info.fileName();
        QString dstItemPath = dstDir.path() + "/" + info.fileName();
        if (info.isDir()) {
        } else if (info.isFile()) {
            if (!QFile::copy(srcItemPath, dstItemPath)) {
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in cpDir";
        }
    }
}

void CfdWidget::saveSolverASM1(){
    settings->setValue("SolverSettings/id", 1);
    settings->setValue("SolverSettings/name", "ASM1Foam");
    settings->setValue("SolverASM1/kh", line_kh->text());
    settings->setValue("SolverASM1/kx", line_kx->text());
    settings->setValue("SolverASM1/nh", line_nh->text());
    settings->setValue("SolverASM1/muh", line_muh->text());
    settings->setValue("SolverASM1/ng", line_ng->text());
    settings->setValue("SolverASM1/ks", line_ks->text());
    settings->setValue("SolverASM1/bh", line_bh->text());
    settings->setValue("SolverASM1/koh", line_koh->text());
    settings->setValue("SolverASM1/kno", line_kno->text());
    settings->setValue("SolverASM1/knhh", line_knhh->text());
    settings->setValue("SolverASM1/mua", line_mua->text());
    settings->setValue("SolverASM1/ba", line_ba->text());
    settings->setValue("SolverASM1/qam", line_qam->text());
    settings->setValue("SolverASM1/koa", line_koa->text());
    settings->setValue("SolverASM1/knha", line_knha->text());

    selectedTimestep = comboTimesteps->currentText();
    settings->setValue("SolverASM1/timestep", selectedTimestep);

    createASM1KineticPropertiesFile();
}

void CfdWidget::createASM1KineticPropertiesFile(){
    QFile file(Globals::instance()->getAppProjectPath() + "constant/ASM1KineticParameters");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\tAMS1KineticParameters;}\n" << endl;
        stream << "kh\tkh [0 0 -1 0 0 0 0]" << line_kh->text() << ";" << endl;
        stream << "Kx\tKx [0 0 0 0 0 0 0]" << line_kx->text() << ";" << endl;
        stream << "nh\tnh [0 0 0 0 0 0 0]" << line_nh->text() << ";" << endl;
        stream << "muH\tmuH [0 0 -1 0 0 0 0]" << line_muh->text() << ";" << endl;
        stream << "ng\tng [0 0 0 0 0 0 0]" << line_ng->text() << ";" << endl;
        stream << "Ks\tKs [0 0 0 0 0 0 0]" << line_ks->text() << ";" << endl;
        stream << "bH\tbH [0 0 -1 0 0 0 0]" << line_bh->text() << ";" << endl;
        stream << "KoH\tKoH [0 0 0 0 0 0 0]" << line_koh->text() << ";" << endl;
        stream << "Kno\tKno [0 0 0 0 0 0 0]" << line_kno->text() << ";" << endl;
        stream << "KnhH\tknhH [0 0 0 0 0 0 0]" << line_knhh->text() << ";" << endl;
        stream << "muA\tmuA [0 0 -1 0 0 0 0]" << line_mua->text() << ";" << endl;
        stream << "bA\tbA [0 0 -1 0 0 0 0]" << line_ba->text() << ";" << endl;
        stream << "qam\tqam [0 0 -1 0 0 0 0]" << line_qam->text() << ";" << endl;
        stream << "KoA\tKoA [0 0 0 0 0 0 0]" << line_koa->text() << ";" << endl;
        stream << "KnhA\tKnhA [0 0 0 0 0 0 0]" << line_knha->text() << ";" << endl;
        file.close();
    }
}

void CfdWidget::saveSolverDriftFlux(){
    settings->setValue("SolverSettings/id", 0);
    settings->setValue("SolverSettings/name", "DriftFluxFoam");
    settings->setValue("SolverDriftFlux/v0", line_v0->text());
    settings->setValue("SolverDriftFlux/rh", line_rh->text());
    settings->setValue("SolverDriftFlux/rp", line_rp->text());
    settings->setValue("SolverDriftFlux/model", line_model->currentText());
    settings->setValue("SolverDriftFlux/coef", line_coeficient->text());
    settings->setValue("SolverDriftFlux/exp", line_exponent->text());
    settings->setValue("SolverDriftFlux/offset", line_offset->text());
    settings->setValue("SolverDriftFlux/gravity", combo_gravity->currentText());

    QFile file(Globals::instance()->getAppProjectPath() + "constant/transportProperties");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\ttransportProperties;}\n"
                  "phases (sludge water);" << endl;
        stream << "sludge {\n\t"
                  "transportModel BinghamPlastic;\n\t"
                  "\"(plastic|BinghamPlastic)Coeffs\"\n\t{\n\t\t"
                  "coeff\t0.00023;\n\t\texponent\t179.26;\n\t\t"
                  "BinghamCoeff\t" << line_coeficient->text() << ";\n\t\t"
                                                                 "BinghamExponent\t" << line_exponent->text() << ";\n\t\t"
                                                                                                                 "BinghamOffset\t" << line_offset->text() << ";\n\t\t"
                                                                                                                                                             "muMax\t10;\n\t}\n\trho\t1050;\n}" << endl;
        stream << "water {\n\t"
                  "transportModel\tNewtonian;\n\t"
                  "nu\t1.78e-06;\n\trho\t996;\n}" << endl;

        if (line_rp->text().toDouble() == 0)
            stream << "relativeVelocityModel\tsimple;" << endl;
        else
            stream << "relativeVelocityModel\tgeneral;" << endl;

        stream << "\"(simple|general)Coeffs\" {" << endl;
        if (combo_gravity->currentText() == "X")
            stream << "\tV0\t(" << line_v0->text() << " 0 0);" << endl;
        else if (combo_gravity->currentText() == "Y")
            stream << "\tV0\t(0 " << line_v0->text() << " 0);" << endl;
        else if (combo_gravity->currentText() == "Z")
            stream << "\tV0\t(0 0 " << line_v0->text() << ");" << endl;
        stream << "\ta\t" << line_rh->text() << ";\n\ta1\t" << line_rp->text() << ";"<< endl;
        stream << "\tresidualAlpha\t0;\n}" << endl;

        file.close();
    }

    file.~QFile();
    new (&file) QFile(Globals::instance()->getAppProjectPath() + "constant/g");
    if (file.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****/\n" << endl;
        stream << "FoamFile{\n\t"
                  "version\t2.0;\n\tclass\tuniformDimensionedVectorField;\n\tformat\tascii;\n"
                  "\tlocation\t\"constant\";\n\tobject\tg;}\n" << endl;
        stream << "dimensions\t[0 1 -2 0 0 0 0];" << endl;
        if (combo_gravity->currentText() == "X")
            stream << "value\t(-9.81 0 0);" << endl;
        else if (combo_gravity->currentText() == "Y")
            stream << "value\t(0 -9.81 0);" << endl;
        else if (combo_gravity->currentText() == "Z")
            stream << "value\t(0 0 -9.81);" << endl;
    }

    QString path = Globals::instance()->getAppProjectPath();

    QDir dstDir(path + "1");
    dstDir.removeRecursively();
    dstDir.mkdir(path + "1");

    QDir srcDir(path + "0");
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        QString srcItemPath = srcDir.path() + "/" + info.fileName();
        QString dstItemPath = dstDir.path() + "/" + info.fileName();
        if (info.isDir()) {
        } else if (info.isFile()) {
            //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - Copying file: " << info.fileName() << " - " << srcDir.path() << " - " << dstDir.path() << endl;
            if (!QFile::copy(srcItemPath, dstItemPath)) {
                //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << "NOOOO" << endl;
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in cpDir";
        }
    }
}

/*
void CfdWidget::createControlDictFile(){
    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    QString solvername = settings->value("SolverSettings/name").toString();

    QFile file(Globals::instance()->getAppProjectPath() + "/system/controlDict");
    if(file.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream stream(&file);
        stream << "/***** File generated by Hydrodeca *****" << endl;
        stream << "FoamFile{\n"
                  "\tversion\t2.0;\n\tclass\tdictionary;\n\tformat\tascii;\n\tobject\tcontrolDict;\n\tlocation\t\"system\";}\n" << endl;

        stream << "application\t" << solvername;
        stream << ";\nstartFrom\tlatestTime;\nstartTime\t0;\nstopAt\tendTime;\nendTime\t0;\n";
        stream << "deltaT\t0.1;\nwriteControl\tadjustableRunTime;\nwriteInterval\t1;\npurgeWrite\t0;\nwriteFormat\tascii;\nwritePrecision\t7;\nwriteCompression\tuncompressed;\n";
        stream << "timeFormat\tgeneral;\ntimePrecision\t7;\nrunTimeModifiable\tyes;\nadjustTimeStep\ton;\nmaxCo\t5;\nmaxDeltaT\t1;" << endl;

        file.close();
    }
}
*/

void CfdWidget::solverSettingsPimpleFoam(QVBoxLayout *layout){

    QLabel* l_model = new QLabel("Sludge transport model");
    QComboBox *cb_model = new QComboBox();
    cb_model->addItem("Power Law");
    cb_model->addItem("Herschel-Bulkley");
    cb_model->addItem("Newtonian");
    cb_model->setCurrentIndex(-1);
    QObject::connect(cb_model, SIGNAL(currentIndexChanged(int)), this, SLOT(onSludgeModelChange()));

    QWidget *wSludgeModelSettings = new QWidget();
    wSludgeModelSettings->setObjectName("widget_sludgemodel_settings");
    QVBoxLayout *lSludgeModelSettings = new QVBoxLayout();
    wSludgeModelSettings->setLayout(lSludgeModelSettings);

    layout->addWidget(l_model);
    layout->addWidget(cb_model);
    layout->addWidget(wSludgeModelSettings);

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    int index = settings->value("SolverPimple/sludge", 0).toInt();
    //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << index << endl;
    cb_model->setCurrentIndex(index);
}

void CfdWidget::solverSettingsDriftFluxFoam(QVBoxLayout *layout){
    QLabel* l_v0 = new QLabel("Settling v0(m/s)");
    line_v0 = new QLineEdit;
    //line_v0->setValidator(Globals::instance()->getDoubleValidator(this));
    line_v0->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_v0->setAlignment(Qt::AlignRight);

    QLabel* l_rh = new QLabel("Settling rh (kg/m3)");
    line_rh = new QLineEdit;
    line_rh->setValidator(Globals::instance()->getDoubleValidator(this));
    line_rh->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_rh->setAlignment(Qt::AlignRight);

    QLabel* l_rp = new QLabel("Settling rp (kg/m3)");
    line_rp = new QLineEdit;
    line_rp->setValidator(Globals::instance()->getDoubleValidator(this));
    line_rp->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_rp->setAlignment(Qt::AlignRight);

    QLabel* l_model = new QLabel("Sludge transport model");
    line_model = new QComboBox;
    //line_model->setObjectName("cb_select_vsedimentation"); //con nombre, al cambiar de solver, la caja no se borra
    line_model->setStyleSheet(Globals::instance()->getCssSelect());
    line_model->addItem("Bingham");
    //QObject::connect(line_model, SIGNAL(currentIndexChanged(int)), this, SLOT(onChange()));

    QLabel* l_c = new QLabel("Coefficient");
    line_coeficient = new QLineEdit;
    line_coeficient->setValidator(Globals::instance()->getDoubleValidator(this));
    line_coeficient->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_coeficient->setAlignment(Qt::AlignRight);

    QLabel* l_e = new QLabel("Exponent");
    line_exponent = new QLineEdit;
    line_exponent->setValidator(Globals::instance()->getDoubleValidator(this));
    line_exponent->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_exponent->setAlignment(Qt::AlignRight);

    QLabel* l_o = new QLabel("Offset");
    line_offset = new QLineEdit;
    line_offset->setValidator(Globals::instance()->getDoubleValidator(this));
    line_offset->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_offset->setAlignment(Qt::AlignRight);

    //QPushButton* btnLoadWWTP = new QPushButton("Load WWTP data", this);
    //btnLoadWWTP->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    //btnLoadWWTP->setMinimumHeight(30);
    //connect(btnLoadWWTP, SIGNAL(clicked()), this, SLOT(loadSettingsPropertiesWWTP()));

    QLabel* l_gravity = new QLabel("Gravity Axis");
    combo_gravity = new QComboBox;
    combo_gravity->addItem("X");
    combo_gravity->addItem("Y");
    combo_gravity->addItem("Z");
    combo_gravity->setStyleSheet(Globals::instance()->getCssLineEdit());


    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSolverDriftFlux()));

    //QVBoxLayout* layout = new QVBoxLayout();
    //layout->addWidget(btnLoadWWTP);
    layout->addWidget(l_v0);
    layout->addWidget(line_v0);
    layout->addWidget(l_rh);
    layout->addWidget(line_rh);
    layout->addWidget(l_rp);
    layout->addWidget(line_rp);
    layout->addWidget(l_model);
    layout->addWidget(line_model);
    layout->addWidget(l_c);
    layout->addWidget(line_coeficient);
    layout->addWidget(l_e);
    layout->addWidget(line_exponent);
    layout->addWidget(l_o);
    layout->addWidget(line_offset);
    layout->addWidget(l_gravity);
    layout->addWidget(combo_gravity);
    layout->addWidget(btnApply);
    //layout->addStretch();

    loadSolverDriftFlux();

    //QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    //areaActionWidget->setLayout(layout);
    //centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    //centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(200);
}

void CfdWidget::solverProperties(){

    showGeometryFaces();

    QLabel* l_solver = new QLabel("Select solver:");
    line_solver = new QComboBox;
    line_solver->setObjectName("cb_select_solver");
    line_solver->setStyleSheet(Globals::instance()->getCssSelect());
    line_solver->addItem("driftFluxFoam");
    line_solver->addItem("ASM1Foam");
    line_solver->addItem("pimpleFoam");
    line_solver->setCurrentIndex(-1);
    QObject::connect(line_solver, SIGNAL(currentIndexChanged(int)), this, SLOT(onSolverChange()));

    QWidget *wSolverSettings = new QWidget();
    wSolverSettings->setObjectName("widget_solver_settings");
    QVBoxLayout *lSolverSettings = new QVBoxLayout();
    wSolverSettings->setLayout(lSolverSettings);
    //lSolverSettings->addWidget(new QLabel(line_solver->currentText()));

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(l_solver);
    layout->addWidget(line_solver);
    layout->addWidget(wSolverSettings);

    //layout->addStretch();

    //loadSettingsProperties();

    QWidget* areaActionWidget = new QWidget(centralWidget->findChild<QScrollArea*>("area_actions"));
    areaActionWidget->setLayout(layout);
    centralWidget->findChild<QScrollArea*>("area_actions")->setWidget(areaActionWidget);
    centralWidget->findChild<QScrollArea*>("area_actions")->setMaximumWidth(200);

    m_sSettingsFile = Globals::instance()->getAppProjectPath() + "settings.ini";
    settings = new QSettings(m_sSettingsFile, QSettings::IniFormat);
    int index = settings->value("SolverSettings/id", 0).toInt();
    line_solver->setCurrentIndex(index);

}

void CfdWidget::solverSludgePower(QVBoxLayout *layout){
    QLabel* l_numin = new QLabel("Viscosity Min (nuMin)");
    line_numin = new QLineEdit();
    line_numin->setValidator(Globals::instance()->getDoubleValidator(this));
    line_numin->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_numin->setAlignment(Qt::AlignRight);

    QLabel* l_numax = new QLabel("Viscosity Max (nuMax)");
    line_numax = new QLineEdit;
    line_numax->setValidator(Globals::instance()->getDoubleValidator(this));
    line_numax->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_numax->setAlignment(Qt::AlignRight);

    QLabel* l_c = new QLabel("Coefficient (k)");
    line_coeficient = new QLineEdit;
    line_coeficient->setValidator(Globals::instance()->getDoubleValidator(this));
    line_coeficient->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_coeficient->setAlignment(Qt::AlignRight);

    QLabel* l_e = new QLabel("Exponent (n)");
    line_exponent = new QLineEdit;
    line_exponent->setValidator(Globals::instance()->getDoubleValidator(this));
    line_exponent->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_exponent->setAlignment(Qt::AlignRight);

    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSolverPimplePower()));

    layout->addWidget(l_numin);
    layout->addWidget(line_numin);
    layout->addWidget(l_numax);
    layout->addWidget(line_numax);
    layout->addWidget(l_c);
    layout->addWidget(line_coeficient);
    layout->addWidget(l_e);
    layout->addWidget(line_exponent);
    layout->addWidget(btnApply);

    line_numin->setText(settings->value("SolverPimple/numin", "0.001").toString());
    line_numax->setText(settings->value("SolverPimple/numax", "0.00001").toString());
    line_coeficient->setText(settings->value("SolverPimple/coef", "0.00001").toString());
    line_exponent->setText(settings->value("SolverPimple/exp", "1").toString());
}

void CfdWidget::solverSludgeHerschel(QVBoxLayout *layout){
    QLabel* l_nu0 = new QLabel("Viscosity (nu0)");
    line_nu0 = new QLineEdit();
    line_nu0->setValidator(Globals::instance()->getDoubleValidator(this));
    line_nu0->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_nu0->setAlignment(Qt::AlignRight);

    QLabel* l_tau0 = new QLabel("Threshold (tau0)");
    line_tau0 = new QLineEdit;
    line_tau0->setValidator(Globals::instance()->getDoubleValidator(this));
    line_tau0->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_tau0->setAlignment(Qt::AlignRight);

    QLabel* l_c = new QLabel("Coefficient (k)");
    line_coeficient = new QLineEdit;
    line_coeficient->setValidator(Globals::instance()->getDoubleValidator(this));
    line_coeficient->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_coeficient->setAlignment(Qt::AlignRight);

    QLabel* l_e = new QLabel("Exponent (n)");
    line_exponent = new QLineEdit;
    line_exponent->setValidator(Globals::instance()->getDoubleValidator(this));
    line_exponent->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_exponent->setAlignment(Qt::AlignRight);

    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSolverPimpleHerschel()));

    layout->addWidget(l_nu0);
    layout->addWidget(line_nu0);
    layout->addWidget(l_tau0);
    layout->addWidget(line_tau0);
    layout->addWidget(l_c);
    layout->addWidget(line_coeficient);
    layout->addWidget(l_e);
    layout->addWidget(line_exponent);
    layout->addWidget(btnApply);

    line_nu0->setText(settings->value("SolverPimple/nu0", "0.001").toString());
    line_tau0->setText(settings->value("SolverPimple/tau0", "1").toString());
    line_coeficient->setText(settings->value("SolverPimple/coef", "0.00001").toString());
    line_exponent->setText(settings->value("SolverPimple/exp", "1").toString());
}

void CfdWidget::solverSludgeNewtonian(QVBoxLayout *layout){
    line_nu0 = new QLineEdit();
    line_nu0->setValidator(Globals::instance()->getDoubleValidator(this));
    line_nu0->setStyleSheet(Globals::instance()->getCssLineEdit());
    line_nu0->setAlignment(Qt::AlignRight);

    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSolverPimpleNewton()));

    layout->addWidget(new QLabel("Viscosity (nu)"));
    layout->addWidget(line_nu0);
    layout->addWidget(btnApply);

    line_nu0->setText(settings->value("SolverPimple/nu", "0.000001").toString());
}

void CfdWidget::onSludgeModelChange(){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget *w = qobject_cast<QWidget *>(combo->parent());
    QWidget *wSludgeSettings = w->findChild<QWidget*>("widget_sludgemodel_settings");

    qDeleteAll(wSludgeSettings->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
    QVBoxLayout *layout_settings = (QVBoxLayout*)wSludgeSettings->layout();
    if (combo->currentIndex() == 0){
        solverSludgePower(layout_settings);
    } else if (combo->currentIndex() == 1){
        solverSludgeHerschel(layout_settings);
    } else if (combo->currentIndex() == 2){
        solverSludgeNewtonian(layout_settings);
    } else {
        layout_settings->addWidget(new QLabel(combo->currentText()));
    }
}

void CfdWidget::solverSettingsASM1Foam(QVBoxLayout *layout){
    comboTimesteps = new QComboBox();

    QStringList list_excluded_dirs;
    list_excluded_dirs << "processor" << "0" << "profiles" << "constant" << "equipments" << "system" << "0.1" << "0.2" << "0.3";

    QDir dir = QDir(Globals::instance()->getAppProjectPath());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::Time | QDir::Reversed);
    QStringList dirList = dir.entryList();
    std::reverse(dirList.begin(), dirList.end());
    QString filename;
    foreach(QString filename, dirList){
        //qDebug() << "(Sergio):" << __FILE__<<":"<<__LINE__<<" in "<<__FUNCTION__<< " - " << filename << endl;
        if(list_excluded_dirs.contains(filename)){
        } else{
            comboTimesteps->addItem(filename);
        }
    }
    if(comboTimesteps->count() == 0){
        layout->addWidget(new QLabel("Please, run a hydrodynamic simulation\nbefore executing this solver."));
        return;
    }

    comboTimesteps->setCurrentText(settings->value("SolverASM1/timestep", filename).toString());
    selectedTimestep = comboTimesteps->currentText();

    QPushButton* btnApply = new QPushButton("Apply", this);
    btnApply->setStyleSheet(Globals::instance()->getCssApplyPushButton());
    btnApply->setMinimumHeight(30);
    connect(btnApply, SIGNAL(clicked()), this, SLOT(saveSolverASM1()));

    QToolBox *boxKinetics = new QToolBox();
    boxKinetics->setStyleSheet(Globals::instance()->getCssToolBox());
    boxKinetics->addItem(tabASM1timestamp(), "Timestamp");
    boxKinetics->addItem(tabASM1kinetics(), "Kinetics parameters");
    //boxKinetics->setMaximumHeight(100);

    layout->addWidget(boxKinetics);
    layout->addWidget(btnApply);

    loadSolverASM1();
}

void CfdWidget::onSolverChange(){
    QComboBox *combo = qobject_cast<QComboBox*>(sender());
    QWidget *w = qobject_cast<QWidget *>(combo->parent());
    QWidget *wSolverSettings = w->findChild<QWidget*>("widget_solver_settings");

    //Delete recursively
    QLayoutItem *child;
    while ((child = wSolverSettings->layout()->takeAt(0)) != 0) {
        delete child->widget();
    }

    QVBoxLayout *layout_settings = (QVBoxLayout*)wSolverSettings->layout();
    if (combo->currentIndex() == 0){
        solverSettingsDriftFluxFoam(layout_settings);
    } else if (combo->currentIndex() == 2){
        solverSettingsPimpleFoam(layout_settings);
        //btnInit->setDisabled(true);
    } else if (combo->currentIndex() == 1){
        solverSettingsASM1Foam(layout_settings);
    } else {
        layout_settings->addWidget(new QLabel(combo->currentText()));
    }

    layout_settings->addStretch();
}

void CfdWidget::startDriftFluxFoam(){
    QFile outputfile(Globals::instance()->getAppProjectPath() + "/foam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Truncate));

    processFoam = new QProcess();
    processFoam->setObjectName("process_simulation");
    connect(processFoam,SIGNAL(readyReadStandardOutput()),this,SLOT(foamOutput()));

    processFoam->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processFoam->setProcessChannelMode(QProcess::MergedChannels);

    QString script = "execFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "hostname > /tmp/project/container_id" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "export PATH=$HOME/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/bin/:$PATH" << endl;
        stream << "export LD_LIBRARY_PATH=/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/fftw-3.3.7/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/CGAL-4.9.1/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/boost_1_64_0/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/gperftools-2.5/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/openmpi-system:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/user/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/site/1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/dummy" << endl;
        stream << "driftFluxFoam" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processFoam->start(POWERSHELL, commands.split(" "));
}

void CfdWidget::startPimpleFoam(){
    QFile outputfile(Globals::instance()->getAppProjectPath() + "/foam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Truncate));

    processFoam = new QProcess();
    processFoam->setObjectName("process_simulation");
    connect(processFoam,SIGNAL(readyReadStandardOutput()),this,SLOT(foamOutput()));

    processFoam->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processFoam->setProcessChannelMode(QProcess::MergedChannels);

    QString script = "execFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "hostname > /tmp/project/container_id" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "export PATH=$HOME/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/bin/:$PATH" << endl;
        stream << "export LD_LIBRARY_PATH=/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/fftw-3.3.7/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/CGAL-4.9.1/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/boost_1_64_0/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/gperftools-2.5/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/openmpi-system:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/user/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/site/1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/dummy" << endl;
        //stream << "export OMPI_MCA_btl_vader_single_copy_mechanism=none" << endl;
        //stream << "decomposePar" << endl;
        stream << "pimpleFoam" << endl;
        //stream << "mpirun -np 4 driftFluxFoam -parallel" << endl;
        //stream << "reconstructPar" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processFoam->start(POWERSHELL, commands.split(" "));
}

void CfdWidget::startASM1Foam(){
    QFile outputfile(Globals::instance()->getAppProjectPath() + "/foam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Truncate));

    processFoam = new QProcess();
    processFoam->setObjectName("process_simulation");
    connect(processFoam,SIGNAL(readyReadStandardOutput()),this,SLOT(foamOutput()));

    processFoam->setWorkingDirectory(Globals::instance()->getAppProjectPath());
    processFoam->setProcessChannelMode(QProcess::MergedChannels);

    QString script = "execFoam.sh";
    QFile fileScript(Globals::instance()->getAppProjectPath() + script);
    if (fileScript.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream stream(&fileScript);
        stream << "#!/bin/bash" << endl;
        stream << "hostname > /tmp/project/container_id" << endl;
        stream << "source /home/user/OpenFOAM/OpenFOAM-v1906/etc/bashrc" << endl;
        stream << "export PATH=$HOME/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/bin/:$PATH" << endl;
        stream << "export LD_LIBRARY_PATH=/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/fftw-3.3.7/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/CGAL-4.9.1/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/boost_1_64_0/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/ThirdParty/platforms/linux64Gcc/gperftools-2.5/lib64:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/openmpi-system:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/user/OpenFOAM/user-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/site/1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib:/home/user/OpenFOAM/OpenFOAM-v1906/platforms/linux64GccDPInt32Opt/lib/dummy" << endl;
        //stream << "export OMPI_MCA_btl_vader_single_copy_mechanism=none" << endl;
        //stream << "decomposePar" << endl;
        stream << "my_ASM1Foam" << endl;
        //stream << "mpirun -np 4 driftFluxFoam -parallel" << endl;
        //stream << "reconstructPar" << endl;
        fileScript.close();
    }

    QString commands = "docker run --rm --user user --workdir /tmp/project -v "
            + Globals::instance()->getAppProjectPath()
            + ":/tmp/project "
            + DOCKERIMAGE
            + " /tmp/project/" + script;
    processFoam->start(POWERSHELL, commands.split(" "));
}


void CfdWidget::foamOutput(){
    QString text;
    text.append(processFoam->readAllStandardOutput());
    QListWidgetItem *line = new QListWidgetItem(text);
    line->setTextColor(Qt::white);
    console->addItem(line);
    console->scrollToBottom();

    QFile outputfile(Globals::instance()->getAppProjectPath() + "/foam.output");
    if (outputfile.open(QFile::WriteOnly|QFile::Append)) {
        QTextStream stream(&outputfile);
        stream << text;
    }
}
